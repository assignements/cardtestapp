//
//  ViewController.swift
//  vinodapp
//
//  Created by vinods.alt  on 14/09/21.
//

import UIKit

class ViewController: UIViewController, SDKStatusListener {
    
    @IBOutlet weak var cardContainerView: UIView!

    let cardId = "98f9a1f4-786c-4be0-8b89-9abcbce5ab9e"
    var isAuthenticated = false
    var isCardBlocked = false
    let token = "eyJhbGciOiJFUzI1NiJ9.eyJ0ZW5hbnRVbmlxdWVWZWN0b3JJZCI6Ijc5ZDJiYTE0LTQ0NGEtNGI5My04ODc1LTllNTdlODg1NjdiOCIsImlhdCI6MTYzMjM5MDQyOCwiZXhwIjoxNjM0NzM2MDM3fQ.AA-9CQfWxXUtHCEkYzobOFFxf99pELYl49kQR4JRsiDxeUGBbx1G2TCr3JH8lDD4MHXQNWayS0bdIDppvf115Q"
    let accID = "79d2ba14-444a-4b93-8875-9e57e88567b8"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.cardManager?.add(self)
        // Do any additional setup after loading the view.
        perform(#selector(authenticate), with: nil, afterDelay: 5)
    }
    
    @objc func authenticate() {
      if(!isAuthenticated) {
        let appdelegate: AppDelegate = UIApplication.shared.delegate! as! AppDelegate
        appdelegate.cardManager?.authenticateSDK(withTenantAuthToken: token, andAccountHolderID: accID)
      }
    }
    
    func statusChanged(_ sdkStatus: SDKStatus) {
      let appdelegate: AppDelegate = UIApplication.shared.delegate! as! AppDelegate
      if sdkStatus == .authenticated {
        isAuthenticated = true
        appdelegate.cardManager?.setUpSDKWithResourceID(nil) {
          print("SDK Setup Success")
        } failure: { error in
          print("SDK Setup Failed with error: %@", error!.message)
        }
      }
    }
    
    @IBAction func showCard(_ sender: Any) {
  //self._view.addSubview(view!, stretchToFit: true)
      let appdelegate: AppDelegate = UIApplication.shared.delegate! as! AppDelegate
      appdelegate.cardManager?.getCardUI(withCardID: cardId, payload: nil, defaultState: "collapsed", templateID: "card_EN_UIVIEW", success: { [weak self] view in
        if view != nil, self != nil {
            self?.cardContainerView.addSubviewWithConstraints(view!)
        }
      }, failure: { error in
        print(" Card error: %@", error.message)
      })
    }
    
    @IBAction func showPin(_ sender: Any) {
      let appdelegate: AppDelegate = UIApplication.shared.delegate! as! AppDelegate
          appdelegate.cardManager?.launchCardPinUI(forCardId: cardId) {
        print("Set Pin Success ")
      } failure: { error in
        print("Set Pin Failed with error: %@", error!.message)
      }
    }
    
//    @IBAction func blockCard(_ sender: Any) {
//        let appdelegate: AppDelegate = UIApplication.shared.delegate! as! AppDelegate
//        appdelegate.cardManager?.blockCard(cardId, description: "block card now", success: {
//            print("Block  Success ")
//        }, failure: { _  in
//            print("Block failed ")
//        })
//    }
//
//    @IBAction func unblockCard(_ sender: Any) {
//        let appdelegate: AppDelegate = UIApplication.shared.delegate! as! AppDelegate
//        appdelegate.cardManager?.unblockCard(cardId, description: "unblock card now", success: {
//            print("UnBlock  Success ")
//        }, failure: { _  in
//            print("UnBlock failed ")
//        })
//    }
  }

//}


extension UIView {
    public func addSubviewWithConstraints(_ subview: UIView) {
        addSubview(subview)
            subview.translatesAutoresizingMaskIntoConstraints = false
            leftAnchor.constraint(equalTo: subview.leftAnchor).isActive = true
            rightAnchor.constraint(equalTo: subview.rightAnchor).isActive = true
            topAnchor.constraint(equalTo: subview.topAnchor).isActive = true
            bottomAnchor.constraint(equalTo: subview.bottomAnchor).isActive = true
    }
}
