//
//  AppDelegate.swift
//  vinodapp
//
//  Created by vinods.alt on 14/09/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var cardManager: ApolloCardManager? = nil
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.cardManager = ApolloCardManager()
        self.cardManager?.authenticateSDK(withTenantAuthToken: "eyJhbGciOiJFUzI1NiJ9.eyJ0ZW5hbnRVbmlxdWVWZWN0b3JJZCI6Ijc5ZDJiYTE0LTQ0NGEtNGI5My04ODc1LTllNTdlODg1NjdiOCIsImlhdCI6MTYzMjM5MDQyOCwiZXhwIjoxNjM0NzM2MDM3fQ.AA-9CQfWxXUtHCEkYzobOFFxf99pELYl49kQR4JRsiDxeUGBbx1G2TCr3JH8lDD4MHXQNWayS0bdIDppvf115Q", andAccountHolderID: "79d2ba14-444a-4b93-8875-9e57e88567b8")
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

