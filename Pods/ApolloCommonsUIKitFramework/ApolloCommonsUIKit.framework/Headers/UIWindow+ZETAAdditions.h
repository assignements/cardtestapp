#import <UIKit/UIKit.h>

@interface UIWindow (ZETAAdditions)

- (UIViewController *)zeta_topViewController;

@end
