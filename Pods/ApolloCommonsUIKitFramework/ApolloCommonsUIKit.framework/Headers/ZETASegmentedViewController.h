#import "HMSegmentedControl.h"
#import <UIKit/UIKit.h>

@protocol ZETASegmentedViewControllerDataSource;

@interface ZETASegmentedViewController : UIViewController

@property (nonatomic, readonly) HMSegmentedControl *segmentedControl;
@property (nonatomic, weak) id<ZETASegmentedViewControllerDataSource> dataSource;
@property (nonatomic) UIColor *bottomBorderBackgroundColor;

- (void)segmentDidChange;
- (NSUInteger)lastSelectedSegmentedIndex;

@end

@protocol ZETASegmentedViewControllerDataSource<NSObject>

- (NSArray *)segmentedViewControllerSectionTitles:
    (ZETASegmentedViewController *)segmentedViewController;

- (HMSegmentedControlSelectionIndicatorLocation)segmentedViewControllerSelectionIndicatorLocation:
    (ZETASegmentedViewController *)segmentedViewController;

- (HMSegmentedControlType)segmentedViewControllerSegmentControlType:
    (ZETASegmentedViewController *)segmentedViewController;

- (NSArray *)segmentedViewControllerViewControllers:
    (ZETASegmentedViewController *)segmentedViewController;

@optional
- (NSArray *)segmentedViewControllerSectionImages:
    (ZETASegmentedViewController *)segmentedViewController;

- (NSArray *)segmentedViewControllerSectionSelectedImages:
    (ZETASegmentedViewController *)segmentedViewController;

- (NSDictionary *)segmentedViewControllerTitleAttributes:
    (ZETASegmentedViewController *)segmentedViewController;

- (NSDictionary *)segmentedViewControllerSelectedTitleAttributes:
    (ZETASegmentedViewController *)segmentedViewController;

- (UIColor *)segmentedViewControllerIndicatorColor:
    (ZETASegmentedViewController *)segmentedViewController;

- (CGFloat)segmentedViewControllerIndicatorHeight:
    (ZETASegmentedViewController *)segmentedViewController;

@end
