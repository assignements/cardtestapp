#import <UIKit/UIKit.h>

@interface ZETAPartialBlockingView : UIView

- (instancetype)initWithFrame:(CGRect)frame interactableUIView:(UIView *)view;

@end
