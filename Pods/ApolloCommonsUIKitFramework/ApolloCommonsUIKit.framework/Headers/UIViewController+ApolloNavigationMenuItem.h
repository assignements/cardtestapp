#import <UIKit/UIKit.h>

@interface UIViewController (ApolloNavigationMenuItem)

- (void)zeta_clearNavigationMenuItems;
- (void)zeta_addLeftNavigationMenuItemWithSelector:(SEL)selector;
- (void)zeta_addLeftNavigationMenuItem;
- (void)zeta_addLeftNavigationMenuItemWithImage:(UIImage *)image
selector:(SEL)selector;

- (void)zeta_leftNavigationMenuItemTapped:(id)sender;
- (void)zeta_closeDrawerItemTapped:(id)sender;

@end
