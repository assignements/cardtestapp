#import <UIKit/UIKit.h>

@interface UIView (ViewAnimations)

- (void)expandViewFromRightWithAnimation:(BOOL)animation
                       animationDuration:(CGFloat)animationDuration
                    withInitialViewFrame:(CGRect)initialViewFrame;

- (void)collapseViewToRightWithAnimation:(BOOL)animation
                       animationDuration:(CGFloat) animationDuration
                    withInitialViewFrame:(CGRect)initalViewFrame;

@end

