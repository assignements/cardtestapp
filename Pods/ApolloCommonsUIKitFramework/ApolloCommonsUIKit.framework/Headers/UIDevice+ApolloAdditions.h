#import <UIKit/UIKit.h>

@interface UIDevice (ApolloAdditions)

/**
  Checks the device model identifier and checks if the model ID is one of the unsupported
  devices.
  @note Contains unsupported device as of Feb 2016.
 */
+ (BOOL)apollo_isTouchIDUnSupportediPhoneDevice;

+ (BOOL)apollo_isDeviceOSBelowIOS8;

@end
