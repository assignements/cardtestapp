#import <UIKit/UIKit.h>

@interface ZETAUndimBackgroundDismissAnimationController : NSObject <UIViewControllerAnimatedTransitioning>

-(instancetype) initWithOverlayView:(UIView *) overlayView;

@end
