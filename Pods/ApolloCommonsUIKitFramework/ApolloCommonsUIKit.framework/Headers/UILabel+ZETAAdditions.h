#import <UIKit/UIKit.h>

@interface UILabel (ZETAAdditions)

- (void)zeta_setFont:(UIFont *)font textColor:(UIColor *)color text:(NSString *)text;

@end
