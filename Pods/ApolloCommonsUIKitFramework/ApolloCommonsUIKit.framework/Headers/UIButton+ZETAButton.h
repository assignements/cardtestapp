#import <UIKit/UIKit.h>

@interface UIButton (ZETAButton)

/**
 * Sets the title of ui button for UIControl state normal
 */
- (void)zeta_setTitle:(NSString *)title;

/**
 * Adds the target action for UIControlEventTouchUpInside
 */
- (void)zeta_addTarget:(id)target action:(SEL)selector;

@end
