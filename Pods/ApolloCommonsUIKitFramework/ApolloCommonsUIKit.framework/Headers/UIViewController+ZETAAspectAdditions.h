#import <UIKit/UIKit.h>
#import <ApolloCommonsMiscellaneous/ApolloMiscellaneous.h>

@interface UIViewController (ZETAAspectAdditions)

- (void)zeta_hookAfterViewDidLayoutSubviewWithBlock:(ZETAEmptyBlockType)block;

@end
