#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface ZETAToast : NSObject

+ (instancetype)toastWithText:(NSString *)text overView:(UIView *)view;

/**
 This method returns a toast with just a UIActivityIndicatorView inside it
 */
+ (instancetype)toastWithActivityIndicatorOverView:(UIView *)view;
+ (instancetype)toastWithProgressBarOverView:(UIView *)view;

- (void)show;

- (void)hide;

- (void)hideWithCompletionBlock:(ZETAEmptyBlockType)completion;
- (void)hideOnMainThreadWithCompletionBlock:(ZETAEmptyBlockType)completion;
/**
 * Set the text for the label that appears below the activity indicator.
 */
- (void)setLabelText:(NSString *)labelText;

/**
 * Set the progress for a progress bar toast.
 */
- (void)setProgress:(float)progress;

@end
