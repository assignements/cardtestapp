#import <UIKit/UIKit.h>

@interface UIButton (ZETAAdditions)

- (void)zeta_setTitle:(NSString *)title font:(UIFont *)font color:(UIColor *)color;
- (void)zeta_setImage:(UIImage *)image
                title:(NSString *)title
                 font:(UIFont *)font
                color:(UIColor *)color;

@end
