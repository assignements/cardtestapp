#import <UIKit/UIKit.h>

@interface UIView (HeightCalculation)

- (CGFloat)heightForContainerWidth:(CGFloat)containerWidth;

@end
