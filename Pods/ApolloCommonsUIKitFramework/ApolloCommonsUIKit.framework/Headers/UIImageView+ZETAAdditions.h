#import <UIKit/UIKit.h>

@interface UIImageView (ZETAAdditions)

/**
 This method updates the image of the receiver in multiple steps:
 1. It starts by setting the image of the receiver to a placeholder image
    obtained from @p placeholderImageName.
 2. If @p URL is nil, then the method ends.
 2. Otherwise it tries to locate a cached image for @p URL. If one is found,
    then that is immediately set as the receiver's image and the method ends.
 3. Otherwise, it initiates a asynchronous network fetch for the image. If
    the fetch is successful, then it enqueues a block on the main thread
    to set the image of the receiver to what was fetched.

 Since the first two three happen synchronously, it is essential that
 this method should only be invoked from the main thread.
 */
- (void)zeta_setImageFromURL:(NSURL *)URL placeholderImageName:(NSString *)placeholderImageName;

/**
 Convenience wrapper for @p zeta_setImageFromURL:placeholderImageName:
 that sets the @p placeholderImageName parameter to @p nil.
 */
- (void)zeta_setImageFromURL:(NSURL *)URL;

- (void)zeta_setImageForReusableImageViewFromURL:(NSURL *)URL;

- (void)zeta_setImageForReusableImageViewFromURL:(NSURL *)URL
                            placeHolderImageName:(NSString *)placeHolderImageName;

@end
