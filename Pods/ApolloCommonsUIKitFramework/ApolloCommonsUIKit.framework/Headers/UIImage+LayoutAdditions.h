#import <UIKit/UIKit.h>

@interface UIImage (LayoutAdditions)

- (UIImage *)imageWithInsets:(UIEdgeInsets)insets;

@end
