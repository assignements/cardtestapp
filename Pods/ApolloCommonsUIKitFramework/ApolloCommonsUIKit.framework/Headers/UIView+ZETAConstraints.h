#import <UIKit/UIKit.h>

@interface UIView (ZETAConstraints)

- (void)zeta_addSameSizedSubView:(UIView *)subView;

- (void)zeta_placeCentrallyAlignedSubView:(UIView *)subView
                      withVerticalPadding:(CGFloat)verticalPadding
                        horizontalPadding:(CGFloat)horizontalPadding;

- (void)zeta_addSubView:(UIView *)subView withEdgeInsets:(UIEdgeInsets)edgeInset;

/**
 It adds the given @p subView to the view and apply the given constraints on all four sides.
 @note This method takes care of not applying the particular constraint whose value is nil.
 @note It takes care of not adding the subView if it already exists.
 */
- (NSArray *)zeta_addSubView:(UIView *)subView
                    topInset:(NSNumber *)topInset
                   leftInset:(NSNumber *)leftInset
                 bottomInset:(NSNumber *)bottomInset
                  rightInset:(NSNumber *)rightInset;

/**
 Adds the given @p subView to the view and apply the @p topInset as the top constraint
 @note It takes care of not adding the subView if it already exists.
 */
- (NSLayoutConstraint *)zeta_addSubView:(UIView *)subView topInset:(CGFloat)topInset;

/**
 Adds the given @p subView to the view and apply the @p bottomInset as the bottom constraint
 @note It takes care of not adding the subView if it already exists.
 */
- (NSLayoutConstraint *)zeta_addSubView:(UIView *)subView bottomInset:(CGFloat)bottomInset;

/**
 Adds the given @p subView to the view and apply the @p leftInset as the left constraint
 @note It takes care of not adding the subView if it already exists.
 */
- (NSLayoutConstraint *)zeta_addSubView:(UIView *)subView leftInset:(CGFloat)leftInset;

/**
 Adds the given @p subView to the view and apply the @p rightInset as the right constraint
 @note It takes care of not adding the subView if it already exists.
 */
- (NSLayoutConstraint *)zeta_addSubView:(UIView *)subView rightInset:(CGFloat)rightInset;

- (void)zeta_addCentralizedSubView:(UIView *)subView;

- (void)zeta_alignLeadingWithView:(UIView *)view;

- (void)zeta_addEquallyWideCentralizedSubView:(UIView *)subview;

- (NSLayoutConstraint *)zeta_addHeightConstraint:(CGFloat)height;

- (NSLayoutConstraint *)zeta_addWidthConstraint:(CGFloat)width;

- (NSLayoutConstraint *)zeta_addBottomConstraintWithView:(UIView *)view
                                                  offset:(CGFloat)offset;

- (void)zeta_addAspectRatioConstraint:(CGFloat)aspectRatio;

- (void)zeta_alignTopChild1:(id)child1 child2:(id)child2 offset:(CGFloat)offset;

- (void)zeta_alignBottomChild1:(id)child1 child2:(id)child2 offset:(CGFloat)offset;

- (void)zeta_alignLeftChild1:(id)child1 child2:(id)child2 offset:(CGFloat)offset;

- (void)zeta_alightRightChild1:(id)child1 child2:(id)child2 offset:(CGFloat)offset;

- (void)zeta_alignHorizontalCenterChild1:(id)child1 child2:(id)child2 offset:(CGFloat)offset;

- (void)zeta_alignVerticalCenterChild1:(id)child1 child2:(id)child2 offset:(CGFloat)offset;

- (void)zeta_centerChildHorizontally:(id)child;

- (void)zeta_centerChildVertically:(id)child;

- (void)zeta_alignChildToLeft:(id)child offset:(CGFloat)offset;

- (void)zeta_alignChildToRight:(id)child offset:(CGFloat)offset;

- (void)zeta_alignChildToTop:(id)child offset:(CGFloat)offset;

- (void)zeta_alignChildToBottom:(id)child offset:(CGFloat)offset;

- (void)zeta_placeHorizontallyChild1:(id)child1 child2:(id)child2 offset:(CGFloat)offset;

- (void)zeta_placeVerticallyChild1:(id)child1 child2:(id)child2 offset:(CGFloat)offset;
- (void)zeta_addEquallyWideBottomAlignedSubView:(UIView *)subview;
- (void)zeta_addEquallyWideTopAlignedSubView:(UIView *)subview;

@end
