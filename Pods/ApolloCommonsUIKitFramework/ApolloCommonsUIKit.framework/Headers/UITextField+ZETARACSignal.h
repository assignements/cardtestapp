#import <ReactiveCocoa/ReactiveCocoa.h>
#import <UIKit/UIKit.h>

@interface UITextField (ZETARACSignal)

- (RACSignal *)zeta_mergedRACTextSignals;

@end
