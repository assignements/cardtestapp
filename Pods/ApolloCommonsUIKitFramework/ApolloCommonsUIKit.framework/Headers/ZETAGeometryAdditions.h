#ifndef ZETAGeometryAdditions_h
#define ZETAGeometryAdditions_h

#endif /* ZETAGeometryAdditions_h */

#include <CoreGraphics/CGGeometry.h>
#import <UIKit/UIKit.h>

/**
 Function to return a new rect after slicing the length equal to different edges of @p insets from
 each side.
 */
FOUNDATION_EXPORT CGRect ZETACGRectEdgeInset(CGRect bounds, UIEdgeInsets insets);

FOUNDATION_EXPORT CGRect ZETACGRectWithNewX(CGRect rect, CGFloat newX);
FOUNDATION_EXPORT CGRect ZETACGRectWithNewY(CGRect rect, CGFloat newY);
FOUNDATION_EXPORT CGRect ZETACGRectWithNewHeight(CGRect rect, CGFloat height);
FOUNDATION_EXPORT CGRect ZETACGRectWithNewWidth(CGRect rect, CGFloat width);
