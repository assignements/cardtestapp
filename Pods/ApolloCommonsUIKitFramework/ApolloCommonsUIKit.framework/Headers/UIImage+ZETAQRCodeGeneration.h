#import <UIKit/UIKit.h>

typedef void (^ZETAQRCodeGenerationCompletionBlock)(UIImage *__nonnull image);

@interface UIImage (ZETAQRCodeGeneration)

/**
 Generate an image from the provided QR code data.
 The operation happens on a non-main queue.
 */
+ (void)zeta_imageFromQRCodeData:(nonnull NSData *)data
                           width:(CGFloat)width
                 completionBlock:(nonnull ZETAQRCodeGenerationCompletionBlock)block;

@end
