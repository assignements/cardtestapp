#import <KLCPopup/KLCPopup.h>

@interface ZETAKLCPopup : KLCPopup

@property (nonatomic) BOOL shouldNotAllowContentTouch;

@end
