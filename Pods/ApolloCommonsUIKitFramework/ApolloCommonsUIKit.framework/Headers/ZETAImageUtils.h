#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <ApolloCommonsMiscellaneous/ApolloMiscellaneous.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

typedef NS_ENUM(NSUInteger, ZETAUploadFileResponseType) {
  ZETAUploadFileResponseTypeJSON,
  ZETAUploadFileResponseTypeXML
};

FOUNDATION_EXPORT CGFloat const UploadedBillLowCompressionRatio;
FOUNDATION_EXPORT CGFloat const UploadedBillHighCompressionRatio;

@interface ZETAImageUtils : NSObject

+ (void)uploadImageData:(NSData *)imageData
              uploadURL:(NSURL *)uploadURL
           responseType:(ZETAUploadFileResponseType)responseType
             POSTParams:(NSDictionary *)postParams
                success:(void (^)(id response))success
                failure:(ZETAFailureBlockType)failure;

/**
 * @param imageView image view where the image is supposed to be set using the url sent by the
 * signal
 * @param URLSignal this signal will send a URL String on the successfull completion of which the
 * profile picture provider will fetch the image from that url and set it on the image  view
 * @param placeholderName name of the image added in the assets for placeholder
 */
+ (void)setImageView:(UIImageView *)imageView
      imageURLSignal:(RACSignal *)URLSignal
placeholderImageName:(NSString *)placeholderName
imageCacheDirectoryName:(NSString *)imageCacheDirectoryName;

+ (CGSize)defaultCompressedSizeForSize:(CGSize)size;
/**
 * Returns the maximum size with height less than @p maxHeight and width less than @p maxWidth
 * and with aspect ratio preserved.
 */
+ (CGSize)compressedSizeForSize:(CGSize)size
                       maxWidth:(CGFloat)maxWidth
                      maxHeight:(CGFloat)maxHeight;
/**
 * Returns a new image after resizing the original based on the input maxWidth and maxHeight.
 * The aspect ratio of the image is preserved.
 */
+ (UIImage *)resizedImageForImage:(UIImage *)image
                         maxWidth:(CGFloat)maxWidth
                        maxHeight:(CGFloat)maxHeight;

+ (UIImage *)defaultResizedImageforImage:(UIImage *)image;

@end
