#import <UIKit/UIKit.h>

@interface UIViewController (ZETABackButton)

- (void)zeta_setLightNavigationBackButton;

- (void)zeta_setLightNavigationBackButtonWithSelector:(SEL)selector;

- (void)zeta_setNavigationBackButtonWithBackButtonImage:(NSString *)backButtonImage;

- (void)zeta_setNavigationBackButtonWithBackButtonImage:(NSString *)backButtonImage
                                               selector:(SEL)selector;

- (void)zeta_setSwipeDownNavigationButtonWithSelector:(SEL)selector;

- (void)zeta_addDarkNavigationBackButton;

- (void)zeta_addLightNavigationBackButton;

- (void)zeta_addDarkNavigationBackButtonWithTopSpace:(NSInteger)space selector:(SEL)selector;

- (UIButton *)zeta_addLightNavigationBackButtonWithSelector:(SEL)selector;

- (void)zeta_addDarkNavigationBackButtonWithSelector:(SEL)selector;

@end
