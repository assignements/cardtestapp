#import <UIKit/UIKit.h>

@protocol ZETAContainerViewController <NSObject>

- (UIViewController *_Nonnull)topViewController;

@end

@interface UIViewController (ZETAAdditions)

- (UIViewController *_Nonnull)zeta_topPresentViewController;
- (UIViewController *_Nonnull)zeta_topViewController;

- (void)presentViewControllerAndDismissAnyVCIfPresent:(nonnull UIViewController *)viewController
                                             animated:(BOOL)animated
                                           completion:(void (^_Nullable)(void))completion;

@end
