#import <UIKit/UIKit.h>

@interface UIView (ZETAAdditions)

/*
 * Returns a view with a circular boundary for a square shaped view
 */
- (void)zeta_createCircularBoundary;
- (CALayer *)zeta_addDimmingLayerOutsideWithFillColor:(UIColor *)fillColor;
- (CALayer *)zeta_addStrokeLayer:(UIColor *)strokeColor width:(CGFloat)width;
- (UIView *)zeta_addBlockingView;
- (UIView *)zeta_addBlurView:(UIBlurEffectStyle)blurEffectStyle;

@end
