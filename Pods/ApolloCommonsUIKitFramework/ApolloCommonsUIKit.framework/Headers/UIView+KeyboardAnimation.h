#import <UIKit/UIKit.h>

@interface UIView (KeyboardAnimation)

- (void)zeta_animateForKeyboardNotification:(NSNotification *)notification
                                 animations:(void (^)(CGRect keyboardRect))animations;

@end
