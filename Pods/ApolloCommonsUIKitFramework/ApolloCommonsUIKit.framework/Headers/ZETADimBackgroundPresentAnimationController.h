#import <UIKit/UIKit.h>

@interface ZETADimBackgroundPresentAnimationController : NSObject<UIViewControllerAnimatedTransitioning>

-(instancetype) initWithOverlayView:(UIView *)overlayView
                            opacity:(CGFloat)opacity;

@end
