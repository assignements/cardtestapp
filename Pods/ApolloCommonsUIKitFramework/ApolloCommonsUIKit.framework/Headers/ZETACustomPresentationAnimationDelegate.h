#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ZETAScreenPresentWithAnimationType) {
  ZETAScreenPresentWithAnimationTypeNone,
  ZETAScreenPresentWithDimBackground
};

@interface ZETACustomPresentationAnimationDelegate : NSObject<UIViewControllerTransitioningDelegate>

- (instancetype) initWithBackgroundOpacity:(CGFloat)opacity
                         presentationStyle:(ZETAScreenPresentWithAnimationType)presentationStyle;

@end
