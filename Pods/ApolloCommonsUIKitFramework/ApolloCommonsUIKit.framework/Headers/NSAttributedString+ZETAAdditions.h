#import <Foundation/Foundation.h>

@interface NSAttributedString (ZETAAdditions)

/**
 Returns an attributed string with given string, font, color, default line height, center alignment
 */
+ (instancetype)zeta_attributedStringWithString:(NSString *)string
                                           font:(UIFont *)font
                                      colorName:(UIColor *)color;

/**
 Returns an attributed string with given string, font, color, default line height, given alignment
 */
+ (instancetype)zeta_attributedStringWithString:(NSString *)string
                                           font:(UIFont *)font
                                      colorName:(UIColor *)color
                                          align:(NSTextAlignment)align;

/**
 Returns an attributed string with given string, font, color, NO line height, given alignment
 */
+ (instancetype)zeta_attributedStringWithNoLineHeightForString:(NSString *)string
                                                          font:(UIFont *)font
                                                     colorName:(UIColor *)color
                                                         align:(NSTextAlignment)align;

/**
 Returns an attributed string with given string, font, background color and foreground text color
 */
+ (instancetype)zeta_attributedStringWithString:(NSString *)string
                                           font:(UIFont *)font
                            backgroundColorName:(UIColor *)bgColor
                            foregroundColorName:(UIColor *)fgColor
                                     attributes:(NSDictionary *)additionalAttributes;

/**
 Returns an attributed string with given string, font, color, line height multiple, given alignment
 */
+ (instancetype)zeta_attributedStringWithString:(NSString *)string
                                           font:(UIFont *)font
                                      colorName:(UIColor *)color
                                          align:(NSTextAlignment)align
                             lineHeightMultiple:(CGFloat)lineHeightMultiple;

+ (instancetype)zeta_attributedStringWithString:(NSString *)string
                                           font:(UIFont *)font
                                      colorName:(UIColor *)color
                                          align:(NSTextAlignment)align
                                     lineHeight:(CGFloat)lineHeight;

+ (instancetype)zeta_attributedStringWithImage:(UIImage *)image;

+ (instancetype)zeta_zetaLogoPrefixedAttributedString:(NSAttributedString *)attString;

/**
 * This method is only supported when the whole sourceString has same set of defined attributes for
 * the whole range, otherwise it will result in setting the attributes which are set for last range
 * attributes
 */
+ (instancetype)zeta_attributedStringWithString:(NSString *)string
                 attributesFromAttributedString:(NSAttributedString *)sourceString;

/**
 * Returns an underlined attributed string with the given string
 */
+ (instancetype)zeta_underlinedStringWithString:(NSString *)string;

+ (instancetype)zeta_underlinedStringWithString:(NSString *)string
                                           font:(UIFont *)font
                                      colorName:(UIColor *)color;

- (NSAttributedString *)zeta_attributedStringByAddingAttribute:(NSString *)name
                                                         value:(id)value
                                                         range:(NSRange)range;

- (NSAttributedString *)zeta_attributedStringByRemovingAttribute:(NSString *)attribute;

@end
