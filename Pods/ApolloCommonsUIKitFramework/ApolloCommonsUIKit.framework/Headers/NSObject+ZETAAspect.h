#import <Foundation/Foundation.h>
#import <ApolloCommonsMiscellaneous/ApolloMiscellaneous.h>

@interface NSObject (ZETAAspect)

+ (void)zeta_aspectHookAfterSelector:(SEL)selector block:(ZETAResponseBlockType)block;

@end
