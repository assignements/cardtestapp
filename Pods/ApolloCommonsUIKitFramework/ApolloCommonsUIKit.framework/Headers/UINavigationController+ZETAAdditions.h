#import <UIKit/UIKit.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

@interface UINavigationController (ZETAAdditions)

- (void)zeta_popViewControllerAnimated:(BOOL)animated completion:(ZETAEmptyBlockType)completion;

- (void)zeta_popToViewController:(UIViewController *)viewController
                        animated:(BOOL)animated
                      completion:(ZETAEmptyBlockType)completion;

- (void)zeta_popToRootViewControllerAnimated:(BOOL)animated
                                  completion:(ZETAEmptyBlockType)completion;

- (UIViewController *)zeta_rootViewController;

@end
