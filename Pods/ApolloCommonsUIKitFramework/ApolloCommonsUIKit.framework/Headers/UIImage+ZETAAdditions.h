#import <UIKit/UIKit.h>

@interface UIImage (ZETAAdditions)

/**
 @return a rectangular image of @p size filled with the solid @p color.
 @param size The size of the generated image in points.
 */
+ (UIImage *)zeta_imageWithColor:(UIColor *)color size:(CGSize)size;

/*
 * This method blurs the image using Apple's Image Effects source. It returns a blurred UIImage
 */
- (UIImage *)zeta_blurWithRadius:(CGFloat)radius;

- (UIImage *)zeta_desaturateImage;

- (NSData *)zeta_JPEGDataWithCompression:(CGFloat)compression;
/**
 * @return A composite image consisting of @p image and @p title with both horizontally center
 * aligned and the title placed below the image.
 * @param imageColor The tint color to be applied to the image.
 * @param imageBottomMargin The vertical spacing between the image and the title.
 */
+ (UIImage *)zeta_imageWithImage:(UIImage *)image
                      imageColor:(UIColor *)imageColor
                       blendMode:(CGBlendMode)blendMode
                           title:(NSString *)title
                 titleAttributes:(NSDictionary *)titleAttributes
               imageBottomMargin:(CGFloat)imageBottomMargin;

@end
