#import <Foundation/Foundation.h>
#import <ApolloCommonsCore/ApolloMacros.h>
#import <iSdkAuthCore/GetAuthSessionBlockAlias.h>
#import <ApolloDoorStream/ZETADoorStreamManager.h>
#import <ApolloAnalyticsCore/ApolloAnalyticsManager.h>

NS_ASSUME_NONNULL_BEGIN

/// ApolloScreensAuthManager is used to provide Auth context for the generic screens module. one time init using tenant token and auth ID is required to setup
@interface ApolloScreensAuthManager : NSObject

+ (instancetype)sharedInstance;

/// Set ups the Authentication manager for the the tenant config dictionary named screenAuthConfig.plist
+ (void)setup;

+ (void)setupWithAnalyticsManager:(ApolloAnalyticsManager *)analyticsManager;

+ (void)resetSharedInstance;

/// Description
/// @param token tenant token of the user
/// @param success for authentication success
/// @param failure for authentication failure
- (void)authenticateWithToken:(NSString *)token
                 successBlock:(ZETAEmptyBlockType)success
                 failureBlock:(ZETAFailureBlockType)failure;

/// Used to refresh auth token for the tenant
- (void)getAuthScreenTokenWithSuccessBlock:(ZETAStringBlockType)success
                              failureBlock:(ZETAFailureBlockType)failure;

/// Used to logout the tenant
- (void)logoutWithSuccessBlock:(ZETAEmptyBlockType)success
                  failureBlock:(ZETAFailureBlockType)failure;

/// Used to get userJID
- (void)userJIDWithSuccessBlock:(ZETAStringBlockType)success
                   failureBlock:(ZETAFailureBlockType)failure;

- (void)getAuthSessionWithSuccessBlock:(nullable GetAuthSessionSuccessBlock)success
                          failureBlock:(nullable ZETAFailureBlockType)failure;

- (nullable NSString *)getInstallationID;

- (nullable ZETADoorStreamManager *)getDoorStream;

- (void)updateUserOnlineStatusForAuthID:(NSString *)authID;

@end

NS_ASSUME_NONNULL_END
