#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const ZETASharedPushDataPushIDsKey;
FOUNDATION_EXPORT NSString *const ZETAPushSourceDoor;
FOUNDATION_EXPORT NSString *const ZETAPushSourceAPNS;
FOUNDATION_EXPORT NSString *const ZETAPushSourceCollections;
