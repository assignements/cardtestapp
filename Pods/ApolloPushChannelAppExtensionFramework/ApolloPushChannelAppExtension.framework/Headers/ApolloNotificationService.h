#import <UserNotifications/UserNotifications.h>

API_AVAILABLE(ios(10.0))
@interface ApolloNotificationService : NSObject

- (instancetype)initWithSharedPushDataAppGroup:(NSString *)sharedPushDataAppGroup;

- (void)handleNotificationRequest:(UNNotificationRequest *_Nonnull)request
               withContentHandler:(void (^_Nonnull)(UNNotificationContent *_Nonnull))contentHandler
               bestAttemptContent:(UNMutableNotificationContent *_Nonnull)bestAttemptContent;

@end
