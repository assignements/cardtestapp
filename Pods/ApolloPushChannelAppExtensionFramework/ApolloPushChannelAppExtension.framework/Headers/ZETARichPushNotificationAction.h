#import <Mantle/Mantle.h>

@interface ZETARichPushNotificationAction : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSURL *iconURL;
@property (nonatomic, readonly) NSURL *URL;
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *eventName;
@property (nonatomic, readonly) NSDictionary *eventAttributes;

@end
