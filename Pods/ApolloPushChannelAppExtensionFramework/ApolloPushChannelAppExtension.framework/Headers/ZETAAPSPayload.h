#import <Mantle/Mantle.h>

@interface ZETAAPSPayload : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *category;
@property (nonatomic, readonly) BOOL isSilentNotification;
@property (nonatomic, readonly) NSString *sound;

@end
