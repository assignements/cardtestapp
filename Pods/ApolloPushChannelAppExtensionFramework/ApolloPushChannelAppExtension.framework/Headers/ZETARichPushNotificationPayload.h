#import <Mantle/Mantle.h>

@class ZETAZetletAnalytics;

@interface ZETARichPushNotificationPayload : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *subtitle;
@property (nonatomic, readonly) NSURL *iconURL;
@property (nonatomic, readonly) NSURL *bigPictureURL;
@property (nonatomic, readonly) NSDate *time;
@property (nonatomic, readonly) NSURL *URL;
@property (nonatomic, readonly) NSNumber *priority;
@property (nonatomic, readonly) NSString *campaignName;
@property (nonatomic, readonly, copy) NSArray *actions;
@property (nonatomic, readonly) BOOL sound;
@property (nonatomic, readonly) BOOL vibrate;
@property (nonatomic, readonly) BOOL light;
@property (nonatomic, readonly) ZETAZetletAnalytics *analytics;

@end
