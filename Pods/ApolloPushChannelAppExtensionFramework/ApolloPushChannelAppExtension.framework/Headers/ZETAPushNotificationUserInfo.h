#import <Mantle/Mantle.h>

@class ZETAAPSPayload;
@class ZETARichPushNotificationPayload;

@interface ZETAPushNotificationUserInfo : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) ZETAAPSPayload *aps;
// FIXME: Do the generic mantle handelling for payloads
@property (nonatomic, readonly) NSDictionary *payloads;
@property (nonatomic, readonly) ZETARichPushNotificationPayload *notification;
@property (nonatomic, readonly) NSString *pushID;
@property (nonatomic, readonly) NSDate *expiry;
@property (nonatomic, readonly) NSString *showPrompt;

@end
