#import <Foundation/Foundation.h>
#import "BaseClient.h"
#import "SetPinRequestPayload.h"
#import "CardStatusRequest.h"
#import "ChangePinRequestPayload.h"

typedef NS_ENUM(NSUInteger, CardClientErrorCode) {
  CardClientErrorCodeUnknown,
  CardClientErrorCodeFailedToGetCard
};

@interface CardClient : BaseClient

- (nonnull instancetype)initWithBaseURL:(nonnull NSURL *)url
                   andUserAgent:(nonnull NSString *)userAgent;

- (void)getCardWithCardID:(nonnull NSString *)cardID
                      ifi:(nonnull NSString *)ifi
                  headers:(nonnull NSDictionary *)headers
                  success:(nullable ZETAResponseBlockType)success
                  failure:(nullable ZETAFailureBlockType)failure;

- (void)setPinWithCardID:(nonnull NSString *)cardID
                     ifi:(nonnull NSString *)ifi
                jwtToken:(nonnull NSString *)jwtToken
               authToken:(nonnull NSString *)authToken
          requestPayload:(nonnull SetPinRequestPayload *)requestPayload
                 success:(nullable ZETAResponseBlockType)success
                 failure:(nullable ZETAFailureBlockType)failure;

-(void)updateCard:(nonnull NSString *)cardID
              ifi:(nonnull NSString *)ifi
       cardStatus:(nonnull CardStatusRequest *)request
          headers:(nonnull NSDictionary *)headers
          success:(nullable ZETAResponseBlockType)success
          failure:(nullable ZETAFailureBlockType)failure;

- (void)changePinWithCardID:(nonnull NSString *)cardID
                        ifi:(nonnull NSString *)ifi
                   jwtToken:(nonnull NSString *)jwtToken
                  authToken:(nonnull NSString *)authToken
             requestPayload:(nonnull ChangePinRequestPayload *)requestPayload
                    success:(nullable ZETAResponseBlockType)success
                    failure:(nullable ZETAFailureBlockType)failure;
@end
