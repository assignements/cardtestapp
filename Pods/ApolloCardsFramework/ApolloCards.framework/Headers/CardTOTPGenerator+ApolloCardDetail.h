#import "CardTOTPGenerator.h"

NS_ASSUME_NONNULL_BEGIN

@interface CardTOTPGenerator (ApolloCardDetail)
+ (NSString *)generateTOTPWithSecret:(NSData *)secret
                              movingFactor:(NSString *)movingFactor
                                    length:(NSInteger)length
                                  duration:(NSTimeInterval)duration;
@end

NS_ASSUME_NONNULL_END
