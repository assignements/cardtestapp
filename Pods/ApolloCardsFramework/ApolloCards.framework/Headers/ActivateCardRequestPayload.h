#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface ActivateCardRequestPayload : MTLModel <MTLJSONSerializing>

- (instancetype)initWithCardNumber:(NSString *)cardNumber
                        expiryDate:(NSString *)expiryDate
                               cvv:(NSString *)cvv;

@property(nonatomic) NSString *panSHA;

@end

NS_ASSUME_NONNULL_END
