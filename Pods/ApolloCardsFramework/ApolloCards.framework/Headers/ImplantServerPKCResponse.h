#import <Mantle/Mantle.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImplantServerPKCResponse : MTLModel<MTLJSONSerializing>
@property(nonatomic) NSString *serverPublicKeyCertEncoded;
@end

NS_ASSUME_NONNULL_END
