#import <Foundation/Foundation.h>
#import <ApolloZapps/ZETAJSPlugin.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

NS_ASSUME_NONNULL_BEGIN

@class ZETAPluginFactory;

@protocol ApolloWebScreenManagerPluginProtocol;

@interface ApolloPluginFactoryGenerator : NSObject

@property (nonatomic, weak) id<ApolloWebScreenManagerPluginProtocol> pluginDelegate;

+ (instancetype)sharedProvider;
+ (void)resetSharedProvider;

- (NSArray *)allAvailblePlugins;
- (NSArray *)basicPlugins;
- (NSArray *)allAvailablePluginNames;
- (NSArray *)basicPluginNames;
- (id<ZETAJSPlugin>)pluginForName:(NSString *)pluginName;

@end

@protocol ApolloWebScreenManagerPluginProtocol<NSObject>

@optional

/// Method is used for web pages which requires authentication. The consumer can pass the token here.
/// @param successBlock String block which passes the Auth token
/// @param failureBlock NSError can be passed in case of failure
/// @note Please refresh the token before passing.
- (void)getAuthTokenWithSuccessBlock:(ZETAStringBlockType)successBlock
                        failureBlock:(ZETAFailureBlockType)failureBlock;

/// Method is used for web pages which requires user JID. The consumer can pass the user JID here.
/// @param successBlock String block which passes the user JID
/// @param failureBlock NSError can be passed in case of failure
/// @note Please refresh the session before passing.
- (void)getUserJIDWithSuccessBlock:(ZETAStringBlockType)successBlock
                      failureBlock:(ZETAFailureBlockType)failureBlock;

@end



NS_ASSUME_NONNULL_END
