#import <Mantle/Mantle.h>

@interface FormFactorProduct : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSNumber *ifi;
@property (nonatomic, readonly) NSString *formFactorProductID;
@property (nonatomic, readonly) NSString *code;
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *desc;
@property (nonatomic, readonly) NSString *type;
@property (nonatomic, readonly) NSDictionary *policies;
@property (nonatomic, readonly) NSString *provider;
@property (nonatomic, readonly) NSString *skuID;
@property (nonatomic, readonly) NSArray *tags;
@property (nonatomic, readonly) NSDictionary *attributes;
@property (nonatomic, readonly) NSString *issuanceStatus;
@property (nonatomic, readonly) NSString *paymentStatus;
@property (nonatomic, readonly) NSDate *createdAt;
@property (nonatomic, readonly) NSDate *modifiedAt;

@end
