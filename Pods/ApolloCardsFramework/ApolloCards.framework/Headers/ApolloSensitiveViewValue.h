#import <Foundation/Foundation.h>

@interface ApolloSensitiveViewValue: NSObject

@property (nonatomic, readonly) NSString *serverPublicKey;
@property (nonatomic, readonly) NSString *encryptedData;
@property (nonatomic, readonly) NSString *iv;

- (instancetype)initWithServerPublicKey:(NSString *)serverPublicKey
                          encryptedData:(NSString *)encryptedData
                                     iv:(NSString *)iv;

@end
