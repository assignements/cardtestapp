#import <Foundation/Foundation.h>
#import "BaseClient.h"
#import "ApolloAuthenticationProvider.h"
NS_ASSUME_NONNULL_BEGIN

@interface ActivateCardClient : BaseClient

- (nonnull instancetype)initWithBaseURL:(nonnull NSURL *)url
                              userAgent:(nonnull NSString *)userAgent
                           authProvider:(id<ApolloAuthenticationProvider>) authProvider;

- (void)activateCardWithCardID:(nonnull NSString *)cardID
                           ifi:(nonnull NSString *)ifi
                       success:(nullable ZETAResponseBlockType)success
                       failure:(nullable ZETAFailureBlockType)failure;

@end

NS_ASSUME_NONNULL_END
