//


#import <Foundation/Foundation.h>
#import "ApolloSharedSecretStore.h"
#import <ApolloUserCredentials/ZETAApolloUserCredentials.h>

NS_ASSUME_NONNULL_BEGIN

@interface CardBaseWorker : NSObject
@property (nonatomic, strong) NSString *apiKey;
@property (nonatomic, strong) ApolloSharedSecretStore *store;

- (instancetype)initWithStore:(nonnull ApolloSharedSecretStore *)store userCredentials:(nullable ZETAApolloUserCredentials *)apolloUserCredentials;
- (NSDictionary *) getHeaderWithAuthToken: (NSString *) token;
- (NSDictionary *) getHeaderValue;
@end

NS_ASSUME_NONNULL_END
