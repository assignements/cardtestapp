#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import <Foundation/Foundation.h>
#import "ProxyCardSKUDetail.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProxyCardExpiry: MTLModel<MTLJSONSerializing>
@property (nonatomic) NSString *year;
@property (nonatomic) NSString *month;
@end

NS_ASSUME_NONNULL_END
