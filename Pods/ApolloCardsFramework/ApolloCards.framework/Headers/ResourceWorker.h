#import <Foundation/Foundation.h>
#import "CardBaseWorker.h"
#import <ApolloUserCredentials/ZETAApolloUserCredentials.h>
#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>
#import "ApolloCardErrorInfo.h"
#import "Resource.h"
#import "ApolloAuthenticationProvider.h"

NS_ASSUME_NONNULL_BEGIN

@interface ResourceWorker : CardBaseWorker

- (instancetype)initWithStore:(ApolloSharedSecretStore *)store
          authSessionProvider:(id<ApolloAuthenticationProvider>)authProvider
              userCredentials:(ZETAApolloUserCredentials *)apolloUserCredentials;

- (void)getResourceForAccountHolder:(NSString *)accountHolderID
                       successBlock:(nonnull void (^)(NSArray <Resource *> *resources))successBlock
                       failureBlock:(ApolloCardFailureBlock)failureBlock;
@end

NS_ASSUME_NONNULL_END
