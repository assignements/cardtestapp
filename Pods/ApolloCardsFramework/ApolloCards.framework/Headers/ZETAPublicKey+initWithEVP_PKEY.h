//

#include <OpenSSL-Universal/openssl/x509.h>
#import <ApolloCoreCrypto/ApolloCoreCryptoHeaders.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZETAPublicKey (initWithEVP_PKEY)
- (instancetype)initWithEVP_PKEY:(EVP_PKEY *) publicKey;
@end

NS_ASSUME_NONNULL_END
