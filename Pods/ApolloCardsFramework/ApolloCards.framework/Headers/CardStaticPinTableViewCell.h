#import "ApolloCardThemeConfig.h"
#import <UIKit/UIKit.h>
#import <CocoaNuggetsViews/ZETANumericKeyboardDelegateHandler.h>

@class ACSetPinTableViewCellViewModel;
NS_ASSUME_NONNULL_BEGIN

@protocol KeyboardDelegate <NSObject>

-(void)showKeyboard;
-(void)hideKeyboard;

@end
@interface CardStaticPinTableViewCell : UITableViewCell
@property(nonatomic, weak) id<KeyboardDelegate> delegate;
@property (nonatomic, strong) ApolloCardThemeConfig *themeConfig;

- (void) configureWithViewModel:(ACSetPinTableViewCellViewModel *)viewModel;

-(void)bindWithNumberPadView:(ZETANumberPadView *)numberPadView;

-(BOOL)makeFirstResponder;
@end

NS_ASSUME_NONNULL_END
