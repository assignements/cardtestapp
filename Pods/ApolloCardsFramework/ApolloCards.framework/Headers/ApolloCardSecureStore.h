#import <Foundation/Foundation.h>
#import "ApolloCardSecuredStoreAccess.h"
#import <ApolloUserCredentials/ZETAApolloUserCredentials.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CipherSecureStore;
@interface ApolloCardSecureStore : NSObject<ApolloCardSecuredStoreAccess, CipherSecureStore>

-(instancetype)initWithIdentifier:(nullable NSString *)identifier
                           appName:(NSString *)appName
                configFileName:(NSString *)configFileName;

@end
NS_ASSUME_NONNULL_END
