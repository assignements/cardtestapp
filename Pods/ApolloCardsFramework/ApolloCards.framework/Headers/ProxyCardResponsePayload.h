#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import <Foundation/Foundation.h>
#import "ProxyCardOrderDetail.h"
NS_ASSUME_NONNULL_BEGIN

@interface ProxyCardResponsePayload: MTLModel<MTLJSONSerializing>
@property (nonatomic) NSString *ifi;
@property (nonatomic) NSString *crn;
@property (nonatomic) NSString *cardType;
@property (nonatomic) NSString *cardID;
@property (nonatomic) NSString *maskedPan;
@property (nonatomic) NSString *cardStatus;
@property (nonatomic) ProxyCardOrderDetail *orderDetails;

@end

NS_ASSUME_NONNULL_END
