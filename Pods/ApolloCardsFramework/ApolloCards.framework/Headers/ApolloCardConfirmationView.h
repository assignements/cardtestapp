
#import <UIKit/UIKit.h>
#import "ApolloCardThemeConfig.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^DismissBlockType)(void);

@interface ApolloCardConfirmationView : UIView
+(void) showConfirmationViewOnView:(UIView *)parentView
                             theme:(ApolloCardThemeConfig *)theme
               confirmationMessage:(NSString *)confirmationMessage
           confirmationButtonTitle:(NSString *)confirmButtonTitle
                     dismissAction:(nullable DismissBlockType) dismissBlock
                     isSuccessView:(BOOL)successView;
@end

NS_ASSUME_NONNULL_END
