#import <UIKit/UIKit.h>

@protocol SuperPinViewProtocol <NSObject>

- (UIView *)getSuperPinViewForTemplateId:(NSString *)templateId;

- (CGFloat)getSuperPinViewHeightForTemplateId:(NSString *)templateId;

-(void)configureSuperPinViewWithSuperPin:(NSString *)superPin
                       validityInSeconds:(NSTimeInterval)validityInSeconds
                              templateId:(NSString *)templateId;

- (void)invalidateTimerIfPresentForTemplateId:(NSString *)templateId;

@end
