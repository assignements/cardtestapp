#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ApolloCardPinCellVMType) {
    ApolloCardPinCellVMTypeSetPin,
    ApolloCardPinCellVMTypeInputPin,
    ApolloCardPinCellVMTypeUpdatePin
};

typedef NS_ENUM(NSUInteger, ApolloCardPinSetErrorType) {
    ApolloCardPinSetErrorUnknown,
    ApolloCardPinSetErrorEnterPin,
    ApolloCardPinSetErrorConfirmPin,
    ApolloCardPinSetErrorTypeWrongPin,
    ApolloCardOldPinErrorTypeWrongPin
};

@protocol ACSetPinTableViewCellViewModelDelegate <NSObject>
- (void) showErrorWithMessage:(NSString *) message type:(ApolloCardPinSetErrorType) type;
- (void) resetErrors;
@end

@protocol ACSetPinTableViewCellViewModelParentDelegate <NSObject>
- (void) dismissed;
- (void) appPinEnetered: (NSString *) appPin;
- (void) enteredOldPin: (NSString *)oldPin newPin:(NSString *)newPin;
@end

@interface ACSetPinTableViewCellViewModel : NSObject

- (instancetype) initWithViewModelType:(ApolloCardPinCellVMType) vmType;

@property (nonatomic, weak) id<ACSetPinTableViewCellViewModelDelegate> delegate;
@property (nonatomic, weak) id<ACSetPinTableViewCellViewModelParentDelegate> parentDelegate;
@property (nonatomic, readonly) NSUInteger pinLength;
@property (nonatomic, readonly) ApolloCardPinCellVMType vmType;
@property (nonatomic, strong) NSString *inputPin;
@property (nonatomic, strong) NSString *confPin;
@property (nonatomic, strong) NSString *oldPin;
@property (nonatomic, readonly) NSString *cellHeader;
@property (nonatomic, readonly) NSString *navigationTitle;
@property (nonatomic, readonly) BOOL hidePinConfirmation;
@property (nonatomic, readonly) BOOL displayOldPIN;


- (void) informPinError;
- (void) submitPIN;
- (void) dismiss;
-(BOOL) isPinValidationSuccesful;
@end

NS_ASSUME_NONNULL_END
