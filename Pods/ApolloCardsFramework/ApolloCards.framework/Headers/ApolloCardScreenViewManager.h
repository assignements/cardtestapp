#import <Foundation/Foundation.h>
#import <ApolloViewGroups/ApolloViewGroupManager.h>
#import <ApolloScreens/ApolloScreensConstants.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloCardScreenViewManager : NSObject

- (instancetype)initWithViewGroupManager:(ApolloViewGroupManager *)viewGroupManager
                   templateCollectionIDs:(NSArray *)templateCollectionIDs;

- (UIViewController *)getScreenWithDeeplink:(NSString *)deeplink;

@end

NS_ASSUME_NONNULL_END
