#import <Mantle/Mantle.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SetIdentityMetadataResponse : MTLModel<MTLJSONSerializing>
@property(nonatomic, nonnull) NSString *serverPKC;
@property(nonatomic, nonnull) NSString *inputJID;

@end

NS_ASSUME_NONNULL_END
