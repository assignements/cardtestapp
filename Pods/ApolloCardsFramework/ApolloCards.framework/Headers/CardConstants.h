#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, CardStatus) {
    CardStatusBlock,
    CardStatusUnBlock,
    CardStatusHotlist
};

NS_ASSUME_NONNULL_END
