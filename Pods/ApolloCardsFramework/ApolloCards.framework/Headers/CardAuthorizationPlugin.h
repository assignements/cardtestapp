#import <Foundation/Foundation.h>
#import <ApolloZapps/ZETABaseJSPlugin.h>

@protocol ApolloWebScreenManagerPluginProtocol;

NS_ASSUME_NONNULL_BEGIN

@interface CardAuthorizationPlugin : ZETABaseJSPlugin

/// Authorization plugin for passing auth context for requested webpage
/// @param pluginDelegate Delgate used to make callback for Auth Token request
- (instancetype)initWithPluginDelegate:(id<ApolloWebScreenManagerPluginProtocol>)pluginDelegate;

NS_ASSUME_NONNULL_END

@end
