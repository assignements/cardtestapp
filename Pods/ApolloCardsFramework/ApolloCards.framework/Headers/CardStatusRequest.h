#import <Foundation/Foundation.h>
#import "CardStatusReasonRequest.h"
#import <Mantle/Mantle.h>
NS_ASSUME_NONNULL_BEGIN

@interface CardStatusRequest: MTLModel<MTLJSONSerializing>
@property(nonatomic, nonnull) NSString *status;
@property(nonatomic, nonnull) CardStatusReasonRequest *reason;
@end

NS_ASSUME_NONNULL_END
