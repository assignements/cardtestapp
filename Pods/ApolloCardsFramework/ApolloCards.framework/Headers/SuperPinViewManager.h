
#import <Foundation/Foundation.h>
#import "ApolloCardError.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^TOTPFetchBlock)(NSString * _Nullable superPin, NSTimeInterval validityInSeconds, ApolloCardErrorInfo * _Nullable error);

@protocol SuperPinViewManagerDelegate;

@interface SuperPinViewManager : NSObject

-(void)fetchUIForSuperPin:(NSString *)superPin
        validityInSeconds:(NSTimeInterval)validityInSeconds
           withCompletion:(void (^)(UIView * superPinView))completionBlock;

@property (weak, nonatomic) id<SuperPinViewManagerDelegate> delegate;
-(void)resetAllData;
@end

@protocol SuperPinViewManagerDelegate <NSObject>

- (void)superPinExpiredWithCompletion:(TOTPFetchBlock)completion;

@end

NS_ASSUME_NONNULL_END
