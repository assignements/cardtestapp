
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@protocol ActivateCardIDCellViewModelDelegate <NSObject>

-(void)enteredCardID:(NSString*)cardID;

@end

@interface ActivateCardIDCellViewModel : NSObject
@property (nonatomic, strong) NSString *cardID;
@property (nonatomic, strong) NSString *DNINumber;
@property (weak,nonatomic) id<ActivateCardIDCellViewModelDelegate> delegate;
-(void)activateCard;
-(BOOL)isCardDetailsValid;
@end

NS_ASSUME_NONNULL_END
