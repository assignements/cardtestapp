#import <Mantle/Mantle.h>
#import "ResourceProduct.h"
#import "FormFactor.h"

@interface Resource : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *resourceId;
@property (nonatomic, readonly) NSNumber *ifi;
@property (nonatomic, readonly) NSString *resourceProductId;
@property (nonatomic, readonly) ResourceProduct *resourceProduct;
@property (nonatomic, readonly) NSString *targetUri;
@property (nonatomic, readonly) NSArray *formFactors;
@property (nonatomic, readonly) NSDictionary *policies;
@property (nonatomic, readonly) NSDictionary *attributes;
@property (nonatomic, readonly) NSArray *tags;
@property (nonatomic, readonly) NSArray *vectors;
@property (nonatomic, readonly) NSString *status;

@end
