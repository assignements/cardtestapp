
#import <UIKit/UIKit.h>
#import "ApolloCardThemeConfig.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^DismissBlockType)(void);

@interface GetPinView : UIView
-(void)customizePinViewWiththeme:(ApolloCardThemeConfig *)theme
             confirmationMessage:(NSString *)confirmationMessage
                             pin:(NSString *)pin
         confirmationButtonTitle:(NSString *)confirmButtonTitle
                   dismissAction:(nullable DismissBlockType) dismissBlock;
-(void)updatePin:(NSString *)pin;
@end

NS_ASSUME_NONNULL_END
