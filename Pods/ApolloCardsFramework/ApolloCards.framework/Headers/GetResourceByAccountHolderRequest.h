#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import <ApolloCoreCrypto/ApolloCoreCryptoHeaders.h>

NS_ASSUME_NONNULL_BEGIN


@interface GetResourceByAccountHolderRequest : MTLModel<MTLJSONSerializing>
- (instancetype)initWithAccountHolder:(NSString *)accountHolderId;
@end

NS_ASSUME_NONNULL_END
