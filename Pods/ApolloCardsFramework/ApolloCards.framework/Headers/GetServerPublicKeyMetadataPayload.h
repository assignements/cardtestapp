#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface GetServerPublicKeyMetadataPayload : MTLModel<MTLJSONSerializing>
@property (nonatomic, strong, nonnull) NSString *clientPKC;
@property (nonatomic, strong, nonnull) NSString *inputJID;

- (instancetype)initWithClientKey:(NSString *)key;
@end

NS_ASSUME_NONNULL_END
