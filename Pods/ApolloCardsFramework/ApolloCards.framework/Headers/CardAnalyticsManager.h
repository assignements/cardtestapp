#import <Foundation/Foundation.h>
#import <ApolloCommonsAgentInfo/ApolloAgentInfo.h>
#import <ApolloAnalyticsKibana/ApolloKibanaAnalytics.h>
#import "ApolloCardErrorInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface CardAnalyticsManager : NSObject
+ (instancetype) defaultManager;
- (void) updateAgentInfo: (TDTAgentInfo *) agentInfo;
- (void)setTenantProvidedSecureStore:(BOOL)tenantProvidedSecureStore;
- (void)logEventWithName:(NSString *)eventName withError:(NSError *) error;
- (void)logEventWithName:(nonnull NSString *)eventName;
- (void)logEventWithName:(nonnull NSString *)eventName
                  params:(nonnull NSDictionary *)params;
- (void)logEventWithName:(NSString *)eventName
                  params:(NSDictionary *) params
               withError:(nonnull NSError *)error;
- (ApolloAnalyticsManager *) analyticsManager;
- (void)logEventWithName:(NSString *)eventName
                  params:(NSDictionary *) params
         withNativeError:(nonnull ApolloCardErrorInfo *)error;
@end

NS_ASSUME_NONNULL_END
