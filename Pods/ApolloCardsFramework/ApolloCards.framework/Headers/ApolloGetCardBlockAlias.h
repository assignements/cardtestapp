#import <Foundation/Foundation.h>
#import "ApolloCardErrorInfo.h"

typedef void (^ApolloCardSuccessBlock)(NSDictionary *securedCardDetails);
typedef void (^ApolloGetCardPanSuccessBlock)(NSString *cardPan);

typedef void (^ApolloCardFailureBlock)(ApolloCardErrorInfo *userInfo);
typedef void (^ApolloErrorBlock)(NSError *error);

typedef void (^ApolloSetPinSuccessBlock)(void);
typedef void (^ApolloCardEmptyBlockType)(void);

typedef void (^ApolloStringSuccessBlock)(NSString *cardPin);

typedef NS_ENUM(NSUInteger, AlertType) {
    BlockCard,
    UnblockCard,
    HotlistCard
};
