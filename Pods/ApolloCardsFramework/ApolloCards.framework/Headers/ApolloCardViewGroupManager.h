
#import <Foundation/Foundation.h>
#import <ApolloViewGroups/ApolloViewGroupManager.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ApolloCardViewGroupManagerProtocol;

@interface ApolloCardViewGroupManager : NSObject

- (instancetype)initWithViewGroupManager:(ApolloViewGroupManager *)viewGroupManager;
@property (nonatomic) ApolloViewGroupManager *viewGroupManager;
@property (weak, nonatomic) id<ApolloCardViewGroupManagerProtocol> delegate;

- (void)clearPersistedData;
- (void)fetchViewGroupsForUserSessinJID:(NSString *)userSessinJID;

@end

@protocol ApolloCardViewGroupManagerProtocol <NSObject>

- (void)viewGroupsFetchedSuccessFully;

@end

NS_ASSUME_NONNULL_END
