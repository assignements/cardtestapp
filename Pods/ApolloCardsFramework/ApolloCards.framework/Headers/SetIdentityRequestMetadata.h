#import <Mantle/Mantle.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SetIdentityRequestMetadata : MTLModel<MTLJSONSerializing>
@property (nonatomic, strong, nonnull) NSString *clientPKC;
@property (nonatomic, strong, nonnull) NSString *serverPKCIdentifier;
@property (nonatomic, strong, nonnull) NSString *inputJID;
@end

NS_ASSUME_NONNULL_END
