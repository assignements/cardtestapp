#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "BinRange.h"
#import "SensitiveView.h"
#import "CardStatusReasonRequest.h"

@interface GetCardResponse: MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *cguid;
@property (nonatomic, readonly) NSString *crn;
@property (nonatomic, readonly) NSString *cardType;
@property (nonatomic, readonly) NSString *maskedPan;
@property (nonatomic, readonly) NSString *cardStatus;
@property (nonatomic, readonly) BinRange *binRange;
@property (nonatomic, readonly) NSArray<NSString *> *vectors;
@property (nonatomic, readonly) SensitiveView *sensitiveView;
@property (nonatomic, readonly) CardStatusReasonRequest *statusReason;

@end
