#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

@protocol ApolloAuthenticationProvider <NSObject>

-(void)authenticateSDKWithSuccess:(nonnull void (^)(SdkAuthSession * _Nonnull authSession)) successBlock
                       andFailure:(nonnull void (^)(NSError * _Nonnull error)) failure;

- (void)getAuthTokenWithSuccess:(nonnull ZETAStringBlockType)success
                        failure:(nonnull ZETAFailureBlockType)failure;

@end
