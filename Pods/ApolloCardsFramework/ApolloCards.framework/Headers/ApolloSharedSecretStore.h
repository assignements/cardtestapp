#import <Foundation/Foundation.h>
#import <ApolloCoreCrypto/ZETAErasablePrivateKey.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import "Resource.h"
#import "ApolloCardSecuredStoreAccess.h"
#import "ApolloGetCardBlockAlias.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^SharedSecretFetchBlock)(NSData * _Nullable secret, NSError * _Nullable error);
typedef void (^PrivateKeyFetchBlock)(ZETAErasablePrivateKey * _Nullable privateKey, NSError * _Nullable error);

typedef NS_ENUM(NSUInteger, ApolloSharedSecretStoreVersion) {
  ApolloSharedSecretStoreVersionOne = 1,
  ApolloSharedSecretStoreCurrentVersion = ApolloSharedSecretStoreVersionOne
};


@interface ApolloSharedSecretStore : NSObject
@property (nonatomic, strong, nullable) id<ApolloCardSecuredStoreAccess> secureStore;


- (void) setSharedSecret: (NSData *)sharedSecret forResource:(NSString *)resourceID completion:(ApolloErrorBlock)completion;
- (void)retreiveSharedSecretForResource:(NSString *)resourceID completion:(SharedSecretFetchBlock)completion;


-(void) saveInputJID:(NSString *)inputJID forResource:(NSString *)resourceID;
-(NSString *) getInputJIDForResource:(NSString *)resourceID;

- (void) setServerPublicCert:(NSData *)publicCert forResource:(NSString *)resourceID;
- (NSData *)serverPublicCertForResource:(NSString *)resourceID;

- (NSArray <Resource *> *)resourcesForAccountHolderID:(NSString *)accountHolderID;
- (void)saveResources:(NSArray <Resource *> *)resources
  forAccountHolderID:(NSString *)accountHolderID;

- (void)logout;

-(void)saveEncryptedSharedSecret:(nonnull NSString *)encryptedSecret;
-(nullable NSString *) getEncryptedSharedSecret;

- (void) setCipherSharedSecret: (NSData *)sharedSecret forResource:(NSString *)resourceID completion:(ApolloErrorBlock)completion;

- (void)retreiveCipherSharedSecretForResource:(NSString *)resourceID
                                   completion:(SharedSecretFetchBlock)completion;

- (void) setCipherServerPublicCert:(NSData *)publicCert
                       forResource:(NSString *)resourceID;
- (NSData *)cipherServerPublicCertForResource:(NSString *)resourceID;

-(void) saveCipherInputJID:(NSString *)inputJID forResource:(NSString *)resourceID;
-(NSString *) getCipherInputJIDForResource:(NSString *)resourceID;
@end

NS_ASSUME_NONNULL_END
