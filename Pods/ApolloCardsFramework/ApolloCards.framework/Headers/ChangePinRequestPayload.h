#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import <ApolloCoreCrypto/ZETAECKeyPair.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChangePinRequestPayload: MTLModel<MTLJSONSerializing>
@property(nonatomic) NSString *iv;
@property(nonatomic) NSString *encryptedData;

-(nonnull instancetype) initWithOldPin:(nonnull NSString *)oldPin
                                newPin:(nonnull NSString *)newPin
                            privateKey:(nonnull ZETAErasablePrivateKey *)privateKey;

-(nonnull instancetype) initWithNewPin:(nonnull NSString *)newPin
                            privateKey:(nonnull ZETAErasablePrivateKey *)privateKey;
@end

NS_ASSUME_NONNULL_END
