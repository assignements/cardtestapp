#import <Foundation/Foundation.h>

@interface ApolloCardErrorInfo : NSObject

@property (nonatomic, readonly, nullable) NSString *traceId;
@property (nonatomic, readonly, nonnull) NSString *code;
@property (nonatomic, readonly, nonnull) NSString *type;
@property (nonatomic, readonly, nonnull) NSString *message;
@property (nonatomic, readonly, nullable) NSString *domain;

- (instancetype _Nullable )initWithTraceId:(NSString *_Nullable)traceId
                                    domain:(NSString *_Nullable)domain
                                      code:(NSString *_Nullable)code
                                      type:(NSString *_Nullable)type
                                   message:(NSString *_Nullable)message;

- (instancetype _Nullable )initWithTraceId:(NSString *_Nullable)traceId
                                      code:(NSString *_Nullable)code
                                      type:(NSString *_Nullable)type
                                   message:(NSString *_Nullable)message;

@end
