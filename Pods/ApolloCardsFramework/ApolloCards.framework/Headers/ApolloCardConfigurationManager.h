#import <Foundation/Foundation.h>
#import <ApolloCommonsAgentInfo/ApolloAgentInfo.h>

@interface ApolloCardConfigurationManager : NSObject

+ (instancetype)sharedManager;

+ (void)resetSharedProvider;

/**
 NOTE: "configViolation" method returns the violation w.r.t config file for cards sdk.
 **/

@property (nonatomic, readonly) NSDictionary *configurationDictionary;

- (void)updateConfigDictionary;

- (TDTAgentInfo *)agentInfo;

- (NSString *)configViolation;

- (NSNumber *)tenantID;

- (NSNumber *)ifiTenantID;

- (NSString *)storeAppID;

- (NSString *)sdkName;

- (NSString *)apiKey;

- (NSURL *)sdkAuthUrl;

- (NSURL *)cardsUrl;

- (NSString *)projectId;

- (NSString *)zeta_ifi;

- (NSString *)platform;

- (NSString *)version;

- (NSInteger)totpLength;

- (NSTimeInterval)totpDuration;

- (NSString *)cardTemplateID;

- (NSString *)cardCollectionID;

- (NSString *)themeCollectionID;

- (NSString *)themeTemplateID;

- (BOOL)isValidClient;

-(NSDictionary *)analyticsDictionary;

- (NSString *)authID;

- (NSURL *)omsBaseUrl;

- (NSURL *)apolloUserBaseUrl;

- (NSString *)appName;


- (BOOL)useProxyCardAPI;

- (NSString *)deeplinkScheme;

- (NSURL *)marioBaseUrl;

- (NSString *)shopHookCollectionId;

- (NSString *)shophooksCategoryCollectionID;

- (NSDictionary *)certPinningConfig;

- (BOOL)isCertPinningEnabled;

- (NSArray *)supportedLocales;

- (NSString *)supercardPublickKey;

- (NSString *)frontFaceCardViewTemplateId;

@end
