#import <Foundation/Foundation.h>
#import <ApolloUserCredentials/ZETAApolloUserCredentials.h>
#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>
#import "ApolloCardConstants.h"

@interface ApolloCardStore : NSObject

@property (nonatomic) BOOL  isFirstLaunchDone;
@property (nonatomic) double serverTimeDifference;
@property (nonatomic, readonly, nullable) NSString *lastUsedTenantToken;
@property (nonatomic, nonnull) ZETAApolloUserCredentials *apolloUserCredentials;
- (nonnull instancetype)init;
- (void)setupKVStore;

- (void) setServerTimeDifference:(double)serverTimeDifference;
- (void) setLastUsedTenantToken:(nonnull NSString *)tenantToken;

- (BOOL)AlreadyAccessedSensitiveInfo;
- (void)setAlreadyAccessedSensitiveInfo:(BOOL)alreadyAccessedSensitiveInfo;

- (nullable NSString *)lastUsedAccountHolderID;
- (void)setLastUsedAccountHolderID:(nonnull NSString *)accountHolderID;
- (void) save;
- (void) logout;

- (void)updateSession:(nonnull SdkAuthSession *)session;
-(void)saveCardPan:(nonnull NSString *)pan cardId:(nonnull NSString *)cardID;
-(nullable NSString *)getCardPanForCardId:(nonnull NSString *)cardID;

-(void)saveCardID:(nonnull NSString *)cardID accountHolderID:(nonnull NSString *)accountHolderID;
-(nullable NSString *)getCardIDForAccountHolderID:(nonnull NSString *)accountHolderID;


-(void)saveCardDetailsSyncedStatusForCard:(nonnull NSString *)cardID syncStatus:(BOOL)isCardSynced;
-(BOOL)isCardSynced:(nonnull NSString *)cardID;

-(nullable NSString *)cipherSecureStoreIdentifier;
@end
