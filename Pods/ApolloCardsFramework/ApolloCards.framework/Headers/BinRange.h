#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface BinRange: MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *bin;
@property (nonatomic, readonly) NSString *range;

@end
