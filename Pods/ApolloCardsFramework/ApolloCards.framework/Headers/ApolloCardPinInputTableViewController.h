#import <UIKit/UIKit.h>
#import "ApolloCardThemeConfig.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ApolloCardPinInputViewControllerType) {
  ApolloCardPinInputViewControllerTypeSetPin,
  ApolloCardPinInputViewControllerTypeInputPin,
  ApolloCardPinInputViewControllerTypeUpdatePin
};
@class ApolloCardPinInputTableViewController;

@protocol ApolloCardPinViewControllerDelegate <NSObject>

- (void) controller:(ApolloCardPinInputTableViewController *) viewController didEnterPin:(NSString *) pin;
- (void) controller:(ApolloCardPinInputTableViewController *) viewController didEnterOldPin:(NSString *) oldPin newPin:(NSString *)newPin;
- (void) refusedPinByController:(ApolloCardPinInputTableViewController *) viewController;

@end

@interface ApolloCardPinInputTableViewController : UIViewController

- (instancetype) initWithType:(ApolloCardPinInputViewControllerType) controllerType;
- (void) showPinError;
@property (nonatomic, weak) id<ApolloCardPinViewControllerDelegate> delegate;
@property (nonatomic, readonly) ApolloCardPinInputViewControllerType pinInputViewControllerType;
@end

NS_ASSUME_NONNULL_END
