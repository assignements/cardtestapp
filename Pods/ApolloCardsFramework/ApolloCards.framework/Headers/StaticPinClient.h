#import <Foundation/Foundation.h>
#import "BaseClient.h"
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import "ApolloAuthenticationProvider.h"
#import "StaticPinResponse.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^ApolloStaticPinBlockType)(StaticPinResponse *response);


@interface StaticPinClient : BaseClient

- (nonnull instancetype)initWithBaseURL:(nonnull NSURL *)url
                              userAgent:(nonnull NSString *)userAgent
                           authProvider:(id<ApolloAuthenticationProvider>) authProvider;

- (void)getStaticPinForIFI:(NSString *)IFI
                    cardID:(NSString *)cardID
                   headers:(NSDictionary *)headers
              successBlock:(ApolloStaticPinBlockType)successBlock
              failureBlock:(ZETAFailureBlockType)failureBlock;
@end

NS_ASSUME_NONNULL_END
