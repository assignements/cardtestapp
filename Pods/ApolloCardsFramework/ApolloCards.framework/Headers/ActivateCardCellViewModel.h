
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ApolloCardActivateCardErrorType) {
    ApolloCardActivateCardExpiryDateError
};

@protocol ActivateCardCellViewModelDelegate <NSObject>
-(void)enteredCardNumber:(NSString*)cardNumber expiryDate:(NSString*)expiryDate cvv:(NSString*)cvv;
@end

@protocol ActivateCardCellViewModelErrorDelegate <NSObject>
- (void) showErrorWithMessage:(NSString *) message type:(ApolloCardActivateCardErrorType) type;
- (void) resetErrors;
@end

@interface ActivateCardCellViewModel : NSObject
@property (nonatomic, strong) NSString *cardNumber;
@property (nonatomic, strong) NSString *expiryDate;
@property (nonatomic, strong) NSString *cvv;
@property (nonatomic, weak) id<ActivateCardCellViewModelDelegate> delegate;
@property (nonatomic, weak) id<ActivateCardCellViewModelErrorDelegate> errorDelegate;
-(void)activateCard;
-(BOOL)isCardDetailsValid;
@end

NS_ASSUME_NONNULL_END
