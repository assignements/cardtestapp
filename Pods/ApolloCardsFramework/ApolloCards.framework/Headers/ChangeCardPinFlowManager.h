#import <Foundation/Foundation.h>
#import "ApolloCardStore.h"
#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>
#import "ApolloCardErrorInfo.h"
#import "ApolloCardThemeConfig.h"
#import "ApolloSharedSecretStore.h"
#import "ApolloAuthenticationProvider.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChangeCardPinFlowManager : NSObject

-(nonnull instancetype)initWithCardStore:(nonnull ApolloCardStore *) store
                            authProvider:(nonnull id<ApolloAuthenticationProvider>) authProvider
                       sharedSecretStore:(nonnull ApolloSharedSecretStore *)sharedSecretStore;

-(void)initializeChangePinUIFlowForCardID:(NSString *)cardID
                               resourceID:(NSString *)resourceID
                                success:(nullable void (^)(void)) successBlock
                                failure:(nullable void (^)(ApolloCardErrorInfo * error)) failureBlock;

-(void)changeCardPinWithCardID:(NSString *)cardID
                    resourceID:(NSString *)resourceID
                        oldPin:(NSString *)oldPin
                        newPin:(NSString *)newPin
                       success:(nullable void (^)(void)) successBlock
                       failure:(nullable void (^)(ApolloCardErrorInfo * error)) failureBlock;
@end

NS_ASSUME_NONNULL_END
