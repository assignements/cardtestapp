#import "CardOTPGenerator.h"

@interface CardOTPGenerator (ZETAAdditions)

- (NSString *)zeta_generateOTPForMovingFactor:(NSString *)movingFactor;

- (NSString *)zeta_generateOTPForMovingFactorData:(NSData *)movingFactorData;

@end
