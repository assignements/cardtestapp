#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "ChangePinRequestPayload.h"

@interface SetPinRequestPayload: MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSDate *nonce;
@property (nonatomic, readonly) ChangePinRequestPayload *sensitivePayload;

- (instancetype)initWithSensitivePayload:(ChangePinRequestPayload *)sensitivePayload
                                   nonce:(NSDate *)nonce;

@end
