#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface SensitiveViewValue: MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *serverPublicKey;
@property (nonatomic, readonly) NSString *encryptedData;
@property (nonatomic, readonly) NSString *iv;

@end
