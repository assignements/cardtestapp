
#import <UIKit/UIKit.h>
#import "ApolloCardThemeConfig.h"
NS_ASSUME_NONNULL_BEGIN

@class ApolloCardActivateCardNoViewController;
@protocol ApolloCardActivateCardNoViewControllerDelegate <NSObject>

- (void) controller:(ApolloCardActivateCardNoViewController *) viewController didEnterCardNo:(NSString *)cardNumber expiryDate:(NSString *)expiryDate cvv:(NSString *)cvv;


@end
@interface ApolloCardActivateCardNoViewController : UIViewController
@property(nonatomic,weak)id<ApolloCardActivateCardNoViewControllerDelegate> delegate;
@end
NS_ASSUME_NONNULL_END
