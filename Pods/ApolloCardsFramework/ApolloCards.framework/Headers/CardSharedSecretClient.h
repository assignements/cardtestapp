#import "CardSharedSecretResponse.h"
#import "SdkClient.h"
#import "GetResourceByAccountHolderRequest.h"
#import "GetServerPublicKeyRequestPayload.h"
#import "SetIdentityRequest.h"
#import "SetIdentityResponse.h"
#import "BaseClient.h"
#import "ApolloAuthenticationProvider.h"

NS_ASSUME_NONNULL_BEGIN

@interface CardSharedSecretClient: BaseClient
typedef void (^SharedSecretCompletionBlock)(CardSharedSecretResponse * _Nullable response, NSError * _Nullable error);
typedef void (^ServerPKCCompletionBlock)(NSString * _Nullable serverPKC, NSError * _Nullable error);
typedef void (^SetIdentityCompletionBlock)(SetIdentityResponse * _Nullable response, NSError * _Nullable error);

- (void)getResourcesForIFI:(NSString *)IFI
                   request:(GetResourceByAccountHolderRequest *)request
                   headers:(NSDictionary *)headers
              successBlock:(ZETAResponseBlockType)successBlock
              failureBlock:(ZETAFailureBlockType)failureBlock;

- (void)generateServerPublicKey:(NSString *)IFI
                     resourceID:(NSString *)resourceID
                        request:(GetServerPublicKeyRequestPayload *)request
                        headers:(NSDictionary *)headers
                     completion:(SharedSecretCompletionBlock)completion;

- (void)generateServerPublicKeyToImplantResource:(NSString *)resourceID
                                           ifiID:(NSString *)IFI
                                         headers:(NSDictionary *)headers
                                      completion:(ServerPKCCompletionBlock)completion;
- (void)setIdentityForIFI:(NSString *)IFI
                resourceID:(NSString *)resourceID
                  request:(SetIdentityRequest *)request
                  headers:(NSDictionary *)headers
               completion:(SetIdentityCompletionBlock)completion;

@end

NS_ASSUME_NONNULL_END
