#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

static NSString *const UserAgent;

static NSString * const AUTH_EXPIRED_NOTIFICATION_CARDS_IOS = @"com.cards.ios.authExpired";
static NSString *const ErrorDomain = @"ApolloCardErrorDomain";
static NSString *const ClientIdentifier = @"ApolloCardClient";
static NSString *const CardHeaderKeyApolloAuthToken = @"Authorization";
static NSString *const CardHeaderKeyJwtToken = @"X-Zeta-JWS";


static const NSInteger unauthorisedStatusCode = 401;
typedef NS_ENUM(NSUInteger, CardManagerState) {
    CardManagerStateNotInitialized,
    CardManagerStateAuthenticated,
    CardManagerStateSecureStoreSetUp,
    CardManagerStateResourceSynced,
    CardManagerStateSharedSecretSynced
};

NS_ASSUME_NONNULL_END
