#import <Foundation/Foundation.h>
#import "ApolloCardErrorInfo.h"
#import "ApolloAuthenticationProvider.h"

NS_ASSUME_NONNULL_BEGIN

@interface ActivateCardIDFlowManager : NSObject

-(nonnull instancetype) initWithAuthProvider:(id<ApolloAuthenticationProvider>) authProvider;

-(void)initializeUIFlowForCardActivationWithDniNumber:(NSString *)dniNumber
                                              success:(nullable void (^)(void)) successBlock
                                              failure:(nullable void (^)(ApolloCardErrorInfo * error)) failureBlock;

-(void)callAPIToActivateCardWithCardId:(NSString *)cardId
                             dniNumber:(NSString *)dniNumber;

-(void)callAPIToActivateCardWithCardId:(NSString *)cardId
                             dniNumber:(NSString *)dniNumber
                          successBlock:(ZETAResponseBlockType)success
                          failureBlock:(ZETAFailureBlockType)failure;

@end

NS_ASSUME_NONNULL_END
