#import <Foundation/Foundation.h>
#import "BaseClient.h"

typedef NS_ENUM(NSUInteger, SdkClientErrorCode) {
  SdkClientErrorCodeUnknown,
  SdkClientErrorCodeFailedToSyncServerTime
};

@interface SdkClient : BaseClient

- (instancetype)initWithBaseURL:(NSURL *)url
                   andUserAgent:(NSString *)userAgent;

- (void)getServerSynchedTimeWithHeaders:(NSDictionary *)headers
                                success:(ZETAResponseBlockType)success
                                failure:(ZETAFailureBlockType)failure;

@end
