#import <Foundation/Foundation.h>
#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>
#import "ApolloAuthenticationProvider.h"
#import <ApolloZetletsCore/ApolloZetletsCoreHeaders.h>
#import <ApolloCollectionsCore/ZETAGenericCollectionManager.h>
#import <ApolloCollectionsDoor/ZETACollectionsClient.h>
NS_ASSUME_NONNULL_BEGIN

@interface ThemeAndCollectionsManager : NSObject

@property(nonatomic, weak) id<ApolloAuthenticationProvider> authSessionProvider;
@property (nonatomic) ZETAZetletCollectionsManager *zetletCollectionManager;
@property (nonatomic) ZETAGenericCollectionManager *collectionManager;
@property (nonatomic) ZETACollectionsClient *collectionClient;

- (void)syncCollections;
- (instancetype)initWithUserStore:(ZETAApolloUserStore *)userStore;

@end

NS_ASSUME_NONNULL_END
