#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

FOUNDATION_EXPORT NSString *const ActivateCardDeeplinkPath;
FOUNDATION_EXPORT NSString *const BlockCardDeeplinkPath;
FOUNDATION_EXPORT NSString *const UnblockCardDeeplinkPath;
FOUNDATION_EXPORT NSString *const HotlistCardDeeplinkPath;
FOUNDATION_EXPORT NSString *const StaticPinDeeplinkPath;
FOUNDATION_EXPORT NSString *const RequestCardReplacementDeeplinkPath;
FOUNDATION_EXPORT NSString *const SettingsPageDeeplinkPath;
FOUNDATION_EXPORT NSString *const AlertDeepLinkPath;
FOUNDATION_EXPORT NSString *const CardViewDeeplinkPath;
FOUNDATION_EXPORT NSString *const SetPinDeeplinkPath;
FOUNDATION_EXPORT NSString *const ChangePinDeeplinkPath;
FOUNDATION_EXPORT NSString *const CardListDeeplinkPath;
FOUNDATION_EXPORT NSString *const CardOrderDeeplinkPath;
FOUNDATION_EXPORT NSString *const CardLimitsDeeplinkPath;
FOUNDATION_EXPORT NSString *const RequestPhysicalCardDeeplinkPath;
FOUNDATION_EXPORT NSString *const BlockAndReplaceCardDeeplinkPath;
FOUNDATION_EXPORT NSString *const CopyCardNumberDeeplink;
FOUNDATION_EXPORT NSString *const GenericCardSdkWebViewDeeplinkPath;

FOUNDATION_EXPORT NSString *const CardListDeeplink;
FOUNDATION_EXPORT NSString *const CardSettingsPageDeeplink;

FOUNDATION_EXPORT NSString *const CardIDParam;
FOUNDATION_EXPORT NSString *const SuperPinParam;
FOUNDATION_EXPORT NSString *const CardSettingsParam;
FOUNDATION_EXPORT NSString *const CardViewStateParam;
FOUNDATION_EXPORT NSString *const CardViewTemplateIDParam;
FOUNDATION_EXPORT NSString *const CardViewAttributesParam;
FOUNDATION_EXPORT NSString *const CardListNavigationParam;
FOUNDATION_EXPORT NSString *const ActivateCardMethodParam;
FOUNDATION_EXPORT NSString *const CallbackURLParam;
FOUNDATION_EXPORT NSString *const DNINumberParam;
FOUNDATION_EXPORT NSString *const WebViewPathParam;
FOUNDATION_EXPORT NSString *const IfiIdParam;
FOUNDATION_EXPORT NSString *const ContextParam;
FOUNDATION_EXPORT NSString *const AccountHolderIdParam;
FOUNDATION_EXPORT NSString *const CardNumberParam;
FOUNDATION_EXPORT NSString *const AngelosConfigurationIdParam;
FOUNDATION_EXPORT NSString *const ThemeIdParam;

NS_ASSUME_NONNULL_END
