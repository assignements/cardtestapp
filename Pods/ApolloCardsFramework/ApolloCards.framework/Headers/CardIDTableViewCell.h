
#import "ApolloCardThemeConfig.h"
#import <UIKit/UIKit.h>
#import <CocoaNuggetsViews/ZETANumericKeyboardDelegateHandler.h>
#import "MaterialTextFields.h"
#import "ActivateCardIDCellViewModel.h"

@class ActivateCardIDCellViewModel;
NS_ASSUME_NONNULL_BEGIN

@protocol KeyboardDelegate <NSObject>
-(void)showKeyboard;
-(void)hideKeyboard;
@end

@interface CardIDTableViewCell : UITableViewCell
@property (nonatomic, strong) ApolloCardThemeConfig *themeConfig;
@property(nonatomic, weak) id<KeyboardDelegate> delegate;
@property (nonatomic, strong) ActivateCardIDCellViewModel *viewModel;
@property (nonatomic) NSString *dniNumber;
-(void)bindWithNumberPadView:(ZETANumberPadView *)numberPadView;
-(BOOL)makeFirstResponder;

@end

NS_ASSUME_NONNULL_END
