
#import <UIKit/UIKit.h>
#import "FrontFaceCardExtensionView.h"
#import <ApolloDeeplinkResolver/ZETANativeView.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ApolloCardNativeViewDelegate;

@interface ApolloCardNativeView : ZETANativeView

@property (nonatomic) id<SuperPinViewProtocol> superPinView;
@property (nonatomic) NSString *currentState;
@property (nonatomic, weak) id<ApolloCardNativeViewDelegate> delegate;

-(void)setupWithCardId:(NSString *)cardId
              cardView:(UIView *)cardView
            templateId:(NSString *)templateId
              superPin:(BOOL)showSuperPin
          cardSettings:(BOOL)showCardSettings
customSuperPinViewProvider:(nullable id<SuperPinViewProtocol>)superPinViewProvider;

-(void)addSuperPinViewWithSuperPin:(NSString *)superPin
                 validityInSeconds:(NSTimeInterval)validityInSeconds;

-(void)removeSuperPinView;

-(void)resetAllData;

@end

@protocol ApolloCardNativeViewDelegate <NSObject>

- (void)superPinDidExpireForCardNativeView:(nonnull ApolloCardNativeView *)cardWrapperView
                                templateId:(NSString *)templateId;

@end


NS_ASSUME_NONNULL_END
