
#import <Foundation/Foundation.h>
#import <ApolloDeeplinkResolver/ZETADeeplinkURLOpener.h>
#import "ApolloGetCardBlockAlias.h"
#import "ApolloCardDeeplinkConstants.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ApolloCardDeeplinkHandlerProtocol;

@interface ApolloCardDeeplinkHandler : NSObject
@property(weak,nonatomic)id<ApolloCardDeeplinkHandlerProtocol> delegate;
@end

@protocol ApolloCardDeeplinkHandlerProtocol <NSObject>
- (ZETABaseFlowManager *)initiateCardListFlowWithNavEmbedded:(BOOL)navEmbedded;
-(void)initiateActivateCardNumberFlowWithCallbackURL:(NSURL *)callbackURL;
-(void)initiateActivateCardIDFlowWithDniNumber:(NSString *)dniNumber
                                   callbackURL:(NSURL *)callbackURL;
-(void)initiateStaticCardPinFlowWithCardID:(NSString *)cardID;
-(void)initiateAlertFLowWithType:(AlertType)alertType
                          cardID:(NSString *)cardID
                     callbackURL:(NSURL *)callbackURL;
-(void)initiateHotlistCardFLowWithCardID:(NSString *)cardID;
-(void)initiateSetCardPinFlowWithCardID:(NSString *)cardID
                            callbackURL:(NSURL *)callbackURL;
-(void)initiateChangeCardPinFlowWithCardID:(NSString *)cardID
                               callbackURL:(NSURL *)callbackURL;
-(UIView *)initiateFrontFaceCardFlowWithCardID:(NSString *)cardID
                                  showSuperPin:(BOOL)showSuperPin
                              showCardSettings:(BOOL)showCardSettings
                                         state:(nullable NSString *)state
                                    templateID:(nullable NSString *)templateID
                                       payload:(nullable NSDictionary *)payload;
-(void)copyCardNumberWithCardID:(NSString *)cardID;

- (void)deeplinkHandler:(ApolloCardDeeplinkHandler *)handler
didOpenRequestNewCardWebViewWithContext:(nonnull NSDictionary *)context
            callbackUrl:(NSURL *)callbackUrl;

- (void)deeplinkHandler:(ApolloCardDeeplinkHandler *)handler
didOpenRequestPhysicalCardWebViewWithContext:(nonnull NSDictionary *)context
                 cardId:(nonnull NSString *)cardId
            callbackUrl:(NSURL *)callbackUrl;

- (void)deeplinkHandler:(ApolloCardDeeplinkHandler *)handler
didOpenTransactionLimitsWebViewWithCardId:(nonnull NSString *)cardId
            callbackUrl:(NSURL *)callbackUrl;

- (void)deeplinkHandler:(ApolloCardDeeplinkHandler *)handler
didOpenBlockAndReplaceCardWebViewWithContext:(nonnull NSDictionary *)context
                 cardId:(nonnull NSString *)cardId
            callbackUrl:(NSURL *)callbackUrl;

@end

NS_ASSUME_NONNULL_END
