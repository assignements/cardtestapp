
#import <Foundation/Foundation.h>
#import "ApolloCardErrorInfo.h"
#import "ApolloAuthenticationProvider.h"

NS_ASSUME_NONNULL_BEGIN

@interface ActivateCardNoFlowManager : NSObject

-(nonnull instancetype) initWithAuthProvider:(id<ApolloAuthenticationProvider>) authProvider;

-(void)initializeUIFlowForCardActivationNoWithSuccess:(nullable void (^)(void)) successBlock
                                              failure:(nullable void (^)(ApolloCardErrorInfo * error)) failureBlock;
-(void)callAPIToActivateCardWithCardNo:(NSString *)cardNumber
                            expiryDate:(NSString *)expiryDate
                                   cvv:(NSString *)cvv;

-(void)callAPIToActivateCardWithCardNo:(NSString *)cardNumber
                            expiryDate:(NSString *)expiryDate
                                   cvv:(NSString *)cvv
                          successBlock:(ZETAResponseBlockType)success
                          failureBlock:(ZETAFailureBlockType)failure;

@end

NS_ASSUME_NONNULL_END
