#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "SensitiveViewValue.h"

@interface SensitiveView: MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *algo;
@property (nonatomic, readonly) SensitiveViewValue *value;

@end
