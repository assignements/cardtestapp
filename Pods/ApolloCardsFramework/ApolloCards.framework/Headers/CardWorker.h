#import <Foundation/Foundation.h>
#import "ApolloGetCardBlockAlias.h"
#import "ApolloCardStore.h"
#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>
#import "ApolloCardDetail.h"
#import "CardConstants.h"
#import "CardBaseWorker.h"
#import "ApolloAuthenticationProvider.h"
#import "CardStatusReasonRequest.h"

NS_ASSUME_NONNULL_BEGIN

@protocol CardWorkerDelegate;
@interface CardWorker: CardBaseWorker
@property(nonatomic) ApolloCardStore *kvStore;
@property(nonatomic) id<ApolloCardSecuredStoreAccess> secureStore;
@property(nonatomic, weak) id<ApolloAuthenticationProvider> authProvider;
@property(nonatomic, weak) id<CardWorkerDelegate> cardWorkerDelegate;;

-(void)syncCardDetailsForCardID:(nonnull NSString *)cardID
                        success:(ApolloCardEmptyBlockType)success
                        failure:(ApolloCardFailureBlock)failure;

-(void)getSecureCardDetailsWithCardID:(NSString *)cardID
                              success:(nonnull void (^)(ApolloCardDetail *card))success
                        failure:(ApolloCardFailureBlock)failure;

-(void)getCardPanWithCardID:(NSString *)cardID
                        success:(ApolloGetCardPanSuccessBlock)successBlock
                        failure:(ApolloCardFailureBlock)failure;

-(void)getCardStatusReasonForCardID:(NSString *)cardID
                            success:(nonnull void (^)(NSString *cardStatus, CardStatusReasonRequest *statusReason))successBlock
                            failure:(ApolloCardFailureBlock)failure;

-(void)updateCard:(NSString *)cardID
       cardStatus:(CardStatus)cardStatus
      description:(nonnull NSString *)description
          success:(ApolloCardEmptyBlockType)success
          failure:(ApolloCardFailureBlock)failure;
@end

@protocol CardWorkerDelegate <NSObject>

-(void)didReciveCryptoFunctionErrorForCardWorker:(CardWorker *)cardWorker;

@end

NS_ASSUME_NONNULL_END
