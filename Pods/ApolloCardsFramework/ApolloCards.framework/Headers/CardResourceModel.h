
#import <Mantle/Mantle.h>
#import "Resource.h"

NS_ASSUME_NONNULL_BEGIN

@interface CardResourceModel : MTLModel <MTLJSONSerializing>

- (instancetype)initWithCardID:(NSString *)cardID
                      resource:(Resource *)resource;

@property (nonatomic, readonly) NSString *cardID;
@property (nonatomic, readonly) Resource *resource;

@end

NS_ASSUME_NONNULL_END
