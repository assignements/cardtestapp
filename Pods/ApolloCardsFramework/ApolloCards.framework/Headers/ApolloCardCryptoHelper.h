#import <Foundation/Foundation.h>
#import <ApolloCoreCrypto/ZETAECKeyPair.h>
#import "CardSharedSecretResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApolloCardCryptoHelper : NSObject
+ (NSString *) generateRootCert;
+ (NSString *) generateRootCertWithCert:(NSString *) encodedCert publicKey:(ZETAPublicKey *) publicKey privateKey: (ZETAErasablePrivateKey *) privateKey;


+ (NSData *) encryptValue:(NSData *)sharedSecretData serverPKC:(NSString *)serverPKC privateKey:(ZETAErasablePrivateKey *)privateKey;

+ (NSString *) encryptIdentityValue:(NSData *)sharedSecretData serverPKC:(NSString *)serverPKC privateKey:(ZETAErasablePrivateKey *)privateKey;

+ (NSData *) decryptSecretFromCardSharedSecretResponse: (CardSharedSecretResponse *) response privateKey: (ZETAErasablePrivateKey *) privateKey;

+ (NSData *) decryptSecretFromEncodedSharedSecret: (NSString *) secret encodedPeerKey:(NSString *) peerkey encodedPrivateKey: (NSString *) privateKey;
+ (NSString *)subjectFromCertificateData:(NSString *)base64EncodedCertificatePEM;
+ (NSString *) subjectCNFromCertificate:(NSString *)base64EncodedCertificatePEM;

+(NSString *)getSubjectIdentifier:(NSString *)serverPKC;

+ (NSString *) enryptSecretFromEncodedSharedSecretData:(NSData *)sharedSecret publicPeerKey:(ZETAPublicKey *)peerkey privateKey:(ZETAErasablePrivateKey *)privateKey;

+ (NSString *) getInputJIDFromServerPKC:(NSData *)acsServerPublicCert;

+ (NSString *) MD5String:(NSString *)stringToEncrypt;
@end

NS_ASSUME_NONNULL_END
