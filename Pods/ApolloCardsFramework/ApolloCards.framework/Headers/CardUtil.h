#import <Foundation/Foundation.h>
#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>
#import <ApolloCoreCrypto/ZETACryptoFunctions.h>

@interface CardUtil : NSObject

+ (void)getHeaderValueForCardClientFromCardID:(NSString *)cardID
                                      success:(void (^)(NSDictionary *headers,ZETAECKeyPair *keyPair))success
                                      failure:(void (^)(NSError *error))failure;

+ (NSData *)getDecryptedCardFromServerPKC:(NSString *)serverPKC
                                       iv:(NSString *)iv
                            encryptedData:(NSString *) encryptedData
                         ephemeralKeyPair:(ZETAECKeyPair *)keyPair;

+ (NSData *)decryptedDataUsingSecret:(NSData *)sharedSecret
                                  data:(NSData *)encryptedData
                                    IV:(NSData *)IV;

+ (NSData *)encryptedDataUsingSecret:(NSData *)sharedSecret
                                data:(NSData *)decryptedData
                                  IV:(NSData *)IV;
@end
