#import <Mantle/Mantle.h>
#import <ApolloCommonsAgentInfo/ApolloAgentInfo.h>

@interface CardUserAgentInfo : MTLModel<MTLJSONSerializing>

- (instancetype)initWithAgentInfo:(TDTAgentInfo *)agentInfo;

@property (nonatomic, readonly) NSString *deviceType;
@property (nonatomic, readonly) NSString *os;
@property (nonatomic, readonly) NSString *appVersion;
@property (nonatomic, readonly) NSString *OSVersion;
@property (nonatomic, readonly) NSString *buildNumber;
@property (nonatomic, readonly) NSString *deviceModel;
@property (nonatomic, readonly) NSString *carrier;
@property (nonatomic, readonly) NSString *networkType;
@property (nonatomic, readonly) NSString *locale;
@property (nonatomic, readonly) NSString *deviceManufacturer;
@property (nonatomic, readonly) NSString *appID;
@property (nonatomic, readonly) NSString *appType;
@property (nonatomic, readonly) NSDictionary *uaInfo;
@end
