#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProxyCardRequest: MTLModel<MTLJSONSerializing>
@property(nonatomic, nonnull) NSString *vectorType;
@property(nonatomic, nullable) NSString *vectorID;

@end

NS_ASSUME_NONNULL_END
