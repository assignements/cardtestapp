#import "ApolloSharedSecretStore.h"
#import <ApolloCoreCrypto/ZETAECSigningAuthority.h>
#import <ApolloUserCredentials/ZETAApolloUserCredentials.h>
#import "CardBaseWorker.h"
#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>
#import "ApolloCardErrorInfo.h"
#import "ApolloAuthenticationProvider.h"
#import "ApolloCardStore.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^TOTPFetchBlock)(NSString * _Nullable superPin, NSTimeInterval validityInSeconds, ApolloCardErrorInfo * _Nullable error);

@interface CardSharedSecretWorker : CardBaseWorker
- (instancetype)initWithStore:(ApolloSharedSecretStore *)store
                      kvStore:(ApolloCardStore *)kvStore
                  authSessionProvider:(id<ApolloAuthenticationProvider>)authProvider
              userCredentials:(ZETAApolloUserCredentials *)apolloUserCredentials;

-(void)generateServerPublicKeyForResource:(NSString *)resourceID
                                  success:(nullable ZETAEmptyBlockType) successBlock
                                  failure:(nullable void (^)(NSError *error)) failureBlock;

- (void) generateTOTPWithSession:(nonnull SdkAuthSession *)authSession
                     forResource:(NSString *)resourceID
            serverTimeDifference:(double)serverTimeDifference
                      completion: (nonnull TOTPFetchBlock) completion;

@end

NS_ASSUME_NONNULL_END
