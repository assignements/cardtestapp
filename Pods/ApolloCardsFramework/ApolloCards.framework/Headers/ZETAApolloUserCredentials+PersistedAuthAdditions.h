//


#import <ApolloUserCredentials/ZETAApolloUserCredentials.h>
#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZETAApolloUserCredentials (PersistedAuthAdditions)
- (void) updateWithAuthSession: (SdkAuthSession *)session;
@end

NS_ASSUME_NONNULL_END
