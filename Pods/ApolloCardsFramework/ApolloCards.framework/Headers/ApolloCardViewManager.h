#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ApolloCardDetail.h"
#import "ApolloCardErrorInfo.h"
#import "ApolloAuthenticationProvider.h"
#import "ApolloGetCardBlockAlias.h"

static NSString * _Nonnull const CARD_EXPANDED_STATE = @"expanded";
static NSString * _Nonnull const CARD_COLLAPSED_STATE = @"collapsed"; 

NS_ASSUME_NONNULL_BEGIN

@protocol CardDataProvider <NSObject>

-(void)getCardUnRestrictedDataWithCardID:(nonnull NSString *)cardID success:(nonnull void (^)(NSString *cardPan))successBlock failure:(nonnull void (^)(ApolloCardErrorInfo *error)) failureBlock;

-(void)getCardRestrictedDataWithCardID:(nonnull NSString *)cardID success:(nonnull void (^)(ApolloCardDetail *card))successBlock failure:(nonnull void (^)(ApolloCardErrorInfo *error)) failureBlock;

@end

@protocol CardZetletStateListener <NSObject>
- (void)cardViewDidMakeTransition:(UIView *)cardView
                           toState:(NSString *)toState
                            cardId:(NSString *)cardId
                        templateId:(NSString *)templateId;

- (void)resetCardToDefaultStateWithTemplateId:(NSString *)templateId;
@end

@interface ApolloCardViewManager : NSObject
@property(nonatomic, weak) id<ApolloAuthenticationProvider> authProvider;
@property(nonatomic) NSString *cardID;
@property(nonatomic, weak) id<CardDataProvider> cardDataProvider;
@property(nonatomic, weak) id<CardZetletStateListener> cardZetletStatusChangeListener;

-(void)fetchCardUIForCardInfo:(nonnull NSString *)cardPan
                      payload:(nullable NSDictionary *)payload
                 defaultState:(nullable NSString *)defaultState
                   templateID:(nullable NSString *)templateID
                      success:(nonnull void (^)(UIView * _Nonnull cardView))successBlock
                      failure:(nonnull void (^)(ApolloCardErrorInfo * _Nonnull error)) failureBlock;
@end

NS_ASSUME_NONNULL_END
