#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (HexColor)
+ (instancetype) ACS_colorWithHexString:(NSString *)hexColor;
@end

NS_ASSUME_NONNULL_END
