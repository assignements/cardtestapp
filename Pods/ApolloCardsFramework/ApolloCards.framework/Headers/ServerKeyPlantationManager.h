#import <UIKit/UIKit.h>
#import "CardBaseWorker.h"
#import <ApolloUserCredentials/ZETAApolloUserCredentials.h>
#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>
#import "ApolloCardErrorInfo.h"
#import "ApolloAuthenticationProvider.h"

NS_ASSUME_NONNULL_BEGIN

@interface ServerKeyPlantationManager : CardBaseWorker
- (instancetype)initWithStore:(ApolloSharedSecretStore *)store
            authTokenProvider:(id<ApolloAuthenticationProvider>) authProvider
              userCredentials:(ZETAApolloUserCredentials *)apolloUserCredentials;

-(void)implantServerPKCForResource:(NSString *)resourceID
                      sharedSecret:(NSData *)sharedSecret
                        publicCert:(NSData *)publicCert
                           success:(nullable ZETAEmptyBlockType) successBlock
                           failure:(nullable void (^)(ApolloCardErrorInfo *error)) failureBlock;
@end

NS_ASSUME_NONNULL_END
