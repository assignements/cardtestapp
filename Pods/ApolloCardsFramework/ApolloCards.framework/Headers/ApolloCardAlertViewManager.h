#import <Foundation/Foundation.h>
#import "ApolloCardAlertView.h"
#import "ApolloGetCardBlockAlias.h"
NS_ASSUME_NONNULL_BEGIN

@protocol ApolloCardAlertViewManagerProtocol;

@interface ApolloCardAlertViewManager : NSObject

-(instancetype)initWithAlertType:(AlertType)alertType;

-(void)generateAlertViewWithCardID:(NSString *)cardID
                       description:(NSString *)description
                           success:(ApolloCardEmptyBlockType)success
                           failure:(ApolloCardFailureBlock)failure;

@property (weak,nonatomic) id<ApolloCardAlertViewManagerProtocol> delegate;

@end

@protocol ApolloCardAlertViewManagerProtocol <NSObject>

- (void)alertConfirmedwithType:(AlertType)alertType
                    withCardID:(NSString *)cardID
               withDescription:(NSString *)description
                       success:(ApolloCardEmptyBlockType)success
                       failure:(ApolloCardFailureBlock)failure;

@end
NS_ASSUME_NONNULL_END
