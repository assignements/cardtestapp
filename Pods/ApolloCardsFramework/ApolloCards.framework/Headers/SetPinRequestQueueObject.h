#import <Foundation/Foundation.h>
#import "ApolloGetCardBlockAlias.h"

@interface SetPinRequestQueueObject : NSObject

@property (nonatomic, readonly) NSString *cardID;
@property (nonatomic, readonly) NSNumber *ifi;
@property (nonatomic, readonly) NSString *pin;
@property (nonatomic, readonly) ApolloSetPinSuccessBlock successBlock;
@property (nonatomic, readonly) ApolloCardFailureBlock failureBlock;

- (instancetype)initWithCardID:(NSString *)cardID
                           ifi:(NSString *)ifi
                           pin:(NSString *)pin
                  successBlock:(ApolloSetPinSuccessBlock)successBlock
                  failureBlock:(ApolloCardFailureBlock)failureBlock;

@end
