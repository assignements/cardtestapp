#import <Foundation/Foundation.h>
#import "ApolloCardErrorInfo.h"
#import "ApolloAuthenticationProvider.h"
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import "ApolloGetCardBlockAlias.h"
#import "ApolloCardStore.h"
#import "CardConstants.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProxyCardManager : NSObject
-(nonnull instancetype) initWithAuthProvider:(id<ApolloAuthenticationProvider>) authProvider
                                   kvStore:(ApolloCardStore *)kvStore;

-(void)syncCardNumberWithAccountHolderID:(NSString *)accountHolderID
                                 success:(ApolloCardEmptyBlockType)success
                                 failure:(ApolloCardFailureBlock)failure;

-(void)getCardPanFromCardID:(NSString *)cardID
            accountHolderID:(NSString *)accountHolderID
                    success:(ZETAStringBlockType)success
                    failure:(ApolloCardFailureBlock)failure;

-(void)getCardIDForAccountHolderID:(NSString *)accountHolderID
                           success:(ZETAStringBlockType)success
                           failure:(ApolloCardFailureBlock)failure;

-(void)updateCard:(NSString *)cardID
       cardStatus:(CardStatus)cardStatus
      description:(nonnull NSString *)description
          success:(ApolloCardEmptyBlockType)success
          failure:(ApolloCardFailureBlock)failure;

@end

NS_ASSUME_NONNULL_END
