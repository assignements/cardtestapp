#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "StaticPinResponsePayload.h"

NS_ASSUME_NONNULL_BEGIN

@interface StaticPinResponse: MTLModel<MTLJSONSerializing>
@property (nonatomic, readonly) NSString *algo;
@property (nonatomic, readonly) StaticPinResponsePayload *value;

@end

NS_ASSUME_NONNULL_END
