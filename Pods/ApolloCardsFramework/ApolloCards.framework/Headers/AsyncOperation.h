#import <Foundation/Foundation.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

NS_ASSUME_NONNULL_BEGIN

@interface AsyncOperation : NSOperation

-(void)startWithBlock:(ZETAEmptyBlockType)block;
+(void)finishOperation:(AsyncOperation *)operation;

@end

NS_ASSUME_NONNULL_END
