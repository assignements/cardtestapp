//
//  ApolloCards_Framework.h
//  ApolloCards_Framework
//
//  Created by Zeta on 08/02/20.
//  Copyright © 2020 Zeta. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for ApolloCards_Framework.
FOUNDATION_EXPORT double ApolloCards_FrameworkVersionNumber;

//! Project version string for ApolloCards_Framework.
FOUNDATION_EXPORT const unsigned char ApolloCards_FrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <apollo-cards-ios/PublicHeader.h>

#import <ApolloCards/ApolloCardManager.h>
#import <ApolloCards/ApolloGetCardBlockAlias.h>
#import <ApolloCards/ApolloCardErrorInfo.h>
#import <ApolloCards/HotlistCardViewController.h>
