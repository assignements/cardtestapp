#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface GetServerSynchedTimeResponse: MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSDate *serverSynchedTime;

@end
