#import <Mantle/Mantle.h>
#import <Foundation/Foundation.h>
#import "SetIdentityMetadataResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface SetIdentityResponse : MTLModel<MTLJSONSerializing>
@property(nonatomic, nonnull) NSString *identityType;
@property(nonatomic, nonnull) NSString *identityValue;
@property(nonatomic, nonnull) SetIdentityMetadataResponse *metadata;
@end

NS_ASSUME_NONNULL_END
