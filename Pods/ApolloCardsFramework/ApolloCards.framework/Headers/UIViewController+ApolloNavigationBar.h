
#import <UIKit/UIKit.h>
#import "ApolloCardThemeConfig.h"
NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (ApolloNavigationBar)
-(void)setUpNavigationBarWithTheme:(ApolloCardThemeConfig *)themeConfig withTitle:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
