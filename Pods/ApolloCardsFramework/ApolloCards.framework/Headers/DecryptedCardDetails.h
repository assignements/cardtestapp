#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface DecryptedCardDetails : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *cardID;
@property (nonatomic, readonly) NSString *createdAt;
@property (nonatomic, readonly) NSString *cvv;
@property (nonatomic, readonly) NSString *expiry;
@property (nonatomic, readonly) BOOL isVirtual;
@property (nonatomic, readonly) NSString *pan;

@end
