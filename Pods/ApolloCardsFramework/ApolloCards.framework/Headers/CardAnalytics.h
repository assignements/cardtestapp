//
#import "CardAnalyticsManager.h"

#ifndef CardAnalytics_h
#define CardAnalytics_h

#define ACSAnalyticsManager [CardAnalyticsManager defaultManager]

static NSString *const APL_Register_SDK = @"APL_Register_SDK";
static NSString *const APL_Init_SDK = @"APL_Init_SDK";
static NSString *const APL_Get_Card_Request = @"APL_Get_Card_Request";
static NSString *const APL_Get_Card_Success = @"APL_Get_Card_Success";
static NSString *const APL_Get_Card_Failed = @"APL_Get_Card_Failed";
static NSString *const APL_Cards_Api_Triggered = @"APL_Cards_Api_Triggered";
static NSString *const APL_Cards_Api_Success = @"APL_Cards_Api_Success";
static NSString *const APL_Cards_Api_Failure = @"APL_Cards_Api_Failure";
static NSString *const APL_Set_Pin_Start = @"APL_Set_Pin_Start";
static NSString *const APL_Set_Pin_Enter_Pin_Start = @"APL_Set_Pin_Enter_Pin_Start";
static NSString *const APL_Set_Pin_Enter_Pin_Done = @"APL_Set_Pin_Enter_Pin_Done";
static NSString *const APL_Set_Pin_Confirm_Pin_Start = @"APL_Set_Pin_Confirm_Pin_Start";
static NSString *const APL_Set_Pin_Confirm_Pin_Done = @"APL_Set_Pin_Confirm_Pin_Done";
static NSString *const APL_Set_Pin_Success = @"APL_Set_Pin_Success";
static NSString *const APL_Set_Pin_Failed = @"APL_Set_Pin_Failed";
static NSString *const APL_Super_Pin_Requested = @"APL_Super_Pin_Requested";
static NSString *const APL_Super_Pin_Success = @"APL_Super_Pin_Success";
static NSString *const APL_Super_Pin_Failed = @"APL_Super_Pin_Failed";
static NSString *const APL_SDK_Logout = @"APL_SDK_Logout";

static NSString *const APL_Get_Resource_By_ACH_Triggered = @"APL_Get_Resource_By_ACH_Triggered";
static NSString *const APL_Get_Resource_By_ACH_Success = @"APL_Get_Resource_By_ACH_Success";
static NSString *const APL_Get_Resource_By_ACH_Failed = @"APL_Get_Resource_By_ACH_Failed";
static NSString *const APL_Get_Identity_By_Resource_Triggered = @"APL_Get_Identity_By_Resource_Triggered";
static NSString *const APL_Get_Identity_By_Resource_Success = @"APL_Get_Identity_By_Resource_Success";
static NSString *const APL_Get_Identity_By_Resource_Failed = @"APL_Get_Identity_By_Resource_Failed";
static NSString *const APL_TOTP_Setup_Start = @"APL_TOTP_Setup_Start";
static NSString *const APL_TOTP_Setup_Success = @"APL_TOTP_Setup_Success";
static NSString *const APL_TOTP_Setup_Failed = @"APL_TOTP_Setup_Failed";

static NSString *const APL_GetServer_PKC_Triggered = @"APL_GetServer_PKC_Triggered";
static NSString *const APL_GetServer_PKC_Success = @"APL_GetServer_PKC_Success";
static NSString *const APL_GetServer_PKC_Failed = @"APL_GetServer_PKC_Failed";
static NSString *const APL_Set_Identity_By_Resource_Started = @"APL_Set_Identity_By_Resource_Started";
static NSString *const APL_Set_Identity_By_Resource_Success = @"APL_Set_Identity_By_Resource_Success";
static NSString *const APL_Set_Identity_By_Resource_Failed = @"APL_Set_Identity_By_Resource_Failed";
static NSString *const APL_Plant_Cipher_Secret_Triggered = @"APL_Plant_Cipher_Secret_Triggered";
static NSString *const APL_Plant_Cipher_Secret_Success = @"APL_Plant_Cipher_Secret_Success";
static NSString *const APL_Plant_Cipher_Secret_Failed = @"APL_Plant_Cipher_Secret_Failed";

static NSString *const APL_Set_Static_Pin_Requested = @"APL_Set_Static_Pin_Requested";
static NSString *const APL_Set_Static_Pin_Success = @"APL_Set_Static_Pin_Success";
static NSString *const APL_Set_Static_Pin_Failed = @"APL_Set_Static_Pin_Failed";

static NSString *const APL_Set_Pin_Api_Triggered = @"APL_Set_Pin_Api_Triggered";
static NSString *const APL_Set_Pin_Api_Success = @"APL_Set_Pin_Api_Success";
static NSString *const APL_Set_Pin_Api_Failure = @"APL_Set_Pin_Api_Failure";

static NSString *const APL_Change_Pin_Api_Triggered = @"APL_Change_Pin_Api_Triggered";
static NSString *const APL_Change_Pin_Api_Success = @"APL_Change_Pin_Api_Success";
static NSString *const APL_Change_Pin_Api_Failure = @"APL_Change_Pin_Api_Failure";

static NSString *const APL_Activate_Card_ID_Api_Triggered = @"APL_Activate_Card_ID_Api_Triggered";
static NSString *const APL_Activate_Card_ID_Api_Success = @"APL_Activate_Card_ID_Api_Success";
static NSString *const APL_Activate_Card_ID_Api_Failure = @"APL_Activate_Card_ID_Api_Failure";

static NSString *const APL_Activate_Card_Api_Triggered = @"APL_Activate_Card_Api_Triggered";
static NSString *const APL_Activate_Card_Api_Success = @"APL_Activate_Card_Api_Success";
static NSString *const APL_Activate_Card_Api_Failure = @"APL_Activate_Card_Api_Failure";

static NSString *const APL_Get_Card_Api_Triggered = @"APL_Get_Card_Api_Triggered";
static NSString *const APL_Get_Card_Api_Success = @"APL_Get_Card_Api_Success";
static NSString *const APL_Get_Card_Api_Failure = @"APL_Get_Card_Api_Failure";

static NSString *const EVENT_REGISTER_TRIGGER = @"AcsRegisterTrigger";
static NSString *const EVENT_REGISTER_SUCCESS = @"AcsRegisterSuccess";
static NSString *const EVENT_REGISTER_FAILURE = @"AcsRegisterFailure";

static NSString *const EVENT_SHARED_SECRET_TRIGGER = @"AcsSharedSecretTrigger";
static NSString *const EVENT_SHARED_SECRET_SUCCESS = @"AcsSharedSecretSuccess";
static NSString *const EVENT_SHARED_SECRET_FAILURE = @"AcsSharedSecretFailure";

static NSString *const EVENT_LOGOUT = @"AcsLogout";

static NSString *const EVENT_SET_PUSK_TOKEN_FAIL = @"AcsSetPushTokenFail";

static NSString *const EVENT_SYNC_TIME_FAIL = @"AcsSyncTimeFail";

static NSString *const APL_GetFISPin_Triggered = @"APL_GetFISPin_Triggered";
static NSString *const APL_GetFISPin_Success = @"APL_GetFISPin_Success";
static NSString *const APL_GetFISPin_Failed = @"APL_GetFISPin_Failed";


static NSString *const RequestedTime = @"reqTime";
static NSString *const CARDID = @"cardID";

#endif /* CardAnalytics_h */
