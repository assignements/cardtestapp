#import <UIKit/UIKit.h>
#import "ApolloCardThemeConfig.h"
#import "ApolloGetCardBlockAlias.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^DismissBlockType)(void);


@protocol ZETACardAlertViewProtocol;

@interface ApolloCardAlertView : UIView

+(ApolloCardAlertView *) showConfirmationViewOnView:(UIView *)parentView
                                          alertType:(AlertType)alertType
                                             cardID:(NSString *)cardID
                                        description:(NSString *)description
                                              theme:(ApolloCardThemeConfig *)theme
                                              title:(NSString *)title
                                            message:(NSString *)message
                            confirmationButtonTitle:(NSString *)confirmButtonTitle
                                  cancelButtonTitle:(NSString *)cancelButtonTitle
                                      dismissAction:(nullable DismissBlockType) dismissBlock;

@property (nonatomic, weak) id<ZETACardAlertViewProtocol> delegate;

@end

@protocol ZETACardAlertViewProtocol <NSObject>

- (void)confirmationViewDidConfirm:(ApolloCardAlertView *)alertView
                          withType:(AlertType)alertType
                        withCardID:(NSString *)cardID
                   withDescription:(NSString *)description;

@end

NS_ASSUME_NONNULL_END
