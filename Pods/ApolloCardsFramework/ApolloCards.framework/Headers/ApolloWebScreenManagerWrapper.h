#import <Foundation/Foundation.h>

#import <ApolloWebScreen/ApolloWebScreenManager.h>
#import <ApolloWebScreen/ApolloWebScreenConfig.h>
#import "ApolloPluginFactoryGenerator.h"

@protocol ApolloWebScreenManagerPluginProtocol;

NS_ASSUME_NONNULL_BEGIN

@interface ApolloWebScreenManagerWrapper : NSObject

- (instancetype)initWithApolloWebScreenConfig:(ApolloWebScreenConfig *)config;

- (void)setUpWebManager;

+ (NSArray *)getPrioritizedTemplateCollectionIDs;

+ (NSString *)getShopHookCollectionId;

+ (NSString *)getShophooksCategoryCollectionID;

- (void)addWebScreenManagerPluginDelegate:(id<ApolloWebScreenManagerPluginProtocol>)pluginDelegate;

@end

NS_ASSUME_NONNULL_END
