#import <Mantle/Mantle.h>
#import "SharedSecretMetadata.h"

NS_ASSUME_NONNULL_BEGIN

@interface CardSharedSecretResponse : MTLModel<MTLJSONSerializing>
@property (nonatomic, readonly) NSString *identityType;
@property (nonatomic, readonly) NSString *identityValue;
@property (nonatomic, readonly) SharedSecretMetadata *metadata;
@end

NS_ASSUME_NONNULL_END
