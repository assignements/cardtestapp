
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloOTPAuthBarClock : UIView

- (id)initWithFrame:(CGRect)frame period:(NSTimeInterval)period;

- (id)initWithFrame:(CGRect)frame
             period:(NSTimeInterval)period
      referenceDate:(NSDate *)referenceDate;

- (void)startUpTimer;
- (void)invalidate;
- (void)redrawTimerWithReferenceDate:(NSDate *)referenceDate;
- (void)redrawTimerWithPeriod:(NSTimeInterval)period referenceDate:(NSDate *)referenceDate;

@end

NS_ASSUME_NONNULL_END
