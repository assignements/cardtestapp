#import <Mantle/Mantle.h>

@interface ResourceProduct : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSNumber *ifi;
@property (nonatomic, readonly) NSString *resourceProductID;
@property (nonatomic, readonly) NSString *code;
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *desc;
@property (nonatomic, readonly) NSArray *formFactorProducts;
@property (nonatomic, readonly) NSDictionary *policies;
@property (nonatomic, readonly) NSArray *tags;
@property (nonatomic, readonly) NSString *status;
@property (nonatomic, readonly) NSDate *createdAt;
@property (nonatomic, readonly) NSDate *modifiedAt;

@end
