
#import <UIKit/UIKit.h>
#import "ApolloGetCardBlockAlias.h"

NS_ASSUME_NONNULL_BEGIN

@protocol HotlistCardViewControllerProtocol;

@interface HotlistCardViewController : UIViewController

-(instancetype)initWithCardId:(NSString *)cardId
                 successBlock:(nullable ApolloCardEmptyBlockType)success
                 failureBlock:(nullable ApolloCardFailureBlock)failure;

@property (weak,nonatomic) id<HotlistCardViewControllerProtocol> delegate;

@end

@protocol HotlistCardViewControllerProtocol <NSObject>

- (void)hotlistCardWithCardId:(NSString *)cardId
                       reason:(NSString *)reason
                 successBlock:(nullable ApolloCardEmptyBlockType)success
                 failureBlock:(nullable ApolloCardFailureBlock)failure;

@end
NS_ASSUME_NONNULL_END
