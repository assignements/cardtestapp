
#import <ApolloDeeplinkResolver/ZETABaseFlowManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface CardListFlowManager : ZETABaseFlowManager <ZETAFlowManagerProtocol>
-(instancetype)initWithViewController:(UIViewController *)vcToDisplay
                          navEmbedded:(BOOL)navEmbedded;
@end

NS_ASSUME_NONNULL_END
