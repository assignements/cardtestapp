#import <Foundation/Foundation.h>

@interface ApolloDecryptedCardDetails : NSObject

@property (nonatomic, readonly) NSString *cardID;
@property (nonatomic, readonly) NSString *createdAt;
@property (nonatomic, readonly) NSString *cvv;
@property (nonatomic, readonly) NSString *expiry;
@property (nonatomic, readonly) BOOL isVirtual;
@property (nonatomic, readonly) NSString *pan;

- (instancetype)initWithCardID:(NSString *)cardID
                     createdAt:(NSString *)createdAt
                           cvv:(NSString *)cvv
                        expiry:(NSString *)expiry
                     isVirtual:(BOOL)isVirtual
                           pan:(NSString *)pan;

@end
