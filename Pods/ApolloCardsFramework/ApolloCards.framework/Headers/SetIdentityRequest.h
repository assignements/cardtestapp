#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "SetIdentityRequestMetadata.h"

NS_ASSUME_NONNULL_BEGIN

@interface SetIdentityRequest : MTLModel<MTLJSONSerializing>
@property (nonatomic, strong, nonnull) NSString *identityValue;
@property (nonatomic, strong, nonnull) NSString *identityType;
@property (nonatomic, strong, nonnull) SetIdentityRequestMetadata *metadata;
@end

NS_ASSUME_NONNULL_END
