#import <Foundation/Foundation.h>
#import "ApolloCardStore.h"
#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>
#import "ApolloCardErrorInfo.h"
#import "ApolloCardThemeConfig.h"
#import "ApolloSharedSecretStore.h"
#import "ApolloAuthenticationProvider.h"
#import "ApolloAuthenticationProvider.h"
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

NS_ASSUME_NONNULL_BEGIN

@interface SetCardPinFlowManager : NSObject

-(nonnull instancetype)initWithCardStore:(nonnull ApolloCardStore *) store
                            authProvider:(nonnull id<ApolloAuthenticationProvider>) authProvider;

-(void)initializeUIFlowForCard:(NSString *)cardID
                     success:(nullable ZETAEmptyBlockType) successBlock
                     failure:(nullable void (^)(ApolloCardErrorInfo * error)) failureBlock;

-(void)setCardPin:(NSString *)cardPin
        forCardID:(NSString *)cardID
          success:(nullable ZETAEmptyBlockType) successBlock
          failure:(nullable void (^)(ApolloCardErrorInfo * error)) failureBlock;

@end

NS_ASSUME_NONNULL_END
