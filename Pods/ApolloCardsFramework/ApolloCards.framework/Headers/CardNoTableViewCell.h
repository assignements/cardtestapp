
#import "ApolloCardThemeConfig.h"
#import <UIKit/UIKit.h>
#import <CocoaNuggetsViews/ZETANumericKeyboardDelegateHandler.h>
#import "MaterialTextFields.h"
@class ActivateCardCellViewModel;
NS_ASSUME_NONNULL_BEGIN

@protocol KeyboardDelegate <NSObject>
-(void)showKeyboard;
-(void)hideKeyboard;
@end

@interface CardNoTableViewCell : UITableViewCell
@property (nonatomic, strong) ApolloCardThemeConfig *themeConfig;
@property(nonatomic, weak) id<KeyboardDelegate> delegate;
@property (nonatomic, strong) ActivateCardCellViewModel *viewModel;
-(void)bindWithNumberPadView:(ZETANumberPadView *)numberPadView;
-(BOOL)makeFirstResponder;

@end

NS_ASSUME_NONNULL_END
