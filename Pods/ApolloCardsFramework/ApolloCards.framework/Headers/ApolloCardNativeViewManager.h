
#import <Foundation/Foundation.h>
#import "ApolloCardErrorInfo.h"
#import "SuperPinViewProtocol.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^TOTPFetchBlock)(NSString *_Nullable superPin, NSTimeInterval validityInSeconds, ApolloCardErrorInfo *_Nullable error);

@protocol NativeCardViewDataProvider;

@interface ApolloCardNativeViewManager : NSObject

- (instancetype)initWithDataProvider:(id<NativeCardViewDataProvider>)dataProvider;

@property (weak, nonatomic, nullable) id<SuperPinViewProtocol> superPinViewProvider;

- (UIView *)getFrontFaceCardWithCardID:(nonnull NSString *)cardID
                          showSuperPin:(BOOL)showSuperPin
                      showCardSettings:(BOOL)showCardSettings
                                 state:(nullable NSString *)state
                            templateID:(nonnull)templateID
                               payload:(nullable NSDictionary *)payload;

- (void)collapseFrontFaceCardWithTemplateID:(NSString *)templateID;

- (void)collapseFrontFaceCardWithNativeActionWithTemplateID:(NSString *)templateID;

- (void)expandFrontFaceCardWithTemplateID:(NSString *)templateID;

- (void)resetAllData;

@end

@protocol NativeCardViewDataProvider <NSObject>

- (void)getCardViewWithCardID:(nonnull NSString *)cardID
                      payload:(nullable NSDictionary *)payload
                 defaultState:(nonnull NSString *)defaultState
                   templateID:(nonnull NSString *)templateID
                      success:(nonnull void (^)(UIView *_Nullable cardView))successBlock
                      failure:(nonnull void (^)(ApolloCardErrorInfo *_Nonnull error))failureBlock;

- (void)getSuperPinWithCompletion:(TOTPFetchBlock)completion;

@end

NS_ASSUME_NONNULL_END

