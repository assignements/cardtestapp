
#import "BaseClient.h"
#import "ActivateCardRequestPayload.h"

NS_ASSUME_NONNULL_BEGIN

@interface ActivateCardNumberClient : BaseClient

- (nonnull instancetype)initWithBaseURL:(nonnull NSURL *)url
                              userAgent:(nonnull NSString *)userAgent
                           authProvider:(id<ApolloAuthenticationProvider>) authProvider;

- (void)activateCardWithIfi:(nonnull NSString *)ifi
             requestPayload:(ActivateCardRequestPayload *)payload
                    success:(nullable ZETAResponseBlockType)success
                    failure:(nullable ZETAFailureBlockType)failure;

@end

NS_ASSUME_NONNULL_END
