#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CertPinningWrapper : NSObject

- (instancetype)initWithCertPinningEnabled:(BOOL)isCertPinningEnabled
                              forDebugMode:(BOOL)isDebugMode;

- (void)resetSecurityProvider;

@end

NS_ASSUME_NONNULL_END
