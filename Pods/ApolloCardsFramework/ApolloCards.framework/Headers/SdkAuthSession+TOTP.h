#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>

NS_ASSUME_NONNULL_BEGIN

@interface SdkAuthSession (TOTP)
- (NSString *)movingFactorWithUserInfo: (NSString *)userInfo;
- (NSString *)movingFactorForDate:(NSDate *)date userInfo:(NSString *)userInfo;
@end

NS_ASSUME_NONNULL_END
