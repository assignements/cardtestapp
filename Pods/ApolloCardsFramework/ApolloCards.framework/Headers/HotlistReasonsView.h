#import <UIKit/UIKit.h>
#import "ApolloCardThemeConfig.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^DismissBlockType)(void);

@protocol HotlistReasonsViewProtocol;

@interface HotlistReasonsView : UIView

+ (HotlistReasonsView *)showHotlistReasonsViewOnView:(UIView *)parentView;

@property (nonatomic, weak) id<HotlistReasonsViewProtocol> delegate;

@end

@protocol HotlistReasonsViewProtocol <NSObject>

- (void)hotlistReasonSelected:(NSString *)reason;

@end

NS_ASSUME_NONNULL_END
