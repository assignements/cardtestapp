#import <Foundation/Foundation.h>

#ifndef ApolloCardSecureStore_h
#define ApolloCardSecureStore_h

typedef NS_ENUM(NSUInteger, ApolloCardSecureStoreType) {
    ApolloCardSecureStoreTypeUnknown,
    ApolloCardSecureStoreTypePIN,
    ApolloCardSecureStoreTypeDeviceLock,
    ApolloCardSecureStoreTypeNone
};

@protocol ApolloCardSecuredStoreAccess
/**
 This method fetched data from the secure store for the key . Please call the completionBlock once your store retrieves the value stored for the key. Pass error as nil if there are no errors.
*/

- (void)dataForKey:(nonnull NSString *)key
        completion:(nullable void (^)(NSError * _Nullable error, NSData * _Nullable data))completion;

/**
  This method will store the passed data into secure store . Please call completionBlock post completion. Pass error as nil if there are no errors.
 */
- (void)setData:(NSData *_Nullable)data forKey:(NSString *_Nonnull)key completion:(nullable void (^)(NSError * _Nullable error))completion;

/**
    Delete all the data stored in your store related to Cipher
 */
- (void)resetAllData;

- (void)invalidateSession;

- (BOOL)doesRequireUnlock;
/**
    Check if securestore is initialized and setUp
 */
- (BOOL) isStoreSetUp;

/**
   Check if securestore contains any data
*/
- (BOOL) containsDataForKey:(nonnull NSString *)key;

/**
   Get configuration securedStoreType
*/
- (ApolloCardSecureStoreType) getCurrentStoreType;

- (void)setEncryptedData:(nonnull NSData *)data forKey:(nonnull NSString *)key;

- (nullable NSData *)getDecryptedDataForKey:(nonnull NSString *)key;

- (void) setUpSecuredStoreWithCompletion:(nullable void (^)(NSError * _Nullable error)) completion;
@end

#endif /* ApolloSecureStore_h */
