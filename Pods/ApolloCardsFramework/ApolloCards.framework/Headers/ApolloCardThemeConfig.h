#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloCardThemeConfig : NSObject

+(nonnull instancetype) defaultConfig;
@property (nonatomic, nonnull) UIColor *tintColor;
@property (nonatomic, nonnull) NSString *backButtonImageName;
@property (nonatomic, nonnull) UIColor *backButtonTintColor;
@property (nonatomic, nonnull) UIColor *navTitleColor;
@property (nonatomic, nonnull) UIColor *titleColor;
@property (nonatomic, nonnull) UIColor *backgroundColor;
@property (nonatomic, nonnull) UIColor *textColorOnBackground;
@property (nonatomic, nonnull) UIColor *disabledButtonBackgroundColor;
@property (nonatomic, nonnull) UIColor *disabledButtonTextColor;
@property (nonatomic, nonnull) UIColor *enabledButtonBackgroundColor;
@property (nonatomic, nonnull) UIColor *enabledButtonTextColor;
@property (nonatomic, nonnull) UIFont *regularFont;
@property (nonatomic, nonnull) UIFont *boldFont;

-(nonnull UIImage *) backButtonImage;
-(nonnull UIImage *) successConfirmImage;
-(nonnull UIImage *) failureConfirmImage;
@end

NS_ASSUME_NONNULL_END
