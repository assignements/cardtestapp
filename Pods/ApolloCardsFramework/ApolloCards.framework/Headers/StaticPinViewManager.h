
#import <Foundation/Foundation.h>
#import "ApolloCardError.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StaticPinViewManager : NSObject

-(void)fetchUIForStaticPin:(NSString *)staticPin
           withCompletion:(void (^)(UIView * staticPinView))completionBlock;

@end

NS_ASSUME_NONNULL_END
