#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProxyCardSKUDetail : MTLModel<MTLJSONSerializing>
@property (nonatomic) NSString *productID;
@property (nonatomic) NSString *bin;
@property (nonatomic) NSString *range;
@end

NS_ASSUME_NONNULL_END
