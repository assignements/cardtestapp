
#import <UIKit/UIKit.h>
#import <ApolloDeeplinkResolver/ZETANativeView.h>
#import "SuperPinViewProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface FrontFaceCardExtensionView : UIView <SuperPinViewProtocol>

-(void)setupWithCardId:(NSString *)cardId
          showSuperPin:(BOOL)superPin
      showCardSettings:(BOOL)cardSettings;

@end

NS_ASSUME_NONNULL_END
