#import <Foundation/Foundation.h>
#import "ApolloAuthenticationProvider.h"
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import "ApolloGetCardBlockAlias.h"
#import "ApolloCardSecuredStoreAccess.h"

NS_ASSUME_NONNULL_BEGIN

@interface StaticPinManager : NSObject
@property(nonatomic) id<ApolloCardSecuredStoreAccess> secureStore;

-(nonnull instancetype) initWithAuthProvider:(id<ApolloAuthenticationProvider>) authProvider;

- (void)getStaticPinForCardID:(nonnull NSString *)cardID
                 successBlock:(nonnull ZETAStringBlockType)successBlock
                 failureBlock:(nullable ApolloCardFailureBlock)failureBlock;

@end

NS_ASSUME_NONNULL_END
