#import <Foundation/Foundation.h>
#import "ApolloSharedSecretStore.h"
#import "ApolloCardStore.h"
#import "ApolloCardError.h"
#import "CardAnalytics.h"
#import "ApolloCardCryptoHelper.h"
#import "CardSharedSecretWorker.h"
#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>
#import "ServerKeyPlantationManager.h"
#import "ApolloAuthenticationProvider.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^TOTPFetchBlockWithView)(NSString * _Nullable superPin, NSTimeInterval validityInSeconds, ApolloCardErrorInfo * _Nullable error, UIView *superPINView);

@interface SharedSecretManager : NSObject

@property(nonatomic, strong)SdkAuthSession *sdkAuthSession;
-(nonnull instancetype)initWithKVStore:(ApolloCardStore *)kvStore
                           secureStore:(id<ApolloCardSecuredStoreAccess>)secureStore
                     sharedSecretStore:(ApolloSharedSecretStore *)sharedSecretStore
                   authSessionProvider:(id<ApolloAuthenticationProvider>)authSessionProvider
                           resourceIDs:(NSArray <NSString *> *)resourceIDs
                 commonAnalyticsParams:(NSDictionary *)analyticsParams
                      cipherResourceID:(NSString *)resourceID;

-(void)implantOrSyncSharedSecretWithSuccess:(nullable ZETAEmptyBlockType)success
                                       failure:(nullable void (^)(ApolloCardErrorInfo *error))failure;

- (void) generateTOTPWithSessionForResource:(NSString *)resourceID completion: (nonnull TOTPFetchBlock) completion;

@end

NS_ASSUME_NONNULL_END
