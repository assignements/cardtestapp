#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import <Foundation/Foundation.h>
#import "ProxyCardExpiry.h"
#import "ProxyCardSKUDetail.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProxyCardOrderDetail: MTLModel<MTLJSONSerializing>
@property (nonatomic) NSString *orderID;
@property (nonatomic) NSString *cardSkuID;
@property (nonatomic) ProxyCardExpiry *expiry;
@property (nonatomic) ProxyCardSKUDetail *cardSku;
@end

NS_ASSUME_NONNULL_END
