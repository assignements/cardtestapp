//
//  NSString+ApolloCardTextField.h
//  ApolloCards_Framework
//
//  Created by chetan.si on 06/10/20.
//  Copyright © 2020 deepak.go. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (ApolloCardTextField)
+(NSString*)getSpacedString:(NSString *)str;
@end

NS_ASSUME_NONNULL_END
