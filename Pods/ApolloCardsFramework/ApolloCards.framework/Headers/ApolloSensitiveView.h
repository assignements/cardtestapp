#import <Foundation/Foundation.h>
#import "ApolloSensitiveViewValue.h"

@interface ApolloSensitiveView: NSObject

@property (nonatomic, readonly) NSString *algo;
@property (nonatomic, readonly) ApolloSensitiveViewValue *value;

-(instancetype)initWithAlgo:(NSString *)algo
                      value:(ApolloSensitiveViewValue *)value;

@end
