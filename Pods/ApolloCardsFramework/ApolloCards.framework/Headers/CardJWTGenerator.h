#import <Foundation/Foundation.h>
#import <ApolloCoreCrypto/ZETAErasablePrivateKey.h>
#import <ApolloCoreCrypto/ZETAPublicKey.h>

@interface CardJWTGenerator : NSObject

+ (NSString *)generateJWTUsingCardID:(NSString *)cardID
                  andClientPublicKey:(NSString *)clientPublicKey
               withSigningPrivateKey:(ZETAErasablePrivateKey *)signingPrivateKey;

@end
