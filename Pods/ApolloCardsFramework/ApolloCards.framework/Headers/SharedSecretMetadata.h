#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface SharedSecretMetadata : MTLModel<MTLJSONSerializing>
@property (nonatomic, readonly) NSString *serverPKC;
@property (nonatomic, readonly) NSString *inputJID;
@end

NS_ASSUME_NONNULL_END
