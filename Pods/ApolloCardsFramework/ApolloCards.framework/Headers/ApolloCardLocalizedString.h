#import <ApolloCommonsCore/ZETALocalizationFunctions.h>

// Common
#define ConfirmationButtonTitle ZETALocalizedStringWithKeyValuePair(@"okay_text", @"Okay", nil);
#define CancelButtonTitle ZETALocalizedStringWithKeyValuePair(@"cancel_text", @"CANCEL", nil);
#define FailureMessage ZETALocalizedStringWithKeyValuePair(@"generic_failure_message", @"Something went wrong. Please retry.", nil);


// Block Card with Alert
#define BlockCardTitle ZETALocalizedStringWithKeyValuePair(@"block_card_title", @"Block the card?", nil);
#define BlockCardMessage ZETALocalizedStringWithKeyValuePair(@"block_card_subtitle", @"Once you block the card, you will temporarily not be able to make any transactions from this card", nil);
#define BlockCardConfirmButtonTitle ZETALocalizedStringWithKeyValuePair(@"block_card_confirmation_title", @"BLOCK", nil);
#define BlockSuccessConfirmationMessage ZETALocalizedStringWithKeyValuePair(@"block_card_success_toast", @"Card blocked successfully", nil);
#define BlockFailureConfirmationMessage ZETALocalizedStringWithKeyValuePair(@"block_card_fail_toast", @"Failed to block card", nil);

// Unblock Card with Alert
#define UnblockCardTitle ZETALocalizedStringWithKeyValuePair(@"unblock_card_title", @"Unblock the card?", nil);
#define UnblockCardMessage ZETALocalizedStringWithKeyValuePair(@"unblock_card_subtitle", @"Once you unblock the card, you can make any transactions from this card", nil);
#define UnblockCardConfirmButtonTitle ZETALocalizedStringWithKeyValuePair(@"unblock_card_confirmation_title", @"UNBLOCK", nil);
#define UnblockSuccessConfirmationMessage ZETALocalizedStringWithKeyValuePair(@"unblock_card_success_toast", @"Card unblocked successfully", nil);
#define UnblockFailureConfirmationMessage ZETALocalizedStringWithKeyValuePair(@"unblock_card_fail_toast", @"Failed to unblock card", nil);

// Hotlist Card with Alert
#define HotlistCardTitle ZETALocalizedStringWithKeyValuePair(@"hotlist_card_title", @"Hotlist the card?", nil);
#define HotlistCardMessage ZETALocalizedStringWithKeyValuePair(@"hotlist_card_subtitle", @"Once you hotlist the card, you will not be able to make any transactions from this card", nil);
#define HotlistCardConfirmButtonTitle ZETALocalizedStringWithKeyValuePair(@"hotlist_card_confirmation_title", @"HOTLIST", nil);
#define HotlistSuccessConfirmationMessage ZETALocalizedStringWithKeyValuePair(@"hotlist_card_success_toast", @"Card hotlisted successfully", nil);
#define HotlistFailureConfirmationMessage ZETALocalizedStringWithKeyValuePair(@"hotlist_card_fail_toast", @"Failed to hotlist card", nil);



// Hotlist Card with Reason
#define HotlistCardReasonMessage ZETALocalizedStringWithKeyValuePair(@"hotlist_reasons_message", @"Select reason to report a lost or damaged card", nil);
#define HotlistCardToolbarTitle ZETALocalizedStringWithKeyValuePair(@"hotlist_reasons_toolbar_title", @"Select reason to report a lost or damaged card", nil);
#define CardLostReasonTitle ZETALocalizedStringWithKeyValuePair(@"card_lost_reason", @"Card Lost", nil);
#define CardDamagedReasonTitle ZETALocalizedStringWithKeyValuePair(@"card_damage_reason", @"Card Damaged", nil);
#define CardStolenReasonTitle ZETALocalizedStringWithKeyValuePair(@"card_stolen_reason", @"Card Stolen", nil);
#define ProxyCardHotlistFailureMessage ZETALocalizedStringWithKeyValuePair(@"proxy_card_hotlist_reason_failure", @"Hotlist card with reasons not allowed for FIS users", nil);


//Front face card
#define CardListPageTitle ZETALocalizedStringWithKeyValuePair(@"card_list_toolbar_title", @"Your Cards", nil);
#define CardSettingsPageTitle ZETALocalizedStringWithKeyValuePair(@"card_settings_toolbar_title", @"Super Card Settings", nil);
#define CurrentSuperPinLabel ZETALocalizedStringWithKeyValuePair(@"current_superpin_label", @"Current Super PIN", nil);
#define CardSettingsButtonLabel ZETALocalizedStringWithKeyValuePair(@"super_card_settings_button_text", @"Super Card Settings", nil);


//Activate Card - general
#define ActivateCardSuccessMessage ZETALocalizedStringWithKeyValuePair(@"activate_card_success_title", @"Your card has been activated successfully", nil);
#define ActivateCardFailureMessage ZETALocalizedStringWithKeyValuePair(@"activate_card_failure_title", @"Your card activation failed. Please retry.", nil);
#define ActivateCardTitle ZETALocalizedStringWithKeyValuePair(@"activate_card_toolbar_title", @"Activate new card", nil);
#define ProceedCardActivationTitle ZETALocalizedStringWithKeyValuePair(@"proceed_text", @"Proceed", nil);


// Activate Card via card number
#define EnterCardNumberPlaceholder ZETALocalizedStringWithKeyValuePair(@"enter_card_number_placeholder", @"Card Number", nil);
#define EnterExpiryDatePlaceholder ZETALocalizedStringWithKeyValuePair(@"enter_expity_date_placeholder", @"Expiry Date", nil);
#define EnterCVVPlaceholder ZETALocalizedStringWithKeyValuePair(@"enter_cvv_placeholder", @"CVV", nil);
#define IncorrectExpiryDateError ZETALocalizedStringWithKeyValuePair(@"incorrect_expiry_date_error", @"Incorrect Expiry date", nil);


// Activate card via card ID
#define EnterCardIDPlaceholder ZETALocalizedStringWithKeyValuePair(@"enter_cardId_placeholder", @"Card ID", nil);
#define EnterDNINumberPlaceholder ZETALocalizedStringWithKeyValuePair(@"enter_dni_number_placeholder", @"DNI Number", nil);


// Change PIN
#define ChangePinTitle ZETALocalizedStringWithKeyValuePair(@"change_pin_toolbar_title", @"Change Static PIN", nil);
#define ApolloCardUpdatePinHeader ZETALocalizedStringWithKeyValuePair(@"change_pin_description", @"Please set a new secure 4 digit PIN to enable transactions.", nil);
#define ChangePinSuccessMessage ZETALocalizedStringWithKeyValuePair(@"change_pin_success_title", @"Your static PIN is changed successfully", nil);
#define ChangePinFailureMessage ZETALocalizedStringWithKeyValuePair(@"change_pin_failure_title", @"Failed to change static PIN", nil);
#define EnterOldPinPlaceholder ZETALocalizedStringWithKeyValuePair(@"enter_old_pin_placeholder", @"Enter Old PIN", nil);


// Set PIN
#define SetPinTitle ZETALocalizedStringWithKeyValuePair(@"set_pin_toolbar_title", @"Set Static PIN", nil);
#define ApolloCardSetPinHeader ZETALocalizedStringWithKeyValuePair(@"set_pin_description", @"Please set a secure 4 digit PIN to enable transactions.", nil);
#define SetPinSuccessMessage ZETALocalizedStringWithKeyValuePair(@"set_pin_success_title", @"Your static PIN is set successfully", nil);
#define SetPinFailureMessage ZETALocalizedStringWithKeyValuePair(@"set_pin_failure_title", @"Failed to set static PIN", nil);
#define EnterPinPlaceholder ZETALocalizedStringWithKeyValuePair(@"enter_pin_placeholder", @"Enter PIN", nil);
#define EnterConfirmPinPlaceholder ZETALocalizedStringWithKeyValuePair(@"enter_confirm_pin_placeholder", @"Confirm PIN", nil);
#define ProceedSetPinTitle ZETALocalizedStringWithKeyValuePair(@"confirm_text", @"Confirm", nil);
#define SetPinLengthError ZETALocalizedStringWithKeyValuePair(@"error_pin_4_digits", @"PIN should be exactly 4 digits", nil);
#define ConfirmPinMatchError ZETALocalizedStringWithKeyValuePair(@"error_pin_unmatch", @"The PIN does not match!", nil);

// Super PIN view
#define SuperPinViewTitle ZETALocalizedStringWithKeyValuePair(@"super_pin_title", @"Your Super PIN", nil);

// Static PIN view
#define StaticPinViewTitle ZETALocalizedStringWithKeyValuePair(@"fis_static_pin_title", @"Your Static PIN", nil);
#define StaticPinViewFailureMessage ZETALocalizedStringWithKeyValuePair(@"fis_static_pin_error", @"Error in getting Card PIN", nil);
#define ConfirmMessage ZETALocalizedStringWithKeyValuePair(@"close_text", @"CLOSE", nil);

// Copy Card PAN
#define CardCopyMessage ZETALocalizedStringWithKeyValuePair(@"copy_card_message", @"Card Number Copied", nil);

