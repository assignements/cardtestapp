#import <Foundation/Foundation.h>
#import "ApolloBinRange.h"
#import "ApolloSensitiveView.h"
#import "ApolloDecryptedCardDetails.h"

@interface ApolloCardDetail : NSObject<NSCoding>

@property (nonatomic, readonly, nonnull) NSString *cguid;
@property (nonatomic, readonly, nonnull) NSString *crn;
@property (nonatomic, readonly, nonnull) NSString *cardType;
@property (nonatomic, readonly, nonnull) NSString *maskedPan;
@property (nonatomic, readonly, nonnull) NSString *cardStatus;
@property (nonatomic, readonly, nonnull) ApolloBinRange *binRange;
@property (nonatomic, readonly, nonnull) NSArray<NSString *> *vectors;
@property (nonatomic, readonly, nonnull) ApolloSensitiveView *sensitiveView;
@property (nonatomic, readwrite, nonnull) ApolloDecryptedCardDetails *cardDetails;

@property (nonatomic, readonly, nonnull) NSDictionary *rawCardDetails;

- (nonnull instancetype)initWithCguid:(nonnull NSString *)cguid
                          crn:(nonnull NSString *)crn
                     cardType:(nonnull NSString *)cardType
                    maskedPan:(nonnull NSString *)maskedPan
                   cardStatus:(nonnull NSString *)cardStatus
                     binRange:(nonnull ApolloBinRange *)binRange
                      vectors:(nonnull NSArray<NSString *> *)vectors
                sensitiveView:(nonnull ApolloSensitiveView *)sensitiveView
                  cardDetails:(nonnull ApolloDecryptedCardDetails *)cardDetails;

- (nonnull instancetype)initWithCguid:(nonnull NSString *)cguid
                          crn:(nonnull NSString *)crn
                     cardType:(nonnull NSString *)cardType
                    maskedPan:(nonnull NSString *)maskedPan
                   cardStatus:(nonnull NSString *)cardStatus
                     binRange:(nonnull ApolloBinRange *)binRange
                      vectors:(nonnull NSArray<NSString *> *)vectors
                sensitiveView:(nonnull ApolloSensitiveView *)sensitiveView
                  cardDetails:(nonnull ApolloDecryptedCardDetails *)cardDetails
               rawCardDetails:(nonnull NSDictionary *) rawCardDetails;

-(nullable NSData *)data;

+(nullable instancetype)getObjectFromData:(nullable NSData *)data;
@end

