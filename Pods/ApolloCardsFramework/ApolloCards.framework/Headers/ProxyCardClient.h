#import <Foundation/Foundation.h>
#import <BaseClient.h>
#import "ProxyCardRequest.h"
#import "ApolloAuthenticationProvider.h"
#import "ProxyCardResponse.h"
#import "CardStatusRequest.h"
NS_ASSUME_NONNULL_BEGIN

@interface ProxyCardClient : BaseClient

- (nonnull instancetype)initWithBaseURL:(nonnull NSURL *)url
                              userAgent:(nonnull NSString *)userAgent
                           authProvider:(id<ApolloAuthenticationProvider>) authProvider;

- (void)fetchCardListFromIFI:(NSString *)ifi
              request:(ProxyCardRequest *)requestPayload
              success:(nullable void (^)(ProxyCardResponse *response))success
              failure:(nullable void (^)(NSError *error))failure;

-(void)updateCard:(nonnull NSString *)cardID
              ifi:(nonnull NSString *)ifi
       cardStatus:(nonnull CardStatusRequest *)request
          headers:(nonnull NSDictionary *)headers
          success:(nullable ZETAResponseBlockType)success
          failure:(nullable ZETAFailureBlockType)failure;

@end

NS_ASSUME_NONNULL_END
