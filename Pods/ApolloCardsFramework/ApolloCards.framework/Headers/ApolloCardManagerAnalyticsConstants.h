#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, SecureStoreType) {
    SecureStoreTypeUnknown,
    SecureStoreTypeParentPin,
    SecureStoreTypeParentDL,
    SecureStoreTypeSDKPin,
    SecureStoreTypeSDKDL
};

typedef NS_ENUM(NSUInteger, GetCardMethodType) {
    GetCardMethodTypeAPI,
    GetCardMethodTypeView,
};

typedef NS_ENUM(NSUInteger, GetCardInfoType) {
    GetCardInfoTypeCardPan,
    GetCardInfoTypeCardSensitiveInfo,
};

NS_ASSUME_NONNULL_END
