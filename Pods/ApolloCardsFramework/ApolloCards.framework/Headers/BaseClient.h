#import <Foundation/Foundation.h>
#import <ApolloHttpClient/ZETABaseHTTPSessionClient.h>
#import "ApolloAuthenticationProvider.h"

NS_ASSUME_NONNULL_BEGIN

@interface BaseClient : ZETABaseHTTPSessionClient
@property (nonatomic, weak) id<ApolloAuthenticationProvider> authProvider;
-(void)handleFailureIfAuthTokenExpiredWithError:(nonnull  NSError *)error;
@end

NS_ASSUME_NONNULL_END
