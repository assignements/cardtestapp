#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
NS_ASSUME_NONNULL_BEGIN

@interface StaticPinResponsePayload: MTLModel<MTLJSONSerializing>
@property (nonatomic, readonly) NSString *serverPublicKey;
@property (nonatomic, readonly) NSString *encryptedData;
@property (nonatomic, readonly) NSString *iv;
@end

NS_ASSUME_NONNULL_END
