#import <Mantle/Mantle.h>
#import "FormFactorProduct.h"

@interface FormFactor : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *ID;
@property (nonatomic, readonly) NSNumber *ifi;
@property (nonatomic, readonly) NSString *formFactorProductID;
@property (nonatomic, readonly) NSString *formFactorID;
@property (nonatomic, readonly) NSString *credentialID;
@property (nonatomic, readonly) NSArray *tags;
@property (nonatomic, readonly) NSString *status;
@property (nonatomic, readonly) NSString *targetURI;
@property (nonatomic, readonly) NSDictionary *policies;
@property (nonatomic, readonly) NSDictionary *attributes;

@end
