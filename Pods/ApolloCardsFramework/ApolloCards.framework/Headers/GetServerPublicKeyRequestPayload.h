#import <Foundation/Foundation.h>
#import "GetServerPublicKeyMetadataPayload.h"
#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface GetServerPublicKeyRequestPayload : MTLModel<MTLJSONSerializing>
@property (nonatomic, strong, nonnull) GetServerPublicKeyMetadataPayload *metadata;
@property (nonatomic, strong, nonnull) NSString *identityType;

- (instancetype)initWithClientKey:(NSString *)key;
@end

NS_ASSUME_NONNULL_END
