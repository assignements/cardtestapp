#import <Foundation/Foundation.h>
#import "ApolloCardErrorInfo.h"
NS_ASSUME_NONNULL_BEGIN

static NSString *const dataNotFoundErrorTraceId = @"com.zeta.card.dataNotFound";
static NSString *const genericErrorTraceId = @"com.zeta.card.genericError";

@interface ApolloCardError : NSObject
+(ApolloCardErrorInfo *) secureStoreNotSetup;
+(ApolloCardErrorInfo *) getCardErrorFromError:(NSError *)error;
+(ApolloCardErrorInfo *) resourceParsingError;
+(ApolloCardErrorInfo *) resourceNotFoundError;
+(ApolloCardErrorInfo *) genericError;
+(ApolloCardErrorInfo *) initializationError;
+(ApolloCardErrorInfo *) registerError;
+(ApolloCardErrorInfo *) userCancellationError;
+(ApolloCardErrorInfo *) resourceConflictError;
+(ApolloCardErrorInfo *) tenantAuthfailureError:(nullable NSString *)traceID;
+(ApolloCardErrorInfo *) sdkAuthfailureError:(nullable NSString *)traceID;
+(ApolloCardErrorInfo *) getCardErrorWithError:(NSError *)error;
+ (ApolloCardErrorInfo *) badRequestErrorForViolation:(nonnull NSString *)violation;
+ (ApolloCardErrorInfo *) decryptCardDetailError;
+(ApolloCardErrorInfo *) dataNotFoundError;
@end

NS_ASSUME_NONNULL_END
