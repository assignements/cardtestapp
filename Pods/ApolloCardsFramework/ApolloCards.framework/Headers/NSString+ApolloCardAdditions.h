
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (ApolloCardAdditions)
-(NSString *)getEncodedString;
@end

NS_ASSUME_NONNULL_END
