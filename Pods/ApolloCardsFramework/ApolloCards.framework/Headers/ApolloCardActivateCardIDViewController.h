
#import <UIKit/UIKit.h>
#import "ApolloCardThemeConfig.h"
NS_ASSUME_NONNULL_BEGIN

@class ApolloCardActivateCardIDViewController;
@protocol ApolloCardActivateCardIDViewControllerDelegate <NSObject>

- (void)controller:(ApolloCardActivateCardIDViewController *)viewController
    didEnterCardID:(NSString *)cardID
         dniNumber:(NSString *)dniNumber;

@end

@interface ApolloCardActivateCardIDViewController : UIViewController

@property (weak , nonatomic)id <ApolloCardActivateCardIDViewControllerDelegate> delegate;

-(instancetype)initWithDniNumber:(NSString *)dniNumber;

@end

NS_ASSUME_NONNULL_END
