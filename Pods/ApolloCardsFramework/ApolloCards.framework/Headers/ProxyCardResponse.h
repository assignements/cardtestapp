#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "ProxyCardResponsePayload.h"
NS_ASSUME_NONNULL_BEGIN

@interface ProxyCardResponse: MTLModel<MTLJSONSerializing>
@property (nonatomic) NSArray <ProxyCardResponsePayload *> *proxyCardViewList;
@end

NS_ASSUME_NONNULL_END
