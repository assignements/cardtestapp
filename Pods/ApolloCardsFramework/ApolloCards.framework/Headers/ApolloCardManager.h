#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ApolloGetCardBlockAlias.h"
#import "ApolloCardErrorInfo.h"
#import "ApolloCardSecuredStoreAccess.h"
#import "SuperPinViewProtocol.h"

typedef NS_ENUM(NSUInteger, SDKStatus) {
  SDKStatusNotInitialized,
  SDKStatusInitialized,
  SDKStatusAuthenticated,
  SDKStatusReady,
  SDKStatusLoggedOut
};

typedef void (^TOTPFetchBlock)(NSString * _Nullable superPin, NSTimeInterval validityInSeconds, ApolloCardErrorInfo * _Nullable error);
typedef void (^DismissBlockType)(void);

@protocol SDKStatusListener <NSObject>

-(void)statusChanged:(SDKStatus)sdkStatus;

@end

@protocol CardViewStatusChangeListener <NSObject>

- (void) cardViewDidMakeTransition:(nonnull UIView *)cardView
                           toState:(nonnull NSString *)toState
                            cardId:(nonnull NSString *)cardId
                        templateId:(nonnull NSString *)templateId;

@end

@protocol CardStatusChangeListener <NSObject>

- (void)cardDidChangedStateWithCardId:(nonnull NSString *)cardId
                              toState:(nonnull NSString *)toState;

@end

@interface ApolloCardManager: NSObject

@property(nonatomic, weak) id<CardViewStatusChangeListener> _Nullable cardViewStatusChangeListener;

@property(nonatomic, weak) id<CardStatusChangeListener> _Nullable cardStatusChangeListener;

-(nonnull instancetype)init;

-(void)addListener:(nonnull id<SDKStatusListener>)sdkListener;

-(void)removeListener;

-(void)authenticateSDKWithTenantAuthToken:(nonnull NSString *) authToken
                       andAccountHolderID:(nonnull NSString *) accountHolderID;

-(void)setupSDKWithSecureStore:(nonnull id<ApolloCardSecuredStoreAccess>)secureStore
                    resourceID:(nullable NSString *)resourceID
                       success:(nullable ApolloCardEmptyBlockType)success
                       failure:(nullable ApolloCardFailureBlock)failure;

-(void)setUpSDKWithResourceID:(nullable NSString *)resourceID
                      success:(nullable ApolloCardEmptyBlockType)success
                      failure:(nullable ApolloCardFailureBlock)failure;

-(void)setSuperPinViewProvider:(nullable id<SuperPinViewProtocol>)superPinViewProvider;

- (void)setUpSecuredStoreWithCompletion:(nullable void (^)(NSError * _Nullable error))completion;

- (void)forceAuthenticateWithClearSession:(BOOL)clearSession
                               completion:(nullable void (^)(NSError * _Nullable error))completion;

-(void)getCardPanWithCardID:(nonnull NSString *)cardID
                    success:(nullable ApolloGetCardPanSuccessBlock)successBlock
                    failure:(nullable ApolloCardFailureBlock)failure;

- (void)getSecureCardDetailsWithCardID:(nonnull NSString *)cardID
                               success:(nullable ApolloCardSuccessBlock)success
                               failure:(nullable ApolloCardFailureBlock)failure;

-(void)getCardUIWithCardID:(nonnull NSString *)cardID
                   payload:(nullable NSDictionary *)payload
              defaultState:(nonnull NSString *)defaultState
                templateID:(nonnull NSString *)templateID
                   success:(nonnull void (^)(UIView * _Nullable cardView))successBlock
                   failure:(nonnull void (^)(ApolloCardErrorInfo * _Nonnull error)) failureBlock;

-(void)setCardPin:(nonnull NSString *)pin
        forCardId:(nonnull NSString *)cardID
          success:(nullable ApolloSetPinSuccessBlock)success
          failure:(nullable ApolloCardFailureBlock)failure;

-(void)changeCardPinForCardID:(nonnull NSString *)cardID
                       oldPin:(nonnull NSString *)oldPin
                       newPin:(nonnull NSString *)newPin
                      success:(nullable ApolloSetPinSuccessBlock)success
                      failure:(nullable ApolloCardFailureBlock)failure;

-(void)launchCardPinUIForCardId:(nonnull NSString *)cardID
                        success:(nullable ApolloSetPinSuccessBlock)success
                        failure:(nullable ApolloCardFailureBlock)failure;

-(void)launchChangeCardPinUIForCardId:(nonnull NSString *)cardID
                              success:(nullable ApolloSetPinSuccessBlock)success
                              failure:(nullable ApolloCardFailureBlock)failure;

- (void)generateSuperPINWithCompletion:(nonnull TOTPFetchBlock) completion;

- (void)generateSuperPINForResource:(nonnull NSString *)resourceID
                         completion:(nullable TOTPFetchBlock)completion;

-(void)getSuperPinUIForResource:(nonnull NSString *)resourceID
                        success:(nonnull void (^)(UIView * _Nonnull superPinView))successBlock
                        failure:(nonnull void (^)(ApolloCardErrorInfo * _Nonnull error)) failureBlock;

-(void)getSuperPinUIWithSuccess:(nonnull void (^)(UIView * _Nonnull superPinView))successBlock
                        failure:(nonnull void (^)(ApolloCardErrorInfo * _Nonnull error)) failureBlock;

- (BOOL) isCardSdkReady;

-(void)blockCard:(nonnull NSString *) cardID
     description:(nonnull NSString *)description
         success:(nullable ApolloCardEmptyBlockType)success
         failure:(nullable ApolloCardFailureBlock)failure;

-(void)unblockCard:(nonnull NSString *) cardID
       description:(nonnull NSString *)description
           success:(nullable ApolloCardEmptyBlockType)success
           failure:(nullable ApolloCardFailureBlock)failure;

-(void)hotlistCard:(nonnull NSString *) cardID
       description:(nonnull NSString *)description
           success:(nullable ApolloCardEmptyBlockType)success
           failure:(nullable ApolloCardFailureBlock)failure;

-(void)blockCardWithAlert:(nonnull NSString *) cardID
              description:(nonnull NSString *)description
                  success:(nullable ApolloCardEmptyBlockType)success
                  failure:(nullable ApolloCardFailureBlock)failure;

-(void)unblockCardWithAlert:(nonnull NSString *) cardID
                description:(nonnull NSString *)description
                    success:(nullable ApolloCardEmptyBlockType)success
                    failure:(nullable ApolloCardFailureBlock)failure;

-(void)hotlistCardWithCardID:(nonnull NSString *) cardID
                     success:(nullable ApolloCardEmptyBlockType)success
                     failure:(nullable ApolloCardFailureBlock)failure;

-(void)getCardStatusForCardId:(nonnull NSString *)cardId
                      success:(nonnull void (^)(NSString * _Nonnull cardStatus))success
                      failure:(nonnull ApolloCardFailureBlock)failure;

-(void)launchActivateCardIDUIWithDniNumber:(nonnull NSString *)dniNumber
                                   success:(nullable ApolloCardEmptyBlockType)success
                                   failure:(nullable ApolloCardFailureBlock)failure;

-(void)activateCardWithCardNo:(nonnull NSString *)cardNumber
                   expiryDate:(nonnull NSString *)expiryDate
                          cvv:(nonnull NSString *)cvv
                 successBlock:(nullable void (^)(_Nullable id response))success
                 failureBlock:(nullable ApolloErrorBlock)failure;

-(void)activateCardWithCardId:(nonnull NSString *)cardId
                    dniNumber:(nonnull NSString *)dniNumber
                 successBlock:(nullable void (^)(_Nullable id response))success
                 failureBlock:(nullable ApolloErrorBlock)failure;

-(void)launchActivateCardNumberUIWithSuccess:(nullable ApolloCardEmptyBlockType)success
                                     failure:(nullable ApolloCardFailureBlock)failure;

- (void)getStaticPinForCardID:(nonnull NSString *)cardID
                 successBlock:(nonnull ApolloStringSuccessBlock)successBlock
                 failureBlock:(nullable ApolloCardFailureBlock)failureBlock;

-(void)getStaticPinUIForCardID:(nonnull NSString *)cardID
                       success:(nonnull void (^)(UIView * _Nonnull staticPinView))successBlock
                       failure:(nonnull void (^)(ApolloCardErrorInfo * _Nonnull error))failureBlock;

- (void)getProxyCardIDWithSuccessBlock:(nonnull ApolloStringSuccessBlock)successBlock
                          failureBlock:(nullable ApolloCardFailureBlock)failureBlock;

- (nullable UIViewController *)getCardList;

- (void)copyCardNumberWithCardId:(nonnull NSString *)cardID;

- (void)logout;

- (void)logoutWithSuccessBlock:(nullable void (^)(_Nullable id response))success
                  failureBlock:(nullable ApolloErrorBlock)failure;

- (void)openRequestNewCardWebViewWithContext:(nonnull NSDictionary *)context
                                 callbackUrl:(nullable NSURL *)callbackUrl;

- (void)openRequestPhysicalCardWebViewWithContext:(nonnull NSDictionary *)context
                                           cardId:(nonnull NSString *)cardId
                                      callbackUrl:(nullable NSURL *)callbackUrl;

- (void)openBlockAndReplaceCardWebViewWithContext:(nonnull NSDictionary *)context
                                           cardId:(nonnull NSString *)cardId
                                      callbackUrl:(nullable NSURL *)callbackUrl;

- (void)openTransactionLimitsWebViewWithCardId:(nonnull NSString *)cardId
                                   callbackUrl:(nullable NSURL *)callbackUrl;

@end
