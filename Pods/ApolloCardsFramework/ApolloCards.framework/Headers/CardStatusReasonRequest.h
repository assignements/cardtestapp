#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface CardStatusReasonRequest: MTLModel<MTLJSONSerializing>
@property(nonatomic, nonnull) NSString *code;
@property(nonatomic, nullable) NSString *reasonDescription;

@end

NS_ASSUME_NONNULL_END
