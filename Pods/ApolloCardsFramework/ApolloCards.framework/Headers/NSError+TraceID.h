#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSError (TraceID)
-(nullable NSString *)getTraceId;
@end

NS_ASSUME_NONNULL_END
