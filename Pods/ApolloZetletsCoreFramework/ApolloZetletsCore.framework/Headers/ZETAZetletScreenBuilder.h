#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>
#import "ZETAZetletAuthManager.h"
#import <Mantle/Mantle.h>
#import <ApolloCommonsUIKit/ApolloUIKit.h>
#import "ZETAZetletData.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>
#import "ZETATemplateResolver.h"
#import "ZETAAlertProvider.h"

@class ZETAZetletScreenViewController;

@interface ZETAZetletScreenBuilder : NSObject

@property (nonatomic) NSString *templateID;
@property (nonatomic) MTLModel<MTLJSONSerializing> *modelObject;
@property (nonatomic) NSDictionary *dictionaryObject;
@property (nonatomic) ZETAZetletData *zetletData;
@property (nonatomic) CGFloat backgroundOpacity;          // default: 1.0f
@property (nonatomic) BOOL showBack;                      // default: YES
@property (nonatomic) BOOL showNavigationBar;             // defualt: YES
@property (nonatomic) NSString *title;                    // default: @""
@property (nonatomic) BOOL reloadOnAppear;                // default: NO
@property (nonatomic) UIEdgeInsets viewMargins;  // default UIEdgeInsetsZero
@property (nonatomic, weak) id<ZETAZetletAuthManager> authManager;
@property (nonatomic) NSManagedObjectContext *readingMOC;
@property (nonatomic) ZETAScreenPresentWithAnimationType presentationAnimationStyle;
@property (nonatomic) ZETAZetletTemplateAtLocation templateAt;  // default: ZETAZetletTemplateAtClient
@property (nonatomic) NSString *source;  // default: @""
@property (nonatomic) ZETAApolloUserStore *apolloUserStore;
@property (nonatomic) NSArray *prioritizedTemplateCollectionIDs;
@property (nonatomic) ZETATemplateResolver *templateResolver;

- (ZETAZetletScreenViewController *)build;

@end
