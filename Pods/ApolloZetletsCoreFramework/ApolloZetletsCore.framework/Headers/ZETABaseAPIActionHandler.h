#import <UIKit/UIKit.h>
#import <ApolloCommonsMiscellaneous/ApolloMiscellaneous.h>
#import "ZETAZetletAuthManager.h"
#import "ZETAAPIAction.h"

@protocol ZETABaseAPIActionHandlerDelegate;

@interface ZETABaseAPIActionHandler : NSObject

@property (nonatomic, weak) id<ZETABaseAPIActionHandlerDelegate> delegate;

+ (instancetype)sharedInstance;

+ (void)resetSharedInstance;

- (void)handleBaseAPIActions:(NSArray *)baseAPIActions completion:(ZETAArrayBlockType)completion;

@end

@protocol ZETABaseAPIActionHandlerDelegate <NSObject>

- (BFTask *)baseApiActionHandler:(ZETABaseAPIActionHandler *)handler
      didRequestTaskForApiAction:(ZETAAPIAction *)apiAction;

- (void)baseApiActionHandler:(ZETABaseAPIActionHandler *)handler
    didRequestCleanupForTask:(BFTask *)task;

@end
