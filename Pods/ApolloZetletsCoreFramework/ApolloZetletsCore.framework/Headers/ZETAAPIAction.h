#import <Mantle/Mantle.h>
#import <Foundation/Foundation.h>
#import "ZETABaseAPIAction.h"

typedef NS_ENUM(NSUInteger, ZETAAPIActionChannelType) {
  ZETAAPIActionChannelUnknownType,
  ZETAAPIActionChannelDoorType,
  ZETAAPIActionChannelProteusType
};

typedef NS_ENUM(NSUInteger, ZETAAPIActionVerbType) {
  ZETAAPIActionVerbUnknownType,
  ZETAAPIActionVerbGetType,
  ZETAAPIActionVerbPostType
};

@interface ZETAAPIAction : ZETABaseAPIAction

@property (nonatomic, readonly) ZETAAPIActionChannelType channel;
@property (nonatomic, readonly) NSURL *baseURL;
@property (nonatomic, readonly) NSString *endPoint;
@property (nonatomic, readonly) NSString *version;
@property (nonatomic, readonly) NSString *method;
@property (nonatomic, readonly) NSDictionary *headers;
@property (nonatomic, readonly) ZETAAPIActionVerbType verb;
@property (nonatomic, readonly) NSDictionary *payload;
@property (nonatomic, readonly) BOOL requiresAuth;
@property (nonatomic, readonly) NSString *dedupableID;
@property (nonatomic, readonly) NSString *nodeID;

@end
