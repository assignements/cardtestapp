#import <Foundation/Foundation.h>
#import "ZETAZetletData.h"

@protocol ZETAZetletViewActionHandlerDelegate;

@interface ZETAZetletViewActionHandler : NSObject

@property (nonatomic, weak) id<ZETAZetletViewActionHandlerDelegate> delegate;

- (instancetype)initWithViewController:(UIViewController *)viewController
      prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;
- (void)handleActionOnZetletView:(UIView *)view actionData:(ZETAZetletAction *)action;
- (void)handleYouTubePlayActionOnZetletView:(UIView *)view forYoutubeVideoID:(NSString *)videoID;
- (void)handleOnLoadAPIActionIfNeeded:(ZETAZetletData *)zetletData;
@end

@protocol ZETAZetletViewActionHandlerDelegate<NSObject>

@optional

- (void)zetletActionHandler:(ZETAZetletViewActionHandler *)zetletActionHandler
didHandleAPIActionWithResponse:(NSDictionary *)response
         apiResponseActions:(NSArray *)apiResponseActions;

@end

