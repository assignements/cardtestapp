#import <UIKit/UIKit.h>
#import <CocoaNuggetsViews/Views.h>
#import "ZETAZetletElement.h"
#import "ZETAZetletElementView.h"

@interface ZETAZetletAttributedLabelView : ZETALabel<ZETAZetletElementView>

@end
