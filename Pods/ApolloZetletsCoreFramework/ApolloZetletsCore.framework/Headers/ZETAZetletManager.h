#import <Foundation/Foundation.h>
#import "ZETAZetletTemplate.h"
#import "ZETAZetletView.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>
#import "ZETAZetletAuthManager.h"
#import "ZETAAlertProvider.h"

@interface ZETAZetletManager : NSObject

- (instancetype)initWithReadingMOC:(NSManagedObjectContext *)readingMOC
                       authManager:(id<ZETAZetletAuthManager>)authManager
  prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (BOOL)canCreateViewForData:(ZETAZetletData *)data;

- (BOOL)canCreateViewForData:(ZETAZetletData *)data
               usingTemplate:(ZETAZetletTemplate *)zetletTemplate;

- (void)canCreateViewForData:(ZETAZetletData *)data
                  completion:(ZETABooleanStatusBlockType)completion;

- (BOOL)canCreateViewForData:(ZETAZetletData *)data
               usingTemplate:(ZETAZetletTemplate *)zetletTemplate
                         MOC:(NSManagedObjectContext *)MOC;

- (void)canCreateViewForData:(ZETAZetletData *)data
               usingTemplate:(ZETAZetletTemplate *)zetletTemplate
                  completion:(ZETABooleanStatusBlockType)completion;

- (NSString *)finalTemplateIDFromTemplateID:(NSString *)templateID;

/*
 * This method creates an empty view using the template data associated with templateID.
 */
- (ZETAZetletView *)createViewForTemplateID:(NSString *)templateID;

/*
 * This method creates an empty view using templateJSON
 */
- (ZETAZetletView *)createViewForTemplateJSON:(NSDictionary *)templateJSON;

/*
 * This method uses an already created zetlet view and configures data into it provided in the
 * zetletData param
 */
- (void)configureZetletView:(ZETAZetletView *)zetletView
             withZetletData:(ZETAZetletData *)zetletData;

- (void)configureZetletView:(ZETAZetletView *)zetletView
             withZetletData:(ZETAZetletData *)zetletData
            actionsReceiver:(id<ZETAZetletActionsReceiver>)actionsReceiver;

/**
  This method lets you create a complete zetlet view along with data by passing the ZetletData

 @note Returns nil if the data is nil or the template doesn't exist for given templateID
 */
- (ZETAZetletView *)createViewAndConfigureWithZetletData:(ZETAZetletData *)data;

/**
  This method lets you create a zetlet view by passing the template and data.
 */
- (ZETAZetletView *)createViewForTemplateJSON:(NSDictionary *)templateJSON
                     andConfigureWithDataJSON:(NSDictionary *)dataJSON;

- (ZETAZetletView *)createViewForTemplate:(ZETAZetletTemplate *)template
                               templateID:(NSString *)templateID;

/*
 * This functions will parse every image URL and youtube image URL required for creations of the
 * zetlet view, and load those images over network. The function will call its completion handler
 * with YES when all the images are loaded and saved in cache.
 */
- (void)prepareViewForZetletData:(ZETAZetletData *)data
                      completion:(ZETABooleanStatusBlockType)completion;

@end
