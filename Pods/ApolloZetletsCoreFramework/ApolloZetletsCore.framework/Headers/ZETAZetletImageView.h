#import <UIKit/UIKit.h>
#import "ZETAZetletData.h"
#import "ZETAZetletElement.h"
#import "ZETAZetletElementView.h"

@interface ZETAZetletImageView : UIImageView<ZETAZetletElementView>

@end
