#import <ApolloCollectionsCore/ZETACollectionItem.h>
#import <ApolloUserStore/ZETAApolloUserStore.h>

@interface ZETACollectionItem (TemplateAdditions)

+ (instancetype)collectionItemWithTemplateID:(NSString *)templateID
            prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs
                                         MOC:(NSManagedObjectContext *)MOC;
+ (instancetype)collectionItemWithTemplateID:(NSString *)templateID
            prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

@end
