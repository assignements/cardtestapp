#import <Foundation/Foundation.h>
#import "ZETAZetletData.h"

@protocol ZETAZetletSubscribersManagerDelegate;

@interface ZETAZetletSubscribersManager: NSObject

@property (nonatomic, weak) id<ZETAZetletSubscribersManagerDelegate> delegate;

- (void)addSubscribersForConfig:(ZETAZetletSubscribersConfig *)subscribersConfig;
- (void)removeAllSubscribers;

@end

@protocol ZETAZetletSubscribersManagerDelegate<NSObject>

- (void)zetletSubscribersManager:(ZETAZetletSubscribersManager *)subscribersManager
   didReceiveAppDataUpdateForKey:(NSString *)key
                         andValue:(NSObject *)value;

@end

