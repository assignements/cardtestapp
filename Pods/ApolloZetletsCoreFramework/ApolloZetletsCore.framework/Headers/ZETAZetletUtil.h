#import <Foundation/Foundation.h>
#import "ZETAZetletElement.h"
#import "ZETAZetletTextDecorator.h"

@interface ZETAZetletUtil : NSObject

+ (NSAttributedString *)processTemplateDataText:(NSString *)text
                           withStyle:(ZETAZetletElementStyle *)style
                       updationBlock:(ZETAZetletTextDecoratorUpdationBlockType)updationBlock;

+ (NSString *)humanReadableDateForDate:(NSDate *)date;

+ (NSString *)youtubeVideoIDFromURL:(NSURL *)youtubeURL;

+ (NSComparator)comparatorForSortingTemplateCollectionIDsUsingStringsInOrder:
(NSArray *)collectionIDs;

@end
