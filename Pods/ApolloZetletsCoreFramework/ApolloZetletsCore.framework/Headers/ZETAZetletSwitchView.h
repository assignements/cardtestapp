#import <UIKit/UIKit.h>
#import "ZETAZetletData.h"
#import "ZETAZetletElement.h"
#import "ZETAZetletElementView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZETAZetletSwitchView : UISwitch <ZETAZetletElementView>

@end

NS_ASSUME_NONNULL_END
