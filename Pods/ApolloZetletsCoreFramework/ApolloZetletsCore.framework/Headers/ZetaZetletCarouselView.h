
#import <UIKit/UIKit.h>
#import "ZETAZetletManager.h"
#import "ZETAZetletElementView.h"
#import "ZETAZetletElement.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZetaZetletCarouselView : UIView <ZETAZetletElementView>

- (instancetype)initWithZetletManager:(ZETAZetletManager *)zetletManager
                        zetletElement:(ZETAZetletElement *)element
     prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

@end

NS_ASSUME_NONNULL_END
