#import <Mantle/Mantle.h>
#import <ApolloCommonsCore/ApolloMacros.h>

@interface ZETAZetletAlertButton : MTLModel<MTLJSONSerializing>

- (instancetype)initWithButtonID:(NSString *)buttonID
                           title:(NSString *)title
                   actionHandler:(ZETAEmptyBlockType)actionHandler;

@property (nonatomic, readonly) NSString *buttonID;
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly, copy) ZETAEmptyBlockType actionHandler;

@end
