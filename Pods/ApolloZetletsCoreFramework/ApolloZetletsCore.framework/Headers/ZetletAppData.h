#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>
#import "ZETAZetletMessage.h"

typedef NS_ENUM(NSUInteger, ZetletAppDataErrorCode) {
  ZetletAppDataErrorCodeSuperclassFailure
};

@interface ZetletAppData : ZETAZetletMessage

@property (nonatomic, retain) NSNumber *sortOrder;

+ (NSString *)entityName;

+ (NSArray *)messagesWithCollectionID:(NSString *)collectionID
                                  MOC:(NSManagedObjectContext *)MOC;

@end
