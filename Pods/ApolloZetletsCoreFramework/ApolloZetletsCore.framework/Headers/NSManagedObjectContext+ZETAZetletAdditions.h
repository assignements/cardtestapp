#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <ApolloCommonsMiscellaneous/ApolloMiscellaneous.h>
#import "ZETAZetletTemplate.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>

@interface NSManagedObjectContext (ZETAZetletAdditions)

- (ZETAZetletTemplate *)templateWithTemplateID:(NSString *)templateID
              prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (void)templateWithTemplateID:(NSString *)templateID
prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs
                    completion:(ZETAResponseBlockType)completion;

- (ZETAZetletTemplateWrapper *)templateWrapperWithTemplateID:(NSString *)templateID
                            prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (void)templateWrapperWithTemplateID:(NSString *)templateID
     prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs
                           completion:(ZETAResponseBlockType)completion;

- (void)clearCacheForTemplateID:(NSString *)templateID;

@end

