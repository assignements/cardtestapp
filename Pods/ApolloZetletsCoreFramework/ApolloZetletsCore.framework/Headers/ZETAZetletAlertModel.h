#import <Mantle/Mantle.h>
#import "ZETAZetletAlertButton.h"

@interface ZETAZetletAlertModel : MTLModel<MTLJSONSerializing>

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
                     imageURL:(NSURL *)imageURL
                      buttons:(NSArray<ZETAZetletAlertButton *> *)buttons;

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *message;
@property (nonatomic, readonly) NSURL *imageURL;
@property (nonatomic, readonly) NSArray<ZETAZetletAlertButton *> *buttons;

@end
