#import <Foundation/Foundation.h>
#import "ZETAZetletTextDecoratorStyle.h"
#import "ZETAZetletElementStyle.h"

typedef void (^ZETAZetletTextDecoratorUpdationBlockType)(NSString *label, NSString *url);

@interface ZETAZetletTextDecorator : NSObject

- (instancetype)initWithStyle:(ZETAZetletElementStyle *)style;
- (NSAttributedString *)decorate:(NSAttributedString *)attributedString
                   updationBlock:(ZETAZetletTextDecoratorUpdationBlockType)updationBlock;

- (NSAttributedString *)
decoratedAttributedStringFromMarkerString:(NSAttributedString *)attributedString
                           decoratorStyle:(ZETAZetletTextDecoratorStyle *)decoratorStyle
                            updationBlock:(ZETAZetletTextDecoratorUpdationBlockType)updationBlock;

- (NSString *)openingMark;
- (NSString *)closingMark;

@property (nonatomic) ZETAZetletElementStyle *style;

@end
