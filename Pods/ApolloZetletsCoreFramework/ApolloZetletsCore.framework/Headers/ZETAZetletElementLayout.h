#import <Mantle/Mantle.h>
#import <UIKit/UIGeometry.h>

typedef NS_ENUM(NSUInteger, ZETAZetletElementVisibility) { ZETAZetletElementVisibilityNotDefined,
                                                           ZETAZetletElementVisibilityVisible,
                                                           ZETAZetletElementVisibilityNone
};

typedef NS_ENUM(NSUInteger, ZETAZetletElementOrientation) { ZETAZetletElementOrientationVertical,
                                                            ZETAZetletElementOrientationHorizontal
};

typedef NS_ENUM(NSUInteger, ZETAZetletElementListType) { ZETAZetletElementListTypeList,
                                                         ZETAZetletElementListTypeGrid
};

typedef NS_ENUM(NSUInteger, ZETAZetletElementAnimationType) { ZETAZetletElementAnimationTypeNone
};

@interface ZETAZetletElementLayout : MTLModel <MTLJSONSerializing>

@property(nonatomic, readonly, copy) NSNumber *width;
@property(nonatomic, readonly, copy) NSNumber *height;

@property(nonatomic, readonly) ZETAZetletElementVisibility visibility;
@property(nonatomic, readonly) ZETAZetletElementOrientation orientation;
@property(nonatomic, readonly) ZETAZetletElementListType listType;
@property(nonatomic, readonly) ZETAZetletElementAnimationType animationType;

// sibling IDs
@property(nonatomic, readonly, copy) NSString *alignLeft;
@property(nonatomic, readonly, copy) NSString *alignRight;
@property(nonatomic, readonly, copy) NSString *alignTop;
@property(nonatomic, readonly, copy) NSString *alignBottom;
@property(nonatomic, readonly, copy) NSString *alignHorizontalCenter;
@property(nonatomic, readonly, copy) NSString *alignVerticalCenter;

@property(nonatomic, readonly) BOOL alignParentLeft;
@property(nonatomic, readonly) BOOL alignParentRight;
@property(nonatomic, readonly) BOOL alignParentTop;
@property(nonatomic, readonly) BOOL alignParentBottom;

@property(nonatomic, readonly) BOOL centerVertical;
@property(nonatomic, readonly) BOOL centerHorizontal;

@property(nonatomic, readonly, copy) NSNumber *marginLeft;
@property(nonatomic, readonly, copy) NSNumber *marginRight;
@property(nonatomic, readonly, copy) NSNumber *marginTop;
@property(nonatomic, readonly, copy) NSNumber *marginBottom;

@property(nonatomic, readonly, copy) NSNumber *loadingGifWidth;
@property(nonatomic, readonly, copy) NSNumber *loadingGifHeight;
// sibling IDs
@property(nonatomic, readonly, copy) NSString *rightOf;
@property(nonatomic, readonly, copy) NSString *leftOf;
@property(nonatomic, readonly, copy) NSString *above;
@property(nonatomic, readonly, copy) NSString *below;

@property(nonatomic, readonly, copy) NSNumber *compressionPriorityYAxis;
@property(nonatomic, readonly, copy) NSNumber *compressionPriorityXAxis;

@property(nonatomic, readonly, copy) NSNumber *zIndex;
@property(nonatomic, readonly, copy) NSNumber *aspectRatio;

@property(nonatomic, readonly, copy) NSNumber *paddingLeft;
@property(nonatomic, readonly, copy) NSNumber *paddingRight;
@property(nonatomic, readonly, copy) NSNumber *paddingTop;
@property(nonatomic, readonly, copy) NSNumber *paddingBottom;

@property(nonatomic, readonly, copy) NSNumber *maxColumnCount;
@property(nonatomic, readonly, copy) NSNumber *maxRowCount;
@property(nonatomic, readonly, copy) NSNumber *minLineSpacing;
@property(nonatomic, readonly, copy) NSNumber *minIteritemSpacing;

//TODO: Add this support to ZetletCollectionView as well.
@property(nonatomic, readonly, copy) NSNumber *listSeparatorBottom;

@property(nonatomic, readonly) BOOL sectionEnabled;

//carousel properties
@property (nonatomic, readonly, copy) NSNumber *carouselItemsSpacing;
@property (nonatomic, readonly, copy) NSNumber *carouselNextItemPreviewWidth;
@property (nonatomic, readonly, copy) NSNumber *pageControlTopInset;
@property (nonatomic, readonly, copy) NSNumber *pageControlBottomInset;
@property (nonatomic, readonly, copy) NSNumber *pageControlHeight;


- (instancetype)layoutByMergingLayout:(ZETAZetletElementLayout *)layout;

- (UIEdgeInsets)insetsFromPadding;

@end
