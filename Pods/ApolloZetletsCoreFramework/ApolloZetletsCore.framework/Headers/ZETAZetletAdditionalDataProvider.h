
@protocol ZETAZetletAdditionalDataProviderDataSource;

@interface ZETAZetletAdditionalDataProvider: NSObject

@property (weak, nonatomic) id<ZETAZetletAdditionalDataProviderDataSource> dataSource;

+ (instancetype)sharedProvider;

- (NSDictionary *)additionalData;

@end

@protocol  ZETAZetletAdditionalDataProviderDataSource<NSObject>

- (NSDictionary *)zetletDataProviderDidRequestAdditionalData:(ZETAZetletAdditionalDataProvider *)zetletDataProvider;

@end


