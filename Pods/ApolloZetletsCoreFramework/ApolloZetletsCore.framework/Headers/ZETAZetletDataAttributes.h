#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

@interface ZETAZetletDataAttributes : ZETAModelWithoutNil<MTLJSONSerializing>

+ (instancetype)zetletDataAttributesFromDictionary:(NSDictionary *)dictionary;
+ (NSDictionary *)dataAttributesDictFromDictionary:(NSDictionary *)dictionary;

@property (nonatomic, readonly) BOOL isFeatured;
@property (nonatomic, copy, readonly) NSNumber *featuredSortOrder;
@property (nonatomic, copy, readonly) NSDate *expiry;
@property (nonatomic, copy, readonly) NSNumber *sortOrder;
@property (nonatomic, readonly) NSArray *ifiList;
@property (nonatomic, readonly) NSArray *corpIDList;

@end
