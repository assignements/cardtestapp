#import <ApolloUserStore/ZETAApolloUserStore.h>

@interface ZETAApolloUserStore (TemplateAdditions)

- (NSString *)latestCollectionIDForTemplateID:(NSString *)templateID
             prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;
- (NSString *)templateVersionForTemplateID:(NSString *)templateID;
- (void)addTemplateID:(NSString *)templateID forCollectionID:(NSString *)collectionID;
- (void)removeTemplateID:(NSString *)templateID forCollectionID:(NSString *)collectionID;
- (void)addTemplateVersion:(NSString *)templateVersion forTemplateID:(NSString *)templateID;
- (void)removeTemplateVersionForTemplateID:(NSString *)templateID;

@end
