#import <UIKit/UIKit.h>
#import "ZETAZetletTourManager.h"

FOUNDATION_EXPORT float const InterItemSpacing;
FOUNDATION_EXPORT float const ScrollLeftInset;
FOUNDATION_EXPORT float const ExtraPortionToBeShownOfOtherView;

@protocol ZETAZetletTourViewController;
@protocol ZETAZetletTourViewControllerDelegate;
@protocol ZETAZetletTourViewControllerDataSource;

@interface ZETAZetletTourViewController : UIViewController

@property (nonatomic, weak) id<ZETAZetletTourViewControllerDataSource> dataSource;
@property (nonatomic, weak) id<ZETAZetletTourViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@protocol ZETAZetletTourViewControllerDataSource<NSObject>

- (ZETAZetletData *)zetletDataForZetletTourViewController:
                        (ZETAZetletTourViewController *)tourViewController
                                                  atIndex:(NSInteger)index;

- (NSInteger)numberOfPagesForZetletTourViewController:
    (ZETAZetletTourViewController *)tourViewController;

- (UIImage *)backgroundImageForZetletTourViewController:
    (ZETAZetletTourViewController *)tourViewController;

- (BOOL)isPageControlVisibleForZetletTourViewController:
    (ZETAZetletTourViewController *)tourViewController;

- (BOOL)isSkipButtonVisibleForZetletTourViewController:
    (ZETAZetletTourViewController *)tourViewController;

- (NSString *)genericButtonTextForZetletTourViewController:
                  (ZETAZetletTourViewController *)tourViewController
                                                   atIndex:(NSInteger)index;

- (CGPoint)genericButtonRightAndBottomMarginForZetletTourViewController:
    (ZETAZetletTourViewController *)tourViewController;

- (UIColor *)genericButtonTitleColorForZetletTourViewController:
    (ZETAZetletTourViewController *)tourViewController;

- (BOOL)shouldApplyThemeGradientBackgroundForZetletTourViewController:
    (ZETAZetletTourViewController *)tourViewController;

- (BOOL)shouldShowCharacterAnimationOnLastTourPageForZetletTourViewController:
    (ZETAZetletTourViewController *)tourViewController;

- (BOOL)shouldShowSkipButtonOnLastTourPageForZetletTourViewController:
    (ZETAZetletTourViewController *)tourViewController;

@optional

- (void)didOpenPageAtIndex:(NSInteger)index
    forZetletTourViewController:(ZETAZetletTourViewController *)tourViewController;

- (void)forwardSwipeHappenedForZetletTourViewController:
    (ZETAZetletTourViewController *)tourViewController;

- (void)backwardSwipeHappenedForZetletTourViewController:
    (ZETAZetletTourViewController *)tourViewController;

@end

@protocol ZETAZetletTourViewController<NSObject>

@property (nonatomic, readonly) NSInteger pageIndex;

@end

@protocol ZETAZetletTourViewControllerDelegate<NSObject>

- (void)zetletTourViewControllerDidFinishTour:(ZETAZetletTourViewController *)controller;
- (void)genericButtonPressedForZetletTourViewController:
    (ZETAZetletTourViewController *)tourViewController;
- (void)skipButtonPressedForZetletTourViewController:
    (ZETAZetletTourViewController *)tourViewController;

@optional

- (void)zetletTourViewControllerDidSkipTour:(ZETAZetletTourViewController *)controller;

@end
