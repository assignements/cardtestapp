#import <Foundation/Foundation.h>

@interface NSURL (ZETAZetletAdditions)

- (BOOL)zeta_isInternalZetletAction;

@end
