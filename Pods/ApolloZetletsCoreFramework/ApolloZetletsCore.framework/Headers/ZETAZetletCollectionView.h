#import <UIKit/UIKit.h>
#import "ZETAZetletElement.h"
#import "ZETAZetletElementView.h"
#import "ZETAZetletAuthManager.h"
#import "ZETAZetletManager.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>
#import "ZETAAlertProvider.h"

@protocol CarouselViewDelegate;

@interface ZETAZetletCollectionView : UICollectionView<ZETAZetletElementView>

- (instancetype)initWithZetletManager:(ZETAZetletManager *)zetletManager
                        zetletElement:(ZETAZetletElement *)element
                          authManager:(id<ZETAZetletAuthManager>)authManager
                           readingMOC:(NSManagedObjectContext *)readingMOC
     prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (instancetype)initWithZetletManager:(ZETAZetletManager *)zetletManager
                        zetletElement:(ZETAZetletElement *)element
     prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs
                       isTypeCarousel:(BOOL)isTypeCarousel;

@property (weak, nonatomic) id<CarouselViewDelegate> carouselViewDelegate;

@end

@protocol CarouselViewDelegate <NSObject>

- (void)collectionView:(ZETAZetletCollectionView *)collectionView
 didScrollToPageNumber:(NSUInteger)pageNumber;

@end
