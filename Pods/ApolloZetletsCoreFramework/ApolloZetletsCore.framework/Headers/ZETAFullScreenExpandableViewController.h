#import <UIKit/UIKit.h>
#import "ZETAZetletAuthManager.h"
#import "ZETAZetletData.h"
#import "ZETAZetletManager.h"
#import "ZETAZetletView.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>

@interface ZETAFullScreenExpandableViewController : UIViewController

- (instancetype)initWithPresentingViewController:(UIViewController *)viewController
                           originalExpandingView:(UIView *)originalView
                                      zetletData:(ZETAZetletData *)zetletData
                                  collpasedState:(NSString *)collapsedState
                                   expandedState:(NSString *)expandedState
                                     authManager:(id<ZETAZetletAuthManager>)authManager
                                      readingMOC:(NSManagedObjectContext *)readingMOC
                prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (void)reloadZetletData:(ZETAZetletData *)zetletData;

@end
