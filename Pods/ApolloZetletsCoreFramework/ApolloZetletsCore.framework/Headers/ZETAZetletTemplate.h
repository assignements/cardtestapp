#import <Mantle/Mantle.h>
#import "ZETAZetletElement.h"

@interface ZETAZetletTemplate : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) ZETAZetletElement *container;
@property (nonatomic, readonly) NSArray *elements;

@end

@interface ZETAZetletTemplateWrapper : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *templateID;
@property (nonatomic, readonly) ZETAZetletTemplate *presentation;
@property (nonatomic, readonly) NSString *dataTemplate;
@property (nonatomic, readonly) NSArray *fieldOrdering;
@property (nonatomic, readonly) NSDictionary *mappings;

@end
