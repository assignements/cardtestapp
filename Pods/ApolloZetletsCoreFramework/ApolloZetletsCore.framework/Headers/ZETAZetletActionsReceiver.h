#import <UIKit/UIKit.h>
#import "ZETATapGestureRecognizer.h"
#import "ZETAZetletData.h"

/**
 This is the protocol that a view must follow if it wants to cater zetlet actions.
 */
@protocol ZETAZetletActionsReceiver<NSObject>

- (void)zetletDidReceiveTapAction:(ZETATapGestureRecognizer *)recognizer;

// TODO: Improve the handling this tap along with normal tap.
- (void)zetletDidReceiveTapOnYoutubeElement:(ZETATapGestureRecognizer *)recognizer;

- (void)zetletDidReceiveSwipeActionWithActionData:(ZETAZetletAction *)action;

- (void)zetletDidReceiveSwitchingActionWithActionData:(ZETAZetletAction *)action;

@optional

- (void)zetletDidReceiveExternalViewRefreshAction:(ZETAZetletAction *)action;

@end
