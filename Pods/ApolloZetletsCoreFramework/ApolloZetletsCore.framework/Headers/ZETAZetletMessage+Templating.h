#import "ZETAInboxTemplateStoreReceipt.h"
#import "ZETAZetletData.h"
#import "ZETAZetletMessage.h"

@interface ZETAZetletMessage (Templating)

- (BOOL)isNonZetletLegacyMessage;

- (BOOL)isStaticStoreReceipt;

- (BOOL)isZetletData;

- (ZETAZetletData *)zetletData;

- (ZETAInboxTemplateStoreReceipt *)staticStoreReceipt;

@end
