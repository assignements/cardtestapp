#import <UIKit/UIKit.h>
#import "ZETAZetletElement.h"
#import "ZETAZetletElementView.h"
#import "ZETAZetletAuthManager.h"
#import "ZETAZetletManager.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>
#import "ZETAAlertProvider.h"

@interface ZETAZetletListView : UITableView<ZETAZetletElementView>

- (instancetype)initWithZetletManager:(ZETAZetletManager *)zetletManager
                        zetletElement:(ZETAZetletElement *)element
                          authManager:(id<ZETAZetletAuthManager>)authManager
                           readingMOC:(NSManagedObjectContext *)readingMOC
     prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

@end
