#import <Mantle/Mantle.h>
#import "ZETABaseAPIAction.h"

typedef NS_ENUM(NSUInteger, ZETADataAPIActionType) {
  ZETADataAPIActionUnknownType,
  ZETADataAPIActionGetType,
  ZETADataAPIActionClearCacheType
};

@interface ZETADataAPIAction : ZETABaseAPIAction

@property (nonatomic, readonly) ZETADataAPIActionType type;
@property (nonatomic, readonly) NSString *key;

@end
