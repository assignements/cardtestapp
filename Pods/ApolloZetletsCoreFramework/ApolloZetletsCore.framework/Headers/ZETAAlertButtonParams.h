#import <Foundation/Foundation.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

@interface ZETAAlertButtonParams : NSObject

- (instancetype)initWithTitle:(NSString *)title actionHandler:(ZETAEmptyBlockType)actionHandler;

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly, copy) ZETAEmptyBlockType actionHandler;

@end
