#import <Foundation/Foundation.h>
#import <ApolloUserStore/ZETAApolloUserStore.h>

typedef void (^ZETATemplateDidUpdateNotificationBlockType)(__weak __nullable id weakSelf,
                                                           NSString *__nonnull templateID,
                                                           NSString *__nonnull lastUpdateTime);

@interface NSObject (TemplateAdditions)

- (void)zeta_observeTemplateUpdatesWithNotificationBlock:(nonnull ZETATemplateDidUpdateNotificationBlockType)block
                                         ;

@end
