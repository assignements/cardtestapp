#import <UIKit/UIKit.h>

@interface ZETATapGestureRecognizer : UITapGestureRecognizer

- (void)addTarget:(id)target action:(SEL)action actionData:(id)actionData;

- (id)actionDataForTarget:(id)target action:(SEL)action;

- (void)addActionData:(id)actionData
               target:(id)target
               action:(SEL)action;

@end
