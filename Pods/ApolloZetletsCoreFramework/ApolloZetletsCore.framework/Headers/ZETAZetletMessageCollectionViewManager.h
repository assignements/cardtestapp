#import <Foundation/Foundation.h>
#import "ZETAZetletAuthManager.h"
#import <ApolloUserStore/ZetaApolloUserStore.h>
#import "ZETAZetletManager.h"
#import "ZETAAlertProvider.h"

@protocol ZETAZetletMessageCollectionViewManagerDelegate;

@protocol ZETAZetletMessageCollectionViewManagerErrorDelegate;

@interface ZETAZetletMessageCollectionViewManager : NSObject

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView
                           authManager:(id<ZETAZetletAuthManager>)authManager
              presentingViewController:(UIViewController *)presentingViewController
                            readingMOC:(NSManagedObjectContext *)readingMOC
               collectionViewCellClass:(Class)collectionViewCellClass
      prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

@property(nonatomic) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic, readonly, copy) NSNumber *collectionViewHeight;
@property(nonatomic, weak) id<ZETAZetletMessageCollectionViewManagerErrorDelegate> errorDelegate;

@property(nonatomic, weak) id<ZETAZetletMessageCollectionViewManagerDelegate> delegate;

- (void)updateActionsReceiver:(id<ZETAZetletActionsReceiver>)actionsReceiver;

@end

@protocol ZETAZetletMessageCollectionViewManagerDelegate <NSObject>

- (BOOL)isZetletMessageCollectionViewVisible:
    (ZETAZetletMessageCollectionViewManager *)collectionViewManager;

@optional
- (void)zetletMessageCollectionViewManager:
            (ZETAZetletMessageCollectionViewManager *)collectionViewManager
             didUpdateCollectionViewHeight:(NSNumber *)height;

- (void)zetletMessageCollectionViewManager:
            (ZETAZetletMessageCollectionViewManager *)collectionViewManager
                          didScrollToIndex:(NSIndexPath *)indexPath;

@end

@protocol ZETAZetletMessageCollectionViewManagerErrorDelegate <NSObject>

- (void)zetletMessageCollectionViewManagerDidRequestToShowErrorScreen:(ZETAZetletMessageCollectionViewManager *)collectionViewManager;
- (void)zetletMessageCollectionViewManagerDidRequestToRemoveErrorScreen:(ZETAZetletMessageCollectionViewManager *)collectionViewManager;

@end
