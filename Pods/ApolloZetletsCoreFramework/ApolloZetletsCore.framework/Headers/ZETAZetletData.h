#import <Mantle/Mantle.h>
#import "ZETAZetletElement.h"
#import <ApolloCommonsUIKit/ApolloUIKit.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

FOUNDATION_EXPORT NSString *const ZETAZetletDataVersionV2;

typedef NS_ENUM(NSUInteger, ZETAZetletActionType) { ZETAZetletActionTypeUnknown,
                                                    ZETAZetletActionTypeTap,
                                                    ZETAZetletActionTypeInlineLinkTap,
                                                    ZETAZetletActionTypeCallback,
                                                    ZETAZetletActionTypeSwipe,
                                                    ZETAZetletActionTypeYoutube,
                                                    ZETAZetletActionTypeToggleSwitch,
                                                    ZETAZetletActionTypeExternalViewRefresh
};

typedef NS_ENUM(NSUInteger, ZETAZetletActionTransitionType) { ZETAZetletActionTransitionUnknown,
                                                              ZETAZetletActionTransitionInline,
                                                              ZETAZetletActionTransitionFullScreen
};

typedef NS_ENUM(NSUInteger, ZETAZetletSQLQuerySortDescriptorOrderType) { ZETAZetletSQLQuerySortDescriptorOrderTypeAscending,
                                                                         ZETAZetletSQLQuerySortDescriptorOrderTypeDescending
};

typedef NS_ENUM(NSUInteger, ZETAZetletTemplateAtLocation) { ZETAZetletTemplateAtClient,
                                                            ZETAZetletTemplateAtServer
};

@interface ZETAZetletSQLQuerySortDescriptor : MTLModel <MTLJSONSerializing>

@property(nonatomic, readonly) NSString *key;
@property(nonatomic, readonly) ZETAZetletSQLQuerySortDescriptorOrderType order;

@end

@interface ZETAZetletSQLQuery : MTLModel <MTLJSONSerializing>

@property(nonatomic, readonly) NSString *db;
@property(nonatomic, readonly) NSString *table;
@property(nonatomic, readonly) NSString *predicate;
@property(nonatomic, readonly, copy) NSArray<ZETAZetletSQLQuerySortDescriptor *> *sortDescriptors;
@property(nonatomic, readonly) NSNumber *limit;

typedef NS_ENUM(NSUInteger, ZETAZetletScreenNavigationType) { ZETAZetletScreenNavigationTypeUnknown,
                                                              ZETAZetletScreenNavigationTypePush,
                                                              ZETAZetletScreenNavigationTypePop,
                                                              ZETAZetletScreenNavigationTypePopAll
};

@end

@interface ZETAZetletScreenConfig : MTLModel <MTLJSONSerializing>

@property(nonatomic, readonly) ZETAZetletScreenNavigationType navigationType;
@property(nonatomic, readonly) BOOL showBack;
@property(nonatomic, readonly) BOOL showNavigationBar;
@property(nonatomic, readonly) NSNumber *backgroundOpacity;
@property(nonatomic, readonly) ZETAScreenPresentWithAnimationType presentationAnimationStyle;

@end

@interface ZETAZetletScreen : MTLModel <MTLJSONSerializing>

@property(nonatomic, readonly) NSString *screenID;
@property(nonatomic, readonly) NSString *flowID;
@property(nonatomic, readonly) NSString *title;
@property(nonatomic, readonly) NSString *templateID;
@property(nonatomic, readonly) NSDictionary *templateData;
@property(nonatomic, readonly) ZETAZetletScreenConfig *config;
@property(nonatomic, readonly) ZETAZetletTemplateAtLocation templateAt;
@property(nonatomic, readonly) NSString *source;
@property(nonatomic, readonly) BOOL reloadOnAppear;

@end

@interface ZETAGenericDrawerScreen : ZETAZetletScreen

@property(nonatomic, readonly) NSURL *url;

@end

@interface ZETAGenericDrawer : MTLModel <MTLJSONSerializing>

@property(nonatomic, readonly) ZETAGenericDrawerScreen *leftScreen;
@property(nonatomic, readonly) ZETAGenericDrawerScreen *rightScreen;
@property(nonatomic, readonly) ZETAGenericDrawerScreen *centerScreen;

@end

@interface ZETAZetletActionTransition : MTLModel <MTLJSONSerializing>

@property(nonatomic, readonly) NSString *toState;
@property(nonatomic, readonly) ZETAZetletActionTransitionType type;

- (instancetype)initWithDestinationState:(NSString *)state
                          transitionType:(ZETAZetletActionTransitionType)type;

@end

@class ZETAZetletElementData;

/**
 Represents the possible zetlet actions.
 */
@interface ZETAZetletAction : MTLModel <MTLJSONSerializing>

@property(nonatomic, readonly) ZETAZetletActionType type;
@property(nonatomic, readonly) BOOL refreshViewGroup;
@property(nonatomic, readonly) BOOL animatedRefresh;
// can be zeta://, share;// deeplinking also
@property(nonatomic, readonly, copy) NSURL *URL;
@property(nonatomic, readonly) BOOL requiresAuth;
@property(nonatomic, readonly) BOOL injectJSPlugins;

@property(nonatomic, readonly) ZETAZetletActionTransition *transition;
@property(nonatomic, readonly) NSDictionary *analytics;

@property(nonatomic, readonly) NSString *eventName;
@property(nonatomic, readonly) NSDictionary *eventAttributes;
@property(nonatomic, readonly) BOOL isImmediate;

@property(nonatomic, readonly, copy) NSArray *api;
@property(nonatomic, readonly) ZETAZetletElementData *loader;
@property(nonatomic, readonly, copy) ZETAZetletScreen *screen;

@property(nonatomic, readonly) NSString *videoID;

@property(nonatomic, readonly, copy) NSArray *dataAPIs;

@property(nonatomic, readonly) NSString *js;

+ (NSString *)actionStringForType:(ZETAZetletActionType)type;
+ (ZETAZetletAction *)generateInlineTransitionActionToState:(NSString *)destinationState;
+ (ZETAZetletAction *)generateExternalViewRefreshActionWithAnimation:(BOOL)animation;

- (instancetype)initWithURL:(NSURL *)URL;
- (instancetype)initWithTransition:(ZETAZetletActionTransition *)transition;
- (instancetype)initWithVideoID:(NSString *)videoID;
- (BOOL)isInternalAction;
- (BOOL)containsNonEmptyURL;

@end

@class ZETAZetletData;

/**
 A wrapper for all kinds of data properties of zetlets
 */
@interface ZETAZetletElementData : MTLModel <MTLJSONSerializing>

@property(nonatomic, readonly, copy) NSString *label;
@property(nonatomic, readonly, copy) NSURL *URL;
@property(nonatomic, readonly, copy) NSString *defaultElementState;
@property(nonatomic, readonly, copy) NSString *qrCode;
@property(nonatomic, readonly, copy) NSURL *placeholderURL;
@property(nonatomic, readonly, copy) NSURL *sliderImageURL;
@property(nonatomic, readonly, copy) NSURL *loadingGifURL;
@property(nonatomic, readonly, copy) NSArray *actions;
@property(nonatomic, readonly, copy) NSArray *listItems;
@property(nonatomic, readonly, copy) NSArray *nestedItems;
@property(nonatomic, readonly, copy) NSDictionary *attrs;
@property(nonatomic, readonly, copy) ZETAZetletElementData *header;
@property(nonatomic, readonly, copy) ZETAZetletElementData *footer;

// segments
@property(nonatomic, readonly, copy) NSURL *selectedIcon;
@property(nonatomic, readonly, copy) NSURL *deselectedIcon;

@property(nonatomic, readonly) ZETAZetletElementStyle *style;
@property(nonatomic, readonly) ZETAZetletElementLayout *layout;

// Composite element data
@property(nonatomic, readonly) ZETAZetletData *childZetletData;

@property(nonatomic, readonly, copy) ZETAZetletSQLQuery *sql;

@end

@interface ZETAZetletSubscribersConfig : MTLModel <MTLJSONSerializing>

@property(nonatomic, readonly, copy) NSArray *appData;

@end

@interface ZETAZetletData : MTLModel <MTLJSONSerializing>

/**
 It creates a @p ZETAZetletData object with passed values using the new template data format with
 states.
 @note It returns nil if the version sent is not supported.
 */
- (instancetype)initWithTemplateID:(NSString *)templateID
                           version:(NSString *)version
                            states:(NSDictionary *)states
                      defaultState:(NSString *)defaultState;

- (instancetype)initWithTemplateID:(NSString *)templateID
                           version:(NSString *)version
                            states:(NSDictionary *)states
                      defaultState:(NSString *)defaultState
                         analytics:(ZETAZetletAnalytics *)analytics
                            onLoad:(ZETAZetletElementData *)onLoad
                 subscribersConfig:(ZETAZetletSubscribersConfig *)subscribersConfig
                     zetletKVStore:(NSDictionary *)kvStore;

/**
 It creates a @p ZETAZetletData object for the v1 template data format without states.
 */
- (instancetype)initWithTemplateID:(NSString *)templateID elementsData:(NSDictionary *)elementsData;

- (instancetype)initWithTemplateID:(NSString *)templateID
                      elementsData:(NSDictionary *)elementsData
                         analytics:(ZETAZetletAnalytics *)analytics
                            onLoad:(ZETAZetletElementData *)onLoad
                 subscribersConfig:(ZETAZetletSubscribersConfig *)subscribersConfig;

@property(nonatomic, readonly, copy) NSString *templateID;
@property(nonatomic, readonly, copy) NSDictionary *parsedElementsData;
@property(nonatomic, readonly, copy) NSDictionary *states;
@property(nonatomic, readonly, copy) NSString *defaultState;
@property(nonatomic, readonly, copy) NSString *version;
@property(nonatomic, copy) NSString *currentState;
@property(nonatomic, readonly) ZETAZetletAnalytics *analytics;
@property(nonatomic, readonly) ZETAZetletElementData *onLoad;
@property(nonatomic, readonly, copy) ZETAZetletSubscribersConfig *subscribersConfig;

@property(nonatomic, readonly) NSDictionary *kvStore;

@end
