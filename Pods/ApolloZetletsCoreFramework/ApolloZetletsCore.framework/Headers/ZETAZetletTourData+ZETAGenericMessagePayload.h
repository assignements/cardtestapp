#import <ApolloUserStore/ZETAGenericMessagePayload.h>
#import "ZETAZetletTourData.h"

@interface ZETAZetletTourData (ZETAGenericMessagePayload)

+ (instancetype)zetletTourDataWithMessage:(ZETAGenericMessagePayload *)message;

@end
