#import <Foundation/Foundation.h>
#import "ZETAZetletManager.h"
#import "ZETAZetletTourViewController.h"
#import "ZETAZetletAuthManager.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>

@protocol ZETAZetletTourManagerDelegate;

@interface ZETAZetletTourManager : NSObject<ZETAZetletTourViewControllerDataSource,
                                            UICollectionViewDelegate, UICollectionViewDataSource>

- (instancetype)initWithTourItems:(NSArray *)tourItems
                            attrs:(NSDictionary *)attrs
                       readingMOC:(NSManagedObjectContext *)readingMOC
                      authManager:(id<ZETAZetletAuthManager>)authManager
                  actionsReceiver:(id<ZETAZetletActionsReceiver>)actionsReceiver
 prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

@property (nonatomic, weak) id<ZETAZetletTourManagerDelegate> delegate;

@end

@protocol ZETAZetletTourManagerDelegate<NSObject>

- (void)zetletTourManager:(ZETAZetletTourManager *)tourManager
     didSwipeRightToIndex:(NSInteger)index;
- (void)zetletTourManager:(ZETAZetletTourManager *)tourManager
      didSwipeLeftToIndex:(NSInteger)index;

@end
