#import <Foundation/Foundation.h>
#import "ZETAZetletAuthManager.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>
#import "ZETAZetletManager.h"
#import "ZETAAlertProvider.h"
#import "ZETAZetletMessage.h"

@protocol ZETAZetletMessageTableViewManagerDelegate;

@protocol ZETAZetletMessageTableViewManagerErrorDelegate;

@interface ZETAZetletMessageTableViewManager : NSObject

- (instancetype)initWithTableView:(UITableView *)tableView
                      authManager:(id<ZETAZetletAuthManager>)authManager
         presentingViewController:(UIViewController *)presentingViewController
                       readingMOC:(NSManagedObjectContext *)readingMOC
               tableViewCellClass:(Class)tableViewCellClass
 prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (instancetype)initWithTableView:(UITableView *)tableView
           tableViewZetletElement:(ZETAZetletElement *)zetletElement
                      authManager:(id<ZETAZetletAuthManager>)authManager
         presentingViewController:(UIViewController *)presentingViewController
                       readingMOC:(NSManagedObjectContext *)readingMOC
               tableViewCellClass:(Class)tableViewCellClass
 prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

@property (nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, weak) id<ZETAZetletMessageTableViewManagerDelegate> delegate;
@property (nonatomic, weak) id<ZETAZetletMessageTableViewManagerErrorDelegate> errorDelegate;

-(void)updateActionsReceiver:(id<ZETAZetletActionsReceiver>)actionsReceiver;

@end

@protocol ZETAZetletMessageTableViewManagerDelegate<NSObject>

- (BOOL)isZetletMessageTableViewVisible:(ZETAZetletMessageTableViewManager *)tableViewManager;

- (void)tableView:(UITableView *)tableView didScrollToEndItem:(ZETAZetletMessage *)zetletMessage;

@optional

- (void)tableView:(UITableView *)tableView didScrollUp:(BOOL)direction;

@end

@protocol ZETAZetletMessageTableViewManagerErrorDelegate<NSObject>

- (void)zetletMessageTableViewManagerDidRequestToShowErrorScreen:(ZETAZetletMessageTableViewManager *)tableViewManager;
- (void)zetletMessageTableViewManagerDidRequestToRemoveErrorScreen:(ZETAZetletMessageTableViewManager *)tableViewManager;

@end
