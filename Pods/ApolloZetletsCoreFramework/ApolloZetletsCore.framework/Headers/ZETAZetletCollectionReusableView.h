#import <UIKit/UIKit.h>
#import "ZETAZetlet.h"

@interface ZETAZetletCollectionReusableView : UICollectionReusableView

- (void)updateZetlet:(ZETAZetlet *)zetlet;

@property (nonatomic, copy) NSString *zetletMessageID;
@property (nonatomic, readonly) ZETAZetlet *zetlet;

@end
