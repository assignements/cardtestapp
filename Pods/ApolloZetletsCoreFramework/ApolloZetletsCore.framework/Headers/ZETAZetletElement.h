#import <Mantle/Mantle.h>
#import "ZETAZetletElementLayout.h"
#import "ZETAZetletElementStyle.h"

typedef NS_ENUM(NSUInteger, ZETAZetletElementType) {

  ZETAZetletElementTypeUnknown,
  ZETAZetletElementTypeText,
  ZETAZetletElementTypeImage,
  ZETAZetletElementTypeButton,
  ZETAZetletElementTypeLine,
  ZETAZetletElementTypeYoutubeVideo,
  ZETAZetletElementTypeComposite,
  ZETAZetletElementTypeList,
  ZETAZetletElementTypeTour,
  ZETAZetletElementTypeGIF,
  ZETAZetletElementTypeAttributedText,
  ZETAZetletElementTypeSegment,
  ZETAZetletElementTypeSwipe,
  ZETAZetletElementTypeNative,
  ZETAZetletElementTypeSwitch,
  ZETAZetletElementTypeCarousel
};

/**
 This class represents an element in the ZETAZetletTemplate.
 It includes layout and styling of the template.
 */
@interface ZETAZetletElement : MTLModel <MTLJSONSerializing>

@property(nonatomic) ZETAZetletElementType type;
@property(nonatomic, readonly, copy) NSString *itemID;

@property(nonatomic, readonly) ZETAZetletElementLayout *layout;
@property(nonatomic, readonly) ZETAZetletElementStyle *style;

@property(nonatomic, readonly, copy) NSString *templateID;

@end
