#import <UIKit/UIKit.h>
#import "ZETAZetletScreenBuilder.h"

FOUNDATION_EXPORT NSString *const ZETAInternalZetletActionIDKey;

@protocol ZETAZetletScreenViewControllerDelegate;
@protocol ZETAZetletScreenViewControllerDataSource;

@interface ZETAZetletScreenViewController : UIViewController

- (void)reloadView;
- (void)reloadViewWithInjectedData:(NSDictionary *)injectedData;

@property (nonatomic, weak) id<ZETAZetletScreenViewControllerDelegate> delegate;
@property (nonatomic, weak) id<ZETAZetletScreenViewControllerDataSource> dataSource;

@end

@protocol ZETAZetletScreenViewControllerDelegate<NSObject>

- (void)zetletScreenViewControllerDidPressBackButton:
    (ZETAZetletScreenViewController *)zetletScreenViewController;

@optional

- (BOOL)zetletScreenViewController:(ZETAZetletScreenViewController *)zetletScreenViewController
                      handleAction:(ZETAZetletAction *)action;

@end

@protocol ZETAZetletScreenViewControllerDataSource<NSObject>

- (NSDictionary *)injectedDataForZetletScreenViewController:
    (ZETAZetletScreenViewController *)zetletScreenViewController;

@end
