#import "NSManagedObjectContext+ZETAZetletAdditions.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>

@protocol ZETATemplateResolverDelegate;

@interface ZETATemplateResolver : NSObject

@property (nonatomic, weak) id<ZETATemplateResolverDelegate> delegate;

+ (instancetype)sharedResolver;

+ (void)reset;

- (NSString *)templateIDFromTemplateGroup:(NSString *)templateGroup
         prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (NSString *)templateGroupFromTemplateID:(NSString *)templateID;

- (NSString *)templateIDFromTemplateGroup:(NSString *)templateGroup
                          templateWrapper:(ZETAZetletTemplateWrapper *)templateWrapper;

- (NSString *)baseTemplateIDFromTemplateGroup:(NSString *)templateGroup;

@end

@protocol ZETATemplateResolverDelegate<NSObject>

@optional

- (NSString *)templateResolverDidRequestCurrentAppLanguage:(ZETATemplateResolver *)resolver;

// 'templateResolverDidRequestCurrentFieldOrderMappings' expects mappings for following keys
// 1. ifi
// 2. corpID
// 3. appID

- (NSDictionary *)templateResolverDidRequestCurrentFieldOrderMappings:(ZETATemplateResolver *)resolver;

@end
