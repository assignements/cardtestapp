#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>
#import "ZETAZetletData.h"
#import "ZETAUnderscoreJSTemplateExecutor.h"

/**
 This class create @p ZETAZetletData from a given @p templateID and @p modelObject by doing the
 substitution using the underscrore template
 */
@interface ZETAZetletDataGenerator : NSObject

- (instancetype)initWithPrioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (void)generateDataForTemplateID:(NSString *)templateID
                defaultTemplateID:(NSString *)defaultTemplateID
                      modelObject:(id<MTLJSONSerializing>)modelObject
                       completion:(ZETAResponseBlockType)completion;

- (void)generateDataForTemplateID:(NSString *)templateID
                      modelObject:(id<MTLJSONSerializing>)modelObject
                       completion:(ZETAResponseBlockType)completion;

- (void)generateDataForTemplateID:(NSString *)templateID
                           object:(NSDictionary *)object
                    zetletKVStore:(NSDictionary *)kvStore
                       completion:(ZETAResponseBlockType)completion;

- (ZETAZetletData *)generateDataForTemplateID:(NSString *)templateID
                            defaultTemplateID:(NSString *)defaultTemplateID
                                  modelObject:(id<MTLJSONSerializing>)modelObject;

- (ZETAZetletData *)generateDataForTemplateID:(NSString *)templateID
                            defaultTemplateID:(NSString *)defaultTemplateID
                                       object:(NSDictionary *)object
                  preserveStateFromZetletData:(ZETAZetletData *)oldData;

- (ZETAZetletData *)generateDataForTemplateID:(NSString *)templateID
                                       object:(NSDictionary *)object
                  preserveStateFromZetletData:(ZETAZetletData *)oldData;

- (ZETAZetletData *)generateDataForTemplateID:(NSString *)templateID
                                 templateData:(NSDictionary *)data
                                       object:(NSDictionary *)object
                  preserveStateFromZetletData:(ZETAZetletData *)oldData;

- (ZETAZetletData *)generateDataForTemplateID:(NSString *)templateID
                            defaultTemplateID:(NSString *)defaultTemplateID
                                       object:(NSDictionary *)object
                                zetletKVStore:(NSDictionary *)kvStore;

- (ZETAZetletData *)generateDataForTemplateID:(NSString *)templateID
                           templateDataString:(NSString *)dataTemplate
                                       object:(NSDictionary *)object
                                zetletKVStore:(NSDictionary *)kvStore;

- (NSString *)evaluateTemplateString:(NSString *)templateString
                            withData:(NSDictionary *)dataObject
                          templateID:(NSString*)templateID;

@end
