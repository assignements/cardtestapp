#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const ZETATemplateCollectionID;
FOUNDATION_EXPORT NSString *const ZETATemplateCollectionIDV2;
FOUNDATION_EXPORT NSString *const ZETATemplateCollectionIDV3;
