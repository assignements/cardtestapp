#import <Foundation/Foundation.h>
#import <ApolloCollectionsCore/ZETAGenericCollectionManager.h>
#import "ZETAApolloUserStore+TemplateAdditions.h"

FOUNDATION_EXPORT NSString *const TemplateUpdateNotificationName;
FOUNDATION_EXPORT NSString *const TemplateUpdateNotificationCollectionID;
FOUNDATION_EXPORT NSString *const TemplateUpdateNotificationItemID;
FOUNDATION_EXPORT NSString *const TemplateUpdateNotificationLastUpdatedTime;

@protocol ZETAZetletCollectionsManagerDelegate;

@interface ZETAZetletCollectionsManager : NSObject

@property (nonatomic, weak) id<ZETAZetletCollectionsManagerDelegate> delegate;

/**
 prioritizedTemplateCollectionIDs: list of collection ids sorted in decending order of priority.
 */
- (instancetype)initWithGenericCollectionManager:(ZETAGenericCollectionManager *)genericCollectionManager
                prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs
                                        delegate:(id<ZETAZetletCollectionsManagerDelegate>)delegate;

- (instancetype)initWithGenericCollectionManager:(ZETAGenericCollectionManager *)genericCollectionManager
                prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (void)loadTemplatesFromDiskInSync:(BOOL)sync;

@end

@protocol ZETAZetletCollectionsManagerDelegate<NSObject>

- (void)templateCollectionLoadedFromDisk:(ZETAZetletCollectionsManager *)zetletCollectionsManager
                    templateCollectionID:(NSString *)templateCollectionID;

@end
