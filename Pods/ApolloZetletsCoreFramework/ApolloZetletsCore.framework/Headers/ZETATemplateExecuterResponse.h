#import <Foundation/Foundation.h>

@interface ZETATemplateExecuterResponse : NSObject

@property (strong, nonatomic, readonly) NSString *templatedString;
@property (strong, nonatomic, readonly) NSDictionary *zetletKVStore;

- (instancetype)initWithTemplatedString:(NSString *)templatedString
                          zetletKVStore:(NSDictionary *)zetletKVStore;

@end

