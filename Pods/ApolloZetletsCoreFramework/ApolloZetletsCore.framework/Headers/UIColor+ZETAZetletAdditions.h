#import <UIKit/UIKit.h>

@interface UIColor (ZETAZetletAdditions)

+ (UIColor *)zeta_defaultTextColor;

+ (UIColor *)zeta_defaultBackgroundColor;

+ (UIColor *)zeta_colorFromHexValue:(NSString *)hexString;

@end
