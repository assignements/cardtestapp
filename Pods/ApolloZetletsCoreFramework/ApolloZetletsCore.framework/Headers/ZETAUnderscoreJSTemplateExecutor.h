#import <Foundation/Foundation.h>
#import "ZETATemplateExecuterResponse.h"

FOUNDATION_EXPORT NSString *const ZETAZetletLogsNotification;

/**
 This class lets you compile a underscoreJS template string with the given model object.
 For more checkout: http://underscorejs.org/#template
 */
@interface ZETAUnderscoreJSTemplateExecutor : NSObject

/**
 Loads the given template string using the _.template function.
 
 @param templateString template string to be compiled.
 @param dataObject the data object for the compiled template.
 
 @returns the final string after applying the data on the templatingString
 */

/**
 
 Additional Data requirement.
 ZETAUnderscoreJSTemplateExecutor adds additional data provided by
 ZETAZetletAdditionalDataProvider for templating purposes.
 
 **/

- (ZETATemplateExecuterResponse *)loadTemplateString:(NSString *)templateString
                                      withDataObject:(NSDictionary *)dataObject
                                          templateID:(NSString *)templateID
                                       zetletKVStore:(NSDictionary *)kvStore;

@end

