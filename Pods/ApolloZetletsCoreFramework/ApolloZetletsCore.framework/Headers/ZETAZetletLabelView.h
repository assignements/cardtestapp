#import <UIKit/UIKit.h>
#import "ZETAZetletElement.h"
#import "ZETAZetletElementView.h"
#import <CocoaNuggetsViews/Views.h>

@interface ZETAZetletLabelView : ZETALabel<ZETAZetletElementView>

@end
