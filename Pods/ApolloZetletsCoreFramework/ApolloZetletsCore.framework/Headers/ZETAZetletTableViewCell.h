#import <UIKit/UIKit.h>
#import "ZETAZetlet.h"

FOUNDATION_EXPORT CGFloat const ZETAZetletTableViewCellLeftPadding;

@interface ZETAZetletTableViewCell : UITableViewCell

- (void)updateCellSeparatorHeight:(CGFloat)height;
+ (CGFloat)cellSeparatorHeight;
- (void)setupView;
- (void)updateZetlet:(ZETAZetlet *)zetlet;

@property (nonatomic) ZETAZetlet *zetlet;
@property (nonatomic, copy) NSString *zetletUniqueMessageKey;
@property (nonatomic) UIView *zetletContainerView;
@property (nonatomic) NSLayoutConstraint *separatorHeightConstraint;
@property (nonatomic) NSLayoutConstraint *leadingConstraint;

@end
