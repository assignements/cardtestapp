#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import <KLCPopup/KLCPopup.h>
#import "ZETAZetlet.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>
#import "ZETAZetletAuthManager.h"
#import "ZETAAlertProvider.h"

FOUNDATION_EXPORT CGFloat const ZeroDimmedMaskAlpha;
FOUNDATION_EXPORT CGFloat const DimmedOverlayAlpha;

@protocol ZETAZetletPopupPresenterDelegate;

@interface ZETAZetletPopupPresenter : NSObject

- (instancetype)initWithReadingMOC:(NSManagedObjectContext *)readingMOC
                   apolloUserStore:(ZETAApolloUserStore *)apolloUserStore
                       authManager:(id<ZETAZetletAuthManager>)authManager
  prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (void)presentPopupWithTemplateID:(NSString *)templateID
                      templateData:(NSDictionary *)templateData
                            object:(MTLModel<MTLJSONSerializing> *)object
                  objectDictionary:(NSDictionary *)objectDictionary
                  horizontalMargin:(CGFloat)margin
                               top:(CGFloat)top
                              view:(UIView *)view
                          duration:(NSTimeInterval)duration
               dimmingOverlayAlpha:(CGFloat)dimmingOverlayAlpha
    shouldDismissOnBackgroundTouch:(BOOL)shouldDimissOnBackgroundTouch;

- (void)presentPopupWithTemplateID:(NSString *)templateID
                      templateData:(NSDictionary *)templateData
                            object:(MTLModel<MTLJSONSerializing> *)object
                  horizontalMargin:(CGFloat)margin
                      centerToView:(UIView *)view
               dimmingOverlayAlpha:(CGFloat)dimmingOverlayAlpha
          presentingViewController:(UIViewController *)presentingViewController;

- (void)presentPopupWithTemplateID:(NSString *)templateID
                      templateData:(NSDictionary *)templateData
                            object:(MTLModel<MTLJSONSerializing> *)object
                      bottomMargin:(CGFloat)bottomMargin
               dimmingOverlayAlpha:(CGFloat)dimmingOverlayAlpha
          presentingViewController:(UIViewController *)presentingViewController;

- (BOOL)presentFullscreenPopupWithTemplateID:(NSString *)templateID
                                      object:(NSDictionary *)object
                                     timeout:(NSTimeInterval)timeout;

- (BOOL)presentFullscreenPopupWithTemplateID:(NSString *)templateID
                                      object:(NSDictionary *)object
                                    showType:(KLCPopupShowType)showType
                                 dismissType:(KLCPopupDismissType)dismissType;

- (BOOL)presentFullscreenPopupWithTemplateID:(NSString *)templateID
                                      object:(NSDictionary *)object
                            inContainerFrame:(CGRect)containerFrame
                                    showType:(KLCPopupShowType)showType
                                 dismissType:(KLCPopupDismissType)dismissType
                    presentingViewController:(UIViewController *)presentingViewController;

- (void)dismissPopupWithTemplateID:(NSString *)templateID animated:(BOOL)animated;
- (void)dismiss:(BOOL)animated;
- (void)hide:(BOOL)hide;
- (void)invalidateTimer;

@property (nonatomic, weak) id<ZETAZetletPopupPresenterDelegate> delegate;
@property (nonatomic, weak) id<ZETAZetletDelegate> zetletDelegate;
@property (nonatomic) BOOL shouldNotAllowContentTouch;

@end

@protocol ZETAZetletPopupPresenterDelegate<NSObject>

@optional

- (void)zetletPopupPresenterWillDismiss:(ZETAZetletPopupPresenter *)zetletPopupPresenter;
- (void)zetletPopupPresenterDidFinishShowingCompletion:
    (ZETAZetletPopupPresenter *)zetletPopupPresenter;

@end
