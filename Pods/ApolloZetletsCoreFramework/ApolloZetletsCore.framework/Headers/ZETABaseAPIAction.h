#import <Mantle/Mantle.h>

typedef NS_ENUM(NSUInteger, ZETABaseAPIActionResponseType) {
  ZETABaseAPIActionResponseUnknownType,
  ZETABaseAPIActionResponseSuccessType,
  ZETABaseAPIActionResponseCancelledType,
  ZETABaseAPIActionResponseFailureType
};

@interface ZETABaseAPIAction : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *responseKey;
@property (nonatomic, readonly) NSArray *viewsToRefresh;
@property (nonatomic, readonly) NSDictionary *params;
@property (nonatomic, readonly) NSArray *successActions;
@property (nonatomic, readonly) NSArray *failureActions;

@end

@interface ZETABaseAPIActionResponse : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) ZETABaseAPIActionResponseType status;
@property (nonatomic, readonly) id response;
@property (nonatomic, readonly) ZETABaseAPIAction *baseAPIaction;

- (instancetype)initWithStatus:(ZETABaseAPIActionResponseType)status
                      response:(id)response
                 baseAPIAction:(ZETABaseAPIAction *)baseAPIaction;

@end
