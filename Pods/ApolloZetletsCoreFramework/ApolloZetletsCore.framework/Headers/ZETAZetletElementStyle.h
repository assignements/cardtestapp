#import <Mantle/Mantle.h>
#import <ApolloCommonsUIKit/ApolloUIKit.h>
#import "ZETAZetletElementLayout.h"

typedef NS_ENUM(NSUInteger, ZETAZetletTextOverflowType) { ZETAZetletTextOverflowTypeNone,
                                                          ZETAZetletTextOverflowTypeEllipse,
                                                          ZETAZetletTextOverflowTypeMarquee
};

typedef NS_ENUM(NSUInteger, ZETAZetletListSeparatorStyle) { ZETAZetletListSeparatorStyleNone,
                                                            ZETAZetletListSeparatorStyleLine
};

@interface ZETAZetletElementStyle : MTLModel <MTLJSONSerializing>

@property(nonatomic, readonly) NSNumber *numberOfLines;

@property(nonatomic, readonly) NSNumber *textSize;
@property(nonatomic, readonly) NSNumber *minimumScaleFactor;
@property(nonatomic, readonly) NSNumber *lineHeight;
@property(nonatomic, readonly) NSNumber *letterSpacing;
@property(nonatomic, readonly) NSTextAlignment align;
@property(nonatomic, readonly) NSTextAlignment alignView;
@property(nonatomic, readonly) NSString *font;
@property(nonatomic, readonly) UIColor *borderColor;
@property(nonatomic, readonly) NSNumber *borderWidth;
@property(nonatomic, readonly) NSNumber *cornerRadius;
@property(nonatomic, readonly) UIColor *backgroundColor;
@property(nonatomic, readonly) UIColor *highlightedBackgroundColor;
@property(nonatomic, readonly) UIColor *listSeparatorColor;
@property(nonatomic, readonly) UIColor *sliderBackgroundColor;

@property(nonatomic, readonly) NSNumber *inlineImageWidth;
@property(nonatomic, readonly) NSNumber *inlineImageHeight;

@property(nonatomic, readonly) ZETAZetletTextOverflowType overflow;
@property(nonatomic, readonly) ZETAZetletListSeparatorStyle listSeparatorStyle;

@property(nonatomic, readonly) UIViewContentMode contentMode;

@property(nonatomic, readonly) BOOL imageDesaturation;
@property(nonatomic, readonly) BOOL scrollEnabled;
@property(nonatomic, readonly) BOOL bounces;

@property(nonatomic, readonly) BOOL useFullWidth;

@property(nonatomic, readonly) BOOL fadeSliderWhileSwiping;
@property(nonatomic, readonly) BOOL animateSlider;

@property(nonatomic, readonly) NSNumber *nextItemPreviewWidthFactor;

@property(nonatomic, readonly) ZETAZetletElementVisibility visible;

// Segment View attributes
@property(nonatomic, readonly) NSString *segmentType;
@property(nonatomic, readonly)
    HMSegmentedControlSelectionIndicatorLocation selectionIndicatorLocation;
@property(nonatomic, readonly) NSNumber *selectedTextSize;
@property(nonatomic, readonly) UIColor *selectedTextColor;
@property(nonatomic, readonly) NSString *selectedFont;
@property(nonatomic, readonly) UIColor *selectionIndicatorColor;
@property(nonatomic, readonly) NSNumber *selectionIndicatorWidth;

// Theme Based Font
@property(nonatomic, readonly) NSString *themedFont;
@property(nonatomic, readonly) NSString *themedSelectedFont;

// Switch button attributes
@property(nonatomic, readonly) UIColor *switchThumbColorEnabled;
@property(nonatomic, readonly) UIColor *switchThumbColorDisabled;
@property(nonatomic, readonly) UIColor *switchTrackColorEnabled;
@property(nonatomic, readonly) UIColor *switchTrackColorDisabled;
// List/Grid attributes
@property (nonatomic, readonly) ZETAZetletElementVisibility verticalScrollIndicator;
@property (nonatomic, readonly) ZETAZetletElementVisibility horizontalScrollIndicator;

//carousel properties
@property (nonatomic, readonly) BOOL displayPageControl;
@property (nonatomic, readonly) BOOL enableCircularPaging;
@property (nonatomic, readonly) UIColor *pagingSelectedColor;
@property (nonatomic, readonly) UIColor *pagingUnselectedColor;
@property (nonatomic, readonly) NSString *themedPagingSelectedColor;
@property (nonatomic, readonly) NSString *themedPagingUnselectedColor;
@property (nonatomic, readonly) UIColor *pageControlBackgroundColor;
@property (nonatomic, readonly) NSString *themedPageControlBackgroundColor;
@property (nonatomic, readonly) UIColor *pageControlBorderColor;
@property (nonatomic, readonly) NSString *themedPageControlBorderColor;
@property (nonatomic, readonly) NSNumber *pageControlBorderWidth;
@property (nonatomic, readonly) NSNumber *pageControlCornerRadius;

// FIXME: if the align is not send in the newStyle, then it is overridden with NSTextAlignmentLeft,
// there is no easy way to fix. For now, send the align if you want to keep it.
/**
 Mixes up the existing style with the new style. Overrides the existing values(if any).

 @note If the newStyle contains @p textColor then it overrides the @p textColorWithOpacity also
 to make the effect visible. Same for bgColor and others with opacity variants.
 */
- (instancetype)styleByMergingStyle:(ZETAZetletElementStyle *)style;

- (UIColor *)fetchThemedBackgroundColor;
- (UIColor *)fetchThemedBorderColor;
- (UIColor *)fetchThemedTextColor;
- (UIColor *)fetchThemedListSeparatorColor;
- (UIColor *)fetchThemedSliderBackgroundColor;
- (UIColor *)fetchThemedHighlightedBackgroundColor;
- (UIColor *)fetchThemedSelectedTextColor;
- (UIColor *)fetchThemedSelectionIndicatorColor;
- (HMSegmentedControlType)getTransformedSegmentType;
- (UIColor *)fetchThemedAfterSlideBackgroundColor;

- (UIColor *)fetchThemedSwitchThumbColorEnabled;
- (UIColor *)fetchThemedSwitchThumbColorDisabled;
- (UIColor *)fetchThemedSwitchTrackColorEnabled;
- (UIColor *)fetchThemedSwitchTrackColorDisabled;

- (UIColor *)fetchThemedPagingSelectedColor;
- (UIColor *)fetchThemedPagingUnselectedColor;
- (UIColor *)fetchThemedPageControlBackgroundColor;
- (UIColor *)fetchThemedPageControlBorderColor;

@end
