#import <Mantle/Mantle.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

@interface ZetletAppDataAttributes: MTLModel<MTLJSONSerializing, ZETAOriginalPayloadModelProtocol>

@property (nonatomic, readonly) NSNumber *sortOrder;

+ (instancetype)zetletDataAttributesFromDictionary:(NSDictionary *)dictionary;

@end
