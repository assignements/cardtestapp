#import <UIKit/UIKit.h>
#import "ZETAZetletActionsReceiver.h"
#import "ZETAZetletData.h"
#import "ZETATapGestureRecognizer.h"

@protocol ZETAZetletViewDelegate;

/**
 This view acts as a UIView represenation of the Zetlet.
 
 It acts as receiver of all actions of all elements inside zetlet and forwards them through
 delegate.
 */
@interface ZETAZetletView : UIView<ZETAZetletActionsReceiver>

- (instancetype)initWithZetletElement:(ZETAZetletElement *)zetletElement;

// TODO: Make it conform ZETAZetletElementView
@property (nonatomic, readonly) ZETAZetletElement *zetletElement;

@property (nonatomic, weak) id<ZETAZetletViewDelegate> delegate;

@end

@protocol ZETAZetletViewDelegate<NSObject>

- (void)zetletView:(UIView *)view
didReceiveTapAction:(ZETATapGestureRecognizer *)recognizer
     tapActionData:(ZETAZetletAction *)action;

- (void)zetletView:(UIView *)view
         childView:(UIView *)childView
didReceivePlayForYoutubeVideoID:(NSString *)videoID;

- (void)zetletView:(UIView *)view
didReceiveSwipeAction:(ZETAZetletAction *)action;

- (void)zetletView:(UIView *)view
didReceiveSwitchingAction:(ZETAZetletAction *)action;

- (void)zetletView:(UIView *)view
didReceiveExternalViewRefreshAction:(ZETAZetletAction *)action;

@end

