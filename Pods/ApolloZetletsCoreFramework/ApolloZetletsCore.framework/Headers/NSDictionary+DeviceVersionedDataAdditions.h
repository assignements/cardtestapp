#import <Foundation/Foundation.h>

@interface NSDictionary (DeviceVersionedDataAdditions)

+ (NSDictionary *)deviceVersionedDataFromDictionary:(NSDictionary *)payload;

@end
