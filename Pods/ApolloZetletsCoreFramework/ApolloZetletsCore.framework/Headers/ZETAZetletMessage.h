#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>
#import <ApolloCollectionsCore/ZETACollectionModelClass.h>
#import <ApolloCollectionsCore/ZETACollectionsMessage.h>
#import <ApolloUserStore/ZETAGenericMessageAttributes.h>
#import <ApolloUserStore/ZETAGenericMessagePayload.h>
#import <ApolloCollectionsCore/ZETACollectionItem.h>
#import "ZETAZetletManager.h"

typedef NS_ENUM(NSUInteger, ZETAZetletMessageErrorCode) {
  ZETAZetletMessageErrorCodeInsufficientInfo
};

@interface ZETAZetletMessage : NSManagedObject<ZETACollectionModelClassProtocol>

@property (nonatomic, retain) NSString* from;
@property (nonatomic, retain) NSString* to;
@property (nonatomic, retain) NSString* messageID;
@property (nonatomic, retain) NSString* collectionID;
@property (nonatomic, retain) NSString* collectionItemID;
@property (nonatomic, retain) NSString* lastUpdateTime;
@property (nonatomic, retain) ZETAGenericMessageAttributes* attributes;
@property (nonatomic, retain) NSDictionary* payloads;
@property (nonatomic) BOOL isUnread;
@property (nonatomic, retain) NSString* templateID;
@property (nonatomic, retain) NSString* templateVersion;
@property (nonatomic) BOOL isTemplateUsable;
@property (nonatomic, retain) NSDate* expiry;
@property (nonatomic, retain) NSString* dataAttributes;
@property (nonatomic, retain) NSString* templateData;

+ (NSString*)entityName;

+ (NSArray*)messagesFilteredUsingPredicte:(NSPredicate*)predicate MOC:(NSManagedObjectContext*)MOC;

+ (NSUInteger)countOfUnreadMessagesForCollectionIDs:(NSArray*)collectionIDs
                                            withMOC:(NSManagedObjectContext*)MOC;

+ (instancetype)messageWithCollectionID:(NSString*)collectionID
                                 itemID:(NSString*)itemID
                                    MOC:(NSManagedObjectContext*)MOC;

+ (NSArray*)messagesWithCollectionIDs:(NSArray*)collectionIDs
                           templateID:(NSString*)templateID
                                  MOC:(NSManagedObjectContext*)MOC;

- (void)updateWithCollectionID:(NSString *)collectionID
                           MOC:(NSManagedObjectContext *)MOC
                 zetletManager:(ZETAZetletManager *)zetletManager
               apolloUserStore:(ZETAApolloUserStore *)apolloUserStore
prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (void)updateWithCollectionID:(NSString *)collectionID
                           MOC:(NSManagedObjectContext *)MOC
                 zetletManager:(ZETAZetletManager *)zetletManager
                collectionItem:(ZETACollectionItem *)collectionItem
               apolloUserStore:(ZETAApolloUserStore *)apolloUserStore
prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

@end
