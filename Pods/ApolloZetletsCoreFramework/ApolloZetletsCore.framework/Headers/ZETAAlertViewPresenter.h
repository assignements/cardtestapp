#import <Foundation/Foundation.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

@interface ZETAAlertViewPresenter : NSObject

- (void)presentAlertView:(UIView *)alertView;

- (void)dismissAlertViewAnimated:(BOOL)animated completion:(ZETAEmptyBlockType)completion;

@end
