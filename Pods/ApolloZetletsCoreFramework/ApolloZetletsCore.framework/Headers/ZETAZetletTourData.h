#import "ZETAZetletData.h"

@interface ZETAZetletTourData : NSObject

- (instancetype)initWithZetletData:(ZETAZetletData *)zetletData pageIndex:(NSUInteger)pageIndex;

@property (nonatomic, readonly) ZETAZetletData *zetletData;
@property (nonatomic, readonly) NSUInteger pageIndex;

@end
