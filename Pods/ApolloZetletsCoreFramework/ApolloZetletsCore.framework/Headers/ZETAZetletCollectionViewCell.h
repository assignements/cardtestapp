#import <UIKit/UIKit.h>
#import "ZETAZetlet.h"

@interface ZETAZetletCollectionViewCell : UICollectionViewCell

- (void)updateZetlet:(ZETAZetlet *)zetlet;

@property (nonatomic, readonly) ZETAZetlet *zetlet;
@property (nonatomic, copy) NSString *zetletMessageID;

@end
