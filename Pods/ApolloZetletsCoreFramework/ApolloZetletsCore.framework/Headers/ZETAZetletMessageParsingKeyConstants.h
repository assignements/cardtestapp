#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const ZETAAttributeValueTemplateData;
FOUNDATION_EXPORT NSString *const ZETAAttributeValuePayload1;
FOUNDATION_EXPORT NSString *const ZETAAttributeValueDataAttributes;
FOUNDATION_EXPORT NSString *const ZETAAttributeValueVersionedTemplateData;
