#import <UIKit/UIKit.h>
#import "ZETAZetletData.h"
#import "ZETAZetletElement.h"
#import "ZETAZetletElementView.h"

@interface ZETAZetletButtonView : UIButton<ZETAZetletElementView>

@end
