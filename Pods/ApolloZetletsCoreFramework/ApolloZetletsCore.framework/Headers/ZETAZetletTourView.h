#import <UIKit/UIKit.h>
#import "ZETAZetletElementView.h"
#import "ZETAZetletManager.h"
#import "ZETAZetletAuthManager.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>

@interface ZETAZetletTourView : UIView<ZETAZetletElementView>

- (instancetype)initWithZetletManager:(ZETAZetletManager *)zetletManager
                        zetletElement:(ZETAZetletElement *)element
                          authManager:(id<ZETAZetletAuthManager>)authManager
                           readingMOC:(NSManagedObjectContext *)readingMOC
     prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

@end
