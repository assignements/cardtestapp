#import <UIKit/UIKit.h>
#import "ZETAZetletData.h"
#import "ZETAZetletElement.h"
#import "ZETAZetletActionsReceiver.h"

@protocol ZETAZetletElementView

- (instancetype)initWithZetletElement:(ZETAZetletElement *)element;

- (ZETAZetletElement *)zetletElement;

- (void)configureWithElementData:(ZETAZetletElementData *)elementData
                 actionsReceiver:(id<ZETAZetletActionsReceiver>)actionsReceiver;

@end
