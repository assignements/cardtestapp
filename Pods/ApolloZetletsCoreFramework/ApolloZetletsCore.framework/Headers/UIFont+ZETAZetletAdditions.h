#import <UIKit/UIKit.h>
#import "ZETAZetletElement.h"

@interface UIFont (ZETAZetletAdditions)

+ (UIFont *)zeta_fontWithZetletStyle:(ZETAZetletElementStyle *)style;

+ (UIFont *)zeta_selectedFontWithZetletStyle:(ZETAZetletElementStyle *)style;

+ (UIFont *)zeta_italicFontWithZetletStyle:(ZETAZetletElementStyle *)style;

+ (UIFont *)zeta_italicFontWithFont:(UIFont *)font;

+ (UIFont *)zeta_boldFontWithZetletStyle:(ZETAZetletElementStyle *)style;

+ (UIFont *)zeta_boldFontWithFont:(UIFont *)font;

@end
