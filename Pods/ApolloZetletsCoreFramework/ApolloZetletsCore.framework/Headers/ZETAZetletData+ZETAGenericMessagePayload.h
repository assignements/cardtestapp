#import <ApolloUserStore/ZETAGenericMessagePayload.h>
#import "ZETAZetletData.h"

@interface ZETAZetletData (ZETAGenericMessagePayload)

+ (instancetype)zetletDataWithMessage:(ZETAGenericMessagePayload *)message;

+ (NSDictionary *)templateDataFromPayload1:(NSDictionary *)payload1;

@end
