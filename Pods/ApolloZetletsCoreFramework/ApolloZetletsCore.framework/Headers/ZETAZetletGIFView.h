#import <FLAnimatedImage/FLAnimatedImage.h>
#import <UIKit/UIKit.h>
#import "ZETAZetletData.h"
#import "ZETAZetletElement.h"
#import "ZETAZetletElementView.h"

@interface ZETAZetletGIFView : FLAnimatedImageView<ZETAZetletElementView>

@end
