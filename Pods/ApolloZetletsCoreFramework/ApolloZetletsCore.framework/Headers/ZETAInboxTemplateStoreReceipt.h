#import <Mantle/Mantle.h>
#import <ApolloCurrency/ZETACurrency.h>

@interface ZETAClaimReceiptInfo : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly, copy) NSString *storeName;
@property (nonatomic, readonly, copy) NSString *storeLocation;
@property (nonatomic, readonly, copy) ZETACurrency *txnAmount;
@property (nonatomic, readonly, copy) NSString *txnStatus;

@end

typedef NS_ENUM(NSUInteger, ZETATapActionMedium) {
  ZETATapActionMediumAlert,
  ZETATapActionMediumUnknown
};

typedef NS_ENUM(NSUInteger, ZETATapActionType) {
  ZETATapActionTypeShowText,
  ZETATapActionTypeUnknown
};

@interface ZETAInboxItemTapAction : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) ZETATapActionType type;
@property (nonatomic, readonly) ZETATapActionMedium medium;
@property (nonatomic, readonly, copy) NSString *text;

@end

@interface ZETAInboxItemPresentationMarkup : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly, copy) NSString *statusColor;
@property (nonatomic, readonly) ZETAInboxItemTapAction *tapAction;

@end

@interface ZETAInboxTemplateStoreReceipt : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) ZETAClaimReceiptInfo *claimInfo;
@property (nonatomic, readonly) ZETAInboxItemPresentationMarkup *presentationMarkup;

@end
