#import <Foundation/Foundation.h>
#import "ZETAAlertButtonParams.h"
#import "ZETAZetletAuthManager.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>

@protocol ZETAAlertProviderDelegate;

@interface ZETAAlertProvider : NSObject

@property(nonatomic, weak) id<ZETAAlertProviderDelegate> delegate;

+ (instancetype)sharedProvider;
+ (void)setupSharedProviderWithAuthManager:(id<ZETAZetletAuthManager>)authManager
                                readingMOC:(NSManagedObjectContext *)readingMOC;
+ (void)resetSharedProvider;

- (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
                   buttons:(NSArray<ZETAAlertButtonParams *> *)buttons
prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
                  imageURL:(NSURL *)imageURL
                   buttons:(NSArray<ZETAAlertButtonParams *> *)buttons
prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (void)showAlertWithMessage:(NSString *)message
                     buttons:(NSArray<ZETAAlertButtonParams *> *)buttons
prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (void)showSystemAlertWithTitle:(NSString *)title
                         message:(NSString *)message
                         buttons:(NSArray<ZETAAlertButtonParams *> *)buttons;

+ (void)zeta_showAlertViewWithMessage:(NSString *)message
     prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs
                           completion:(ZETAEmptyBlockType)completionBlock;

+ (void)zeta_showAlertViewWithMessage:(NSString *)message
     prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

+ (void)zeta_showAlertViewWithTitle:(NSString *)title
                            message:(NSString *)message
   prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

+ (void)zeta_showAlertViewWithTitle:(NSString *)title
                            message:(NSString *)message
   prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs
                    completionBlock:(ZETAEmptyBlockType)completionBlock;

+ (void)zeta_showAlertViewWithTitle:(NSString *)title
                            message:(NSString *)message
                           imageURL:(NSURL *)imageURL
   prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs
                    completionBlock:(ZETAEmptyBlockType)completionBlock;

@end

@protocol ZETAAlertProviderDelegate <NSObject>

-(BOOL)zetaAlertProviderShouldShowZetletAlerts:(ZETAAlertProvider *)alertProvider;

@end
