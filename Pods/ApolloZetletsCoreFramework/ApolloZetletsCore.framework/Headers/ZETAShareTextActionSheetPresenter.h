#import <Foundation/Foundation.h>

@interface ZETAShareTextActionSheetPresenter : NSObject

+ (void)presentShareOptionsWithString:(NSString *)shareString
                   fromViewController:(UIViewController *)presentingViewController;

@end

