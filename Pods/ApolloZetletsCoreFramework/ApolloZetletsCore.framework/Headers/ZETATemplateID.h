#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const ZETASimpleAlertTemplateIDFormat;
FOUNDATION_EXPORT NSString *const ZETASingleMessageAlertTemplateIDFormat;
FOUNDATION_EXPORT NSString *const ZETAImageAlertTemplateIDFormat;
