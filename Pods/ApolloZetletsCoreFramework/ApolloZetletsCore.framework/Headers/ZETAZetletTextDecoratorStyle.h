#import <Mantle/Mantle.h>

@interface ZETAZetletTextDecoratorStyle : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSNumber *textSize;
@property (nonatomic, readonly) UIColor *textColor;
@property (nonatomic, readonly) NSString *font;
@property (nonatomic, readonly) NSURL *URL;

@end
