#import <Foundation/Foundation.h>
#import "ZETAZetletView.h"
#import "ZETAZetletAuthManager.h"
#import "ZETAZetletManager.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>
#import "ZETAZetletTemplate.h"

@protocol ZETAZetletDelegate;

@interface ZETAZetlet: NSObject

@property (nonatomic, weak) id<ZETAZetletDelegate> delegate;
@property (nonatomic, weak) id<ZETAZetletActionsReceiver> actionsReceiver;

@property (nonatomic) ZETAZetletView *view;
@property (nonatomic, readonly) NSString *currentState;
@property (nonatomic, readonly) ZETAZetletData *data;
@property (nonatomic, readonly) NSDictionary *injectedData;

/**
prioritizedTemplateCollectionIDs: list of collection ids to be used for templates, sorted in decending order of priority.
*/

- (instancetype)initWithTemplateID:(NSString *)templateID
                      injectedData:(NSDictionary *)injectedData
                        zetletData:(ZETAZetletData *)data
                 defaultTemplateID:(NSString *)defaultTemplateID
          presentingViewController:(UIViewController *)presentingViewController
                        readingMOC:(NSManagedObjectContext *)readingMOC
                       authManager:(id<ZETAZetletAuthManager>)authManager
               shouldConfigureData:(BOOL)shouldConfigureData
  prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (instancetype)initWithTemplateID:(NSString *)templateID
                      injectedData:(NSDictionary *)injectedData
                        zetletData:(ZETAZetletData *)data
                      templateData:(NSDictionary *)templateData
          presentingViewController:(UIViewController *)presentingViewController
                        readingMOC:(NSManagedObjectContext *)readingMOC
                       authManager:(id<ZETAZetletAuthManager>)authManager
               shouldConfigureData:(BOOL)shouldConfigureData
  prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (instancetype)initWithTemplateWrapper:(ZETAZetletTemplateWrapper *)templateWrapper
                             readingMOC:(NSManagedObjectContext *)readingMOC
                            authManager:(id<ZETAZetletAuthManager>)authManager
       prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (void)performAction:(ZETAZetletAction *)action;

- (void)updateInjectedData:(NSDictionary *)additionalData
                andRefresh:(BOOL)shouldRefresh;

- (void)updateZetletData:(ZETAZetletData *)updatedZetletData
              andRefresh:(BOOL)shouldRefresh;

- (void)handleApiActionResponse:(NSDictionary *)response
             andResponseActions:(NSArray *)responseActions;

- (void)configureForCurrentData;

@end

@protocol ZETAZetletDelegate<NSObject>

@optional

- (BOOL)zetlet:(ZETAZetlet *)zetlet
  handleAction:(ZETAZetletAction *)action;

// NOTE: Any consumer that maintains a copy of zetlet data or injected data separately (like lists) need
//       to implement this so that data inside consumer and inside zetlet can stay in sync.
//       This gets invoked when either injectedData or zetletData gets modified due to
//       to internal causes like API calls or inline transitions.
- (void)zetletDidUpdateData:(ZETAZetlet *)zetlet;

@end

