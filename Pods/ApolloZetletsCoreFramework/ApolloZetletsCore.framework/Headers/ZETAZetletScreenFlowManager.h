#import <ApolloDeeplinkResolver/ZETABaseFlowManager.h>
#import "ZETAZetletAuthManager.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZETAZetletScreenFlowManager : ZETABaseFlowManager

- (instancetype)initWithURL:(NSURL *)URL
                authManager:(id<ZETAZetletAuthManager>) authmanager
            apolloUserStore:(ZETAApolloUserStore *)apolloUserStore
                 readingMOC:(NSManagedObjectContext *)readingMOC
prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;


@end

NS_ASSUME_NONNULL_END
