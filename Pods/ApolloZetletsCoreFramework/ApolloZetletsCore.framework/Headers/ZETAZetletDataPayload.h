#import <Mantle/Mantle.h>
#import "ZETAZetletData.h"

@interface ZETAZetletDataPayload : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *version;
@property (nonatomic, readonly) NSString *defaultState;
@property (nonatomic, readonly) NSDictionary *states;
@property (nonatomic, readonly) ZETAZetletAnalytics *analytics;
@property (nonatomic, readonly) ZETAZetletElementData *onLoad;
@property (nonatomic, readonly) ZETAZetletSubscribersConfig *subscribersConfig;

@end
