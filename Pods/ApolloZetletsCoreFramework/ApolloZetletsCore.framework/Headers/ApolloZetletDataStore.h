#import <Foundation/Foundation.h>
#import <ApolloUserStore/ZETAApolloUserStore.h>
#import <CoreData/CoreData.h>

@interface ApolloZetletDataStore : NSObject

@property (nonatomic, readonly) ZETAApolloUserStore *zetletUserStore;
@property (nonatomic, readonly) NSManagedObjectContext *zetletMainMOC;
@property (nonatomic, readonly) NSManagedObjectContext *zetletBackgroundMOC;
@property (nonatomic, readonly) NSManagedObjectContext *zetletBackgroundReadingMOC;

+ (instancetype)sharedInstance;
+ (void)resetSharedInstance;

@end
