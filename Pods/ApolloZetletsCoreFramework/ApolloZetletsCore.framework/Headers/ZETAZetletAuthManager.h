@protocol ZETAZetletAuthManager <NSObject>

- (NSString *)sessionAuthToken;

@end
