#import <Foundation/Foundation.h>
#import <ApolloDoorStream/ZETABaseDoorServiceClient.h>
#import <ApolloCollectionsCore/ZETACollectionsMessage.h>
#import <ApolloDoorStream/ZETADoorStreamManager.h>
#import <ApolloCollectionsCore/ZETAGetCollectionItemRequestPayload.h>
#import <ApolloCollectionsCore/ZETAGetCollectionRequestPayload.h>
#import <ApolloCollectionsCore/ZETAGetLatestCollectionItemsResponsePayload.h>
#import <ApolloCollectionsCore/ZETAGetLatestCollectionRequestPayload.h>
#import <ApolloCollectionsCore/ZETASetCollectionItemRequest.h>
#import <ApolloCollectionsCore/ZETASetMultipleCollectionItemsRequest.h>
#import <ApolloCollectionsCore/ZETASubscribeRequest.h>
#import <ApolloCollectionsCore/ZETAUnSubscribeRequest.h>
#import <ApolloCollectionsCore/ZETACollectionClientProtocol.h>

FOUNDATION_EXPORT NSString *const ZETACollectionsClientErrorDomain;

typedef void (^ZETAGetLatestCollectionItemsResponseBlock)(
    ZETAGetLatestCollectionItemsResponsePayload *response);
typedef void (^ZETACollectionsResponseBlock)(ZETACollectionItemDiff *collectionItemDiff,
                                             id collectionItem);
typedef void (^ZETAGetLatestCollectionItemsFragmentResponseBlock)(
    ZETAStringCollectionItem *collectionItem, id itemPayload);
typedef void (^ZETACollectionsCompletionBlock)(ZETACollectionsCompletionMessage *payload);
typedef void (^ZETACollectionFileLoadResponse)(ZETACollectionsDump *collectionsDump);
typedef void (^ZETAGetCollectionItemResponseBlock)(id collectionItem);

@class ZETAPushChannel;

@interface ZETACollectionsClient : ZETABaseDoorServiceClient <ZETACollectionClientProtocol>

- (instancetype)initWithDoorStream:(ZETADoorStreamManager *)doorStream
                  analyticsManager:(ApolloAnalyticsManager *)analyticsManager;

/**
 Ask the collection service to send updates for a given collectionID, from a given
 collectionVersion.
 */
- (void)triggerCollectionDiffForCollectionsRequest:(ZETAGetCollectionRequestPayload *)requestPayload
                                requestUniquingTag:(NSString *)requestUniquingTag
                                         itemClass:(Class)itemClass
                                       diffHandler:(ZETACollectionsResponseBlock)diffHandler
                                           success:(ZETACollectionsCompletionBlock)success
                                           failure:(ZETAFailureBlockType)failure;

/**
 Ask the collection service to send a fragmented response of latest collectionItems for a given
 collectionID
 */
- (void)
triggerLatestCollectionItemsDiffForRequest:(ZETAGetLatestCollectionRequestPayload *)requestPayload
                        requestUniquingTag:(NSString *)requestUniquingTag
                                 itemClass:(Class)itemClass
                               diffHandler:
                                   (ZETAGetLatestCollectionItemsFragmentResponseBlock)diffHandler
                                   success:(ZETACollectionsCompletionBlock)success
                                   failure:(ZETAFailureBlockType)failure;

/**
 Ask the collection service to send latest collectionItems for a given collectionID
 */
- (void)getLatestCollectionItemForRequest:(ZETAGetLatestCollectionRequestPayload *)requestPayload
                       requestUniquingTag:(NSString *)requestUniquingTag
                                  success:(ZETAGetLatestCollectionItemsResponseBlock)success
                                  failure:(ZETAFailureBlockType)failure;

/**
 Ask the collection service to subscribe to the collection
 specified in @p requestPayload.
 */
- (void)subscribeToCollectionsRequest:(ZETASubscribeRequest *)requestPayload
                   requestUniquingTag:(NSString *)requestUniquingTag
                              success:(ZETAEmptyBlockType)success
                              failure:(ZETAFailureBlockType)failure;


/**
 Ask the collection service to unsubscribe to the collection
 specified in @p requestPayload.
 */
- (void)unsubscribeToCollectionsRequest:(ZETAUnSubscribeRequest *)requestPayload
                     requestUniquingTag:(NSString *)requestUniquingTag
                                success:(ZETAEmptyBlockType)success
                                failure:(ZETAFailureBlockType)failure;

/**
 Usually the @p subscribeToCollectionsRequest:success:failure method is
 used in conjuction with the @p triggerCollectionDiffForCollectionsRequest
 method.

 This method allows a subscription to happen without triggering an
 initial collection request. This is useful in cases when we
 just wish to be notified of live changes.
 */
- (void)subscribeToCollectionsRequest:(ZETASubscribeRequest *)requestPayload
                   requestUniquingTag:(NSString *)requestUniquingTag
                            itemClass:(Class)itemClass
                     diffHandlerBlock:(ZETACollectionsResponseBlock)diffHandlerBlock
                              success:(ZETAEmptyBlockType)success
                              failure:(ZETAFailureBlockType)failure;

/**
 @param itemClass Must be a derived class of @p ZETABaseCollectionItem.
 */
- (void)getCollectionItem:(ZETAGetCollectionItemRequestPayload *)requestPayload
       requestUniquingTag:(NSString *)requestUniquingTag
                itemClass:(Class)itemClass
                  success:(ZETAGetCollectionItemResponseBlock)success
                  failure:(ZETAFailureBlockType)failure;

- (void)getAnonCollectionItem:(ZETAGetCollectionItemRequestPayload *)requestPayload
           requestUniquingTag:(NSString *)requestUniquingTag
                    itemClass:(Class)itemClass
                      success:(ZETAGetCollectionItemResponseBlock)success
                      failure:(ZETAFailureBlockType)failure;

- (void)setCollectionItem:(ZETASetCollectionItemRequest *)requestPayload
       requestUniquingTag:(NSString *)requestUniquingTag
                  success:(ZETAEmptyBlockType)success
                  failure:(ZETAFailureBlockType)failure;

- (void)setMultipleCollectionItems:(ZETASetMultipleCollectionItemsRequest *)requestPayload
                requestUniquingTag:(NSString *)requestUniquingTag
                           success:(ZETAEmptyBlockType)success
                           failure:(ZETAFailureBlockType)failure;
/**
 Searches for the given file name in app bundle and loads it into memory and returns the data in
 success block.

 @p items Array of ZETAJSONCollectionItem objects.
 */
- (void)loadCollectionDataFromFileName:(NSString *)fileName
                               success:(ZETACollectionFileLoadResponse)sucess
                               failure:(ZETAFailureBlockType)failure;
/**
 Attaches a listener which are useful in case updates for a collection (for which user has subscribed to) are received.
 */
- (void)attachListenerForUpdatesOfCollectionID:(NSString *)collectionID
                                     itemClass:(Class)itemClass
                              diffHandlerBlock:(ZETACollectionsResponseBlock)diffHandlerBlock;

@end
