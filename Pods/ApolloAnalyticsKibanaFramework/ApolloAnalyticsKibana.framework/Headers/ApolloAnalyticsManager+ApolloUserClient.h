#import <ApolloAnalyticsCore/ApolloAnalyticsCore.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloAnalyticsManager (ApolloUserClient)

- (instancetype)initWithTenantID:(NSNumber *)tenantID
                       projectID:(NSString *)projectID
                           appID:(nullable NSString *)appID
                         sdkName:(nullable NSString *)sdkName
                        platform:(nullable NSString *)platform
                      sdkVersion:(nullable NSString *)sdkVersion
                          apiKey:(nullable NSString *)apiKey
                           ifiID:(nullable NSNumber *)ifiID
               apolloUserBaseUrl:(NSURL *)apolloUserBaseUrl
                          authID:(NSString *)authID
                    authProvider:(id<ApolloAuthProviderProtocol>)authProvider;

@end

NS_ASSUME_NONNULL_END
