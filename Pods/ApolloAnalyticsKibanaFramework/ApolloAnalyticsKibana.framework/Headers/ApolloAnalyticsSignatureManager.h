#import <Foundation/Foundation.h>
#import <ApolloCoreCrypto/ZETAECKeyPair.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import "ApolloAnalyticsKeyPairRequest.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^ApolloAuthTokenFetchBlockType)(ZETAStringBlockType completionBlock);
typedef void (^ApolloUserJIDFetchBlockType)(ZETAStringBlockType completionBlock);

@interface ApolloAnalyticsSignatureManager : NSObject

+ (ApolloAnalyticsSignatureManager *)sharedInstance;
+ (void)resetSharedInstance;

+ (void)setupSharedInstanceWithApolloUserBaseUrl:(NSURL *)baseUrl;

- (void)getKeyPairForAuthID:(NSString *)authID
                    userJID:(NSString *)userJID
        authTokenFetchBlock:(ApolloAuthTokenFetchBlockType)authTokenFetchBlock
                 completion:(ZETAKeyPairResponseBlock)completion;

- (void)clearDataForAuthID:(NSString *)authID;

@end

NS_ASSUME_NONNULL_END
