#import <Foundation/Foundation.h>
#import <ApolloCoreCrypto/ZETAECKeyPair.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^ZETAKeyPairResponseBlock)(ZETAECKeyPair * _Nullable keyPair);

@interface ApolloAnalyticsKeyPairRequest : NSObject

@property (nonatomic, copy, readonly) ZETAKeyPairResponseBlock completion;

- (instancetype)initWithCompletionBlock:(ZETAKeyPairResponseBlock)completion;

@end

NS_ASSUME_NONNULL_END
