#import <Foundation/Foundation.h>


/**
 Allows one to easily construct UA-Info strings required by Door and Proteus. It is recommended
 that you create instances using @p +defaultAgentInfo, which will return useful defaults for most
 fields.

 To serialize the object to string, use @p -stringRepresentation.

 If you want a dictionary of fields and values, use @p -dictionaryRepresentation.

 For a descripton of the fields, see the spec at https://docs.google.com/document/d/1RN-H7bmeHgTWG7b-gWV3ZFpzjBRgKJ0mHW7ygnqX5pU/
 */
@interface TDTAgentInfo : NSObject

/**
 It is recommended that you create instances using this method, which will return useful
 defaults for most fields.
 */
+ (instancetype)defaultAgentInfo;

// Required properties
@property (nonatomic, copy) NSString *deviceType;
@property (nonatomic, copy) NSString *OS;
@property (nonatomic, copy) NSString *OSVersion;
@property (nonatomic, copy) NSString *appName;
@property (nonatomic, copy) NSString *appVersion;

// Optional properties
@property (nonatomic, copy) NSString *carrier;
@property (nonatomic, copy) NSString *networkType;
@property (nonatomic, copy) NSString *locale;

@property (nonatomic, copy) NSString *deviceManufacturer;

@property (nonatomic, copy) NSString *deviceModel;

/**
 Time zone to be used in the agent info dictionary. If not set, 
 @p localTimeZone from @p NSTimeZone is used.
 */
@property (nonatomic) NSTimeZone *timeZone;

- (NSDictionary *)dictionaryRepresentation;
- (NSString *)stringRepresentation;

@end
