#import "ZETAUserData.h"
#import "TDTAgentInfo.h"

@interface TDTAgentInfo (ZETAUserData)

- (ZETAUserData *)zeta_userData;

@end
