#import <UIKit/UIKit.h>

@interface UIDevice (ZETADeviceID)

/**
 Creates and stores a unique identifier for the device.

 @discussion This identifier stays the same, even if the app is removed and reinstalled, as long as
 the user doesn't factory reset the device. Apple does not provide any identifier which can be used
 to uniquely identify a device even after the app is removed and re-installed. So as a workaround,
 we create our own identifier and store it in the keychain, where it stays even after the app is
 removed.

 For more details, @see https://blog.onliquid.com/persistent-device-unique-identifier-ios-keychain/
 */
+ (NSString *)zeta_deviceID;
+ (BOOL)zeta_isTouchIDUnSupportediPhoneDevice;


@end
