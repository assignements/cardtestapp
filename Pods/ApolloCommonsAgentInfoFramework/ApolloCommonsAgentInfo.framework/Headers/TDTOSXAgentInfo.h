#import <Foundation/Foundation.h>
#import "TDTAgentInfo.h"

#if ! TARGET_OS_IPHONE

@interface TDTOSXAgentInfo : TDTAgentInfo

@end

#endif
