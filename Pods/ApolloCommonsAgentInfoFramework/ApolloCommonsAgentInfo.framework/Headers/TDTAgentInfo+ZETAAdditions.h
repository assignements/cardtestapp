#import "TDTAgentInfo.h"

typedef NS_ENUM(NSUInteger, ZETANetworkConnectionType) {
    ZETANetworkConnectionTypeUnknown = 0,
    ZETANetworkConnectionTypeEdge = 1,
    ZETANetworkConnectionType3G = 2,
    ZETANetworkConnectionTypeLTE = 3,
    ZETANetworkConnectionTypeWiFi = 4,
};

@interface TDTAgentInfo (ZETAAdditions)

+ (instancetype)zeta_defaultAgentInfoWithAppName:(NSString *)appName;

+ (instancetype)zeta_defaultAgentInfoWithAppName:(NSString *)appName
                                        deviceID:(NSString *)deviceID;

+ (instancetype)zeta_defaultAgentInfoWithAppName:(NSString *)appName
                                        deviceID:(NSString *)deviceID
                                           appID:(NSString *)appID;

+ (instancetype)zeta_defaultAgentInfoWithAppName:(NSString *)appName
                                        deviceID:(NSString *)deviceID
                                           appID:(NSString *)appID
                                         scopeID:(NSString *)scopeID
                                   isApolloAgent:(BOOL)isApolloAgent;

+ (instancetype)zeta_defaultAgentInfoWithAppName:(NSString *)appName
                                        deviceID:(NSString *)deviceID
                                           appID:(NSString *)appID
                                         scopeID:(NSString *)scopeID
                                   isApolloAgent:(BOOL)isApolloAgent
                            apolloInstallationID:(NSString *)apolloInstallationID;

+ (instancetype)zeta_defaultAgentInfoWithAppName:(NSString *)appName
                                        deviceID:(NSString *)deviceID
                                           appID:(NSString *)appID
                                         scopeID:(NSString *)scopeID
                                   isApolloAgent:(BOOL)isApolloAgent
                            apolloInstallationID:(NSString *)apolloInstallationID
                                 apolloSdkAuthID:(NSString *)apolloSdkAuthID;

/**
 It returns a unique identifier for the device which remains the same for the lifetime of the device
 across installations of the app.
 
 @see @p UIDevice + ZETADeviceID for details
 */
@property (nonatomic, readonly) NSString *zeta_deviceID;
@property (nonatomic, readonly) NSString *zeta_appID;
@property (nonatomic, readonly) NSString *zeta_scopeID;
@property (nonatomic, readonly) NSNumber *zeta_isApolloAgent;
@property (nonatomic, readonly) NSString *zeta_apolloInstallationID;
@property (nonatomic, readonly) NSString *zeta_apolloSdkAuthID;

/**
 The screen resolutions measured in points. It is returned in the form of a string:
 `width`x`height`.
 e.g. for a screen of resolution 320 * 568. This method returns 320x568
 */
- (NSString *)zeta_screenResolution;
- (NSNumber *)zeta_SIMCount;
- (ZETANetworkConnectionType)zeta_connectionType;
- (NSString *)zeta_language;
- (NSString *)zeta_appShortVersion;

/**
 This method extracts the installation ID from the @p identifierForVendor property on the @p
 UIDevice object. This value stays the same for a device as long as the device has atleast one app
 from the same vendor. If all the apps from a given vendor are removed from the device and app is
 installed again a new @p identifierForVendor is assigned to the device. Usually this value is not
 @p nil, however there are scenarios during app launch when it can be @p nil.
 
 Quoting the documentation for @p identifierForVendor:
 
 "If the value is nil, wait and get the value again later. This happens,
 for example, after the device has been restarted but before the user has
 unlocked the device."
 */
- (NSString *)zeta_installationID;

- (BOOL)zeta_hasFrontCamera;
- (BOOL)zeta_isLocationEnabled;

- (NSDictionary *)zeta_dictionaryRepresentation;
- (NSDictionary *)zeta_dictionaryRepresentationForStartPacket;

@end
