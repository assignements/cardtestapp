#import <Mantle/Mantle.h>
#import "TDTAgentInfo+ZETAAdditions.h"

@interface ZETAUserData : MTLModel<MTLJSONSerializing>

- (instancetype)initWithAppVersion:(NSString *)appVersion
                                OS:(NSString *)OS
                         OSVersion:(NSString *)OSVersion
                            locale:(NSString *)locale
                deviceManufacturer:(NSString *)deviceManufacturer
                       deviceModel:(NSString *)deviceModel
                  screenResolution:(NSString *)screenResolution
                           appName:(NSString *)appName
                        deviceType:(NSString *)deviceType
                         operators:(NSArray *)operators
                          SIMCount:(NSNumber *)SIMCount
                    connectionType:(ZETANetworkConnectionType)connectionType
                          deviceID:(NSString *)deviceID
                    installationID:(NSString *)installationID
                    hasFrontCamera:(BOOL)hasFrontCamera
                 isLocationEnabled:(BOOL)isLocationEnabled
                             appID:(NSString *)appID;

@property (nonatomic, readonly, copy) NSString *appVersion;
@property (nonatomic, readonly, copy) NSString *OS;
@property (nonatomic, readonly, copy) NSString *OSVersion;
@property (nonatomic, readonly, copy) NSString *locale;
@property (nonatomic, readonly, copy) NSString *deviceManufacturer;
@property (nonatomic, readonly, copy) NSString *deviceModel;
@property (nonatomic, readonly, copy) NSString *screenResolution;
@property (nonatomic, readonly, copy) NSString *appName;
@property (nonatomic, readonly, copy) NSString *deviceType;

@property (nonatomic, readonly, copy) NSArray *operators;
@property (nonatomic, readonly, copy) NSNumber *SIMCount;
@property (nonatomic, readonly) ZETANetworkConnectionType connectionType;
@property (nonatomic, readonly, copy) NSString *deviceID;
@property (nonatomic, readonly, copy) NSString *installationID;

@property (nonatomic, readonly) BOOL hasFrontCamera;
@property (nonatomic, readonly) BOOL isLocationEnabled;
@property (nonatomic, readonly, copy) NSString *appID;

@end
