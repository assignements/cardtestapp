#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface UpdateLocaleRequest : MTLModel <MTLJSONSerializing>

- (instancetype)initWithTenantID:(NSNumber *)tenantID
                         userJID:(NSString *)userJID
                          locale:(NSString *)locale
                           attrs:(nullable NSDictionary *)attrs;

@end

NS_ASSUME_NONNULL_END
