#import <Mantle/Mantle.h>

@interface AddVersionInfoRequest : MTLModel <MTLJSONSerializing>

- (instancetype)initWithTenantID:(NSNumber *)tenantID
                          apiKey:(NSString *)apiKey
                       projectID:(NSString *)projectID
                           appID:(NSString *)appID
                         sdkName:(NSString *)sdkName
                        platform:(NSString *)platform
                      sdkVersion:(NSString *)sdkVersion
                  installationID:(NSString *)installationID
                       versionID:(NSString *)versionID
                           attrs:(NSDictionary *)attrs;

@end
