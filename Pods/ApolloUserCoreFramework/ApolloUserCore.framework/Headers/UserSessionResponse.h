#import <Mantle/Mantle.h>
#import "AddSessionRequest.h"

@interface UserSessionResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *userSessionID;
@property (nonatomic, readonly) NSString *installationID;
@property (nonatomic, readonly) NSString *userID;
@property (nonatomic, readonly) NSString *resourceID;
@property (nonatomic, readonly) NSString *locale;
@property (nonatomic, readonly) UserSessionState state;

@end
