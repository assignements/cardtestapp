#import <Foundation/Foundation.h>
#import "ApolloUserClient.h"
#import "UserSessionResponse.h"
#import "UserProfileResponse.h"
#import "AddAppInstallResponse.h"
#import "AppVersionInfoResponse.h"
#import "AddSdkInstallResponse.h"
#import "PublishEventObject.h"
#import "NotificationPreferencesResponsePayload.h"
#import "NotificationPreferencesRequestPayload.h"
#import "GetNotificationPreferencesRequestPayload.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ApolloUserManagerDelegate;

@interface ApolloUserManager : NSObject

@property (nonatomic, weak) id<ApolloUserManagerDelegate> delegate;

- (instancetype)initWithApolloUserClient:(ApolloUserClient *)client;

- (void)registerAppInstallWithTenantID:(NSNumber *)tenantID
                                apiKey:(NSString *)apiKey
                             projectID:(NSString *)projectID
                                 appID:(NSString *)appID
                               sdkName:(NSString *)sdkName
                              platform:(NSString *)platform
                            sdkVersion:(NSString *)sdkVersion
                             versionID:(NSString *)versionID
                              deviceID:(NSString *)deviceID
                  deviceInstallationID:(NSString *)deviceInstallationID
                           packageName:(NSString *)packageName
                                uaInfo:(NSDictionary *)uaInfo
                                 attrs:(NSDictionary *)attrs;

- (void)updateUserOnlineStatus:(BOOL)status
                       userJID:(NSString *)userJID
                         attrs:(NSDictionary *)attrs
                      tenantID:(NSNumber *)tenantID;

- (void)unregisterUserSessionForUserJID:(NSString *)userJID
                               tenantID:(NSNumber *)tenantID;

- (void)updateUserProfileWithUserID:(NSString *)userID
                               name:(NSString *)name
                       phoneNumbers:(NSArray *)phoneNumbers
                             emails:(NSArray *)emails
                              state:(NSString *)state
                           tenantID:(NSNumber *)tenantID;

- (void)setPushToken:(NSString *)pushToken
          forUserJID:(NSString *)userJID
               appID:(NSString *)appID
            deviceID:(NSString *)deviceID
           sdkAuthID:(NSString *)sdkAuthID
               attrs:(NSDictionary *)attrs
              uaInfo:(NSDictionary *)uaInfo
              config:(NSDictionary *)config
            tenantID:(NSNumber *)tenantID;

- (void)setPushTokenV2:(NSString *)pushToken
            forUserJID:(NSString *)userJID
                 appID:(NSString *)appID
              deviceID:(NSString *)deviceID
             sdkAuthID:(NSString *)sdkAuthID
                 attrs:(NSDictionary *)attrs
                uaInfo:(NSDictionary *)uaInfo
                config:(NSDictionary *)config
              tenantID:(NSNumber *)tenantID
       mercuryTenantID:(NSNumber *)mercuryTenantID;

- (void)registerVersionInfoWithTenantID:(NSNumber *)tenantID
                                 apiKey:(NSString *)apiKey
                              projectID:(NSString *)projectID
                                  appID:(NSString *)appID
                                sdkName:(NSString *)sdkName
                               platform:(NSString *)platform
                             sdkVersion:(NSString *)sdkVersion
                         installationID:(NSString *)installationID
                              versionID:(NSString *)versionID
                                  attrs:(NSDictionary *)attrs;

- (void)updateUserSessionLocale:(NSString *)locale
                     ForUserJID:(NSString *)sessionID
                       tenantID:(NSNumber *)tenantID
                          attrs:(NSDictionary *)attrs;

- (void)updateUserSessionLocaleV2:(NSString *)locale
                          userJID:(NSString *)userJID
                         tenantID:(NSNumber *)tenantID
                     successBlock:(ZETAResponseBlockType)success
                     failureBlock:(ZETAFailureBlockType)failure;

- (void)addUserSessionWithUserJID:(NSString *)userID
                   installationID:(NSString *)installationID
                        sdkAuthID:(NSString *)sdkAuthID
                            appID:(NSString *)appID
                           locale:(NSString *)locale
                            state:(UserSessionState)state
                         tenantID:(NSNumber *)tenantID
                            attrs:(NSDictionary *)attrs;

- (void)registerSdkInstallWithTenantID:(NSNumber *)tenantID
                                apiKey:(NSString *)apiKey
                             projectID:(NSString *)projectID
                                 appID:(NSString *)appID
                        installationID:(NSString *)installationID
                               sdkName:(NSString *)sdkName
                           platformSDK:(NSString *)platformSDK
                    platformSDKVersion:(NSString *)platformSDKVersion
                                 attrs:(NSDictionary *)attrs;

- (void)registerReceiverWithUserJID:(NSString *)userJID
                           tenantID:(NSNumber *)tenantID
                          projectID:(NSString *)projectID
                              appID:(NSString *)appID
                  mercuryPlatformID:(NSString *)mercuryPlatformID
                    mercuryTenantID:(NSNumber *)mercuryTenantID
                     mercuryScopeID:(NSString *)mercuryScopeID
                       successBlock:(nullable ZETAResponseBlockType)success
                       failureBlock:(nullable ZETAFailureBlockType)failure;

-(void)setUserNotificationPreferencesWithTenantID:(NSNumber *)tenantID
                                       projectID:(NSString *)projectID
                                           appID:(NSString *)appID
                                        domainID:(NSString *)domainID
                                          userID:(NSString *)userID
                                         request:(NotificationPreferencesRequestPayload *)request
                                    successBlock:(nullable ZETAResponseBlockType)success
                                   failureBlock:(nullable  ZETAFailureBlockType)failure;

-(void)getUserNotificationPreferencesWithTenantID:(NSNumber *)tenantID
                                       projectID:(NSString *)projectID
                                           appID:(NSString *)appID
                                        domainID:(NSString *)domainID
                                          userID:(NSString *)userID
                                      platformID:(NSString *)platformID
                                 mercuryTenantID:(NSNumber *)mercuryTenantID
                                         scopeID:(NSString *)scopeID
                                    successBlock:(nullable ZETAResponseBlockType)success
                                    failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)registerPublicKey:(NSString *)base64EncodedPublicKey
               forUserJID:(NSString *)userJID
              authHeaders:(nullable NSDictionary *)authHeaders
             successBlock:(nullable ZETAResponseBlockType)success
             failureBlock:(nullable ZETAFailureBlockType)failure;

-(void)publishEventsWithRequest:(PublishEventsRequest *)request
                      signature:(nullable NSString *)signature
                   signatoryJID:(nullable NSString *)signatoryJID
                   stringToSign:(nullable NSString *)stringToSign
                   successBlock:(nullable ZETAResponseBlockType)success
                   failureBlock:(nullable ZETAFailureBlockType)failure;

@end

@protocol ApolloUserManagerDelegate <NSObject>

@optional
- (void)apolloUserManager:(ApolloUserManager *)manager
    didRegisterAppInstall:(AddAppInstallResponse *)appInstallResponse;
- (void)apolloUserManager:(ApolloUserManager *)manager
failedToRegisterAppInstallWithError:(NSError *)error;

- (void)apolloUserManager:(ApolloUserManager *)manager
didRegisterAppVersionInfo:(AppVersionInfoResponse *)appVersionInfoResponse;
- (void)apolloUserManager:(ApolloUserManager *)manager
failedToRegisterAppVersionInfoWithError:(NSError *)error;

- (void)apolloUserManager:(ApolloUserManager *)manager
          didSetPushToken:(NSString *)pushToken;
- (void)apolloUserManager:(ApolloUserManager *)manager
failedToSetPushTokenWithError:(NSError *)error;

- (void)apolloUserManager:(ApolloUserManager *)manager
        didAddUserSession:(UserSessionResponse *)userSessionResponse;
- (void)apolloUserManager:(ApolloUserManager *)manager
failedToAddUserSessionWithError:(NSError *)error;

- (void)apolloUserManager:(ApolloUserManager *)manager
didUpdateUserSessionLocale:(UserSessionResponse *)userSessionResponse;
- (void)apolloUserManager:(ApolloUserManager *)manager
failedToUpdateUserSessionLocaleWithError:(NSError *)error;

- (void)apolloUserManager:(ApolloUserManager *)manager
     didLogoutUserSession:(UserSessionResponse *)userSessionResponse;
- (void)apolloUserManager:(ApolloUserManager *)manager
failedToLogoutUserSessionWithError:(NSError *)error;

- (void)apolloUserManager:(ApolloUserManager *)manager
     didUpdateUserProfile:(UserProfileResponse *)userProfileResponse;
- (void)apolloUserManager:(ApolloUserManager *)manager
failedToUpdateUserProfileWithError:(NSError *)error;

- (void)apolloUserManager:(ApolloUserManager *)manager
    didRegisterSdkInstall:(AddSdkInstallResponse *)sdkInstallResponse;
- (void)apolloUserManager:(ApolloUserManager *)manager
failedToRegisterSdkInstallWithError:(NSError *)error;

- (void)apolloUserManager:(ApolloUserManager *)manager
didUpdateUserOnlineStatus:(BOOL)status
               forUserJID:(NSString *)installationID;
- (void)apolloUserManager:(ApolloUserManager *)manager
failedToUpdateUserOnlineStatus:(BOOL)status
               forUserJID:(NSString *)installationID;

@end

NS_ASSUME_NONNULL_END
