#import <ApolloHttpClient/ZETAHTTPSessionClient.h>
#import "AddAppInstallRequest.h"
#import "UpdateSessionRequest.h"
#import "AddPushTokenRequest.h"
#import "AddVersionInfoRequest.h"
#import "UpdateUserProfileRequest.h"
#import "AddSessionRequest.h"
#import "AddSdkInstallRequest.h"
#import "LogoutUserSessionRequest.h"
#import "UpdateAppInstallRequest.h"
#import "RegisterReceiverRequestPayload.h"
#import "NotificationPreferencesResponsePayload.h"
#import "NotificationPreferencesRequestPayload.h"
#import "PublishEventsRequest.h"
#import "RegisterPublicKeyRequest.h"

NS_ASSUME_NONNULL_BEGIN

FOUNDATION_EXPORT NSString *const ApolloUserClientStringToSignHeaderKey;
FOUNDATION_EXPORT NSString *const ApolloUserClientSignatureHeaderKey;
FOUNDATION_EXPORT NSString *const ApolloUserClientSignatoryJIDHeaderKey;

@protocol ApolloUserClientDelegate;

@interface ApolloUserClient : ZETAHTTPSessionClient

@property (nonatomic, weak) id<ApolloUserClientDelegate> tokenProvider;

- (void)addAppInstallWithRequestObject:(AddAppInstallRequest *)request
                               success:(ZETAResponseBlockType)success
                               failure:(ZETAFailureBlockType)failure;

- (void)updateAppInstallWithRequestObject:(UpdateAppInstallRequest *)request
                           installationID:(NSString *)installationID
                                  success:(ZETAResponseBlockType)success
                                  failure:(ZETAFailureBlockType)failure;

- (void)updateSessionWithRequestObject:(MTLModel<MTLJSONSerializing> *)request
                               success:(ZETAResponseBlockType)success
                               failure:(ZETAFailureBlockType)failure;

- (void)updateSessionV2WithRequestObject:(MTLModel<MTLJSONSerializing> *)request
                                 success:(ZETAResponseBlockType)success
                                 failure:(ZETAFailureBlockType)failure;

- (void)addSessionWithRequestObject:(AddSessionRequest *)request
                            success:(ZETAResponseBlockType)success
                            failure:(ZETAFailureBlockType)failure;

- (void)logoutSessionWithRequestObject:(LogoutUserSessionRequest *)request
                               success:(ZETAResponseBlockType)success
                               failure:(ZETAFailureBlockType)failure;

- (void)addPushTokenWithRequestObject:(AddPushTokenRequest *)request
                              success:(ZETAResponseBlockType)success
                              failure:(ZETAFailureBlockType)failure;

- (void)addPushTokenV2WithRequestObject:(AddPushTokenRequest *)request
                                success:(ZETAResponseBlockType)success
                                failure:(ZETAFailureBlockType)failure;

- (void)addVersionInfoWithRequestObject:(AddVersionInfoRequest *)request
                                success:(ZETAResponseBlockType)success
                                failure:(ZETAFailureBlockType)failure;

- (void)updateUserProfileWithUserID:(NSString *)userID
                      requestObject:(UpdateUserProfileRequest *)request
                            success:(ZETAResponseBlockType)success
                            failure:(ZETAFailureBlockType)failure;

- (void)addSdkInstallWithRequestObject:(AddSdkInstallRequest *)request
                               success:(ZETAResponseBlockType)success
                               failure:(ZETAFailureBlockType)failure;

- (void)registerReceiverWithRequestObject:(RegisterReceiverRequestPayload *)request
                                 tenantID:(NSNumber *)tenantID
                                projectID:(NSString *)projectID
                                    appID:(NSString *)appID
                             successBlock:(ZETAResponseBlockType)success
                             failureBlock:(ZETAFailureBlockType)failure;

-(void)setUserNotificationPreferencesWithTenantID:(NSNumber *)tenantID
                                       projectID:(NSString *)projectID
                                           appID:(NSString *)appID
                                        domainID:(NSString *)domainID
                                          userID:(NSString *)userID
                                         request:(NotificationPreferencesRequestPayload *)request
                                    successBlock:(nullable ZETAResponseBlockType)success
                                    failureBlock:(nullable  ZETAFailureBlockType)failure;

-(void)getUserNotificationPreferencesWithTenantID:(NSNumber *)tenantID
                                       projectID:(NSString *)projectID
                                           appID:(NSString *)appID
                                        domainID:(NSString *)domainID
                                          userID:(NSString *)userID
                                      platformID:(NSString *)platformID
                                 mercuryTenantID:(NSNumber *)mercuryTenantID
                                         scopeID:(NSString *)scopeID
                                    successBlock:(nullable ZETAResponseBlockType)success
                                    failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)registerPublicKeyRequest:(RegisterPublicKeyRequest *)request
                     authHeaders:(nullable NSDictionary *)headers
                    successBlock:(nullable ZETAResponseBlockType)success
                    failureBlock:(nullable ZETAFailureBlockType)failure;

-(void)publishEventsWithBody:(PublishEventsRequest *)request
                   signature:(nullable NSString *)signature
                signatoryJID:(nullable NSString *)signatoryJID
                stringToSign:(nullable NSString *)stringToSign
                successBlock:(nullable ZETAResponseBlockType)success
                failureBlock:(nullable ZETAFailureBlockType)failure;

@end

@protocol ApolloUserClientDelegate <NSObject>

- (void)apolloUserClient:(ApolloUserClient *)client
didRequestTokenWithSuccessBlock:(ZETAStringBlockType)successBlock
            failureBlock:(ZETAFailureBlockType)failureBlock;

@optional
- (NSString *)apolloUserClientDidRequestAuthToken:(ApolloUserClient *)client;

@end

NS_ASSUME_NONNULL_END
