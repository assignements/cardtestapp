#import <Mantle/Mantle.h>

FOUNDATION_EXPORT NSString *const AppTypeKey;
FOUNDATION_EXPORT NSString *const ScopeIDKey;
FOUNDATION_EXPORT NSString *const PlatformIDKey;

@interface AddPushTokenRequest : MTLModel <MTLJSONSerializing>

- (instancetype)initWithTenantID:(NSNumber *)tenantID
                 mercuryTenantID:(NSNumber *)mercuryTenantID
                         userJID:(NSString *)userJID
                           appID:(NSString *)appID
                       pushToken:(NSString *)pushToken
                        deviceID:(NSString *)deviceID
                       sdkAuthID:(NSString *)sdkAuthID
                           attrs:(NSDictionary *)attrs
                          uaInfo:(NSDictionary *)uaInfo
                          config:(NSDictionary *)config;

- (instancetype)initWithTenantID:(NSNumber *)tenantID
                         userJID:(NSString *)userJID
                           appID:(NSString *)appID
                       pushToken:(NSString *)pushToken
                        deviceID:(NSString *)deviceID
                       sdkAuthID:(NSString *)sdkAuthID
                           attrs:(NSDictionary *)attrs
                          uaInfo:(NSDictionary *)uaInfo
                          config:(NSDictionary *)config;

@end
