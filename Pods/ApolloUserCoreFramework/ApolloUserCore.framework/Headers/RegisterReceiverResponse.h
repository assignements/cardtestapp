#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface RegisterReceiverResponse : MTLModel <MTLJSONSerializing>

- (NSString *)getInboxCollectionID;
- (NSString *)getSpotlightCollectionID;

@end

NS_ASSUME_NONNULL_END
