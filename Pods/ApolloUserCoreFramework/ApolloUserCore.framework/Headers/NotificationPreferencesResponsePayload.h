#import <Foundation/Foundation.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>
#import "CommunicationPolicyResponsePayload.h"

NS_ASSUME_NONNULL_BEGIN

@interface NotificationPreferencesResponsePayload : ZETAModelWithoutNil<MTLJSONSerializing>
@property(nonatomic, readonly) NSString *receiver;
@property(nonatomic, readonly) NSString *scopeID;
@property(nonatomic, readonly) NSString *language;
@property(nonatomic, readonly) NSString *createdAt;
@property(nonatomic, readonly) NSString *updatedAt;
@property(nonatomic, readonly) NSArray<CommunicationPolicyResponsePayload *> *communicationPolicy;
@end

NS_ASSUME_NONNULL_END
