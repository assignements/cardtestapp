#import <Mantle/Mantle.h>

@interface UpdateAppInstallRequest : MTLModel <MTLJSONSerializing>

- (instancetype)initWithIsActive:(BOOL)isActive;

@end
