#import <Mantle/Mantle.h>
#import "PublishEventObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface PublishEventsRequest : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSNumber *tenantID;
@property (nonatomic, readonly) NSString *projectID;

- (instancetype)initWithTenantID:(NSNumber *)tenantID
                       projectID:(NSString *)projectID
                           appID:(nullable NSString *)appID
                         sdkName:(nullable NSString *)sdkName
                        platform:(nullable NSString *)platform
                      sdkVersion:(nullable NSString *)sdkVersion
                          apiKey:(nullable NSString *)apiKey
                             ifi:(NSNumber *)ifiID
                         userJID:(nullable NSString *)userJID
                  installationID:(nullable NSString *)installationID
                          events:(NSArray<PublishEventObject *> *)events;

@end

NS_ASSUME_NONNULL_END
