#import <Mantle/Mantle.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

@interface AddAppInstallRequest : MTLModel <MTLJSONSerializing>

- (instancetype)initWithTenantID:(NSNumber *)tenantID
                          apiKey:(NSString *)apiKey
                       projectID:(NSString *)projectID
                           appID:(NSString *)appID
                         sdkName:(NSString *)sdkName
                        platform:(NSString *)platform
                      sdkVersion:(NSString *)sdkVersion
                       versionID:(NSString *)versionID
                        deviceID:(NSString *)deviceID
            deviceInstallationID:(NSString *)deviceInstallationID
                          uaInfo:(NSDictionary *)uaInfo
                     packageName:(NSString *)packageName
                           attrs:(NSDictionary *)attrs;

@end
