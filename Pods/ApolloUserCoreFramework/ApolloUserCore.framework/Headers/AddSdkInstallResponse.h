#import <Mantle/Mantle.h>

@interface AddSdkInstallResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *sdkInstallationID;
@property (nonatomic, readonly) NSString *installationID;
@property (nonatomic, readonly) NSString *sdkName;
@property (nonatomic, readonly) NSString *platformSDK;
@property (nonatomic, readonly) NSString *platformSDKVersion;
@property (nonatomic, readonly) NSDictionary *attrs;

@end
