#import <Mantle/Mantle.h>

@interface LogoutUserSessionRequest : MTLModel <MTLJSONSerializing>

- (instancetype)initWithUserJID:(NSString *)userJID
                       tenantID:(NSNumber *)tenantID;

@end
