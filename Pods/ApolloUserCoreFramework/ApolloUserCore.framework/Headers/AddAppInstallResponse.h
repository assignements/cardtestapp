#import <Mantle/Mantle.h>

@interface AddAppInstallResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *installationID;
@property (nonatomic, readonly) NSString *appID;
@property (nonatomic, readonly) NSString *versionID;
@property (nonatomic, readonly) NSString *deviceID;
@property (nonatomic, readonly) NSString *deviceInstallationID;
@property (nonatomic, readonly) NSDictionary *uaInfo;
@property (nonatomic, readonly) NSString *locale;
@property (nonatomic, readonly) NSString *packageName;
@property (nonatomic, readonly) NSNumber *installationTimestamp;

@end
