#import <Mantle/Mantle.h>

@interface AddSdkInstallRequest : MTLModel <MTLJSONSerializing>

- (instancetype)initWithTenantID:(NSNumber *)tenantID
                          apiKey:(NSString *)apiKey
                       projectID:(NSString *)projectID
                           appID:(NSString *)appID
                  installationID:(NSString *)installationID
                         sdkName:(NSString *)sdkName
                     platformSDK:(NSString *)platformSDK
              platformSDKVersion:(NSString *)platformSDKVersion
                           attrs:(NSDictionary *)attrs;

@end
