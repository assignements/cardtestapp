#import <Foundation/Foundation.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>
#import "CommunicationPolicyResponsePayload.h"
NS_ASSUME_NONNULL_BEGIN

@interface NotificationPreferencesRequestPayload : ZETAModelWithoutNil<MTLJSONSerializing>

-(instancetype)initWithMercuryTenantID:(NSNumber *)mercuryTenantID
                            platformID:(NSString *)platformID
                               scopeID:(NSString *)scopeID
                   communicationPolicy:(NSArray <CommunicationPolicyResponsePayload *> *)communicationPolicy;
@end

NS_ASSUME_NONNULL_END
