#import <Mantle/Mantle.h>

@interface AppVersionInfoResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *installationID;
@property (nonatomic, readonly) NSString *versionID;

@end
