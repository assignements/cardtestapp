#import <Foundation/Foundation.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommunicationRuleValueResponsePayload : ZETAModelWithoutNil<MTLJSONSerializing>
@property(nonatomic, assign) BOOL value;
@property(nonatomic) NSString *channel;

@end

NS_ASSUME_NONNULL_END
