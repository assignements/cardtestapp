#import <Foundation/Foundation.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>
#import "CommunicationRulesResponsePayload.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommunicationPolicyResponsePayload : ZETAModelWithoutNil<MTLJSONSerializing>
@property(nonatomic) NSString *type;
@property(nonatomic) NSArray<CommunicationRulesResponsePayload *> *rules;
@end

NS_ASSUME_NONNULL_END
