#import <Foundation/Foundation.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>
#import "CommunicationRuleValueResponsePayload.h"
NS_ASSUME_NONNULL_BEGIN

@interface CommunicationRulesResponsePayload : ZETAModelWithoutNil<MTLJSONSerializing>
@property(nonatomic) NSString *key;
@property(nonatomic) NSArray <CommunicationRuleValueResponsePayload *> *value;

@end

NS_ASSUME_NONNULL_END
