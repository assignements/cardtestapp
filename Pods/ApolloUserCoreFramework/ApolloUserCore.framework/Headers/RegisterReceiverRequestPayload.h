#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

@interface RegisterReceiverRequestPayload : ZETAModelWithoutNil<MTLJSONSerializing>

- (instancetype)initWithPlatformID:(NSString *)platformID
                           userJID:(NSString *)userJID
                   mercuryTenantID:(NSNumber *)mercuryTenantID
                           scopeID:(NSString *)scopeID;

@end
