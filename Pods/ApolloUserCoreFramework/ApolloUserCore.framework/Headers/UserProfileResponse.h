#import <Mantle/Mantle.h>

@interface UserProfileResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *userID;
@property (nonatomic, readonly) NSString *tenantID;
@property (nonatomic, readonly) NSString *tenantUserID;
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSArray *phoneNumbers;
@property (nonatomic, readonly) NSArray *emailIDs;
@property (nonatomic, readonly) NSString *state;

@end
