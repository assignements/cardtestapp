#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface UpdateSessionRequest : MTLModel <MTLJSONSerializing>

- (instancetype)initWithTenantID:(NSNumber *)tenantID
                         userJID:(NSString *)userJID
                           attrs:(nullable NSDictionary *)attrs
                          locale:(nullable NSString *)locale
                           state:(nullable NSString *)state;

- (instancetype)initWithTenantID:(NSNumber *)tenantID
                         userJID:(NSString *)userJID
                           attrs:(nullable NSDictionary *)attrs
                          locale:(nullable NSString *)locale
                           state:(nullable NSString *)state
                        isActive:(BOOL)isActive;

@end

NS_ASSUME_NONNULL_END
