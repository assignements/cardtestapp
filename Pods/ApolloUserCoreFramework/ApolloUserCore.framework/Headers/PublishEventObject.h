#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface PublishEventObject : MTLModel <MTLJSONSerializing>

- (instancetype)initWithEventName:(NSString *)eventName
                        eventData:(NSDictionary *)eventData;

@end

NS_ASSUME_NONNULL_END
