#import <Mantle/Mantle.h>

typedef NS_ENUM(NSUInteger, UserSessionState) {
  UserSessionStateUnknown,
  UserSessionStateBlocked,
  UserSessionStateActive
};

@interface AddSessionRequest : MTLModel <MTLJSONSerializing>

- (instancetype)initWithTenantID:(NSNumber *)tenantID
                         userJID:(NSString *)userID
                  installationID:(NSString *)installationID
                       sdkAuthID:(NSString *)sdkAuthID
                           appID:(NSString *)appID
                          locale:(NSString *)locale
                           state:(UserSessionState)state
                           attrs:(NSDictionary *)attrs;

@end
