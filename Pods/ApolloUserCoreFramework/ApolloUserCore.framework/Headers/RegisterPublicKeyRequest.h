#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface RegisterPublicKeyRequest : MTLModel <MTLJSONSerializing>

- (instancetype)initWithUserJID:(NSString *)userJID
         base64EncodedPublicKey:(NSString *)publicKey;

@end

NS_ASSUME_NONNULL_END
