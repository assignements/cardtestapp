#import <Mantle/Mantle.h>

@interface UpdateUserProfileRequest : MTLModel <MTLJSONSerializing>

- (instancetype)initWithName:(NSString *)name
                phoneNumbers:(NSArray *)phoneNumbers
                    emailIDs:(NSArray *)emailIDs
                       state:(NSString *)state
                    tenantID:(NSNumber *)tenantID;

@end
