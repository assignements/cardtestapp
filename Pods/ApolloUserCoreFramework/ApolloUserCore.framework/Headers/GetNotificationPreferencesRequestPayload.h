#import <Foundation/Foundation.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

NS_ASSUME_NONNULL_BEGIN

@interface GetNotificationPreferencesRequestPayload : ZETAModelWithoutNil<MTLJSONSerializing>

-(instancetype)initWithMercuryTenantID:(NSNumber *)mercuryTenantID
                            platformID:(NSString *)platformID
                               scopeID:(NSString *)scopeID;

@end

NS_ASSUME_NONNULL_END
