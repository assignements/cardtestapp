#import <Mantle/Mantle.h>

/**
 Base class representing NSObject based "Collection Item" model objects
 serialized from the JSON received over the network. This is different from
 the Core Data based "Collection Item" objects.
 */
@interface ZETABaseCollectionItem : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) BOOL isDeleted;
@property (nonatomic, readonly) NSString *itemID;
@property (nonatomic, readonly) NSString *createdAt;
@property (nonatomic, readonly) NSString *updatedAt;

@end
