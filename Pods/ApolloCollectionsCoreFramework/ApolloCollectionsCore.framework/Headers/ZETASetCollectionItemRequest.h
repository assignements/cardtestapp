#import <Mantle/Mantle.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

@interface ZETASetCollectionItemRequest : ZETAModelWithoutNil<MTLJSONSerializing>

- (instancetype)initWithCollectionID:(NSString *)collectionID
                              itemID:(NSString *)itemID
                          dataObject:(MTLModel<MTLJSONSerializing> *)dataObject;

- (instancetype)initWithCollectionID:(NSString *)collectionID
                              itemID:(NSString *)itemID
                dataObjectDictionary:(NSDictionary *)dataObjectDictionary;

@property (nonatomic, copy, readonly) NSString *collectionID;
@property (nonatomic, copy, readonly) NSString *itemID;
@property (nonatomic, readonly) MTLModel<MTLJSONSerializing> *dataObject;

@end
