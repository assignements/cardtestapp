#import <Mantle/Mantle.h>

@interface ZETAGetLatestCollectionItemsResponsePayload : MTLModel<MTLJSONSerializing>

/// ZETAJSONCollectionItem
@property (nonatomic, copy, readonly) NSArray *items;

@end
