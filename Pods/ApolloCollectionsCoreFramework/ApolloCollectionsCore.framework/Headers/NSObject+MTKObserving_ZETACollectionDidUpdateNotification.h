#import <Foundation/Foundation.h>

typedef void (^ZETACollectionDidUpdateNotificationBlockType)(__weak __nullable id weakSelf,
                                                             NSString *__nonnull itemID,
                                                             NSString *__nonnull lastUpdateTime);

@interface NSObject (MTKObserving_ZETACollectionDidUpdateNotification)

- (void)zeta_observeCollectionUpdate:(nonnull NSString *)collectionID
               withNotificationBlock:(nonnull ZETACollectionDidUpdateNotificationBlockType)block;

@end
