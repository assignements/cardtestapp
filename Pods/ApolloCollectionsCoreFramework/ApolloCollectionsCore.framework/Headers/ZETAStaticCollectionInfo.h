#import "ZETAGenericCollectionInfo.h"

typedef NS_ENUM(NSUInteger, ZETAStaticCollectionSyncMode) {
  ZETAStaticCollectionSyncProActiveMode,
  ZETAStaticCollectionSyncOnDemandMode,
  ZETAStaticCollectionSyncLazyLoadingMode,
};

@interface ZETAStaticCollectionInfo : ZETAGenericCollectionInfo

@property (nonatomic, readonly) ZETAStaticCollectionSyncMode syncMode;

- (instancetype)initWithCollectionID:(NSString *)collectionID
                     shouldSubscribe:(BOOL)shouldSubscribe
                          modelClass:(Class)modelClass
                           itemClass:(Class)itemClass
                            syncMode:(ZETAStaticCollectionSyncMode)syncMode
                    diffHandlerBlock:(ZETACollectionDiffHandlerBlock)diffHandlerBlock;

@end
