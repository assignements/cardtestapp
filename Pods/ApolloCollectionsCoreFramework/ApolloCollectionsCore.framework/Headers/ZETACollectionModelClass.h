#import <Foundation/Foundation.h>
#import "ZETABaseCollectionItem.h"

@protocol ZETACollectionModelClassProtocol<NSObject>

+ (instancetype)createOrUpdateCollectionID:(NSString *)collectionID
                     collectionItemPayload:(ZETABaseCollectionItem *)collectionItemPayload
                                  itemData:(id)itemData
                          sourceMethodName:(NSString *)sourceMethodName
                            isInitialFetch:(BOOL)isInitialFetch
                                       MOC:(NSManagedObjectContext *)MOC
                                     error:(NSError **)error;

+ (void)deleteCollectionItemWithCollectionID:(NSString *)collectionID
                                      itemID:(NSString *)itemID
                                         MOC:(NSManagedObjectContext *)MOC;

@end
