#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

/**
 * param items : @{itemID:itemData}
 * wiki :http://phab.corp.zeta.in/w/api/collections/#setCollectionItems
 */

@interface ZETASetMultipleCollectionItemsRequest : ZETAModelWithoutNil<MTLJSONSerializing>

- (instancetype)initWithCollectionID:(NSString *)initWithCollectionID items:(NSDictionary *)items;

@property (nonatomic, copy, readonly) NSString *collectionID;

@end
