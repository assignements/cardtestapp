#import <Foundation/Foundation.h>
#import "ZETACollectionsMessage.h"
#import "ZETAGenericCollectionInfo.h"
#import <CoreData/CoreData.h>

/**
 A utility class which existing consumers of collections module can use to handle collection responses and save in core data db
 */

@interface ZETACollectionResponseHelper : NSObject

+ (void)processAndSaveCollectionResponse:(ZETAStringCollectionItem *)collectionItem
                             itemPayload:(id)itemPayload
                            collectionID:(NSString *)collectionID
                            sourceMethod:(NSString *)sourceMethodName
                          isInitialFetch:(BOOL)isInitialFetch
                              modelClass:(Class)modelClass
                     itemProcessingBlock:(ZETACollectionItemProcessingBlock)itemProcessingBlock
                                     MOC:(NSManagedObjectContext *)MOC;

+ (void)processAndSaveLocalCollections:(NSArray *)collectionItems
                             itemClass:(Class)itemClass
                          collectionID:(NSString *)collectionID
                                  sync:(BOOL)sync
                   itemProcessingBlock:(ZETACollectionItemProcessingBlock)itemProcessingBlock
                                   MOC:(NSManagedObjectContext *)MOC;

@end
