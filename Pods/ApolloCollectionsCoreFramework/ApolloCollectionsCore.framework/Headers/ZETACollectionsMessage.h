#import <Mantle/Mantle.h>
#import "ZETABaseCollectionItem.h"

@interface ZETAStringCollectionItem : ZETABaseCollectionItem

@property (nonatomic, readonly) NSString *data;

@end

@interface ZETAJSONCollectionItem : ZETABaseCollectionItem

@property (nonatomic, readonly) NSDictionary *data;

@end

@interface ZETABooleanCollectionItem : ZETABaseCollectionItem

@property (nonatomic, readonly) BOOL data;

@end

/**
 @see http://phab.corp.zeta.in/w/api/collections/#server-s-push-message-fr
 */
@interface ZETACollectionItemDiff : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *collectionID;
@property (nonatomic, readonly) NSString *currentVersion;
@property (nonatomic, readonly) NSString *previousVersion;
@property (nonatomic, readonly) ZETAStringCollectionItem *change;

@end

@interface ZETACollectionsCompletionMessage : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly, copy) NSString *collectionID;
@property (nonatomic, readonly, copy) NSString *collectionVersion;
@property (nonatomic, readonly, copy) NSNumber *numPackets;

@end

@interface ZETACollectionsDump : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *dumpVersion;
@property (nonatomic, readonly) NSString *collectionID;
@property (nonatomic, readonly) NSString *collectionVersion;
@property (nonatomic, readonly) NSString *previousVersion;
@property (nonatomic, readonly) NSArray *changes;

@end
