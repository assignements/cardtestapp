#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

@interface ZETAGetCollectionItemRequestPayload : ZETAModelWithoutNil<MTLJSONSerializing>

- (instancetype)initWithCollectionID:(NSString *)collectionID itemID:(NSString *)itemID;

@property (nonatomic, readonly) NSString *collectionID;
@property (nonatomic, readonly) NSString *itemID;

@end
