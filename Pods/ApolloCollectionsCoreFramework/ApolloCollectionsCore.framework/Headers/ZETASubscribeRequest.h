#import <Mantle/Mantle.h>

@interface ZETASubscribeRequest : MTLModel<MTLJSONSerializing>

- (instancetype)initWithCollectionID:(NSString *)collectionID;

@property (nonatomic, readonly, copy) NSString *collectionID;

@end
