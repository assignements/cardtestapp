#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>
#import "ZETACollectionModelClass.h"

typedef void (^ZETACollectionItemCreatedOrUpdatedBlockType)(id collectionItem, BOOL isUpdate);
FOUNDATION_EXPORT NSString *const CollectionItemWillSaveNotification;
FOUNDATION_EXPORT NSString *const CollectionItemWillSaveNotificationItemKey;

typedef NS_ENUM(NSUInteger, ZETACollectionItemErrorCode) {
  ZETACollectionItemErrorCodeInsufficientInfo
};

@interface ZETACollectionItem : NSManagedObject<ZETACollectionModelClassProtocol>

@property (nonatomic, retain) NSString *itemID;
@property (nonatomic, retain) NSString *createdAt;
@property (nonatomic, retain) NSString *updatedAt;
@property (nonatomic, retain) NSDictionary *itemData;
@property (nonatomic, retain) NSString *collectionID;

+ (instancetype)collectionItemWithCollectionID:(NSString *)collectionID
                                        itemID:(NSString *)itemID
                                           MOC:(NSManagedObjectContext *)MOC;

+ (NSArray *)collectionItemsWithCollectionID:(NSString *)collectionID
                                         MOC:(NSManagedObjectContext *)MOC;

+ (NSArray *)collectionItemsFilteredUsingPredicte:(NSPredicate *)predicate
                                              MOC:(NSManagedObjectContext *)MOC;

@end
