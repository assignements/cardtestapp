#import <Foundation/Foundation.h>
#import "ZETACollectionClientProtocol.h"
#import "ZETAStaticCollectionInfo.h"
#import "ZETAGenericCollectionInfo.h"
#import <ApolloUserStore/ZETAApolloCollectionHasSyncedOnceData.h>

FOUNDATION_EXPORT NSString *const ZETACollectionDidUpdateNotification;
FOUNDATION_EXPORT NSString *const ZETACollectionFetchCompletionNotification;
FOUNDATION_EXPORT NSString *const ZETACollectionIDKey;
FOUNDATION_EXPORT NSString *const ZETAStatusKey;
FOUNDATION_EXPORT NSString *const ZETACollectionItemIDKey;
FOUNDATION_EXPORT NSString *const ZETACollectionItemLastUpdateTimeKey;
FOUNDATION_EXPORT NSString *const ZETAInitialCollectionVersion;
FOUNDATION_EXPORT NSString *const ZETAGetCollectionDiffFragmentedMethodName;
FOUNDATION_EXPORT NSString *const ZETAGetLatestCollectionItemsFragmentedMethodName;

typedef void (^ZETACollectionsDumpBlock)(ZETACollectionsDump *collectionsDump);
typedef void (^ZETALocalCollectionItemsHandlerBlock)(NSArray *collectionItems);

@class ZETAApolloUserStore;

@interface ZETAGenericCollectionManager : NSObject

@property (nonatomic, readonly) ZETAApolloUserStore *apolloUserStore;
@property (nonatomic, readonly) id<ZETACollectionClientProtocol> collectionsClient;

- (instancetype)initWithCollectionClient:(id<ZETACollectionClientProtocol>)collectionsClient
                         apolloUserStore:(ZETAApolloUserStore *)apolloUserStore
                               debugMode:(BOOL)debugMode
                   shouldHonourAppConfig:(BOOL)shouldHonourAppConfig;

- (instancetype)initWithCollectionClient:(id<ZETACollectionClientProtocol>)collectionsClient
                         apolloUserStore:(ZETAApolloUserStore *)apolloUserStore
                               debugMode:(BOOL)debugMode
                   shouldHonourAppConfig:(BOOL)shouldHonourAppConfig
                                  authID:(NSString *)authID;

/**
 Supported modelClasses as of now: ZETACollectionItem, ZETAZetletMessage, ZETAOfferMessage
 @note Model classes which are not subclasses of NSManagedObject or do not conform to
 ZETACollectionModelClassProtocol will not be saved to Core Data.
*/

// generic collection methods
- (void)removeCollectionID:(NSString *)collectionID;

- (void)handleSubscriptionForCollectionInfo:(ZETAGenericCollectionInfo *)info;

- (void)unsubscribeToCollectionID:(NSString *)collectionID
                          success:(ZETAEmptyBlockType)success
                          failure:(ZETAFailureBlockType)failure;

- (void)triggerSyncForCollectionIDInfo:(ZETAGenericCollectionInfo *)info
                            completion:(ZETABooleanStatusBlockType)completion;

- (void)getLatestCollectionItemsForCollectionIDInfo:(ZETAGenericCollectionInfo *)info
                                          batchSize:(NSUInteger)batchSize
                                         completion:(ZETABooleanStatusBlockType)completion;

// static collection methods
- (void)addCollectionID:(NSString *)collectionID
        shouldSubscribe:(BOOL)shouldSubscribe
             modelClass:(Class)modelClass
              itemClass:(Class)itemClass
       diffHandlerBlock:(ZETACollectionDiffHandlerBlock)diffHandlerBlock;

- (void)addCollectionID:(NSString *)collectionID
        shouldSubscribe:(BOOL)shouldSubscribe
             modelClass:(Class)modelClass
              itemClass:(Class)itemClass
       diffHandlerBlock:(ZETACollectionDiffHandlerBlock)diffHandlerBlock
        completionBlock:(ZETABooleanStatusBlockType)completionBlock;

- (void)addCollectionID:(NSString *)collectionID
        shouldSubscribe:(BOOL)shouldSubscribe
             modelClass:(Class)modelClass
              itemClass:(Class)itemClass
               syncMode:(ZETAStaticCollectionSyncMode)syncMode
       diffHandlerBlock:(ZETACollectionDiffHandlerBlock)diffHandlerBlock;

- (void)addCollectionID:(NSString *)collectionID
        shouldSubscribe:(BOOL)shouldSubscribe
             modelClass:(Class)modelClass
              itemClass:(Class)itemClass
               syncMode:(ZETAStaticCollectionSyncMode)syncMode
       diffHandlerBlock:(ZETACollectionDiffHandlerBlock)diffHandlerBlock
        completionBlock:(ZETABooleanStatusBlockType)completionBlock;

- (void)setCollectionItem:(ZETASetCollectionItemRequest *)requestPayload
                  success:(ZETAEmptyBlockType)success
                  failure:(ZETAFailureBlockType)failure;

- (void)onDemandFetchRequestForCollectionID:(NSString *)collectionID
                                 completion:(ZETABooleanStatusBlockType)completion;
- (void)fetchMoreRequestForCollectionID:(NSString *)collectionID
                             completion:(ZETABooleanStatusBlockType)completion;

- (void)loadItemsFromDiskForCollectionID:(NSString *)collectionID
                       itemsHandlerBlock:(ZETALocalCollectionItemsHandlerBlock)itemHandlerBlock
                         completionBlock:(ZETAEmptyBlockType)completionBlock;
- (void)loadItemsFromDiskForCollectionID:(NSString *)collectionID
                               itemClass:(Class)itemClass
                       itemsHandlerBlock:(ZETALocalCollectionItemsHandlerBlock)itemHandlerBlock
                         completionBlock:(ZETAEmptyBlockType)completionBlock;

- (void)fetchItemsFromDiskForCollectionID:(NSString *)collectionID
                          completionBlock:(ZETACollectionsDumpBlock)completionBlock;

- (nullable ZETAApolloCollectionHasSyncedOnceData *)collectionSyncStatusInfoForCollectionID:(nonnull NSString *)collectionID;

- (void)removeCollectionSyncInfoForCollectionID:(nonnull NSString *)collectionID;

@end
