#import "ZETACollectionsMessage.h"
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
typedef void (^ZETACollectionItemProcessingBlock)(id item, BOOL isDeleted);
typedef void (^ZETACollectionDiffHandlerBlock)(ZETAStringCollectionItem *collectionItem, id itemPayload, NSString *sourceMethod, BOOL isInitialFetch);

@interface ZETAGenericCollectionInfo : NSObject

- (instancetype)initWithCollectionID:(NSString *)collectionID
                     shouldSubscribe:(BOOL)shouldSubscribe
                          modelClass:(Class)modelClass
                           itemClass:(Class)itemClass
                    diffHandlerBlock:(ZETACollectionDiffHandlerBlock)diffHandlerBlock;

@property (nonatomic, copy) NSString *collectionID;
@property (nonatomic) BOOL shouldSubscribe;
@property (nonatomic) Class modelClass;
@property (nonatomic) Class itemClass;
@property (nonatomic, copy) ZETACollectionDiffHandlerBlock diffHandlerBlock;

@end
