#import <Mantle/Mantle.h>
#import <ApolloCommonsJSONParsing/ZETAOriginalPayloadModelProtocol.h>

@interface ZETAUserProfile: MTLModel<MTLJSONSerializing, ZETAOriginalPayloadModelProtocol>

@property (nonatomic, readonly) NSString *themeID;

- (NSString *)uniqueIdentifier;

- (NSString *)companyName;

- (NSString *)userID;

@end
