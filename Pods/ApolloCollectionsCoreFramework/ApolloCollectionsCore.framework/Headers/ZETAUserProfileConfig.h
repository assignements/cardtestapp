#import <Mantle/Mantle.h>
#import <ApolloCommonsJSONParsing/ZETAOriginalPayloadModelProtocol.h>

@interface ZETAUserProfileConfig: MTLModel<MTLJSONSerializing, ZETAOriginalPayloadModelProtocol>

@property (nonatomic, readonly) NSArray *employeeProfiles;

-(NSArray *)getCollections;

@end


