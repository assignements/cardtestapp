#import <Mantle/Mantle.h>

@interface ZETAUnSubscribeRequest : MTLModel<MTLJSONSerializing>

- (instancetype)initWithCollectionID:(NSString *)collectionID;

@property (nonatomic, readonly, copy) NSString *collectionID;

@end
