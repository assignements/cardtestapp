#import <Mantle/Mantle.h>
#import <ApolloCommonsJSONParsing/ZETAModelWithoutNil.h>

@interface ZETAGetCollectionRequestPayload : ZETAModelWithoutNil<MTLJSONSerializing>

- (instancetype)initWithCollectionID:(NSString *)collectionID
                             version:(NSString *)version
                 includeDeletedItems:(BOOL)includeDeletedItems;

@property (nonatomic, copy, readonly) NSString *collectionID;
@property (nonatomic, copy, readonly) NSString *collectionVersion;
@property (nonatomic, readonly) BOOL includeDeletedItems;

@end
