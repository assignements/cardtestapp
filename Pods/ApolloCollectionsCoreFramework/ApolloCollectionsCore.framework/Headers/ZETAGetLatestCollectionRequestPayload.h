#import <Foundation/Foundation.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

@interface ZETAGetLatestCollectionRequestPayload : ZETAModelWithoutNil<MTLJSONSerializing>

- (instancetype)initWithCollectionID:(NSString *)collectionID count:(NSUInteger)count;

- (instancetype)initWithCollectionID:(NSString *)collectionID
                               count:(NSUInteger)count
                         tillVersion:(NSString *)tillVersion;

@property (nonatomic, copy, readonly) NSString *collectionID;
@property (nonatomic, copy, readonly) NSString *tillVersion;
@property (nonatomic, readonly) NSUInteger count;

@end
