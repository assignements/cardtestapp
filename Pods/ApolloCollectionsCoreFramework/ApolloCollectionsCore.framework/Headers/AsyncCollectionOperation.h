#import <Foundation/Foundation.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

NS_ASSUME_NONNULL_BEGIN

@interface AsyncCollectionOperation : NSOperation

-(void)startWithBlock:(ZETAEmptyBlockType)block;
+(void)finishOperation:(AsyncCollectionOperation *)operation;

@end

NS_ASSUME_NONNULL_END
