#import <Foundation/Foundation.h>
#import "ZETAErasablePrivateKey.h"
#import "ZETAPublicKey.h"

typedef NS_ENUM(NSUInteger, ZETAJWTAlgorithm) {
  ZETAJWTAlgorithmUnknown,
  ZETAJWTAlgorithmHS256,
  ZETAJWTAlgorithmES256
};

@interface ZETAJWT : NSObject

- (instancetype)initWithAlgorithm:(ZETAJWTAlgorithm)algorithm payloads:(NSDictionary *)payloads;

// Use below method for HS algorithm
- (NSString *)encodedJWTTokenWithSecret:(NSData *)secret;

// Use below method for ES256 algorithm
- (NSString *)encodedJWTTokenWithES256PrivateKey:(ZETAErasablePrivateKey *)privateKey;

@end
