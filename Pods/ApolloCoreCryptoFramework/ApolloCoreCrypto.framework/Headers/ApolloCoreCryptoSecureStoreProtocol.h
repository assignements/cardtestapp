
/**
* To support signing methods, the stores should provide private key. They should implement this protocol
*/
#import "ZETAErasablePrivateKey.h"

typedef void (^ZETAPrivateKeyGetterBlock)(NSError *error, ZETAErasablePrivateKey *privateKey);

@protocol ApolloCoreCryptoSecureStoreProtocol

- (void)privateKeyWithIdentifier:(NSString *)keyID
                      userPrompt:(BOOL)promptUser
                      completion:(ZETAPrivateKeyGetterBlock)completion;

@end

