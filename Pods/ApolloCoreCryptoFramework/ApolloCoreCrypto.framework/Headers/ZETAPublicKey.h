#import <Foundation/Foundation.h>
#include <OpenSSL-Universal/openssl/evp.h>

@interface ZETAPublicKey : NSObject<NSCoding>

- (instancetype)initWithPublicKeyData:(NSData *)publicKeyData;

@property (nonatomic, copy, readonly) NSData *publicKeyData;

- (NSString *)base64EncodedString;

/**
 Returns the first three bytes of SHA256 of base64Encoded public key.
 It server as the public key identifier.
 SHA256(base64(publicKey))[0..2
 */
- (NSData *)publicKeyIdentifier;

/**
Generates EVP_PKEY from the public key.
*/
- (EVP_PKEY *)EVP_PKEY;

@end
