#import <Foundation/Foundation.h>
#import "ZETAErasablePrivateKey.h"
#import "ZETAPublicKey.h"

@interface ZETAECKeyPair : NSObject

- (instancetype)initWithPublicKey:(ZETAPublicKey *)publicKey
                       privateKey:(ZETAErasablePrivateKey *)privateKey;

+ (ZETAECKeyPair *)generateKeyPair;

+ (ZETAECKeyPair *)generateKeyPairForCurveName:(ZETAECCurveName)curveName;

@property (nonatomic, readonly) ZETAErasablePrivateKey *privateKey;
@property (nonatomic, readonly) ZETAPublicKey *publicKey;

@end
