#import "ZETAErasablePrivateKey.h"
#import "ZETAPublicKey.h"

@interface ZETAVoucherGenerationParams : NSObject<NSCoding>

- (instancetype)initWithPrivateKey:(ZETAErasablePrivateKey *)privateKey
                         publicKey:(ZETAPublicKey *)publicKey
                     mintingSecret:(NSData *)mintingSecret
                          minterID:(NSString *)minterID;

@property (nonatomic, readonly) ZETAErasablePrivateKey *privateKey;
@property (nonatomic, readonly) ZETAPublicKey *publicKey;
@property (nonatomic, readonly, copy) NSData *mintingSecret;
@property (nonatomic, readonly, copy) NSString *minterID;

@end
