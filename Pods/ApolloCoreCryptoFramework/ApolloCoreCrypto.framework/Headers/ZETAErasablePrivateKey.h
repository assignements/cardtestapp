#import <Foundation/Foundation.h>
#import <OpenSSL-Universal/openssl/evp.h>
#import "ZETAECConstants.h"

@interface ZETAErasablePrivateKey : NSObject<NSCoding>

- (instancetype)initWithPrivateKeyData:(NSMutableData *)privateKeyData;

- (instancetype)initWithSlicedKeyData:(NSData *)slicedData curveName:(ZETAECCurveName)curveName;

/**
 Generates EVP_PKEY version of the ZETAErasablePrivateKey.
 */
- (EVP_PKEY *)EVP_PKEY;

- (BOOL)isPrivateKeyNegative;

- (NSData *)slicedPrivateKeyData;

- (NSData *)encodedPrivateKeyData;

@end
