#import <Foundation/Foundation.h>
#import "ZETAECKeyPair.h"

@interface ZETAAESGCMEncryptionResponse : NSObject

@property (nonatomic, readonly) NSData *cipherData;
@property (nonatomic, readonly) NSData *authenticationTag;

@end

FOUNDATION_EXPORT NSString *const ZETACryptoFunctionsErrorDomain;

@interface ZETACryptoFunctions : NSObject

+ (NSData *)AES128EncryptWithKey:(NSData *)key
                              IV:(NSData *)IV
                       inputData:(NSData *)input
                           error:(NSError **)error;

+ (NSData *)AES256EncryptWithKey:(NSData *)key
                              IV:(NSData *)IV
                       inputData:(NSData *)input
                           error:(NSError **)error;

+ (NSData *)AES128DecryptWithKey:(NSData *)key
                              IV:(NSData *)IV
                       inputData:(NSData *)input
                           error:(NSError **)error;

+ (NSData *)AES256DecryptWithKey:(NSData *)key
                              IV:(NSData *)IV
                       inputData:(NSData *)input
                           error:(NSError **)error;

+ (NSData *)AES128KeyFromPassword:(NSString *)password salt:(NSData *)salt error:(NSError **)error;

/**
 * ECIES : Elliptic Curve Integrated Encryption Scheme
 * Java reference code :
 * http://phab.corp.zeta.in/diffusion/CL/browse/master/IesCipher/src/main/java/in/zeta/cipher/ECIESCipher.java
 * Uses AES128 bit encryption
 * PKI currently used is 3 empty bytes
 * It also generates a ECKeypair using NID_X9_62_prime256v1 curve
 */

+ (NSData *)ECIESEncryptData:(NSData *)input publicKey:(NSData *)publicKey error:(NSError **)error;

+ (NSData *)ECIESEncryptData:(NSData *)input
             serverPublicKey:(NSData *)publicKey
                 userKeyPair:(ZETAECKeyPair *)keyPair
                       error:(NSError **)error;

/**
 * This method first creates a shared secret using the private key and public key provided.
 * Gets the first 16 bytes of the shared secret and uses it as the AES decryption key with IV
 * provided.
 */
+ (NSData *)decryptData:(NSData *)encryptedData
         userPrivateKey:(ZETAErasablePrivateKey *)privateKey
          peerPublicKey:(ZETAPublicKey *)publicKey
                     IV:(NSData *)IV;

+ (ZETAAESGCMEncryptionResponse *)encryptPlainData:(NSData *)plainData
                                               aad:(NSData *)aad
                                               key:(NSData *)key
                                                IV:(NSData *)IV;

@end
