#import <Foundation/Foundation.h>
#import <ApolloCoreCrypto/ZETAECKeyPair.h>

@interface ApolloCryptoHelper : NSObject

+ (NSString *)generateRootCertWithIdentifier:(NSString *)identifier;

+ (NSString *)generateRootCertForPublicKey:(ZETAPublicKey *)publicKey
                                identifier:(NSString *)identifier;

+ (NSString *)generateRootCertForPublicKey:(ZETAPublicKey *)publicKey
                        signWithPrivateKey:(ZETAErasablePrivateKey *)privateKey
                                identifier:(NSString *)identifier;

+ (NSString *)generateRootCertWithCert:(NSString *)encodedCert
                             publicKey:(ZETAPublicKey *)publicKey
                            privateKey:(ZETAErasablePrivateKey *)privateKey
                            identifier:(NSString *)identifier;

+ (NSData *)decryptSecretFromEncodedSharedSecret:(NSString *)secret
                                  encodedPeerKey:(NSString *)peerkey
                               encodedPrivateKey:(NSString *)privateKey
                                      identifier:(NSString *)identifier;

+ (NSString *)subjectFromCertificateData:(NSString *)base64EncodedCertificatePEM;
+ (NSString *)subjectCNFromCertificate:(NSString *)base64EncodedCertificatePEM;

@end
