#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "ZETASignedAuthorization.h"

FOUNDATION_EXPORT NSString *const ZETARequestPayloadHeaderKey;

FOUNDATION_EXPORT NSString *const ZETARequestPayloadHeaderSignatoryJIDKey;
FOUNDATION_EXPORT NSString *const ZETARequestPayloadHeaderSignatureKey;

@interface ZETASignablePayload : MTLModel<MTLJSONSerializing>

@property (nonatomic) NSDictionary *headers;

- (NSDictionary *)propertyNameToSigningKeyMapping;
- (NSString *)signatoryJID;
- (NSString *)signature;

@end
