#import <Foundation/Foundation.h>

@interface ZETAJWE : NSObject

- (instancetype)initWithPlainText:(NSString *)plainText;

- (NSString *)encodedJWETokenWithPublicKey:(NSString *)publicKey;

@end
