#import <Foundation/Foundation.h>
#import "ZETAErasablePrivateKey.h"
#import "ZETAPublicKey.h"
#import "ZETASignablePayload.h"

@protocol ZETASignableObject
- (id) signableData;
@end

@interface ZETASignatureUtil : NSObject

/**
 This method generates a shared secret using the User's private key and peer's public Key.

 @note This method takes care of erasing the privateKeyData after using.
 */
+ (NSData *)sharedSecretFromUserPrivateKey:(ZETAErasablePrivateKey *)privateKey
                             peerPublicKey:(ZETAPublicKey *)peerKey;

/**
 Returns the signable string for the `ZETASignablePayload` object.

 Every object inside `object` hierarchy should belong to one of following:
 `NSURL`, `NSNumber`, `NSString`, `NSArray`, `NSDictionary`, `ZETASignablePaylad` or should implement the protocol ZETASignableObject
 */
+ (NSString *)signableStringForPayload:(id)object;

+ (NSString *)signableStringForJSONObject:(NSDictionary *)JSONObject;

@end
