#import <CommonCrypto/CommonCrypto.h>
#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const ZETAAESOperationErrorDomain;

@interface ZETACryptoUtil : NSObject

+ (NSData *)generateRandomDataOfLength:(NSUInteger)length;

+ (NSData *)XORData1:(NSData *)data1 data2:(NSData *)data2;

+ (NSArray *)generateSecureXORPairForData:(NSData *)data;

+ (NSData *)emptyDataWithNumberOfBytes:(NSUInteger)numberOfBytes;

@end
