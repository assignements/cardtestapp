#include <OpenSSL-Universal/openssl/x509.h>
#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSUInteger const ZETAECKeyASN1Flag;

typedef NS_ENUM(NSUInteger, ZETAECCurveName) {
  ZETAECCurveNamePrime256v1 = NID_X9_62_prime256v1,
  ZETAECCurveNameSecp112r2 = NID_secp112r2,
  ZETAECCurveNamePrime192v1 = NID_X9_62_prime192v1
};
