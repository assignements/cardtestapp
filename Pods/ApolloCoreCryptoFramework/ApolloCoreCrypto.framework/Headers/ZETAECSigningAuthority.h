#import <Foundation/Foundation.h>
#import "ZETAECKeyPair.h"
#import "ZETAErasablePrivateKey.h"
#import "ApolloCoreCryptoSecureStoreProtocol.h"
#import "ZETASignablePayload.h"
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

typedef void (^ZETASignedJSONObjectCompletionBlock)(NSError *error, NSDictionary *signedJSONObject);

@interface ZETAECSigningAuthority : NSObject

/** Using this initializer, default privateKeyIdentifier "EncryptedPrivateKey" is used
    to fetch private key from secure store.
 */
- (instancetype)initWithAuthDataStore:(id<ApolloAuthDataProtocol>)authDataStore
                          secureStore:(id<ApolloCoreCryptoSecureStoreProtocol>)secureStore;

- (instancetype)initWithAuthDataStore:(id<ApolloAuthDataProtocol>)authDataStore
                          secureStore:(id<ApolloCoreCryptoSecureStoreProtocol>)secureStore
                 privateKeyIdentifier:(NSString *)privateKeyIdentifier;

/**
 Convenience method for `signSignablePayload:promptUser:completion` with promptUser as YES
 @param context This is the context string used to print error message.
 */
- (void)signSignablePayload:(ZETASignablePayload *)payload
                 completion:(ZETAFailureBlockType)completion
             signingContext:(NSString *)context;

/**
 This method signs the provided payload with the private key in `userCredentials`
 By signing, it sets the signatoryJID and signature headers to the payload.

 @param promptUser trigger authentication if needed
 @param context This is the context string used to print error message.
 */
- (void)signSignablePayload:(ZETASignablePayload *)payload
                 promptUser:(BOOL)promptUser
                 completion:(ZETAFailureBlockType)completion
             signingContext:(NSString *)context;

- (void)signJSONObject:(NSDictionary *)JSONObject
            promptUser:(BOOL)promptUser
            completion:(ZETASignedJSONObjectCompletionBlock)completion
        signingContext:(NSString *)context;

/**
 Generates Zeta signature for the data with passed private key.
 */
+ (NSData *)zetaSignatureForData:(NSData *)data
                  withPrivateKey:(ZETAErasablePrivateKey *)privateKey
                       publicKey:(ZETAPublicKey *)publicKey;

/**
 Generates normal signature for the data with passed private key.
 */
+ (NSData *)signatureForData:(NSData *)data privateKey:(ZETAErasablePrivateKey *)privateKey;

/**
 Generates modified ES256 signature for the data with passed private key.
 https://forums.developer.apple.com/thread/123723
 https://bitcoin.stackexchange.com/questions/58853/how-do-you-figure-out-the-r-and-s-out-of-a-signature-using-python
 The method extracts R and S from signature data and concatenates them to generate required signature.
 */
+ (NSData *)es256SignatureForData:(NSData *)data privateKey:(ZETAErasablePrivateKey *)privateKey;

/**
 Verifies the normal signature for the passed message and public key.
 */
+ (BOOL)verifySignature:(NSData *)signature
              publicKey:(ZETAPublicKey *)publicKey
                message:(NSData *)message;

+ (BOOL)verifySignatureInPayload:(ZETASignablePayload *)payload
                       publicKey:(ZETAPublicKey *)publicKey;

/**
 * This method can be used for asking the user for an authorization without actually singing some
 * concrete signable payload
 */
- (void)signFakePayloadWithSuccess:(ZETAEmptyBlockType)success
                           failure:(ZETAFailureBlockType)failure;

@end
