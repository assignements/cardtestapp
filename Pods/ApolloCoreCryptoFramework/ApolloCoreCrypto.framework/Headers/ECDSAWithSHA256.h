#ifndef Zeta_ECDSAWithSHA256_H
#define Zeta_ECDSAWithSHA256_H

#include <OpenSSL-Universal/openssl/evp.h>
#include <stdio.h>

#ifndef OPENSSL_NO_SHA256
const EVP_MD *EVP_ecdsa_sha256(void);
#endif

#endif
