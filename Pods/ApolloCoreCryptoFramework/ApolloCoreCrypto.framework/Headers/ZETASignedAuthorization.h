#import <Foundation/Foundation.h>
/**
 * Every authorization which needs to be sent to the payment connect should adhere to this protocol
 */

@protocol ZETASignedAuthorization<NSObject>

@end
