#import <UIKit/UIKit.h>
#import "ZETALabelWithBorder.h"

@interface ZETAInboxItemTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet ZETALabelWithBorder *statusLabel;

@end
