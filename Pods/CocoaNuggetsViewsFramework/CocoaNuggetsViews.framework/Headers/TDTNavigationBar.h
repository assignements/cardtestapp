//
//  TDTNavigationBar.h
//  Talkto
//
//  Created by Paresh Shukla on 14/01/14.
//  Copyright (c) 2014 Talk.to FZC. All rights reserved.

#import <UIKit/UIKit.h>

/**
 We subclass UINavigationBar to add methods in it so that a view may be appended
 to or removed from navigation bar based on its priority. This is required to
 show small views that we may need to add based on certain state of external
 factors like internet connection. For e.g., presenting a "No Internet Connection"
 view when there is no internet connection.
 */
@interface TDTNavigationBar : UINavigationBar

/**
 This method is to append a view to NavigationBar.
 @param notificationView View to be presented below the navigation bar.
 @param height Height of the notificationView
 @param priority Priority decides the position of view in the view hierarchy,
 i.e., view with higher priority comes above the view with lower priority.
 */
- (void)showNotificationView:(UIView *)notificationView height:(CGFloat)height priority:(NSUInteger)priority;

/**
 This method is to remove an appended view from Navigaton bar.
 @param notificationView View to be hidden from its position below the navigation
 bar
 */
- (void)hideNotificationView:(UIView *)notificationView;

@end
