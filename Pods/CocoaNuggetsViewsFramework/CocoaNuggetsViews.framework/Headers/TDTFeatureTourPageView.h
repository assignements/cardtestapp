//
//  TDTFeatureTourPageView.h
//  Talkto
//
//  Created by Yatin Sarbalia on 5/1/13.
//  Copyright (c) 2013 Talk.to FZC. All rights reserved.
//
// This represents a page view to be presented in feature tour.

#import <UIKit/UIKit.h>

@interface TDTFeatureTourPageView : UIView

/**
 Outlet for the title label to be shown at the top.
 */
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

/**
 Outlet for the subtitle label to be shown at the bottom.
 */
@property (nonatomic, strong) IBOutlet UILabel *subtitleLabel;

/**
 Outlet for the image to be shown in the middle.
 */
@property (nonatomic, strong) IBOutlet UIImageView *imageView;

/**
 Factory method which gives an initialized page view for feature tour.

 @param title  Text to be shown at top as title of the page.
 @param subTitle  Text to be shown at the bottom as subtitle of the page.
 @param image  Image to be shown in the middle of the page.

 @return An initialized instance of TDTFeatureTourPageView.
 */
+ (id)pageViewWithTitle:(NSString *)title
               subTitle:(NSString *)subTitle
                  image:(UIImage *)image;

@end
