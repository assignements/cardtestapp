#import <UIKit/UIKit.h>
#import "ZETANumberPadView.h"

@interface ZETAPinKeypadView : UIView

+ (instancetype)defaultPinKeypad;

@property (nonatomic, weak) IBOutlet id<ZETANumberPadViewDelegate> delegate;

@end
