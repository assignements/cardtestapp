//
//  TDTMimeTypes.h
//  Talkto
//
//  Created by Udit Agarwal on 04/11/15.
//  Copyright © 2015 Talk.to FZC. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const TDTMimeTypeImageGIF;
