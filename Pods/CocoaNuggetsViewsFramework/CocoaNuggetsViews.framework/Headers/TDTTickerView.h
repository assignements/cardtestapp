//
//  TDTTickerView.h
//  Talkto
//
//  Created by Yatin Sarbalia on 1/11/13.
//  Copyright (c) 2013 Talk.to FZC. All rights reserved.
//
// This class represents a ticker view to be presented on top of roster

#import <UIKit/UIKit.h>

@protocol TDTTickerViewDelegate;


@interface TDTTickerView : UIView

@property (nonatomic, weak) id<TDTTickerViewDelegate> delegate;

- (void)setLabelText:(NSString *)labelText;
- (void)setLabelAttributedText:(NSMutableAttributedString *)attributedText;
- (void)setLabelAccessibilityIdentifier:(NSString *)accessibilityIdentifier;

- (id)initWithFrame:(CGRect)frame
          iconImage:(UIImage *)iconImage
    backgroundColor:(UIColor *)backgroundColor
              title:(NSString *)title
           subtitle:(NSString *)subtitle
        buttonTitle:(NSString *)buttonTitle;

@end


@protocol TDTTickerViewDelegate <NSObject>

- (void)tickerViewDidPressButton:(TDTTickerView *)tickerView;

@end
