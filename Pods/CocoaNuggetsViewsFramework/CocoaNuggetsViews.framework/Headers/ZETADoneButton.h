#import <UIKit/UIKit.h>

/**
 * ZETADoneButton is used to automatically set the background image for highlighted, normal and
 * disabled state.
 *
 * Note: You have to mention the type of the button defined as Custom in the IB
 */

IB_DESIGNABLE
@interface ZETADoneButton : UIButton

@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable UIColor *backgroundImageColor;
@property (nonatomic) IBInspectable UIColor *borderColor;

@end
