//
//  FLAnimatedImageView+TDTAdditions.h
//  Talkto
//
//  Created by Udit Agarwal on 06/11/15.
//  Copyright (c) 2015 Talk.to FZC. All rights reserved.
//

#import <FLAnimatedImage/FLAnimatedImageView.h>

@interface FLAnimatedImageView (TDTAdditions)

- (void)tdt_setImageWithData:(NSData *)data;

@end
