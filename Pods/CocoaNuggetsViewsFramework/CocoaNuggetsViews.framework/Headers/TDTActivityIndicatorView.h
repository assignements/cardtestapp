//
//  TDTActivityIndicatorView.h
//  Talkto
//
//  Created by Yatin Sarbalia on 31/08/12.
//  Copyright (c) 2012 Talk.to FZC. All rights reserved.
//
// This class represents a custom view similar to activityIndicator control in iOS.
// Reason for implementing this class is the constraint that the activityIndicator control
// is only available in two sizes : small and large.

#import <UIKit/UIKit.h>

@interface TDTActivityIndicatorView : UIView

/**
 A Boolean value that controls whether the view is hidden when the animation is stopped.
 */
@property (nonatomic, assign) BOOL hidesWhenStopped;

/**
 Designated Initializer. Create an instance with frame and spinner image

 @param  frame Frame for this view.
 @param  spinnerImage  Image of ppinner to be shown on this view.
 */
- (id)initWithFrame:(CGRect)frame spinnerImage:(UIImage *)spinnerImage;

/**
 Similar to UIActivityIndicatorView to start animation.
 */
- (void)startAnimating;

/**
 Similar to UIActivityIndicatorView to stop animation.
 */
- (void)stopAnimating;

@end
