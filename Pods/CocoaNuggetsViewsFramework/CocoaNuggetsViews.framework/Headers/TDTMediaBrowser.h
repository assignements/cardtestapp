
#import <UIKit/UIKit.h>

@protocol TDTMediaBrowserDataSource, TDTMediaBrowserDelegate;

/**
 This class presents a set of media items.
 */
@interface TDTMediaBrowser : UIViewController

/**
 @see @p TDTMediaBrowserDataSource.
 */
@property (nonatomic) id<TDTMediaBrowserDataSource> dataSource;

/**
 @see @p TDTMediaBrowserDelegate.
 */
@property (nonatomic, weak) id<TDTMediaBrowserDelegate> delegate;

@property (nonatomic, copy) NSString *closeButtonText;
@property (nonatomic, copy) NSString *replyWithDoodleButtonText;
/**
 Title to be shown when no images in browser
 */
@property (nonatomic, copy) NSString *controllerTitle;

/**
 The format gets two @p NSString denoting image index and count.
 */
@property (nonatomic, copy) NSString *imageIndexFormat;


/**
 Change the starting index of the browser.
 */
- (void)setInitialPageIndex:(NSUInteger)index;

/**
 @return YES if the controls are hidden, else NO.
 */
- (BOOL)areControlsHidden;

/**
 Toggle controls visibility.

 @param animated YES to animate the toggle.
 */
- (void)toggleControlsAnimated:(BOOL)animated;

/**
 @param index for which the content needs to be updated
 This method will reload the image data if visible page's index lies
 in the index's range
 */
- (void)replaceViewForIndex:(NSUInteger)index;

@end

@protocol TDTMediaBrowserDataSource <NSObject>

/**
 @return Number of media items to be shown.
 */
- (NSUInteger)numberOfMediaItemsInMediaBrowser:(TDTMediaBrowser *)browser;

- (BOOL)shouldShowActivityIndicatorForIndex:(NSUInteger)index;

@optional

/**
 @return Media item @p NSData at given index.
 */
- (NSData *)mediaBrowser:(TDTMediaBrowser *)browser mediaItemDataAtIndex:(NSUInteger)index;

/**
 @return Caption for media item at given index.
 */
- (NSString *)mediaBrowser:(TDTMediaBrowser *)browser captionAtIndex:(NSUInteger)index;

@end

@protocol TDTMediaBrowserDelegate <NSObject>

/**
 Tells the delegate that the user has tapped close.

 @param mediaBrowser A @p TDTMediaBrowser object informing the delegate about the
        impending cancellation.
 */
- (void)mediaBrowserDidTapCloseButton:(TDTMediaBrowser *)mediaBrowser;

/**
 Tells the delegate that the user wants to draw doodle on the selected image.

 @param mediaBrowser A @p TDTMediaBrowser object informing the delegate about the
        impending cancellation.
 @param image The image selected by the user.

 @see @p TDTDoodleViewController
 */
- (void)mediaBrowser:(TDTMediaBrowser *)mediaBrowser presentDoodleViewControllerWithBackgroundImage:(UIImage *)image;

@end
