#import <UIKit/UIKit.h>

/**
 The methods from this category can be called from a view controller's `-viewDidLayoutSubviews`
 method to set the view controller's background as per zeta theme
 */
@interface UIViewController (ZETABackground)

/**
 Adds a gradient layer of zeta background colors. The layer size is same as view controller's view
 bounds.
 */
- (void)zeta_setThemeGradientBackground;

/**
 Adds a gradient layer of zeta background colors even if layer size is same
 as view controller's view bounds using the specified opacity.
 */
- (void)zeta_setThemeGradientBackgroundForcefullyWithOpacity:(CGFloat)opacity;

/**
 Adds a gradient layer of zeta background colors. The layer size is equal to view controller's view
 bounds with the @p insets subtracted from it.

 @param insets view controller's view's insets for which the background layer should not be added
 */
- (void)zeta_setThemeGradientBackgroundWithInsets:(UIEdgeInsets)insets;

/**
 Adds a gradient layer of zeta background colors. The layer size is same as view controller's view
 bounds. The layer has an opacity equal to @p opacity.

 @param opacity opacity of the background gradient layer
 */
- (void)zeta_setThemeGradientBackgroundWithOpacity:(CGFloat)opacity;

/**
 Adds a gradient layer of zeta background colors. The layer size is same as content view's
 bounds. The layer has an opacity equal to @p opacity.
 
 @param opacity opacity of the background gradient layer
 
 @param contentView is a view that contains the content of view controller
 */
- (void)zeta_setThemeGradientBackgroundWithOpacity:(CGFloat)opacity
                                     onContentView:(UIView *)contentView;

@end
