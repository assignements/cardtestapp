#import <Foundation/Foundation.h>
#import "ZETANumberPadView.h"

@protocol ZETANumericKeyboardProtocol<NSObject>

@property (readonly, nonatomic) ZETANumberPadView *numberPadView;

@end
