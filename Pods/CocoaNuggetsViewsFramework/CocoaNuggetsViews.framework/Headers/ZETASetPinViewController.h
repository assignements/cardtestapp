#import <UIKit/UIKit.h>
#import <ApolloCommonsCore/ApolloMacros.h>
#import <ApolloAnalyticsCore/ApolloAnalyticsCore.h>
#import "PinViewControllerConfig.h"

FOUNDATION_EXPORT NSString *const ZETAWrongPinEnteredErrorDomain;
typedef NS_ENUM(NSUInteger, ZETAWrongPinErrorCode) { ZETAWrongPinEnteredErrorCode };

typedef NS_ENUM(NSUInteger, ZETASetPinFlowType) {
  ZETASetPinSignupFlowType,
  ZETASetPinSuperCardFlowType
};

@protocol ZETASetPinViewControllerDelegate;
@protocol ZETASetPinViewControllerDataSource;

@interface ZETASetPinViewController : UIViewController

- (instancetype)initWithSetPinFlowType:(ZETASetPinFlowType)setPinFlowType
                      analyticsManager:(ApolloAnalyticsManager *)analyticsManager;

- (instancetype)initWithBackButtonEnabled:(BOOL)backButtonEnabled
                           setPinFlowType:(ZETASetPinFlowType)setPinFlowType
                         analyticsManager:(ApolloAnalyticsManager *)analyticsManager;

@property (nonatomic, weak) id<ZETASetPinViewControllerDelegate> delegate;
@property (nonatomic, weak) id<ZETASetPinViewControllerDataSource> dataSource;

@end

@protocol ZETASetPinViewControllerDelegate<NSObject>

- (void)setPinViewControllerDidEnterWrongPin:(ZETASetPinViewController *)viewController
                             completionBlock:(ZETAEmptyBlockType)completionBlock;

- (void)setPinViewController:(ZETASetPinViewController *)viewController
               didCollectPin:(NSString *)pin;

- (PinViewControllerConfig *)setPinViewControllerDidRequestViewConfig:(ZETASetPinViewController *)viewController;

@optional
- (void)setPinViewControllerDidTapDismiss:(ZETASetPinViewController *)viewController;

@end

@protocol ZETASetPinViewControllerDataSource<NSObject>

- (NSString *)setPinTitleForSetPinViewController:(ZETASetPinViewController *)viewController;
- (NSString *)confirmPinTitleForSetPinViewController:(ZETASetPinViewController *)viewController;

@end
