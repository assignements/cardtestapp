//
//  TDTSwipeCell.h
//  Talkto
//
//  Created by Yatin Sarbalia on 04/07/12.
//  Copyright (c) 2012 Talk.to FZC. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 This class represents a custom UITableViewCell with swipe functionality.
 TDTSwipeController uses instances of this class to provide swipe functionality
 for tableView cells.
 */
@interface TDTSwipeCell : UITableViewCell

/**
 This property represents the default view to be shown in the receiver cell.
 */
@property (nonatomic) UIView *customView;

/**
 This property represents the view with menu options to be shown when the cell is swiped.
 Instance of TDTSwipeMenuView can be used to set this property.
 */
@property (nonatomic) UIView *menuView;

/**
 This property represents if the cell can be swiped or not.
 */
@property (nonatomic) BOOL isSwipable;

/**
 This property identifies whether swipe in or out animation is currently goin on or not.
 */
@property (nonatomic) BOOL isAnimating;

/**
 Returns whether `menuView` is visible or not.
 */
- (BOOL)isMenuViewVisible;

/**
 This method hides `menuView` if visible.
 */
- (void)hideMenuView:(BOOL)animated;

/**
 This method reveals `menuView` completely.
 */
- (void)showMenuView:(BOOL)animated;

/**
 Method to show `menuView` partially with given `translation`.
 */
- (void)showMenuViewPartiallyWithWidth:(CGFloat)width;

/**
 This method calls the `setNeedsDisplay` method on `customView` property.
 This method must be called after changing any property of `customView` through public methods.
 */
- (void)redisplay;

@end
