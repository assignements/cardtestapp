#import <UIKit/UIKit.h>

extern const CGFloat TDTInviteTickerViewSingleLineWidthThreshold;

@protocol TDTInviteTickerViewDelegate;

/**
 This view presents an invite ticker on chat screens.
 If the width of the `TDTInviteTickerView` is less than the defined threshold,
 `TDTInviteTickerView` layouts it's subviews according to its double line layout.
 Otherwise the single line layout is used.
 */
@interface TDTInviteTickerView : UIView

@property (nonatomic, weak) id<TDTInviteTickerViewDelegate> delegate;

/**
 Used to give TDTInviteTickerView a chance to tell its preferred height for single line layout.
 */
+ (CGFloat)preferredHeightForSingleLineLayout;

/**
 Used to give TDTInviteTickerView a chance to tell its preferred height for double line layout.
 */
+ (CGFloat)preferredHeightForDoubleLineLayout;

/**
 Designated Initializer.

 @param individualName Name of the individual to be invited.

 @return An initialized object.
 */
- (id)initWithIndividualName:(NSString *)individualName;

@end

@protocol TDTInviteTickerViewDelegate <NSObject>

/**
 This method is called when the invite button is pressed on ticker view.

 @param tickerView Reference of TDTInviteTickerView on which the invite button was present.
 */
- (void)inviteTickerViewDidPressInviteButton:(TDTInviteTickerView *)tickerView;

/**
 This method is called when the later button is pressed on ticker view.

 @param tickerView Reference of TDTInviteTickerView on which the later button was present.
 */
- (void)inviteTickerViewDidPressLaterButton:(TDTInviteTickerView *)tickerView;

@end
