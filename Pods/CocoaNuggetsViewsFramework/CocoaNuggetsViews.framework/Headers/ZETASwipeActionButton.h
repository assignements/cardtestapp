#import <UIKit/UIKit.h>

@protocol ZETASwipeActionButtonDelegate;

@interface ZETASwipeActionButton : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) id<ZETASwipeActionButtonDelegate> delegate;
@property (nonatomic) UIColor *holderFillColor;
@property (nonatomic) NSURL *holderImageURL;
@property (nonatomic) CGFloat holderPadding;
@property (nonatomic) UIColor *afterSlideBackgroundColor;
@property (nonatomic) NSURL *loadingGifURL;
@property (nonatomic) BOOL fadeHolderWhileTranslating;
@property (nonatomic) BOOL shouldNudge;
@property (nonatomic) CGFloat loaderWidth;
@property (nonatomic) CGFloat loaderHeight;
@property (nonatomic) CGFloat providedCornerRadius;

- (void) rightAlignText;
- (void) centerAlignText;
- (void) leftAlignText;


@end

@protocol ZETASwipeActionButtonDelegate<NSObject>

- (void)swipeActionButtonDidSwipe:(ZETASwipeActionButton *)button;

@end
