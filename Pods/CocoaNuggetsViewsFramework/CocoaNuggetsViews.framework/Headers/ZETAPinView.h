#import <UIKit/UIKit.h>
#import "ZETANumberPadView.h"

@protocol ZETAPinViewDelegate;

IB_DESIGNABLE
@interface ZETAPinView : UIView

@property (nonatomic, weak) IBOutlet id<ZETAPinViewDelegate> delegate;

@property (nonatomic) IBInspectable UIColor *selectedDotColor;
@property (nonatomic) IBInspectable UIColor *unselectedDotColor;

- (void)activate;

- (void)reset;

- (void)bindWithNumericKeyboard:(ZETANumberPadView *)numberPadView;

@end

@protocol ZETAPinViewDelegate<NSObject>

- (void)pinView:(ZETAPinView *)view didSetPin:(NSString *)pin;

@end
