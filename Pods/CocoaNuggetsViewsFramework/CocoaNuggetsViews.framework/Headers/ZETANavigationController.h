#import <UIKit/UIKit.h>

@interface ZETANavigationController : UINavigationController

@property (nonatomic, readonly) BOOL canPushOrPop;
@property (nonatomic, readonly) id navLock;

- (void) setTransitioningDelegateProperty:(id<UIViewControllerTransitioningDelegate>) transitioningDelegate;

@end
