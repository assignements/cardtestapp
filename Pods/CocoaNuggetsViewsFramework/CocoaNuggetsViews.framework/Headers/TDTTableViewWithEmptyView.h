//
//  TDTTableViewWithEmptyView.h
//  Talkto
//
//  Created by Yatin Sarbalia on 26/03/12.
//  Copyright (c) 2012 Talk.to FZC. All rights reserved.
//
// This represents a tableview which shows a emptyView when it has zero number of rows
// The emptyView is a UIView passed during initialisation.

#import <UIKit/UIKit.h>
#import "TDTTableView.h"
@class TDTEmptyScreenView;

@interface TDTTableViewWithEmptyView : TDTTableView

/**
 Empty view to be shown when this table view has zero number of rows.
 */
@property (nonatomic, strong) UIView *emptyView;

- (id)initWithFrame:(CGRect)frame
              style:(UITableViewStyle)style
          emptyView:(UIView *)emptyView;

- (id)initWithCoder:(NSCoder *)aDecoder
          emptyView:(UIView *)emptyView;

@end
