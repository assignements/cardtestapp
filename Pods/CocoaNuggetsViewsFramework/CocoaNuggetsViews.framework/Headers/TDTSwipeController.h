//
//  TDTSwipeController.h
//  Talkto
//
//  Created by Yatin Sarbalia on 04/07/12.
//  Copyright (c) 2012 Talk.to FZC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TDTSwipeCell, TDTSwipeMenuView;

/**
 This abstract class represents a UITableViewController subclass with swipe functionality on rows.
 This class must be subclassed in order to implement swipe functionality.
 Also, the custom subclasses must use instances of TDTSwipeCell as cells for the `tableView`.
 TDTSwipeCell class can be subclassed to create custom cells for the custom subclass of TDTSwipeController.
 */
@interface TDTSwipeController : UITableViewController <UIGestureRecognizerDelegate>

/**
 This property represents a recognizer for recognizing dragging gesture on `tableView`.
 */
@property (nonatomic, readonly) UIPanGestureRecognizer *dragGestureRecognizer;

/**
 This property identifies the last indexPath on which swipe was performed.
 */
@property (nonatomic) NSIndexPath *lastSwipedIndexPath;

/**
 This property identifies whether swipe view is currently visible on any cell.
 */
@property (nonatomic) BOOL isSwipeViewVisible;

/**
 Represents whether swipe is currently going on or not.
 */
@property (nonatomic) BOOL isSwiping;

/**
 This method creates an instance of TDTSwipeMenuView with `buttons`.

 @param buttons Array of instances of UIButton to be shown inside menu view.
 */
- (TDTSwipeMenuView *)viewForSwipeWithButtons:(NSArray *)buttons;

/**
 This method is called whenever the swipe gets closed completely.
 This method is an empty method, override this method to handle hiding of swipeView.
 */
- (void)swipeViewDidClose;

/**
 Remove the side swipe view.

 @param animated If animated is YES, then the swipe view gets hidden using a bounce effect animation.
 */
- (void)hideSwipeView:(BOOL)animated;

@end
