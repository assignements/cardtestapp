#import <UIKit/UIKit.h>
#import "ZETANumericKeyboardProtocol.h"

@interface ZETANumericKeyboardView : UIView <ZETANumericKeyboardProtocol>

+ (instancetype)keyboardWithDefaultInsets;

+ (instancetype)keyboardWithEdgeInsets:(UIEdgeInsets)edgeInsets;

@end
