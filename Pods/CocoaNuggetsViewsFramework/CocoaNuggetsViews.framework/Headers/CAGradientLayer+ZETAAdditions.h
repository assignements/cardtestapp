#import <QuartzCore/QuartzCore.h>

@interface CAGradientLayer (ZETAAdditions)

/**
 @param colors NSArray<UIColor *>
 */
+ (instancetype)zeta_gradientLayerWithColors:(NSArray *)colors frame:(CGRect)frame;

/**
 * @description Start Color : 633EA5
 *              End Color   : 38235C
 */
+ (instancetype)zeta_gradientLayerWithThemeColorsWithFrame:(CGRect)frame;

@end
