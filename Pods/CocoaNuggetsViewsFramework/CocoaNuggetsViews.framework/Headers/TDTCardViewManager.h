//
//  TDTCardViewManager.h
//  Talkto
//
//  Created by udit.ag on 18/12/13.
//  Copyright (c) 2013 Talk.to FZC. All rights reserved.
//
//  This presents a card-like view with translucent background over the remaining screen.
//  This view is shown on the keyWindow property of [UIApplication sharedApplication].

#import <UIKit/UIKit.h>

@interface TDTCardViewManager : NSObject

/**
 This property tells whether card view is allowed to rotate or not.
 By default, its value is NO.
 */
@property (nonatomic) BOOL shouldAutorotate;

/**
 The size of the content view.
 */
@property (nonatomic) CGSize contentSize;

/**
 This property tells whether card view should be closed by tapping translucent view.
 By default, its value is NO.
 */
@property (nonatomic) BOOL dismissOnTappingTranslucentView;

/**
 Designated initializer.

 @param contentView   View to be shown on this controller.
 @param contentSize The size of the content view.
 This can also be changed later on using property contentSize.
 @return An initialized object.
 */
- (id)initWithContentView:(UIView *)contentView
              contentSize:(CGSize)contentSize;

/**
 This shows the card view on the keyWindow.
 */
- (void)show;

/**
 This removes the card view from its superview.
 */
- (void)hide;

@end
