//
//  TDTMediaCaptionView.h
//  Talkto
//
//  Created by Yatin Sarbalia on 3/30/13.
//  Copyright (c) 2013 Talk.to FZC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDTMediaCaptionView : UIView

- (id)initWithCaption:(NSString *)caption;

@property (nonatomic, copy) NSString *caption;

@end
