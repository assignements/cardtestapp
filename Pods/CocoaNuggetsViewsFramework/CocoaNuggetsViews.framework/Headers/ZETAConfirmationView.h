#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ZETAConfirmViewProtocol;

@interface ZETAConfirmationView : UIView

@property (nonatomic, weak) id<ZETAConfirmViewProtocol> delegate;

- (instancetype)initWithImage:(UIImage *)image
                    titleText:(NSString *)titleText
                  messageText:(NSAttributedString *)messageText
            confirmButtonText:(NSString *)confirmButtonText
             cancelButtonText:(NSString *)cancelButtonText;

- (instancetype)initWithImage:(UIImage *)image
                    titleText:(NSString *)titleText
                  messageText:(NSAttributedString *)messageText
            confirmButtonText:(NSString *)confirmButtonText;

@end

@protocol ZETAConfirmViewProtocol <NSObject>

- (void)confirmationViewDidConfirm:(ZETAConfirmationView *)responseView;

@optional
- (void)confirmationViewDidCancel:(ZETAConfirmationView *)responseView;

@end

NS_ASSUME_NONNULL_END
