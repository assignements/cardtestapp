//
//  TDTImagePreviewController.h
//  Talkto
//
//  Created by Ayush on 30/01/13.
//  Copyright (c) 2013 Talk.to FZC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TDTImagePreviewControllerDelegate;

/**
 Present a preview of the provided image. The delegate is notified of any
 actions on the view controller.

 The view controller optionally shows a `Cancel` button as the
 leftBarButtonItem, making it suitable to be presented modally.
 */
@interface TDTImagePreviewController : UIViewController

@property (nonatomic, weak) id<TDTImagePreviewControllerDelegate> delegate;

@property (nonatomic) NSString *cancelButtonText;

/**
 @param image image to be shown in preview
 @param title title to be used for the confirmation button
 
 @discussion The confirmation button is always shown.
 */
- (instancetype)initWithImage:(UIImage *)image
      confirmationButtonTitle:(NSString *)title
              controllerTitle:(NSString *)controllerTitle;

/**
 @param imageData data of image to be shown in preview
 @param title title to be used for the confirmation button
 
 @discussion The confirmation button is always shown.
 */
- (instancetype)initWithImageData:(NSData *)imageData
          confirmationButtonTitle:(NSString *)title
                  controllerTitle:(NSString *)controllerTitle;

/**
 Show/hide cancel button.

 @note The title used for cancel button is "Cancel".
 */
- (void)setShowCancelButton:(BOOL)showCancelButton;

@end

@protocol TDTImagePreviewControllerDelegate <NSObject>

@optional

/**
 Tells the delegate that the user chose an image.
 
 @param imagePreviewController The `TDTImagePreviewController` object informing
 the delegate of the event.
 @param imageData The selected image's data.
 
 @discussion The delegate method is invoked when the user taps on confirmation
 button of @p imagePreviewController.
 
 @note Delegate method @p imagePreviewController:didSelectImage: will also be
 called along with this.
 */
- (void)imagePreviewController:(TDTImagePreviewController *)imagePreviewController
        didSelectImageWithData:(NSData *)imageData;

/**
 Tells the delegate that the user chose an image.
 
 @param imagePreviewController The `TDTImagePreviewController` object informing
 the delegate of the event.
 @param image The selected image.

 @discussion The delegate method is invoked when the user taps on confirmation
 button of @p imagePreviewController.
 
 @note Delegate method @p imagePreviewController:didSelectImageWithData: will
 also be called along with this.
 */
- (void)imagePreviewController:(TDTImagePreviewController *)imagePreviewController
                didSelectImage:(UIImage *)image;

/**
 Tells the delegate that the user cancelled the image selection.

 @param imagePreviewController The `TDTImagePreviewController` object informing
 the delegate of the event.

 @discussion The delegate method is invoked when the user taps on cancel
 button of `imagePreviewController`.
 */
- (void)imagePreviewControllerDidTapCancelButton:(TDTImagePreviewController *)imagePreviewController;

@end
