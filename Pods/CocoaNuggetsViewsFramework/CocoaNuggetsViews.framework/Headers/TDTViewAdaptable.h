#import <Foundation/Foundation.h>

/**
 This protocol should ideally be implemented by @p UIView subclasses only.
 */
@protocol TDTViewAdaptable <NSObject>

/**
 This method sets horizontal center drift to be considered while laying out
 subviews centrally in horizontal direction.
 */
- (void)setHorizontalCenterDrift:(CGFloat)horizontalCenterdrift;

@end
