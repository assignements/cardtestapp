//
//  TDTSwipeMenuView.h
//  Talkto
//
//  Created by Yatin Sarbalia on 05/04/12.
//  Copyright (c) 2012 Talk.to FZC. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 This class represents a custom UIView used to show menu options on side swiping a UITableViewCell.
 This class should be used with UITableView's subclass TDTSwipeCell, where instance of this class is set as
 `menuView` property of instance of TDTSwipeCell.
 */
@interface TDTSwipeMenuView : UIView

/**
 Desginated initializer.

 @param frame Initial frame to be set.
 @param buttons Array of instances of UIButton to be shown inside the receiver view.
 */
- (id)initWithFrame:(CGRect)frame buttons:(NSArray *)buttons;

@end
