//
//  TDTZoomingScrollView.h
//  Talkto
//
//  Created by Yatin Sarbalia on 3/29/13.
//  Copyright (c) 2013 Talk.to FZC. All rights reserved.
//
// This view class can be used to show an image. The view adds zooming
// capability to the image on pinch and tapping gestures.
// Additional work can be done by delegates on single and double taps.

#import <UIKit/UIKit.h>

@interface TDTZoomableImageView : UIView

@property (nonatomic) NSData *imageData;

// This method is called to adjust zooming scales depending upon current bounds
- (void)setMaxMinZoomScalesForCurrentBounds;

// Clean any saved data so that this view can be used
- (void)prepareForReuse;

- (void)showActivityIndicator:(BOOL)shouldShowActivityIndicator;

@end
