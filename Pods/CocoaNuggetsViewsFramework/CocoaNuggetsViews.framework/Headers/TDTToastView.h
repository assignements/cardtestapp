//
//  TDTToastView.h
//  Talkto
//
//  Created by Abhay Singh on 03/07/12.
//  Copyright (c) 2012 Talk.to FZC. All rights reserved.
//
// This class represents a toast view with fade-in and fade-out animation

#import <UIKit/UIKit.h>

typedef enum {
  TDTToastViewDisplayOrderMessageSubject,
  TDTToastViewDisplayOrderSubjectMessage
} TDTToastViewDisplayOrder;

extern const CGFloat TDTToastViewWidthNormal;
extern const CGFloat TDTToastViewWidthWide;
extern const CGFloat TDTToastViewMinWidth;

@interface TDTToastView : UIView

@property (nonatomic, assign) CGFloat duration;

/**
 Designated initializer.

 @param subject This is shown with a bold font, and is truncated if it's size exceeds
 the maximum allowed toast size.
 @param message This will be wrapped around to multiple lines if it exceeds the max width.
 @param image This will be shown at top, and horizontally centered.
 @param displayOrder It determines which of |message| and |subject| appears first.
 Any of |image|, |message| and |subject| can be nil, in which case the corresponding component is skipped.
 @param width It determines the width of toast which can be normal or wider.
 It should be greater than or equal to TDTToastViewMinWidth
 */
- (id)initWithImage:(UIImage *)image
            message:(NSString *)message
            subject:(NSString *)subject
       displayOrder:(TDTToastViewDisplayOrder)displayOrder
              width:(CGFloat)width;


/**
 Convenience initializer for creating a toast without a subject string,
 TDTToastViewDisplayOrder and TDTToastViewWidth
 */
- (id)initWithMessage:(NSString *)message image:(UIImage *)image;

/**
 This returns a toast which is shown when a member is added to a group.
 */
- (id)initWithMessageForAddingGroupMember:(NSString *)message;

/**
 This shows the toast on keyWindow. The toast will be shown for a short default duration.
 */
- (void)show;

/**
 This shows the toast on keyWindow.

 @param interval Duration for which the toast should be shown.
 */
- (void)showForDuration:(float)interval;

/**
 This shows a spinner toast on keyWindow.

 @param title Text to be shown above spinner on the toast.
 */
- (void)showSpinnerToastWithTitle :(NSString *)title;

/**
 This hides the spinner toast.
 */
- (void)hideSpinnerToast;

@end
