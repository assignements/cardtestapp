#import <UIKit/UIKit.h>
#import "ZETALabel.h"

IB_DESIGNABLE
@interface ZETALabelWithBorder : ZETALabel

@property (nonatomic) IBInspectable NSUInteger borderWidth;
@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable NSUInteger cornerRadius;
@property (nonatomic) IBInspectable NSUInteger leftPadding;
@property (nonatomic) IBInspectable NSUInteger rightPadding;
@property (nonatomic) IBInspectable NSUInteger topPadding;
@property (nonatomic) IBInspectable NSUInteger bottomPadding;

@end
