//
//  TDTChatScreenMenuView.h
//  Talkto
//
//  Created by Abhay Singh on 11/12/13.
//  Copyright (c) 2013 Talk.to FZC. All rights reserved.
//

// UIView meant to be used for menu view shown on chat screen.
// Has a left and right button with a caption and image.

#import <UIKit/UIKit.h>

@protocol TDTChatScreenMenuViewDelegate;

@interface TDTChatScreenMenuView : UIView

// Title for left button
@property (nonatomic, copy) NSString *leftButtonTitle;

// Image name for left button in normal state
@property (nonatomic, copy) NSString *leftButtonImageName;

// Image name for left button in highlighted state
@property (nonatomic, copy) NSString *leftButtonHighlightedImageName;

// Image name for left button in disabled state
@property (nonatomic, copy) NSString *leftButtonDisabledImageName;

// Title for right button
@property (nonatomic, copy) NSString *rightButtonTitle;

// Image name for right button in normal state
@property (nonatomic, copy) NSString *rightButtonImageName;

// Image name for right button in highlighted state
@property (nonatomic, copy) NSString *rightButtonHighlightedImageName;

// Image name for right button in disabled state
@property (nonatomic, copy) NSString *rightButtonDisabledImageName;

// Object for calling TDTChatScreenMenuViewDelegate protocol methods
@property (nonatomic, weak) id<TDTChatScreenMenuViewDelegate> delegate;

/**
 Update menu view's left button state.

 @param isEnabled
 Pass NO to disable left button.
 */
- (void)updateLeftButtonStateIsEnabled:(BOOL)isEnabled;

/**
 Update menu view's right button state.

 @param isEnabled
 Pass NO to disable right button.
 */
- (void)updateRightButtonStateIsEnabled:(BOOL)isEnabled;;

/**
 Update menu view's appearance to layout its buttons appropriately.
 Should call this method after setting properties.
 */
- (void)updateMenuViewAppearance;

@end

@protocol TDTChatScreenMenuViewDelegate <NSObject>

/**
 Informs the delegate when left button is pressed.
 @param chatScreenMenuView
 View informing the left button pressed event.
 */
- (void)chatScreenMenuViewDidPressLeftButton:(TDTChatScreenMenuView *)chatScreenMenuView;

/**
 Informs the delegate when right button is pressed.
 @param chatScreenMenuView
 View informing the right button press event.
 */
- (void)chatScreenMenuViewDidPressRightButton:(TDTChatScreenMenuView *)chatScreenMenuView;

@end
