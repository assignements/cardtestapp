#import "ZETADoneButton.h"

IB_DESIGNABLE
@interface ZETALightDoneButton : UIButton

@property (nonatomic) IBInspectable CGFloat cornerRadius;

@end
