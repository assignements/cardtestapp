#import <Foundation/Foundation.h>

FOUNDATION_EXPORT CGFloat const UnselectedPinDotOpacity;

@interface PinViewControllerConfig : NSObject

@property (nonatomic) UIColor *titleColor;
@property (nonatomic) UIColor *selectedDotColor;
@property (nonatomic) UIColor *unselectedDotColor;
@property (nonatomic) UIColor *forgotPinColor;
@property (nonatomic) NSString *appLogoImageName;

- (instancetype)initWithTitleColor:(UIColor *)titleColor
                  selectedDotColor:(UIColor *)selectedDotColor
                unselectedDotColor:(UIColor *)unselectedDotColor
                    forgotPinColor:(UIColor *)forgotPinColor
                  appLogoImageName:(NSString *)appLogoImageName;

+ (PinViewControllerConfig *)defaultConfig;

@end
