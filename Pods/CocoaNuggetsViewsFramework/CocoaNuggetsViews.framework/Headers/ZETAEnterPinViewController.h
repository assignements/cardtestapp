#import <UIKit/UIKit.h>
#import <ApolloAnalyticsCore/ApolloAnalyticsCore.h>
#import <ApolloCommonsCore/ApolloMacros.h>
#import "PinViewControllerConfig.h"

@protocol ZETAEnterPinViewControllerDelegate;

@interface ZETAEnterPinViewController : UIViewController

@property (nonatomic, weak) id<ZETAEnterPinViewControllerDelegate> delegate;

- (instancetype)initWithAppName:(NSString *)appName
               analyticsManager:(ApolloAnalyticsManager *)analyticsManager;

- (void)reset;

@end

@protocol ZETAEnterPinViewControllerDelegate<NSObject>

- (PinViewControllerConfig *)enterPinViewControllerDidRequestViewConfig:(ZETAEnterPinViewController *)viewController;

- (void)enterPinViewControllerDidForgetPin:(ZETAEnterPinViewController *)viewController
                           completionBlock:(ZETAEmptyBlockType)completionBlock;

- (void)enterPinViewController:(ZETAEnterPinViewController *)viewController
                 didCollectPin:(NSString *)pin;

@end
