#import <UIKit/UIKit.h>
#import "ZETANumericKeyboardProtocol.h"

@interface UITextField (ZETANumericKeyboard)

- (void)zeta_setZetaNumericKeyboard;

- (void)zeta_setViewAsNumericKeyboard:(UIView<ZETANumericKeyboardProtocol> *)keyboardView;

- (void)zeta_updateNextButtonState:(BOOL)shouldEnable;

/*
 To be used by view controllers which take up the responsibility of managing keyboard view and its
 delegate methods on its own.
 */
- (BOOL)zeta_numberPadView:(ZETANumberPadView *)numberPadView
    shouldChangeSelectedTextToString:(NSString *)replacementString;

@end
