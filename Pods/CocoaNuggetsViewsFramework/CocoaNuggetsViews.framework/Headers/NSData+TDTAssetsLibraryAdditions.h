//
//  NSData+TDTAssetsLibraryAdditions.h
//  Talkto
//
//  Created by Udit Agarwal on 01/12/15.
//  Copyright © 2015 Talk.to FZC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (TDTAssetsLibraryAdditions)

/**
 Fetches a data object containing the data from the location specified by a 
 given assets library URL.
 
 @param assetURL The assets library URL from which to read data.
 @param completion This block will be called after successful fetching of data.
 
 @note @p nil will be passed to the completion block if data object could not be
       created.
 */
+ (void)tdt_getDataAtAssetsLibraryURL:(NSURL *)assetURL
                  withCompletionBlock:(void(^)(NSData *))completion;

@end
