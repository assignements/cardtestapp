//
//  TDTEmptyScreenView.h
//  Talkto
//
//  Created by Yatin Sarbalia on 23/03/12.
//  Copyright (c) 2012 Talk.to FZC. All rights reserved.
//
// Represents the view for empty states used in the application
// This widget uses another widget: TDTActivityIndicatorView

#import <UIKit/UIKit.h>

@protocol TDTEmptyScreenViewDelegate;

@interface TDTEmptyScreenView : UIView

/**
 Image related to empty screen
 */
@property (nonatomic, strong) UIImage *image;

@property (nonatomic, copy) NSString *normalStateText;
@property (nonatomic, copy) NSString *normalStateSubText;

@property (nonatomic, copy) NSString *loadingStateText;
@property (nonatomic, copy) NSString *loadingStateSubText;

@property (nonatomic, readonly) UIButton *button;
@property (nonatomic, readonly) UILabel *label;
@property (nonatomic, readonly) UILabel *subLabel;

@property (nonatomic) CGFloat bottomPadding;

@property (nonatomic, weak) id<TDTEmptyScreenViewDelegate> delegate;

/**
 This method shows loading state (with activity indicator and text)
 */
- (void)showLoadingState;

/**
 This method hides loading state and shows normal state again
 */
- (void)hideLoadingState;

/**
 This method shows a button with `buttonTitle` as title
 */
- (void)setButtonTitle:(NSString *)buttonTitle;

@end

@protocol TDTEmptyScreenViewDelegate <NSObject>

@optional
- (void)emptyScreenViewDidPressButton:(TDTEmptyScreenView *)emptyScreenView;

@end
