//
//  TDTTextFieldWithShowHideButton.h
//  Talkto
//
//  Created by Prateek Khandelwal on 12/12/13.
//  Copyright (c) 2013 Talk.to FZC. All rights reserved.
//

/**
 TDTTextFieldWithButton is a UITextField with a button for showing or hiding the text
 written in the text field
 */

#import <UIKit/UIKit.h>

@class TDTTextFieldWithButton;

@protocol TDTTextFieldWithButtonDelegate <UITextFieldDelegate>

@optional
/**
 Informs the delegate when textField button is pressed.

 @param button
 Button which is pressed.
 */
- (void)textFieldWithButton:(TDTTextFieldWithButton *)textFieldWithButton
             didPressButton:(UIButton *)button;

@end

@interface TDTTextFieldWithButton:UITextField

@property (nonatomic, weak) id <TDTTextFieldWithButtonDelegate> delegate;

// Initialize the text field with defualt frame and button on the right side
- (id)initWithButtonTitle:(NSString *)title alternateTitle:(NSString *)alternateTitle;

// Name for button in default state
@property (nonatomic, copy) NSString *textFieldButtonTitle;

// Alternate name for button when pressed
@property (nonatomic, copy) NSString *textFieldButtonAlternateTitle;

// Boolean to check whether the text field is editable
@property (nonatomic, assign) BOOL isEditable;

@end
