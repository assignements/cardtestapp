#import <UIKit/UIKit.h>

@interface UINavigationController (ZETANavigationBarAppearance)

- (void)zeta_configureNavigationBarAppearance;
- (void)zeta_setTransparentNavigationBar;

@end
