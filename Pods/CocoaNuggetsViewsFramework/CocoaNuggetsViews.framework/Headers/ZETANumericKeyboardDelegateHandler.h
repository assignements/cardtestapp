#import <Foundation/Foundation.h>
#import "ZETANumberPadView.h"

@interface ZETANumericKeyboardDelegateHandler : NSObject<ZETANumberPadViewDelegate>

- (instancetype)initWithTextFields:(NSArray *)textFields;

@end
