#import <UIKit/UIKit.h>

/**
 The iOS 8.0 SDK allows one to enable "Automatic Preferred Max Layout Width"
 for a multi-line label by leaving the "Preferred Max Layout Width" set to
 non-"Explicit" in Interface Builder.

 This subclass of UILabel provides the same functionality for iOS 7.
 It sets the @p preferredMaxLayoutWidth of multi-line labels to
 their bound width whenever their bounds change.

 Once we transition to an iOS 8 deployment target, this class can be removed
 and the Interface Builder setting can be set back to automatic.
 */
@interface ZETALabel : UILabel

@end
