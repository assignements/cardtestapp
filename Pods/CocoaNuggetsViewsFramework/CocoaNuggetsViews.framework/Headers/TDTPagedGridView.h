//
//  TDTPagedGridView.h
//  Talkto
//
//  Created by nikhil.ji on 11/12/13.
//  Copyright (c) 2013 Talk.to FZC. All rights reserved.
//
//  This class represents a generic class for springboard like horizontal page views.
//  It adjusts the item in a springboard like fashion depending upon fixed item size,
//  padding etc which it takes as parameters. It takes the Items Data from its dataSource and
//  notifies its delegate about any tap events.
//  It is used by EmojiTabbedView, StickersTabbedView etc.

#import <UIKit/UIKit.h>

@protocol TDTPagedGridViewDataSource, TDTPagedGridViewDelegate;

@interface TDTPagedGridView : UIView

// Designated initializer :
// itemsize: Size of each Item to be shown in the pageView.
// edgeInsets: It is the Border padding of the PageView.
// minHorizontalSpacing: It is the minimum spacing between items in a row.
// minVerticalSpacing: It is the minimum spacing between items in a column.
// pageControlHeight: It is the height of the pageControl shown at the bottom of pageView.
- (id)initWithFrame:(CGRect)frame
           itemSize:(CGSize)itemSize
         edgeInsets:(UIEdgeInsets)edgeInsets
minHorizontalSpacing:(CGFloat)minHorizontalSpacing
 minVerticalSpacing:(CGFloat)minVerticalSpacing
  pageControlHeight:(CGFloat)pageControlHeight;

// Delegate is notified of UI events like Emoji pressed etc.
@property (nonatomic, weak) id<TDTPagedGridViewDelegate> delegate;

// DataSource supplies the Data required to fill in the Page View.
@property (nonatomic, weak) id<TDTPagedGridViewDataSource> dataSource;

@end

@protocol TDTPagedGridViewDelegate <NSObject>

// It is called when an item in a pageView is tapped By the user. It passes the item number as the argument.
- (void)pagedGridView:(TDTPagedGridView *)pagedGridView didSelectItemAtIndex:(NSUInteger)index;

@end

@protocol TDTPagedGridViewDataSource <NSObject>
@required

// Returns number of items to be shown for the PageView.
- (NSUInteger)numberOfItemsInPagedGridView:(TDTPagedGridView *)pagedGridView;

// Returns the Button to be shown at a given position. Placement of this button after this in view
// is taken care by this class.
- (UIButton*)pagedGridView:(TDTPagedGridView *)pagedGridView buttonForItemAtIndex:(NSUInteger)index;

@end
