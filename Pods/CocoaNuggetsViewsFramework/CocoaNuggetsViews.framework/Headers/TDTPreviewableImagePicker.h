
#import <Foundation/Foundation.h>

@protocol TDTPreviewableImagePickerDelegate;

/**
 @discussion A replacement for UIImagePickerController when a preview is
 required for a chosen image when
 `sourceType = UIImagePickerControllerSourceTypePhotoLibrary`.

 @note Present/dismiss the property `imagePickerController` of this class.

 Keep a reference to the created instance till you dismiss `imagePickerController`.

 @see `UIImagePickerController`
 */
@interface TDTPreviewableImagePicker : NSObject

/**
 Present and dismiss this controller.
 */
@property (nonatomic, readonly) UIViewController *pickerController;

@property (nonatomic, weak) id<TDTPreviewableImagePickerDelegate> delegate;

/**
 Confirmation button title on preview controller to be presented.
 */
@property (nonatomic, copy) NSString *imagePreviewConfirmationButtonTitle;

@property (nonatomic, copy) NSString *imagePreviewControllerTitle;
@property (nonatomic, copy) NSString *imagePickerControllerCancelButtonText;

/**
 @note Default value is `UIImagePickerControllerSourceTypePhotoLibrary`.
 */
@property (nonatomic) UIImagePickerControllerSourceType sourceType;

/**
 @return YES if `sourceType` is available.
 */
+ (BOOL)isSourceTypeAvailable:(UIImagePickerControllerSourceType)sourceType;

@end

@protocol TDTPreviewableImagePickerDelegate <NSObject>

/**
 Tells the delegate that the user finished and picked a media.

 @param picker The `TDTPreviewableImagePicker` object informing
 the delegate of the event.
 @param info The dictionary contains any relevant information of the picked
 media. The keys for this dictionary are listed in `Editing Information
 Keys` of Apple documentation.

 @discussion Your delegate object’s implementation of this method should pass
 the specified media on to any custom code that needs it, and
 should then dismiss the picker view.

 @see `UIImagePickerControllerDelegate`
 */
- (void)previewableImagePicker:(TDTPreviewableImagePicker *)picker
 didFinishPickingMediaWithInfo:(NSDictionary *)info;

/**
 Tells the delegate that the user finished and cancelled picking any media.

 @param picker The `TDTPreviewableImagePicker` object informing
 the delegate of the event.

 @discussion Your delegate’s implementation of this method should dismiss the
 picker view by calling the dismissModalViewControllerAnimated:
 method of the parent view controller.

 @see `UIImagePickerControllerDelegate`
 */
- (void)previewableImagePickerDidCancel:(TDTPreviewableImagePicker *)picker;

@end
