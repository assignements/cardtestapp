#import <UIKit/UIKit.h>

/**
 * Rearranges the layout with imageview on top and label below it
 */
@interface ZETATopLabelButton : UIButton

@end
