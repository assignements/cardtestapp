#import <UIKit/UIKit.h>

@protocol ZETANumberPadViewDelegate;

@interface ZETANumberPadView : UIView

+ (instancetype)defaultNumberPad;

@property (nonatomic, weak) IBOutlet id<ZETANumberPadViewDelegate> delegate;

- (void)setNextButtonEnabled:(BOOL)isEnabled;

@end

@protocol ZETANumberPadViewDelegate <NSObject>

- (void)numberPadView:(ZETANumberPadView *)numberPadView
         didTapNumber:(NSString *)number;
- (void)numberPadViewNextButtonTapped:(ZETANumberPadView *)numberPadView;
- (void)numberPadViewBackSpaceButtonTapped:(ZETANumberPadView *)numberPadView;

@end
