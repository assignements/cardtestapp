//
//  TDTDateTimeView.h
//  Talkto
//
//  Created by Abhay on 6/3/13.
//  Copyright (c) 2013 Talk.to FZC. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Class to be used as subview of TDTChatMessageCell and TDTAffiliationChangeCell to
 show time and/or date. Presents a UIView to be directly added as subview in
 TDTChatMessageCellInnerView or TDTAffiliationChangeCell object.
 */
@interface TDTDateTimeView : UIView

// UILabel for showing date of message.
@property (nonatomic, strong) UILabel *dateLabel;

// UILabel for showing time of message.
@property (nonatomic, strong) UILabel *timeLabel;

// The separator shown above date label.
@property (nonatomic, strong) UIView *dateSeparatorView;

// BOOL value to indicate whether separator should be displayed.
@property (nonatomic, assign) BOOL shouldDisplayDateSeparator;

// BOOL value to indicate whether date should be displayed.
@property (nonatomic, assign) BOOL shouldDisplayDate;

// BOOL value to indicate whether time should be displayed.
@property (nonatomic, assign) BOOL shouldDisplayTime;

/**
 Class method to get height of date time view.

 @param dateSeparatorVisible BOOL to indicate if separator should be shown.
 @param displayDateVisible BOOL to indicate if date should be shown.
 @param displayTimeVisible BOOL to indicate if time should be shown.

 @return CGFloat value which is the height for this view.
 */
+ (CGFloat)heightWithDateSeparatorVisible:(BOOL)dateSeparatorVisible
                       displayDateVisible:(BOOL)displayDateVisible
                       displayTimeVisible:(BOOL)displayTimeVisible;

/**
 Instance method to get height of date time view.

 @return CGFloat value which is the height for this view.
 */
- (CGFloat)height;

@end
