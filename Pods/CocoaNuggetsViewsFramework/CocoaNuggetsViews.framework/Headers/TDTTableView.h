//
//  TDTTableView.h
//  Talkto
//
//  Created by Amandeep Singh on 12/6/17.
//  Copyright © 2017 Talk.to FZC. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 This class was created to override default values of estimated heights which
 was changed in iOS 11, as we do not use estimated heights
 */
@interface TDTTableView : UITableView

@end
