//
//  TDTButtonWithSpinner.h
//  Talkto
//
//  Created by udit.ag on 12/12/13.
//  Copyright (c) 2013 Talk.to FZC. All rights reserved.
//
//  This class creates a widget for UIButton with a spinner. It will have two states:
//  1. Normal state: In this, a normal UIButton with title is shown.
//  2. Spinner state: In this, a spinner(left side) and a title(right side) are shown.

#import <UIKit/UIKit.h>

@interface TDTButtonWithSpinner : UIButton

/**
 This removes the spinner from the button if it's there.

 @param title   Text to be shown on button.
 */
- (void)switchToNormalStateWithTitle:(NSString *)title;

/**
 This shows a spinner with a title on the button. Spinner will be shown on left hand side and title on right side.

 @param title   Text to be shown on button.
 */
- (void)switchToSpinnerStateWithTitle:(NSString *)title;

@end
