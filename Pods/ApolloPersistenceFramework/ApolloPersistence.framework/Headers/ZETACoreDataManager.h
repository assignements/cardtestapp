#import <Foundation/Foundation.h>

@interface ZETACoreDataManager : NSObject

@property (nonatomic, readonly) NSManagedObjectContext *mainMoc;
@property (nonatomic, readonly) NSManagedObjectContext *backgroundMoc;
@property (nonatomic, readonly) NSManagedObjectContext *backgroundReadingMOC;

/**
 Takes the model name, and creates the sqlite file for persistent store with the same name as the model name, and saves the file at 'Persistence' folder in Documents directory.
 @param resourceName Name of the model file.
 */
- (instancetype)initWithResourceName:(NSString *)resourceName;
- (instancetype)initWithResourceName:(NSString *)resourceName
                              bundle:(NSBundle *)resourceBundle;

/**
 Takes the model name, the name of sqlite file for persistent store, and the location where to save the sqlite file and sets up the Core data requirements.
 @param resourceName Name of the model file.
 @param persistentStoreName Name of the sqlite file for persistent store.
 @param location Where the sqlite file will be stored.
 */
- (instancetype)initWithResourceName:(NSString *)resourceName
                 persistentStoreName:(NSString *)persistentStoreName
                            location:(NSURL *)location;
- (instancetype)initWithResourceName:(NSString *)resourceName
                 persistentStoreName:(NSString *)persistentStoreName
                            location:(NSURL *)location
                              bundle:(NSBundle *)resourceBundle;
/**
 Deletes all the files related to the persistent store managed by the instance of Core data manager this method is called on.
 */
- (void)dropDB;

/**
 Deletes the 'Persistence' folder and all its files in the Documents directory.
 */
+ (void)clearPersistentStore;

/**
 Deletes all files related to the persistent store managed by the instance of Core data manager this method is called on and recreates them.
 */
- (void)dropAndRecreateDB;

@end
