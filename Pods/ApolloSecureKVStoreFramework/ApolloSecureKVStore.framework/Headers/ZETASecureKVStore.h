#import <Foundation/Foundation.h>

@protocol ZETASecureKVStore

- (instancetype)initWithIdentifier:(NSString *)identifier;
- (void)setData:(NSData *)data key:(NSString *)key;
- (NSData *)dataForKey:(NSString *)key;
- (void)setString:(NSString *)string forKey:(NSString *)key;
- (NSString *)stringForKey:(NSString *)key;
- (void)setCodingCompliantData:(id<NSCoding>)data key:(NSString *)key;
- (id<NSCoding>)codingCompliantDataForKey:(NSString *)key;
- (void)removeDataForKey:(NSString *)key;
- (void)deleteAllItems;

@end
