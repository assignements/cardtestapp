#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloCoreUtils : NSObject

+ (UIViewController *)apollo_rootViewController;
+ (UIImage *)apollo_imageNamed:(NSString *)imageName;

@end

NS_ASSUME_NONNULL_END
