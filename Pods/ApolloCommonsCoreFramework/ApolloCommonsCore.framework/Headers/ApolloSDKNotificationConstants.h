#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const ZETAAnalyticsNotification;
FOUNDATION_EXPORT NSString *const ZETAAnalyticsEventName;
FOUNDATION_EXPORT NSString *const ZETAAnalyticsEventParams;
FOUNDATION_EXPORT NSString *const ZETAAnalyticsEventIsImmediateParam;

FOUNDATION_EXPORT NSString *const ApolloSDKAuthenticatedNotification;
FOUNDATION_EXPORT NSString *const ApolloSDKLogoutNotification;
FOUNDATION_EXPORT NSString *const ApolloSDKNotificationParamSdkName;
