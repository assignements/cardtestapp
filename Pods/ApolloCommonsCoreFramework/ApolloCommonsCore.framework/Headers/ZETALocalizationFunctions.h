#import <Foundation/Foundation.h>

NSString* ZETALocalizedString(NSString* key, NSString* comment);
NSString* ZETALocalizedStringWithKeyValuePair(NSString* key, NSString* value, NSString* comment);
