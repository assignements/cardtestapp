#import <UIKit/UIKit.h>
#import "ApolloScope.h"
#import "ApolloCoreUtils.h"

typedef void (^ZETAFailureBlockType)(NSError *error);
typedef void (^ZETARetryableFailureBlockType)(NSError *error, BOOL *shouldRetry);
typedef void (^ZETAHTTPFailureBlockType)(NSError *error, NSHTTPURLResponse *response);
typedef void (^ZETAEmptyBlockType)(void);
typedef void (^ZETAResponseBlockType)(id responsePayload);
typedef void (^ZETAAPIResponseBlockType)(NSArray *ApiResponses);
typedef void (^ZETABooleanStatusBlockType)(BOOL status);
typedef void (^ZETAStringBlockType)(NSString *string);
typedef void (^ZETAArrayBlockType)(NSArray *array);

#define AnalyticsLongPrefix @"_longValue"
#define AnalyticsDoublePrefix @"_doubleValue"
#define AnalyticsEpochPrefix @"_epochValue"
#define ZETAUserActionsNotificationNameFormat @"ZETAUserActionsManager.%@"

#define propertyString(property) NSStringFromSelector(@selector(property))
#define analyticsLongPropertyString(property) \
[NSStringFromSelector(@selector(property)) stringByAppendingString:AnalyticsLongPrefix]
#define analyticsDoublePropertyString(property) \
[NSStringFromSelector(@selector(property)) stringByAppendingString:AnalyticsDoublePrefix]
#define analyticsEpochPropertyString(property) \
[NSStringFromSelector(@selector(property)) stringByAppendingString:AnalyticsEpochPrefix]

#define DNC [NSNotificationCenter defaultCenter]
#define ZETAMainQueue dispatch_get_main_queue()
#define ZETADefaultQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

#define ZETARegisterNib(tableView, className, reuseID)                                          \
[tableView registerNib:[UINib nibWithNibName:NSStringFromClass([className class]) bundle:nil] \
forCellReuseIdentifier:reuseID];

#define ZETARegisterCollectionNib(collectionView, className, reuseID)                    \
[collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([className class]) \
bundle:nil]                                 \
forCellWithReuseIdentifier:reuseID];

#define ZETARegisterCollectionClass(collectionView, className, reuseID) \
[collectionView registerClass:[className class] forCellWithReuseIdentifier:reuseID];

#define ZETARegisterCollectionReusableViewNib(collectionView, className, viewKind, reuseID) \
[collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([className class])    \
bundle:nil]                                    \
forSupplementaryViewOfKind:viewKind                                                   \
withReuseIdentifier:reuseID];

#define ZETARegisterNibForHeaderFooterView(tableView, className, reuseID)                       \
[tableView registerNib:[UINib nibWithNibName:NSStringFromClass([className class]) bundle:nil] \
forHeaderFooterViewReuseIdentifier:reuseID];

#define ZETARegisterClass(tableView, className, reuseID) \
[tableView registerClass:[className class] forCellReuseIdentifier:reuseID]

#define LoadFromNib(className)                                                                     \
[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([className class]) owner:nil options:nil] \
lastObject]

#define ZETAInitWithNib(className) \
[[className alloc] initWithNibName:NSStringFromClass([className class]) bundle:nil]

#define ZETAInitViewControllerFromStoryboard(storyboardName)   \
[[UIStoryboard storyboardWithName:storyboardName bundle:nil] \
instantiateViewControllerWithIdentifier:NSStringFromClass([self class])]

#define ZETAInitViewControllerFromStoryboardAndClassName(storyboardName, className) \
[[UIStoryboard storyboardWithName:storyboardName bundle:nil]                      \
instantiateViewControllerWithIdentifier:className]

#define TICK NSDate *startTime = [NSDate date];
#define TOCK(purpose) \
TDTLogInfo(@"Elapsed Time for %@ : %f", purpose, -[startTime timeIntervalSinceNow]);

#define ZETAEqualJoinedStringFormat(object1, object2) \
[NSString stringWithFormat:@"%@=%@", object1, object2]

#define ZETA_QUOTE(...) @ #__VA_ARGS__

#define ZETA_CLASS_CUM_METHOD_NAME \
[@[NSStringFromClass([self class]), NSStringFromSelector(_cmd)] componentsJoinedByString:@"."]

#define ZETA_CLASS_CUM_METHOD_NAME_WITH_TAG(tag)                       \
[@[NSStringFromClass([self class]), NSStringFromSelector(_cmd), tag] \
componentsJoinedByString:@"."]

#define ZETARACObserve(TARGET, KEYPATH)                                     \
({                                                                        \
__weak id target_ = (TARGET);                                           \
[target_ rac_valuesForKeyPath:@keypath(TARGET, KEYPATH) observer:self]; \
})

#define UserActionNotificationName(notification) \
([NSString stringWithFormat:ZETAUserActionsNotificationNameFormat, notification])

#define ZETAIosElevenCheck @available(iOS 11.0, *)

#define ZETABackButton(selector)                                                  \
[[UIBarButtonItem alloc] initWithImage:[ApolloCoreUtils apollo_imageNamed:@"apollo_white_arrow"] \
style:UIBarButtonItemStylePlain                \
target:self                                     \
action:selector];

#define ZETABackButtonWithBackImage(selector,backImage)                                                  \
[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:backImage] \
style:UIBarButtonItemStylePlain                \
target:self                                     \
action:selector];

#define ZETATopPresentViewController                                           \
([[ApolloCoreUtils apollo_rootViewController] \
zeta_topPresentViewController])

#define ZETATopViewController \
([[ApolloCoreUtils apollo_rootViewController] zeta_topViewController])

#define ZETARootViewController                                           \
([ApolloCoreUtils apollo_rootViewController])
