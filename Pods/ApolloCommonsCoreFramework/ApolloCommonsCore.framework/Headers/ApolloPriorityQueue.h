#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
  ApolloPriorityQueueItemOrderAscending,
  ApolloPriorityQueueItemOrderEqual,
  ApolloPriorityQueueItemOrderDescending,
} ApolloPriorityQueueItemOrder;

@protocol ApolloPriorityQueueItem <NSObject>

- (ApolloPriorityQueueItemOrder)compareWith:(id<ApolloPriorityQueueItem>)otherObject;

@end

/**
 Min priority queue. Items need to conform to ApolloPriorityQueueItem protocol
 */
@interface ApolloPriorityQueue : NSObject

- (instancetype)initWithIdentifier:(NSString *)identifier;
- (void)addObject:(id<ApolloPriorityQueueItem>)object;
- (nullable id<ApolloPriorityQueueItem>)topObject;
- (void)popObject;
- (NSUInteger)size;

@end

NS_ASSUME_NONNULL_END
