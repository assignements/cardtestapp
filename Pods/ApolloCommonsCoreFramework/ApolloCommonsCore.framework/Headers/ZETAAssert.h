#import <Foundation/Foundation.h>
#import <ApolloCommonsTDTFoundationAdditions/ApolloCommonsTDTFoundationAdditions.h>

/**
 WIP asserts under consideration for replacing the ones in Chocolate.
 
 - NSAssert on DEBUG builds.
 - Print a TDTLogError on non DEBUG builds.
 */

#define ZETAAssert(condition, ...)                                           \
do {                                                                       \
NSAssert(condition, __VA_ARGS__);                                        \
if (condition == NO) {                                                   \
TDTLogError(__VA_ARGS__);                                              \
}                                                                        \
} while (0)

#define ZETAParameterAssert(condition) \
ZETAAssert((condition), @"Invalid parameter not satisfying: %s", #condition)

#define ZETAPerformSelectorAssert(object, selector)                                  \
ZETAAssert([object respondsToSelector:selector], @"%@ should perform selector %@", \
NSStringFromClass([object class]), NSStringFromSelector(selector))

#define ZETAAssertFailure(format...) ZETAAssert(NO, format)
#define ZETAAssertNil(obj, format...) ZETAAssert(obj == nil, format)
