#import "ApolloMacros.h"
@class ZETAJID;
@class ZETAPublicKey;
@class ZETAPublicKeyCertificate;
@class ZETAUserSession;

@protocol ApolloAuthDataProtocol <NSObject>

- (ZETAUserSession *)userSession;
- (void)setUserSession:(id)userSession;
- (void)removeUserSession;

- (NSString *)userSessionJID;
- (void)setUserSessionJID:(NSString *)userSessionJID;

- (NSData *)sharedSecret;
- (void)setSharedSecret:(NSData *)sharedSecret;

- (NSNumber *)authCounter;
- (void)setAuthCounter:(NSNumber *)authCounter;

- (ZETAJID *)userJID;

- (ZETAPublicKey *)publicKey;
- (void)setPublicKey:(ZETAPublicKey *)publicKey;

- (ZETAPublicKeyCertificate *)publicKeyCertificate;
- (void)setPublicKeyCertificate:(ZETAPublicKeyCertificate *)publicKeyCertificate;

@end

@protocol ApolloServerTimeSyncProtocol <NSObject>

- (NSTimeInterval)serverTimeDifference;
- (void)setServerTimeDifference:(NSTimeInterval)serverTimeDiff;

- (NSDate *)serverAdjustedDateForDate:(NSDate *)date;

- (NSTimeInterval)timeIntervalSinceServerAdjustedDateForDate:(NSDate *)date;

- (NSDate *)serverAdjustedCurrentDate;

@end

@protocol ApolloAuthProviderProtocol <NSObject>

- (void)getAuthTokenForAuthID:(NSString *)authID
                 successBlock:(nullable ZETAStringBlockType)success
                 failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)getUserSessionJidForAuthID:(NSString *)authID
                      successBlock:(nullable ZETAStringBlockType)success
                      failureBlock:(nullable ZETAFailureBlockType)failure;

@end
