#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const ZETAImageCachingDirectory;
FOUNDATION_EXPORT NSString *const ZETAGIFCachingDirectory;
