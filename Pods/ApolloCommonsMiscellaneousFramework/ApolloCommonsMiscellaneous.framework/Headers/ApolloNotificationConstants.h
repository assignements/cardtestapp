#import <Foundation/Foundation.h>

// Event Names
FOUNDATION_EXPORT NSString *const ZETALogoutNotification;
FOUNDATION_EXPORT NSString *const ZETAZetletLogsNotification;
FOUNDATION_EXPORT NSString *const ZETAPushMessageNotification;
FOUNDATION_EXPORT NSString *const ApolloShellDataDidUpdateNotification;
FOUNDATION_EXPORT NSString *const ZETAInvalidSignatureExceptionReceivedNotification;

// Notification UserInfo Keys
FOUNDATION_EXPORT NSString *const ZETAPayloadKey;
FOUNDATION_EXPORT NSString *const ZETAPushMessageTypeKey;
FOUNDATION_EXPORT NSString *const ZETAPushMessageKey;
FOUNDATION_EXPORT NSString *const ZETAPushMessageActionIdentifierKey;
FOUNDATION_EXPORT NSString *const ZETAPushMessageSourceKey;
FOUNDATION_EXPORT NSString *const ApolloShellDataModifiedKey;
FOUNDATION_EXPORT NSString *const ApolloShellDataModifiedData;
