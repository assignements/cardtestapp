#import <FLAnimatedImage/FLAnimatedImage.h>
#import <Foundation/Foundation.h>
#import "ZETANetworkMediaProvider.h"

typedef void (^ZETANetworkGIFProviderFetchCompletionBlock)(
    FLAnimatedImage *__nonnull animatedImage);

@interface ZETAGIFProvider : NSObject

+ (nullable instancetype)sharedProvider;

- (nullable instancetype)initWithCacheDirectoryName:(nonnull NSString *)cacheDirectoryName;

- (void)fetchGIFFromURL:(nullable NSURL *)URL
        completionBlock:(nonnull ZETANetworkGIFProviderFetchCompletionBlock)completionBlock;

@end
