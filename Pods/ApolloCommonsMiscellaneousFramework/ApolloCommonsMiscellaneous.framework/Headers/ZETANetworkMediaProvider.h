#import <Foundation/Foundation.h>

extern NSString *const ZETANetworkMediaProviderErrorDomain;

typedef NS_ENUM(NSUInteger, ZETANetworkMediaProviderErrorCode) {
  ZETANetworkMediaProviderErrorCodeMediaNotAvailable,
  ZETANetworkMediaProviderErrorCodeInvalidURL
};

/**
 This class is responsible for providing media (gif/video/audio) either from network or from disk.
 */
@interface ZETANetworkMediaProvider : NSObject

typedef void (^ZETAMediaFetchCompletionBlock)(NSError *error, NSData *data);

- (instancetype)initWithCacheDirectoryNamed:(NSString *)cacheDirectoryName;

- (void)loadMediaFromURL:(NSURL *)URL
         completionBlock:(ZETAMediaFetchCompletionBlock)completionBlock;

- (NSString *)localPathForURL:(NSURL *)url;

@end
