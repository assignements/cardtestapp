#import <Foundation/Foundation.h>
#import <ApolloCommonsMiscellaneous/ApolloMiscellaneous.h>

@class BFTask;

@interface ZETAImageProvider : NSObject

+ (void)fetchImageFromURL:(nullable NSURL *)URL
          completionBlock:(nonnull ZETANetworkImageProviderFetchCompletionBlock)completionBlock;

+ (nullable BFTask *)imageFetchTaskFromURL:(nullable NSURL *)URL;

+ (UIImage *)imageWithName:(NSString *)imageName;

@end
