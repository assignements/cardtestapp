#import <Foundation/Foundation.h>

/**
  This class provides an abstraction to fetch any kind of data from network/local storage and cache
  them for repeated usage.
 */

typedef void (^ZETAFileDataProviderDataFetchCompletionBlock)(NSError *error, NSData *fileData);

@interface ZETAFileDataProvider : NSObject

+ (instancetype)sharedProvider;

- (void)fileDataForFileWithURL:(NSURL *)URL
             shouldFetchAlways:(BOOL)shouldFetchAlways
                    completion:(ZETAFileDataProviderDataFetchCompletionBlock)completion;

- (NSData *)dataForFileAtLocalFilePath:(NSString *)path;

@end
