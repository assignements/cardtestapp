#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "TDTNetworkImageProvider.h"

typedef void (^ZETANetworkImageProviderFetchCompletionBlock)(UIImage *__nonnull image);

@interface ZETANetworkImageProvider : NSObject

+ (nullable instancetype)sharedProvider;

- (nullable instancetype)initWithCacheDirectoryName:(nonnull NSString *)cacheDirectoryName;

/**
 @param completionBlock The completion block is invoked with a valid
 image as the argument after the image has been successfully fetched.
 If the image was found in the cache then the block will be invoked
 synchronously on the calling thread; otherwise, if the image was
 fetched over the network then the block will be invoked asynchronously
 on the main thread.
 In case of errors, this block will never be invoked (this method will
 internally log a warning message).
 */
- (void)fetchImageFromURL:(nullable NSURL *)URL
          completionBlock:(nonnull ZETANetworkImageProviderFetchCompletionBlock)completionBlock;

/**
 Returns a signal which sends events with placeholder image and finally the fetched image.
 
 @param URL URL to be fetched. It must not be nil
 @param imageName Name of the placeholder image.
 
 @note: It also sends error events in case there is an error. So, you should handle that properly.
 */
- (nullable RACSignal *)imageFetchSignalFromURL:(nullable NSURL *)URL
                           placeHolderImageName:(nullable NSString *)imageName;

/**
 Loads image from the given URL and sends it in the completion block.
 
 @param URL URL from which the image is to be loaded.
 @param imageDataValueTransformerName Transformer for image data.
 @param completionBlock completion block called with image in case of
 success and error in case of failure.
 
 */
- (void)loadImageFromURL:(NSURL *_Nonnull)URL
imageDataValueTransformerName:(NSString *_Nullable)imageDataValueTransformerName
         completionBlock:(TDTImageFetchCompletionBlock _Nonnull )completionBlock;

@end
