#import <Foundation/Foundation.h>

@interface ZETAMediaCache : NSObject

- (NSData *)dataForPath:(NSString *)path;
- (void)setData:(NSData *)data forPath:(NSString *)path;

/**
 Removes all objects cached.
 */
- (void)removeAllObjects;

@end
