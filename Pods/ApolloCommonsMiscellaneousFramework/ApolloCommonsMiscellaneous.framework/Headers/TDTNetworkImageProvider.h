//
//  TDTNetworkImageProvider.h
//  Talkto
//
//  Created by Yatin Sarbalia on 12/20/13.
//  Copyright (c) 2013 Talk.to FZC. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const TDTNetworkImageProviderErrorDomain;

typedef NS_ENUM(NSUInteger, TDTNetworkImageProviderErrorCode) {
  TDTNetworkImageProviderErrorCodeImageNotAvailable,
  TDTNetworkImageProviderErrorCodeInvalidURL
};

/**
 This class is responsible for providing image either from network or from disk.
 */
@interface TDTNetworkImageProvider : NSObject


typedef void (^TDTImageFetchCompletionBlock)(NSError *error, UIImage *image);

/**
 Initializes an instance of @p TDTNetworkImageProvider with the given directory path and
 session configuration.

 This is the designated initializer.

 @param directoryPath The path under which the downloaded images are saved. Consider using a
 discardable cache directory like @p Library/Caches to save disk space.

 @param configuration The URL session configuration to use. If nil @p defaultSessionConfiguration is used.

 @returns A newly initialized instance of @p TDTNetworkImageProvider
 */
- (instancetype)initWithDirectoryPath:(NSString *)directoryPath
                 sessionConfiguration:(NSURLSessionConfiguration *)configuration;

/**
 Convenience initializer that creates a subdirectory with the given name
 under Library/Caches.
 */
- (instancetype)initWithCacheDirectoryNamed:(NSString *)cacheDirectoryName
                       sessionConfiguration:(NSURLSessionConfiguration *)configuration;

- (instancetype)initWithCacheDirectoryNamed:(NSString *)cacheDirectoryName;

/**
 Provides image stored with `url` and `imageDataValueTransformerName`.
 This method also fetches image if not already present. This method should be called on main thread only.

 @param URL URL of image to be downloaded.

 @param imageDataValueTransformersName Instance of `NSValueTransformer` class
 to apply transformation to downloaded image's NSData.
 If `imageDataValueTransformerName` is nil, then no transformation is applied.

 @param completionBlock Block to be executed on successful or failed completion of image download process.
 This will only be exectuted when after image is fetched from network with success or failure.
 `completionBlock` will be executed on main thread.

 @return An image after applying transformation on the image related to `url` saved on disk.
 This method returns nil if image is not saved already and needed to be fetched.
 */
- (UIImage *)imageFromURL:(NSURL *)URL
imageDataValueTransformerName:(NSString *)imageDataValueTransformerName
          completionBlock:(TDTImageFetchCompletionBlock)completionBlock;

/**
 Variant of @p imageFromURL:imageDataValueTransformerName:completionBlock
 that always invokes the completion block even if the image was fetched
 from the cache. This makes it a more suitable API in certain cases since
 the caller only needs to use the image in a single place.

 @param completionBlock This is either invoked synchronously on the
 calling thread (when the image is found in the cache) or asynchronously
 on the main thread (when the image is fetched from the network).

 Usually, a method calls its completionBlock either exclusively
 synchronously or exclusively asynchronously. The reason for this
 method doing both is than an imageView might have a placeholder
 image (set in the nib, or viewDidLoad). In cases where we have
 the images cached locally, doing the callback asynchronously
 results in a "flash" - the view is rendered the first time with
 the placeholder, then then second time with the cached image is
 set in the completionBlock. By calling it synchronously, the
 first rendering is skipped.
 */
- (void)loadImageFromURL:(NSURL *)URL
imageDataValueTransformerName:(NSString *)imageDataValueTransformerName
          completionBlock:(TDTImageFetchCompletionBlock)completionBlock;

/**
 Sets `image` for given `url` and saves `image` on disk.
 This method should be called on main thread only.

 @param image Image to be saved.

 @param url Associated URL with `image`.
 */
- (void)setImage:(UIImage *)image
          forURL:(NSURL *)url;

/**
 Clears all cached images.
 */
- (void)clearCache;

@end
