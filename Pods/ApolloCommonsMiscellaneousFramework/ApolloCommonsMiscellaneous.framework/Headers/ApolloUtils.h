#import <UIKit/UIKit.h>

@interface ApolloUtils : NSObject

/**
 This method safely loads image on a tableViewCell taking into consideration the
 cell reuse.

 @param imageName placeholder image name to load till the point tableview loads.
 @param URL URL of the image to load.
 @param createdCell cell which is created.
 @param setImageCellBlock this block passes the cell and cached image to use.

 @note If nil is passed for @p imageName, no placeholder will be set.
 */
+ (void)safelyLoadImagePlaceholder:(NSString *)imageName
                               URL:(NSURL *)URL
                         tableView:(UITableView *)tableView
                         indexPath:(NSIndexPath *)indexPath
                       createdCell:(UITableViewCell *)createdCell
                 setImageCellBlock:
                     (void (^)(UITableViewCell *cell, UIImage *image))setImageCellBlock;

+ (NSString *)appVersionNumberString;

+ (NSComparator)comparatorForSortingUsingClassesInOrder:(NSArray *)classes;

+ (void)postNotificationForAnalyticsEventWithName:(NSString *)eventName
                               andEventParameters:(NSDictionary *)eventParams;

+ (void)postNotificationForAnalyticsEventWithName:(NSString *)eventName
                               andEventParameters:(NSDictionary *)eventParams
                                      isImmediate:(BOOL)immediate;

///  @return YES for Debug build else NO
+ (BOOL)isDebugBuild;

/// Method to check if sim is inserted
+ (BOOL)isSimInserted;

/// Method to get current network code
+ (NSString *)currentNetworkCode;

@end
