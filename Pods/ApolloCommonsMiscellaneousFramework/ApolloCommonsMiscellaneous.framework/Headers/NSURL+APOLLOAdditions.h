#import <Foundation/Foundation.h>

@interface NSURL (APOLLOAdditions)

- (BOOL)zeta_isEmpty;

- (BOOL)zeta_isNonEmpty;

/**
 Returns YES if URL is a file URL.
 e.g., file://cashback
 */
- (BOOL)zeta_isZetaFileURL;

- (NSString *)zeta_imageName;

@end
