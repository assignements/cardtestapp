#import <UIKit/UIKit.h>

@interface UIScreen (ZETAAdditions)

+ (BOOL)zeta_isNonRetina;
+ (BOOL)zeta_isRetina;
+ (BOOL)zeta_isRetinaHD;
+ (BOOL)zeta_is3_5InchIPhone;
+ (CGFloat)zeta_screenHeight;

@end
