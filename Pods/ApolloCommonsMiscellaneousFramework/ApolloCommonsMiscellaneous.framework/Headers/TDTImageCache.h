//
//  TDTImageCache.h
//  Talkto
//
//  Created by Chaitanya Gupta on 23/07/12.
//  Copyright (c) 2012 Talk.to FZC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TDTImageCache : NSObject

- (UIImage *)imageForPath:(NSString *)path;
- (void)setImage:(UIImage *)image forPath:(NSString *)path;

/**
 Removes all objects cached.
 */
- (void)removeAllObjects;

@end
