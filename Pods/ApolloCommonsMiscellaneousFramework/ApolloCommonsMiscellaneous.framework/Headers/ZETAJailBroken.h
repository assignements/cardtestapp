@interface ZETAJailBroken: NSObject

+ (float) firmwareVersion;
+ (BOOL) isDeviceJailbroken;
+ (BOOL) isAppCracked;
+ (BOOL) isAppStoreVersion;

@end
