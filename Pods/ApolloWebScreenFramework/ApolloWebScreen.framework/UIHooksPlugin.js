;(function(){

  console.log('Inject UIHooksPlugin');
  window.zeta = window.zeta || {};

  window.zeta.showTemplate = function(args, callback, errback) {
  zetaBridge.ask("UIHooks", "showTemplate", args, callback, errback);
  };

  window.zeta.hideTemplate = function(args, callback, errback) {
  zetaBridge.ask("UIHooks", "hideTemplate", args, callback, errback);
  };

  })();