#import <ApolloGatekeeper/ZETAGatekeeperManager.h>

@interface ZETAGatekeeperManager (ApolloGateKeeperPlugin)

- (BOOL)zeta_isGateOpenForGatekeeper:(NSString *)gatekeeper;

@end
