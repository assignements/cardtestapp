#import <Foundation/Foundation.h>
#import <ApolloZapps/ZETABaseJSPlugin.h>

@class ZETAGatekeeperManager;

@interface ApolloGatekeeperPlugin : ZETABaseJSPlugin

- (instancetype)initWithGatekeeperManager:(ZETAGatekeeperManager *)gatekeeperManager;

@end
