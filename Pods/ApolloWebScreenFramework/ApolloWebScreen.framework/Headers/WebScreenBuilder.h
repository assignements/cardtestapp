#import <ApolloAnalyticsCore/ApolloAnalyticsManager.h>
#import <ApolloCollectionsProteus/ZETAProteusCollectionsClient.h>
#import "ApolloWebScreenConfig.h"

NS_ASSUME_NONNULL_BEGIN

@interface WebScreenBuilder : NSObject

+(ApolloWebScreenConfig *)buildWebScreenConfigForSDKConfig:(NSDictionary *)SDKConfig
                                             tokenProvider:(id<ZETAProteusCollectionsClientDelegate>)tokenProvider;

@end

NS_ASSUME_NONNULL_END
