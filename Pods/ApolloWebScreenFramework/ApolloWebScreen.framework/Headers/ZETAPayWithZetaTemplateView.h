#import <UIKit/UIKit.h>

@interface ZETAPayWithZetaTemplateView : UIView

- (instancetype)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle;

@end
