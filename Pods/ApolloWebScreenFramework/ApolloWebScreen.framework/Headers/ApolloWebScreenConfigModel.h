#import <ApolloUserAuthManager/ApolloSDKConfigModel.h>
NS_ASSUME_NONNULL_BEGIN

@interface ApolloWebScreenConfigModel : ApolloSDKConfigModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSURL *collectionBaseUrl;
@property (nonatomic, readonly) NSString *userAgent;
@property (nonatomic, readonly) NSString *templatesCollectionId;
@property (nonatomic, readonly) NSString *shophooksCollectionID;
@property (nonatomic, readonly) NSString *shophooksCategoryCollectionID;
@property (nonatomic, readonly) NSString *themeConfigCollectionId;
@property (nonatomic, readonly) NSString *scheme;
@property (nonatomic, readonly) NSString *gatekeeperListId;
@property (nonatomic, readonly) BOOL isCertificatePinning;
@property (nonatomic, readonly, nullable, copy) NSDictionary *domainSslCertConfig;

- (BOOL)isValid;

@end

NS_ASSUME_NONNULL_END
