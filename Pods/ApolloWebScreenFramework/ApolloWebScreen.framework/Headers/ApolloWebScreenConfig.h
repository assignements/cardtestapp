#import <Foundation/Foundation.h>
#import "ApolloPluginsProvider.h"
#import <ApolloZapps/ZETAPluginFactory.h>
#import <ApolloZapps/ZETAShophooksManager.h>
#import "ApolloWebScreenConfigModel.h"

@class ApolloAnalyticsManager;
@class ZETAPluginFactory;
@class ZETAShophooksManager;
@class ZETAZetletPopupPresenter;
@class ZETAGatekeeperManager;

@protocol ApolloPluginFactoryDataSource;
@protocol ZETAZappLaunchFlowManagerDelegate;

NS_ASSUME_NONNULL_BEGIN

@interface ApolloWebScreenConfig : NSObject

@property (nonatomic, readonly) NSString *sdkName;
@property (nonatomic, readonly) ZETAGenericCollectionManager *collectionManager;
@property (nonatomic, readonly) ZETAShophooksManager *shophooksManager;
@property (nonatomic, readonly) ZETAPluginFactory *pluginFactory;
@property (nonatomic, readonly) ApolloAnalyticsManager *analyticsManager;
@property (nonatomic, readonly) NSArray *prioritizedTemplateCollectionIDs;
@property (nonatomic, readonly) ZETAZetletPopupPresenter *popupPresenter;
@property (nonatomic, readonly) ApolloPluginsProvider *pluginsProvider;
@property (nonatomic, readonly) BOOL isBuilderMode;
@property (nonatomic, readonly) ApolloWebScreenConfigModel *config;
@property (nonatomic, readonly) ZETAGatekeeperManager *gatekeeperManager;

- (instancetype)initWithSDKName:(NSString *)sdkName
                      agentInfo:(TDTAgentInfo *)agentInfo
                 popupPresenter:(ZETAZetletPopupPresenter *)popupPresenter
               analyticsManager:(ApolloAnalyticsManager *)analyticsManager
              gatekeeperManager:(ZETAGatekeeperManager *)gatekeeperManager
prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs
              collectionManager:(ZETAGenericCollectionManager *)collectionManager
          shophooksCollectionID:(NSString *)shophooksCollectionID
  shophooksCategoryCollectionID:(NSString *)shophooksCategoryCollectionID;


- (instancetype)initWithSDKName:(NSString *)sdkName
                      agentInfo:(TDTAgentInfo *)agentInfo
                 popupPresenter:(ZETAZetletPopupPresenter *)popupPresenter
               analyticsManager:(ApolloAnalyticsManager *)analyticsManager
              gatekeeperManager:(ZETAGatekeeperManager *)gatekeeperManager
prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs
              collectionManager:(ZETAGenericCollectionManager *)collectionManager
          shophooksCollectionID:(NSString *)shophooksCollectionID
  shophooksCategoryCollectionID:(NSString *)shophooksCategoryCollectionID
                    configModel:(nullable ApolloWebScreenConfigModel *)configModel
                  isBuilderMode:(BOOL)isBuilderMode NS_DESIGNATED_INITIALIZER;

/**
@param config sdk config passed as dictionary
@return instance of ApolloWebScreenConfig
*/
- (instancetype)initWithConfig:(NSDictionary *)config;

- (void)updatePluginFactories;

- (BOOL)isValid;

@end

NS_ASSUME_NONNULL_END
