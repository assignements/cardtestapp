#import <Foundation/Foundation.h>

#import <ApolloGatekeeper/ZETAGatekeeperManager.h>
#import <ApolloAnalyticsCore/ApolloAnalyticsManager.h>
#import <ApolloZapps/ZETAPluginFactory.h>
#import <ApolloZapps/ZETAShophooksManager.h>

NS_ASSUME_NONNULL_BEGIN

@class ZETAZetletPopupPresenter;

@protocol ApolloPluginFactoryDataSource;

@interface ApolloPluginsProvider : NSObject

- (instancetype)initWithShophookManager:(ZETAShophooksManager *)shophooksManager
                       analyticsManager:(ApolloAnalyticsManager *)analyticsManager
                      gatekeeperManager:(ZETAGatekeeperManager *)gatekeeperManager
                         popupPresenter:(ZETAZetletPopupPresenter *)popupPresenter
                              agentInfo:(TDTAgentInfo *)agentInfo NS_DESIGNATED_INITIALIZER;

- (ZETAPluginFactory *)pluginFactory;
- (void)addPluginFactoryDataSource:(id<ApolloPluginFactoryDataSource>)dataSource;

@end

@protocol ApolloPluginFactoryDataSource <NSObject>

- (NSArray *)apolloPluginFactoryDidRequestAllAvailablePlugins:(ApolloPluginsProvider *)apolloPluginFactory;

- (NSArray *)apolloPluginFactoryDidRequestBasicPlugins:(ApolloPluginsProvider *)apolloPluginFactory;

- (NSArray *)apolloPluginFactoryDidRequestAllAvailablePluginNames:(ApolloPluginsProvider *)apolloPluginFactory;

- (NSArray *)apolloPluginFactoryDidRequestBasicPluginNames:(ApolloPluginsProvider *)apolloPluginFactory;

- (nullable id<ZETAJSPlugin>)apolloPluginFactory:(ApolloPluginsProvider *)apolloPluginFactory
                didRequestPluginForName:(NSString *)pluginName;

@end

NS_ASSUME_NONNULL_END
