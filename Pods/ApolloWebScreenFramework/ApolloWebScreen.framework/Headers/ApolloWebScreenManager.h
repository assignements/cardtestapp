#import <Foundation/Foundation.h>
#import <ApolloZapps/ZETAZappLaunchFlowManager.h>
#import <ApolloDeeplinkResolver/ZETADeeplinkURLOpener.h>


@class ApolloAnalyticsManager;
@class ApolloWebScreenConfig;
@class ZETAGatekeeperManager;

@protocol ApolloWebScreenManagerDelegate;
@protocol ApolloPluginFactoryDataSource;

NS_ASSUME_NONNULL_BEGIN

@interface ApolloWebScreenManager : NSObject

- (instancetype)initWithApolloWebScreenConfig:(ApolloWebScreenConfig *)config
                                     delegate:(id<ApolloWebScreenManagerDelegate>)delegate;

- (instancetype)initWithWebScreenDelegate:(id<ApolloWebScreenManagerDelegate>)delegate;

- (void)addPluginFactoryDataSource:(id<ApolloPluginFactoryDataSource>)dataSource;

@end

@protocol ApolloWebScreenManagerDelegate<NSObject>

- (void)zappLaunchFlowManagerDidCancelFlow:(ZETAZappLaunchFlowManager *)flowManager;
- (void)zappLaunchFlowManagerDidCompleteFlow:(ZETAZappLaunchFlowManager *)flowManager;

@optional
- (void)zappLaunchFlowManager:(ZETAZappLaunchFlowManager *)flowManager
        didFinishWithCallback:(NSURL *)URL;

@optional
- (void)getUserIDForshophooksManager:(ZETAShophooksManager *)shophooksManager
                          shophookID:(NSString *)shophookID
                             completion:(ZETAStringBlockType)success;

@optional
- (void)getBase64JWETokenForshophooksManager:(ZETAShophooksManager *)shophooksManager
                                  shophookID:(NSString *)shophookID
                                     completion:(ZETAStringBlockType)success;
@optional
- (void)getAuthTokenForshophooksManager:(ZETAShophooksManager *)shophooksManager
                             shophookID:(NSString *)shophookID
                                completion:(ZETAStringBlockType)success;
@optional
- (BOOL)gatekeeperManager:(ZETAGatekeeperManager *)gatekeeperManager didRequestDefaultValueForGatekeeper:(NSString *)gatekeeper;

@end
NS_ASSUME_NONNULL_END
