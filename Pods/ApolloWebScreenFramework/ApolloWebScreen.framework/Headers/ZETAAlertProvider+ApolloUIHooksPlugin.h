#import <ApolloZetletsCore/ApolloZetletsCoreHeaders.h>
#import <ApolloCommonsMiscellaneous/ApolloMiscellaneous.h>

/**
 All the methods in this category support action block for only one button. If you want to have an
 action block corresponding to the cancel button (or multiple buttons), then use ZETAAlertProvider
 directly
 */
@interface ZETAAlertProvider (ApolloUIHooksPlugin)

+ (void)zeta_showAlertViewWithTitle:(NSString *)title
                            message:(NSString *)message
                         buttonText:(NSString *)buttonText;

@end
