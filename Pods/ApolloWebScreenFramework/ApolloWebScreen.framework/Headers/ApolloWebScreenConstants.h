#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const WebScreenDeeplinkPath;
FOUNDATION_EXPORT NSString *const WebScreenLogPrefix;

#pragma mark - Parameters

FOUNDATION_EXPORT NSString *const DispatchWebviewEventNotification;
FOUNDATION_EXPORT NSString *const ZappID;
FOUNDATION_EXPORT NSString *const ParamsType;
FOUNDATION_EXPORT NSString *const DispatchEventDeeplinkPath;
