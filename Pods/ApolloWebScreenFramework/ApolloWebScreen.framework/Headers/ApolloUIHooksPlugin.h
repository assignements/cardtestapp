#import <Foundation/Foundation.h>
#import <ApolloZapps/ZETABaseJSPlugin.h>

@class ZETAZetletPopupPresenter;
@class ZETAGatekeeperManager;

@interface ApolloUIHooksPlugin : ZETABaseJSPlugin

- (instancetype)initWithZetletPopupPresenter:(ZETAZetletPopupPresenter *)popupPresenter
                           gatekeeperManager:(ZETAGatekeeperManager *)gatekeeperManager;

@end
