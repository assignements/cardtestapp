#import <UIKit/UIKit.h>

@interface UIView (ZETAXibView)

- (void)zeta_addSelfViewFromXib;

@end
