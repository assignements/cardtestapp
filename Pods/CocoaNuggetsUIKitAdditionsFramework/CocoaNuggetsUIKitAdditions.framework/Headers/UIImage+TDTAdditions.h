//
//  UIImage+TDTAdditions.h
//  Talkto
//
//  Created by Yatin Sarbalia on 10/09/12.
//  Copyright (c) 2012 Talk.to FZC. All rights reserved.
//
// UIImage category to add border and rounded corners
// Refer to http://vocaro.com/trevor/blog/2009/10/12/resize-a-uiimage-the-right-way/

#import <UIKit/UIKit.h>

@interface UIImage (TDTAdditions)

// The documentation guide "App-Related Resources" mentions that a following
// names as valid for a launch image:
//
//   <basename><usage_specific_modifiers><scale_modifier><device_modifier>.png
//   <basename><orientation_modifier><scale_modifier><device_modifier>.png
//
// While +imageNamed: supports scale and device modifiers, it doesn't support
// the orientation modifier which is required for iPad or the usage_specific_modifier
// for iPhone 5.
//
// This method takes into account these two modifiers. Note that there is yet
// another modifier which we don't yet take care of -- the url_scheme modifier
// which can be used depending on the URL used to launch the app.
//
// This method also assumes that the basename of the launch image begins with
// "Default". The desired orientation for launch image should be provided too.
// Orientation changes are only taken into account for iPad.
+ (UIImage *)tdt_launchImageForOrientation:(UIInterfaceOrientation)orientation;

// This method assumes imageSize and size having same aspect ratio
+ (UIImage *)tdt_resizeImage:(UIImage *)image size:(CGSize)size;

// This method crops image with given crop rectangle
+ (UIImage *)tdt_cropImage:(UIImage *)image withCropRect:(CGRect)cropRect;

// Returns an empty image with no alpha channel of given `size`. In other
// words, the returned image is completely transparent.
+ (UIImage *)tdt_clearImageOfSize:(CGSize)size;

// Returns UIImage created from given image name
+ (UIImage *)tdt_buttonImageWithImageName:(NSString *)imageName;

// Creates a copy of this image with rounded corners
// If borderSize is non-zero, a transparent border of the given size will also be added
- (UIImage *)tdt_roundedCornerImage:(NSInteger)cornerSize borderSize:(NSInteger)borderSize;

// Adds a rectangular path to the given context and rounds its corners by the given extents
- (void)tdt_addRoundedRectToPath:(CGRect)rect context:(CGContextRef)context ovalWidth:(CGFloat)ovalWidth ovalHeight:(CGFloat)ovalHeight;

// Returns true if the image has an alpha layer
- (BOOL)tdt_hasAlpha;

// Returns a copy of the given image, adding an alpha channel if it doesn't already have one
- (UIImage *)tdt_imageWithAlpha;

/**
 Returns a copy of the given image with the specified alpha

 @param alpha required value of alpha

 @return A UIImage with the specified alpha
 */
- (UIImage *)tdt_imageWithAlpha:(CGFloat)alpha;
@end
