#import <Foundation/Foundation.h>
#import <ApolloCommonsTDTFoundationAdditions/ApolloCommonsTDTFoundationAdditions.h>

typedef NSString * (^TDTTableViewArrayDataSourceIdentificationBlock)(id item);
typedef void (^TDTTableViewArrayDataSourceConfigurationBlock)(UITableViewCell *cell, id item, NSIndexPath *indexPath);

/**
 A data source implementation that based on an array of items, with blocks
 to determine the cell identifier and configure the block for a given
 entry in the array.
 */
@interface TDTTableViewArrayDataSource : NSObject <UITableViewDataSource>

/**
 @note You need to call @p reloadData on the tableView yourself
 after updating the items array.
 */
@property (nonatomic, copy) NSArray *items;

/**
 @param identificationBlock Block which will be used to get the cell identifer
 for the cell corresponding to a particular item.

 @note The table view for which the receiver is set as the data source should
 have cells associated with an identifier. This can be done either by adding
 cells with identifiers in the nib, or by explicitly calling
 `registerNib:forCellReuseIdentifier:` on the table view.

 @param configurationBlock Block which can be used to configure the cell for
 a given item.
 */
- (instancetype)initWithIdentificationBlock:(TDTTableViewArrayDataSourceIdentificationBlock)identificationBlock
                         configurationBlock:(TDTTableViewArrayDataSourceConfigurationBlock)configurationBlock NS_DESIGNATED_INITIALIZER;

/**
 Convenience initializer that uses the same cell identifier for all the rows.
 */
- (instancetype)initWithIdentifier:(NSString *)identifier
                configurationBlock:(TDTTableViewArrayDataSourceConfigurationBlock)configurationBlock;

@end
