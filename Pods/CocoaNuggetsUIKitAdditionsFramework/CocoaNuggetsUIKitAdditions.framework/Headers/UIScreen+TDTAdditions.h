#import <UIKit/UIKit.h>

@interface UIScreen (TDTAdditions)

/**
 @return YES if the receiver is a 4-inch iPhone screen.

 Known devices with 4-inch screen: iPhone 5/5S.
*/
- (BOOL)tdt_is4InchIPhone;

/**
 @return YES if the receiver is a 4.7-inch iPhone screen.
 
 Known devices with 4.7-inch screen: iPhone 6.
 */
- (BOOL)tdt_is4_7InchIPhone;

/**
 @return YES if the receiver is a 5.5-inch iPhone screen.
 
 Known devices with 5.5-inch screen: iPhone 6 Plus.
 */
- (BOOL)tdt_is5_5InchIPhone;

@end
