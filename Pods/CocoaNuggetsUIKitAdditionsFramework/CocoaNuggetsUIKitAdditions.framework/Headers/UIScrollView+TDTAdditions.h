#import <UIKit/UIKit.h>

@interface UIScrollView (TDTAdditions)

/**
 This will make the content of the scroll view stick to the bottom.
 One use case of this is chat screen where we want messages to stick to bottom.
 
 @note This is required to be called whenever @p contentSize changes.
 One of the ways this can be achieved is by using @p KVO.
 You can observe @p contentSize property and call this method in the @p observer.
 */
- (void)tdt_adjustContentInsetToStickContentAtBottom;

@end
