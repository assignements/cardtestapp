#import <UIKit/UIKit.h>

typedef void(^TDTAlertViewCompletionBlockType)(UIAlertView *alertView, NSInteger buttonIndex);

@interface UIAlertView (TDTAdditions)

- (void)tdt_showWithCompletion:(TDTAlertViewCompletionBlockType)completionBlock;

@end
