#import <UIKit/UIKit.h>

@interface UIDevice (ZETAAdditions)

/**
  Checks the device model identifier and checks if the model ID is one of the unsupported
  devices.
  @note Contains unsupported device as of Feb 2016.
 */
+ (BOOL)zeta_isTouchIDUnSupportediPhoneDevice;

+ (BOOL)zeta_isDeviceOSBelowIOS8;

@end
