#import <UIKit/UIKit.h>

/**
 Gives you method to scroll @p UIScrollView programatically.
 */
@interface UIScrollView (TDTScrolling)

/**
 Tells you whether the scroll position of receiver is bottom or not.
 @return Returns @p YES if the receivers scroll position is bottom.
 */
- (BOOL)tdt_isScrolledToBottom;

/**
 Scrolls the receiver to bottom
 
 @param animated Animation is used while scrolling.
 */
- (void)tdt_scrollToBottomAnimated:(BOOL)animated;
@end
