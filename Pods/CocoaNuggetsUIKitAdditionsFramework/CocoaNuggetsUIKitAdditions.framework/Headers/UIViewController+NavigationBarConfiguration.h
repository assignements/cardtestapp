#import <UIKit/UIKit.h>
#import "ApolloViewControllerNavigationBarConfig.h"

@interface UIViewController (NavigationBarConfiguration)

- (void)configureNavigationBarWithConfig:(ApolloViewControllerNavigationBarConfig *)config;

- (void)configureNavigationBarWithConfig:(ApolloViewControllerNavigationBarConfig *)config
                  withBackButtonSelector:(SEL)backButtonSelector;

- (void)configureNavigationBarAppearance;


@end
