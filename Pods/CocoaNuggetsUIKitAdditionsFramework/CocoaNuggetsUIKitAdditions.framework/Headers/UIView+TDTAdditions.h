//
//  UIView+TDTAdditions.h
//  Talkto
//
//  Created by Yatin Sarbalia on 5/17/13.
//  Copyright (c) 2013 Talk.to FZC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (TDTAdditions)

- (UIImage *)tdt_screenshot;

- (void)tdt_removeAllSubviews;

- (CGPoint)tdt_topCenterPointInView:(UIView *)targetView;

- (void)tdt_fadeIn;
- (void)tdt_fadeOut;

@end
