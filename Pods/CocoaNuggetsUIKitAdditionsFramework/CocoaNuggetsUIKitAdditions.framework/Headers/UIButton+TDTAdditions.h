//
//  UIButton+TDTAdditions.h
//  Talkto
//
//  Created by Yatin Sarbalia on 5/17/13.
//  Copyright (c) 2013 Talk.to FZC. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^TDTButtonCompletionBlockType)(UIButton *button);

@interface UIButton (TDTAdditions)

// This method returns UIButton set with given parameters
// * frame
// * title -> UIButton label's title
// * textColor -> UIButton label's textColor
// * backgroundImageName -> image for UIButton in normal state
// * backgroundActiveImageName -> image for UIButton in active state
+ (UIButton *)tdt_buttonWithFrame:(CGRect)frame
                            title:(NSString *)title
                        textColor:(NSUInteger)color
              backgroundImageName:(NSString *)imageName
        backgroundActiveImageName:(NSString *)activeImageName;

+ (UIButton *)tdt_buttonWithType:(UIButtonType)buttonType
                           frame:(CGRect)frame
                           title:(NSString *)title
                       textColor:(NSUInteger)color
                       textAlpha:(CGFloat)alpha
                        fontName:(NSString *)fontName
                        fontSize:(CGFloat)fontSize;

/**
 Returns an instance of UIButton with translucent image
 for highlighted state and normal image for normal state.

 @param imageName Name of image of UIButton.
 @return An instance of UIButton
 */
+ (UIButton *)buttonWithImageName:(NSString *)imageName;

// Adjust `self` insets to place the image above text with `padding` in between.
// Do ensure to call this method, only when `self` has an exceptable frame.
- (void)tdt_placeImageAboveTextWithPadding:(CGFloat)padding;

/**
 Call this method on a UIButton with a completion block, completion block will
 contain whatever you want the button to do when pressed.
 */
- (void)tdt_buttonPressedWithCompletion:(TDTButtonCompletionBlockType)completionBlock;

@end
