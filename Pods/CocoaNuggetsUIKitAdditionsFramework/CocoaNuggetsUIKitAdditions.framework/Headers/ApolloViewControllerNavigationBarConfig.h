#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, NavigationBarVisibility) {
  NavigationBarVisibilityUndefined,
  NavigationBarVisibilityVisibile,
  NavigationBarVisibilityGone
};

@interface ApolloViewControllerNavigationBarConfig : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NavigationBarVisibility toolbarVisibility;
@property (nonatomic, readonly) BOOL toolbarEnableBack;
@property (nonatomic, readonly) NSString *toolbarTitle;
@property (nonatomic, readonly) NSString *backImage;

@end

NS_ASSUME_NONNULL_END
