#import <UIKit/UIKit.h>

#define TDT_IS_IPAD_IDIOM (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define TDT_IS_IPHONE_IDIOM (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define TDT_VALUE_AS_PER_DEVICE_TYPE(iPadFontSize, iPhoneFontSize) (TDT_IS_IPAD_IDIOM ? iPadFontSize : iPhoneFontSize)
