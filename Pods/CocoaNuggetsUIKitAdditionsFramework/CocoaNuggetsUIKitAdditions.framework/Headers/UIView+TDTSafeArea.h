//
//  UIView+TDTSafeArea.h
//  Talkto
//
//  Created by Yash Bhardwaj on 1/4/18.
//  Copyright © 2018 Talk.to FZC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (TDTSafeArea)

- (UIEdgeInsets)tdt_safeAreaInsets;

@end
