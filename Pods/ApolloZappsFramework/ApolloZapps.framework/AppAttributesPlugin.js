;(function(){

  console.log('Inject AppAttributesPlugin');
  window.zeta = window.zeta || {};

  window.zeta.setAppAttributes = function(attributes, callback, errback) {
  zetaBridge.ask("AppAttributes", "setAppAttributes", attributes, callback, errback);
  };

  window.zeta.getAppAttributes = function(callback, errback) {
  zetaBridge.ask("AppAttributes", "getAppAttributes", {}, callback, errback);
  };

  })();
