;(function(){

  console.log('Inject Clipboard Plugin');
  window.zeta = window.zeta || {};
  window.zeta.copyToClipboard = function(text, callback, errback) {
	zetaBridge.ask("Clipboard", "copyToClipboard", {"text" : text}, callback, errback);
  };

  })();