#import <Foundation/Foundation.h>
#import "ZETAJSPlugin.h"

typedef void (^ZETAPluginResponseBlockType)(id response);

@interface ZETABaseJSPlugin : NSObject<ZETAJSPlugin>

- (NSArray *)supportedActions;

- (void)handleAction:(NSString *)action
                args:(NSDictionary *)args
             success:(ZETAPluginResponseBlockType)success
             failure:(ZETAPluginResponseBlockType)failure;
@end
