#import <Mantle/Mantle.h>

@interface ZETABridgeMessageFromJS : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *callbackID;
@property (nonatomic, copy, readonly) NSString *service;
@property (nonatomic, copy, readonly) NSString *action;
@property (nonatomic, copy, readonly) NSString *args;

@end
