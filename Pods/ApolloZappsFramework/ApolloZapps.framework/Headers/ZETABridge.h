#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

FOUNDATION_EXPORT NSString *const ZETAWebviewLogsNotification;

@protocol ZETABridgeDelegate;
@class ZETAJSPluginManager;
@class ZETAShophookMetaData;
@class ZETAFileDataProvider;

@interface ZETABridge : NSObject

- (instancetype)initWithPluginManager:(ZETAJSPluginManager *)pluginManager
                             shophook:(ZETAShophookMetaData *)shophook
                     fileDataProvider:(ZETAFileDataProvider *)networkFileProvider
                        configuration:(WKWebViewConfiguration *)configuration;

- (void)dispatchEventWithName:(NSString *)eventName eventData:(NSDictionary *)eventData;

@property (nonatomic, weak) WKWebView *webView;
@property (nonatomic, weak) id<ZETABridgeDelegate> delegate;

@end

@protocol ZETABridgeDelegate<NSObject>

- (void)bridgeDidStartScriptInjection:(ZETABridge *)bridge;

- (void)bridgeDidFinishScriptInjection:(ZETABridge *)bridge;

- (void)bridgeDidReceiveCloseFromWindow:(ZETABridge *)bridge;

@end
