#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

@interface ZETAShophookCategory : MTLModel<MTLJSONSerializing, ZETAOriginalPayloadModelProtocol>

@property (nonatomic, readonly) NSString *category;
@property (nonatomic, readonly) NSString *displayName;
@property (nonatomic, readonly) NSNumber *sortOrder;

@end
