#import <Foundation/Foundation.h>
#import "ZETAJSPlugin.h"
#import <ApolloAnalyticsCore/ApolloAnalyticsManager.h>

@class ZETABridgeMessageFromJS;

@interface ZETAJSPluginManager : NSObject

- (instancetype)initWithAnalyticsManager:(ApolloAnalyticsManager *)analyticsManager;

- (void)registerPlugin:(id<ZETAJSPlugin>)plugin;

- (void)unregisterPlugin:(id<ZETAJSPlugin>)plugin;

- (NSArray *)allRegisteredPlugins; /** id<ZETAJSPlugin> */

- (void)handleMessageFromJS:(ZETABridgeMessageFromJS *)message
                    success:(ZETAPluginResponseBlockType)success
                    failure:(ZETAPluginResponseBlockType)failure;

@end
