#import <Foundation/Foundation.h>
#import "ZETABridgeMessageFromJS.h"

// TODO: Move to MTLModel returned as response
typedef void (^ZETAPluginResponseBlockType)(id response);

@protocol ZETAJSPlugin<NSObject>

- (NSString *)JSFileToInject;

- (NSString *)pluginServiceName;

- (BOOL)canHandleMessage:(ZETABridgeMessageFromJS *)message;

- (void)handleMessage:(ZETABridgeMessageFromJS *)message
              success:(ZETAPluginResponseBlockType)success
              failure:(ZETAPluginResponseBlockType)failure;

@end
