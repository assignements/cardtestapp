#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import <ApolloAnalyticsCore/ApolloAnalyticsManager.h>
#import <CocoaNuggetsUIKitAdditions/ApolloViewControllerNavigationBarConfig.h>

@class ZETAGenericWebViewController;
@class ZETAShophookMetaData;
@class ZETAAnalyticsManager;
@class ZETAZetletPopupPresenter;

@interface ZETAWebViewBuilder : MTLModel<MTLJSONSerializing>

@property (nonatomic) NSString *title;
@property (nonatomic) NSURL *URL;
@property (nonatomic) NSURL *callbackURL;
@property (nonatomic) ZETAShophookMetaData *shophookData;
@property (nonatomic) BOOL injectJSPlugins;             // default: NO
@property (nonatomic) BOOL injectAuthorizationPlugin;   // default: NO
@property (nonatomic) BOOL cancelButtonNeeded;          // defualt: YES
@property (nonatomic) BOOL shouldDisplayNavigationBar;  // default: YES
@property (nonatomic) BOOL enableBackNavigationGesture; // default: NO
@property (nonatomic) BOOL skipCommonQueryParams;       // default: NO
@property (nonatomic) ApolloViewControllerNavigationBarConfig *navConfig;
@property (nonatomic) ZETAZetletPopupPresenter *popupPresenter;
@property (nonatomic) ApolloAnalyticsManager *analyticsManager;

- (ZETAGenericWebViewController *)build;

@end
