#import <Foundation/Foundation.h>
#import <ApolloDeeplinkResolver/ZETABaseFlowManager.h>

@class ZETAPluginFactory;
@class ZETAWebViewBuilder;

@interface ZETAGenericWebviewFlowManager : ZETABaseFlowManager

- (instancetype)initWithBuilder:(ZETAWebViewBuilder *)builder
                  pluginFactory:(ZETAPluginFactory *)pluginFactory
       presentingViewController:(UIViewController *)viewController;

@end
