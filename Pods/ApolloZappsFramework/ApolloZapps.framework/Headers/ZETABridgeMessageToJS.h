#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface ZETABridgeMessageToJS : MTLModel<MTLJSONSerializing>

- (instancetype)initWithCallbackID:(NSString *)callbackID success:(BOOL)success args:(id)args;

@property (nonatomic, copy, readonly) NSString *callbackID;
@property (nonatomic, readonly) BOOL isSuccess;
@property (nonatomic, copy, readonly) id args;

@end
