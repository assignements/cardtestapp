#import <Mantle/Mantle.h>

@interface ZETAPluginErrorResponse : MTLModel<MTLJSONSerializing>

- (instancetype)initWithErrorCode:(NSString *)errorCode
                          message:(NSString *)message;

@end
