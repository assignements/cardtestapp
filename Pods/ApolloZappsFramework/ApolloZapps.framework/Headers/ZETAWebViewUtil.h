#import <Foundation/Foundation.h>

@protocol ZETAWebViewUtilDataSource;

@interface ZETAWebViewUtil : NSObject

+ (instancetype)sharedProvider;
+ (void)resetSharedProvider;

@property (weak, nonatomic) id<ZETAWebViewUtilDataSource> dataSource;

- (NSDictionary *)webviewQueryParams;

@end

@protocol  ZETAWebViewUtilDataSource<NSObject>

- (NSDictionary *)webViewUtilDidRequestCommonWebviewQueryParams:(ZETAWebViewUtil *)webViewUtil;

@end
