#import <Mantle/Mantle.h>

@class ZETAShophooksManager;

@interface ZETAShophooksZetletModel : MTLModel<MTLJSONSerializing>

- (instancetype)initWithShophooksManager:(ZETAShophooksManager *)shophooksManager;

@property (nonatomic, readonly) NSArray *shophooks;
@property (nonatomic, readonly) NSArray *shophooksCategory;

@end
