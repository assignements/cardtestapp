#import <Mantle/Mantle.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

@class ZETAShophookAttributes;

@interface ZETAShophookMetaData : MTLModel<MTLJSONSerializing, ZETAOriginalPayloadModelProtocol>

@property (nonatomic, readonly) NSString *shophookID;
@property (nonatomic, readonly) NSString *displayName;
@property (nonatomic) NSURL *launchURL;
@property (nonatomic, readonly) NSURL *iconURL;
@property (nonatomic, readonly) NSURL *scriptBundleURL;
@property (nonatomic, readonly) NSNumber *sortOrder;
@property (nonatomic, readonly) ZETAShophookAttributes *attributes;

@end
