#import <Mantle/Mantle.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

@class ZETAShophookLoaderConfig;
@protocol ZETAOriginalPayloadModelProtocol;

@interface ZETAShophookIOSAttributes
    : MTLModel<MTLJSONSerializing, ZETAOriginalPayloadModelProtocol>

@property (nonatomic, readonly) NSString *minSupportedVersion;
@property (nonatomic, readonly) NSString *maxSupportedVersion;
@property (nonatomic, readonly) BOOL isDisabled;
@property (nonatomic, readonly) BOOL disableWebViewBounce;

@end

@interface ZETAShophookCommonAttributes
    : MTLModel<MTLJSONSerializing, ZETAOriginalPayloadModelProtocol>

@property (nonatomic, readonly) BOOL addUserIdentificationToken;
@property (nonatomic, readonly) BOOL addSessionAuthToken;
@property (nonatomic, readonly) BOOL addUserID;
@property (nonatomic, readonly) NSArray *requiredPlugins;
@property (nonatomic, readonly) BOOL removeNavigationBar;
@property (nonatomic, readonly) ZETAShophookLoaderConfig *startLoader;
@property (nonatomic, readonly) NSString *category;
@property (nonatomic, readonly) BOOL loadInBackground;
@property (nonatomic, readonly) NSArray *ifiList;
@property (nonatomic, readonly) NSArray *corpIDList;
@property (nonatomic, readonly) BOOL removeBlockingLoader;
@property (nonatomic, readonly) BOOL enableBackNavigationGesture;

// TODO: Either remove this support for useBasicPlugins completely or push it via collections that
// what all exists in basic set of plugins
@property (nonatomic, readonly) BOOL useBasicPlugins;

@end

@interface ZETAShophookAttributes : MTLModel<MTLJSONSerializing, ZETAOriginalPayloadModelProtocol>

@property (nonatomic, readonly) ZETAShophookCommonAttributes *common;
@property (nonatomic, readonly) ZETAShophookIOSAttributes *ios;

- (BOOL)isDisabled;
- (BOOL)addUserIdentificationToken;
- (BOOL)addSessionAuthToken;
- (BOOL)addUserID;
- (NSString *)minSupportedVersion;
- (NSString *)maxSupportedVersion;
- (NSArray *)requiredPlugins;
- (BOOL)useBasicPlugins;
- (BOOL)removeNavigationBar;
- (BOOL)removeBlockingLoader;
- (BOOL)enableBackNavigationGesture;

@end
