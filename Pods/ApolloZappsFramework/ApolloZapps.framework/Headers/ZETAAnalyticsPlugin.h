#import "ZETABaseJSPlugin.h"
#import <ApolloAnalyticsCore/ApolloAnalyticsCore.h>

@interface ZETAAnalyticsPlugin : ZETABaseJSPlugin

- (instancetype)initWithAnalyticsManager:(ApolloAnalyticsManager *)analyticsManager;

@end
