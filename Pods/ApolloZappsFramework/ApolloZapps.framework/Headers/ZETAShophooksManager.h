#import <Foundation/Foundation.h>
#import "ZETAShophookMetaData.h"
#import <ApolloCollectionsCore/ZETAGenericCollectionManager.h>
#import <ApolloCommonsAgentInfo/ApolloAgentInfo.h>

FOUNDATION_EXPORT NSString *const ShophooksCollectionID;

@protocol ZETAShophooksManagerDelegate;

@interface ZETAShophooksManager : NSObject

- (instancetype)initWithGenericCollectionManager:(ZETAGenericCollectionManager *)collectionManager
                           shophooksCollectionID:(NSString *)shophooksCollectionID
                   shophooksCategoryCollectionID:(nullable NSString *)shophooksCategoryCollectionID
                                       agentInfo:(TDTAgentInfo *)agentInfo
                  isAuthorizationPluginGKEnabled:(BOOL)isAuthorizationPluginGKEnabled;

@property (nonatomic, readonly) NSDictionary *shophooks;
@property (nonatomic, readonly) NSDictionary *shophooksCategory;

@property (nonatomic, weak) id<ZETAShophooksManagerDelegate> delegate;

- (NSArray *)availableShophooks;
- (ZETAShophookMetaData *)getInitialMetadata:(NSString *)shophookID;
/// Async : Adds URL params using attributes
/// @param shophookID zapp id
/// @param completion returns updated MetaData
- (void)updatedMetaDataForShophookWithID:(NSString *)shophookID completion:(ZETAResponseBlockType)completion;

@end

@protocol ZETAShophooksManagerDelegate<NSObject>

- (void)getAuthTokenForshophooksManager:(ZETAShophooksManager *)shophooksManager
                             shophookID:(NSString *)shophookID
                             completion:(ZETAStringBlockType)completion;

- (void)getBase64JWETokenForshophooksManager:(ZETAShophooksManager *)shophooksManager
                                  shophookID:(NSString *)shophookID
                                  completion:(ZETAStringBlockType)completion;

- (void)getUserIDForshophooksManager:(ZETAShophooksManager *)shophooksManager
                          shophookID:(NSString *)shophookID
                          completion:(ZETAStringBlockType)completion;

@end
