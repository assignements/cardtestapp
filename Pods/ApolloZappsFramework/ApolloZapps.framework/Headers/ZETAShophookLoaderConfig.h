#import <Mantle/Mantle.h>

@interface ZETAShophookLoaderConfig : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString* templateID;
@property (nonatomic, readonly) NSDictionary* templateData;

@end
