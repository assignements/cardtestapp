#import <Foundation/Foundation.h>

@class ZETAShophookAttributes;
@protocol ZETAJSPlugin;
@protocol ZETAPluginFactoryDelegate;

@interface ZETAPluginFactory : NSObject

- (instancetype)initWithAllAvailblePluginNames:(NSArray *)allAvailblePluginNames
                              basicPluginNames:(NSArray *)basicPluginNames;

@property (nonatomic) NSString *shophookID;

- (NSArray *)allAvailblePlugins;

/**
 * @param plugins is an array of ZETAPlugin enum values
 */
- (NSArray *)pluginsWithRequirements:(NSArray *)plugins;
- (NSArray *)pluginsForShophookAttributes:(ZETAShophookAttributes *)shophookAttributes;

@property (nonatomic, weak) id<ZETAPluginFactoryDelegate> delegate;

@end

@protocol ZETAPluginFactoryDelegate<NSObject>

- (id<ZETAJSPlugin>)pluginFactory:(ZETAPluginFactory *)pluginFactory pluginForName:(NSString *)pluginName;

@end
