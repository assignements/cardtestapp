#import "ZETABaseJSPlugin.h"

@class TDTAgentInfo;
@protocol ZETAUAInfoPluginDelegate;

@interface ZETAUAInfoPlugin : ZETABaseJSPlugin

@property (nonatomic, weak) id<ZETAUAInfoPluginDelegate> delegate;

@end

@protocol ZETAUAInfoPluginDelegate<NSObject>

- (TDTAgentInfo *)agentInfoForUAInfoPlugin:(ZETAUAInfoPlugin *)UAInfoPlugin;

@end
