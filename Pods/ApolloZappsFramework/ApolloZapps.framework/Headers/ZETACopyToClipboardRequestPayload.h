#import <Mantle/Mantle.h>

@interface ZETACopyToClipboardRequestPayload : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *text;

@end
