#import <Foundation/Foundation.h>

@interface NSJSONSerialization (ZETAAddition)

+ (NSString *)zeta_JSONStringFromObject:(id)object;

+ (BOOL)zeta_isValidJSONValue:(id)object;

@end
