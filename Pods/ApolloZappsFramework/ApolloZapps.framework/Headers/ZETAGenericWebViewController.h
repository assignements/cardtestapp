#import <UIKit/UIKit.h>

@protocol ZETAGenericWebViewControllerDelegate;
@protocol ZETAGenericWebViewControllerJSPluginDataSource;
@class ZETAShophookMetaData;

@interface ZETAGenericWebViewController : UIViewController

@property (nonatomic, weak) id<ZETAGenericWebViewControllerDelegate> delegate;
@property (nonatomic, weak) id<ZETAGenericWebViewControllerJSPluginDataSource> JSPluginDataSource;

- (void)refreshShophookMetaData:(ZETAShophookMetaData *)shophookData;
- (void)refreshShophookMetaData:(ZETAShophookMetaData *)shophookData webViewURL:(NSURL *)webViewURL;
- (ZETAShophookMetaData *)getShophookMetaData;

@end

@protocol ZETAGenericWebViewControllerDelegate<NSObject>

- (void)genericWebViewControllerCloseButtonTapped:(ZETAGenericWebViewController *)viewController;
- (void)genericWebViewControllerBackButtonTapped:(ZETAGenericWebViewController *)viewController;

@optional
- (void)genericWebViewControllerDidFinishLoading:(ZETAGenericWebViewController *)viewController;
- (void)genericWebViewController:(ZETAGenericWebViewController *)viewController
         didFailLoadingWithError:(NSError *)error;
- (void)genericWebViewControllerDidFinishScriptInjection:
    (ZETAGenericWebViewController *)viewController;
- (void)genericWebViewController:(ZETAGenericWebViewController *)viewController
           didFinishWithCallback:(NSURL *)URL;

@end

@protocol ZETAGenericWebViewControllerJSPluginDataSource<NSObject>

/**
 @returns An array of id<ZETAJSPlugin> to be injected
*/
- (NSArray *)pluginsForWebViewController:(ZETAGenericWebViewController *)webviewController;

@end
