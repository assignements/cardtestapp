#import <Foundation/Foundation.h>
#import <ApolloDeeplinkResolver/ZETABaseFlowManager.h>
#import <ApolloAnalyticsCore/ApolloAnalyticsCore.h>

@class ZETAAnalyticsManager;
@class ZETAPluginFactory;
@class ZETAShophooksManager;
@class ZETAZetletPopupPresenter;
@protocol ZETAZappLaunchFlowManagerDelegate;

@interface ZETAZappLaunchFlowManager : ZETABaseFlowManager

- (instancetype)initWithZappID:(NSString *)zappID
                        params:(NSDictionary *)params
              shophooksManager:(ZETAShophooksManager *)shophooksManager
                 pluginFactory:(ZETAPluginFactory *)pluginFactory
      presentingViewController:(UIViewController *)viewController
              analyticsManager:(ApolloAnalyticsManager *)analyticsManager
                popupPresenter:(ZETAZetletPopupPresenter *)popupPresenter
prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

- (instancetype)initWithZappID:(NSString *)zappID
                   queryParams:(NSDictionary *)queryParams
                      fragment:(NSString *)fragment
                   callbackURL:(NSURL *)callbackURL
              shophooksManager:(ZETAShophooksManager *)shophooksManager
                 pluginFactory:(ZETAPluginFactory *)pluginFactory
      presentingViewController:(UIViewController *)viewController
              analyticsManager:(ApolloAnalyticsManager *)analyticsManager
                popupPresenter:(ZETAZetletPopupPresenter *)popupPresenter
prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs;

@property (nonatomic, weak) id<ZETAZappLaunchFlowManagerDelegate> delegate;

@end

@protocol ZETAZappLaunchFlowManagerDelegate<NSObject>

- (void)zappLaunchFlowManagerDidCancelFlow:(ZETAZappLaunchFlowManager *)flowManager;
- (void)zappLaunchFlowManagerDidCompleteFlow:(ZETAZappLaunchFlowManager *)flowManager;

@optional

- (void)zappLaunchFlowManager:(ZETAZappLaunchFlowManager *)flowManager
        didFinishWithCallback:(NSURL *)URL;

@end
