#import "ZETABaseJSPlugin.h"

@class ZETAShophooksManager;

@interface ZETAShophookPlugin : ZETABaseJSPlugin

- (instancetype)initWithShophooksManager:(ZETAShophooksManager *)shophooksManager
                              shophookID:(NSString *)shophookID;

@end
