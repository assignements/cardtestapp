;(function() {
  // not using Event constructor as it isn't available in older Webkit browsers
  
  /*
   Recommended not to execute javascripts instructions directly over the jscontext.
   It results in random behaviour.
   Instead do it using setTimeout.
   */
  setTimeout(function() {
             var event = document.createEvent('Event');
             event.initEvent('zetaready', true, true);
             document.dispatchEvent(event);
             }, 0);
  window.webkit.messageHandlers._zetaInjectCallback.postMessage(null);
  })();
