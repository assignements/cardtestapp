;(function(){
  
  console.log('Inject echo');
  window.zeta = window.zeta || {};
  window.zeta.echo = function(callback, errback) {
    zetaBridge.ask("EchoService", "echo", {}, callback, errback);
  };
  
})();
