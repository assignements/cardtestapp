#import <AFNetworking/AFURLResponseSerialization.h>

static NSString * const JSONResponseSerializerBodyKey = @"body";
static NSString * const JSONResponseSerializerStatusCodeKey = @"statusCode";

@interface ZETAJSONResponseSerializerWithData : AFJSONResponseSerializer

@end
