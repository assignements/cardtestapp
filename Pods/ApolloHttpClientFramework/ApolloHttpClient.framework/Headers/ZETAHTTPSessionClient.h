#import "ZETABaseHTTPSessionClient.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZETAHTTPSessionClient : ZETABaseHTTPSessionClient

- (instancetype)initWithBaseURL:(NSURL *)URL
                   andUserAgent:(NSString *)userAgent;

@end

NS_ASSUME_NONNULL_END
