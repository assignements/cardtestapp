#import <Foundation/Foundation.h>

@interface NSError (ApolloHttpClientAdditions)

- (NSDictionary *)zeta_APIResponseBody;

@end
