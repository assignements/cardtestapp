#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPSessionManager.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import <Mantle/Mantle.h>
#import <ApolloCertPinning/ApolloCertPinningDelegate.h>

FOUNDATION_EXPORT NSString *const ZETAAuthHeaderField;

typedef NS_ENUM(NSUInteger, ZETABaseHTTPClientErrorType) {
    ZETABaseHTTPClientErrorCodeUnknown,
    ZETABaseHTTPClientErrorCodeResponseParsingFailed,
    ZETABaseHTTPClientErrorCodeAuthTokenNotProvided
};

@protocol ApolloBaseHTPClientDelegate;

@interface ZETABaseHTTPSessionClient : NSObject

@property (nonatomic) AFHTTPSessionManager *HTTPSessionManager;
@property (nonatomic, weak) id <ApolloCertPinningDelegate> delegate;

- (instancetype)initWithBaseURL:(NSURL *)URL
                      userAgent:(NSString *)userAgent;

- (void)dispatchPOSTRequestWithPayload:(MTLModel<MTLJSONSerializing> *)requestPayload
                              endPoint:(NSString *)endPoint
                         responseClass:(Class)responseClass
                          requiresAuth:(BOOL)requiresAuth
                               headers:(NSDictionary *)headers
                               success:(ZETAResponseBlockType)success
                               failure:(ZETAFailureBlockType)failure;

- (void)dispatchPUTRequestWithPayload:(MTLModel<MTLJSONSerializing> *)requestPayload
                              endPoint:(NSString *)endPoint
                         responseClass:(Class)responseClass
                          requiresAuth:(BOOL)requiresAuth
                               headers:(NSDictionary *)headers
                               success:(ZETAResponseBlockType)success
                              failure:(ZETAFailureBlockType)failure;

- (void)dispatchGETRequestWithPayload:(MTLModel<MTLJSONSerializing> *)requestPayload
                             endPoint:(NSString *)endPoint
                        responseClass:(Class)responseClass
                         requiresAuth:(BOOL)requiresAuth
                              headers:(NSDictionary *)headers
                              success:(ZETAResponseBlockType)success
                              failure:(ZETAFailureBlockType)failure;

- (void)dispatchPATCHRequestWithPayload:(MTLModel<MTLJSONSerializing> *)requestPayload
                               endPoint:(NSString *)endPoint
                          responseClass:(Class)responseClass
                           requiresAuth:(BOOL)requiresAuth
                                headers:(NSDictionary *)headers
                                success:(ZETAResponseBlockType)success
                                failure:(ZETAFailureBlockType)failure;

- (void)dispatchDELETERequestWithPayload:(MTLModel<MTLJSONSerializing> *)requestPayload
                                endPoint:(NSString *)endPoint
                           responseClass:(Class)responseClass
                            requiresAuth:(BOOL)requiresAuth
                                 headers:(NSDictionary *)headers
                                 success:(ZETAResponseBlockType)success
                                 failure:(ZETAFailureBlockType)failure;

- (NSString *)clientErrorDomain;

- (NSString *)clientIdentifier;

- (NSString *)sessionAuthToken;

- (void)sessionAuthTokenWithSuccessBlock:(ZETAStringBlockType)successBlock
                            failureBlock:(ZETAFailureBlockType)failureBlock;

- (BOOL)isClientSpecificError:(NSError *)error task:(NSURLSessionDataTask *)task;

- (NSInteger)errorCodeForClientError:(NSError *)error task:(NSURLSessionDataTask *)task;

@end

