#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "TDTZCommon.h"
#import "TDTZCompressor.h"
#import "TDTZDecompressor.h"

FOUNDATION_EXPORT double CocoaZVersionNumber;
FOUNDATION_EXPORT const unsigned char CocoaZVersionString[];

