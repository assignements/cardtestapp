#import <Foundation/Foundation.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ApolloUserHelperDelegate;

@interface ApolloUserHelper : NSObject

@property (nonatomic, weak) id<ApolloUserHelperDelegate> delegate;

- (nullable NSString *)apolloInstallationID;

- (instancetype)initWithBaseUrl:(NSURL *)baseUrl
                      userAgent:(NSString *)userAgent
                       tenantID:(NSNumber *)tenantID
                         apiKey:(NSString *)apiKey
                      projectID:(NSString *)projectID
                          appID:(NSString *)appID
                        sdkName:(NSString *)sdkName
                       platform:(NSString *)platform
                     sdkVersion:(NSString *)sdkVersion
                      versionID:(NSString *)versionID
                       deviceID:(NSString *)deviceID
                         uaInfo:(NSDictionary *)uaInfo
                  appAttributes:(NSDictionary *)appAttributes
                  sdkAttributes:(NSDictionary *)sdkAttributes;

- (void)registerInstallWithSuccessBlock:(ZETAStringBlockType)success
                           failureBlock:(ZETAFailureBlockType)failure;

- (void)logoutWithUserJID:(NSString *)userJID
             successBlock:(ZETAEmptyBlockType)success
             failureBlock:(ZETAFailureBlockType)failure;

- (void)updateUserOnlineStatus:(BOOL)status
                    forUserJID:(NSString *)userJID
                         attrs:(NSDictionary *)attrs;

- (void)clearPersistentData;

@end

@protocol ApolloUserHelperDelegate <NSObject>

- (void)apolloUserHelper:(ApolloUserHelper *)apolloUserHelper
didRequestTokenWithSuccessBlock:(ZETAStringBlockType)successBlock
            failureBlock:(ZETAFailureBlockType)failureBlock;

@end

NS_ASSUME_NONNULL_END
