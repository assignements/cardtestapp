#import <Foundation/Foundation.h>
#import <ApolloUserCore/ApolloUserCoreHeaders.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloUserDataStore : NSObject

- (instancetype)initWithSdkName:(NSString *)sdkName;

- (nullable AddAppInstallResponse *)getSavedAppInstall;
- (nullable AppVersionInfoResponse *)getSavedAppVersionInfo;
- (nullable AddSdkInstallResponse *)getSavedSdkInstall;
- (NSString *)deviceInstallationID;

- (void)saveAppInstall:(AddAppInstallResponse *)appInstall;
- (void)saveAppVersionInfo:(AppVersionInfoResponse *)versionInfo;
- (void)saveSdkInstall:(AddSdkInstallResponse *)sdkInstall;

- (void)clearPersistentData;

@end

NS_ASSUME_NONNULL_END
