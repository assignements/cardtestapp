#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ResourceBundle : NSObject

+(nullable UIImage *) getImageWithName:(nonnull NSString *)imageName;
@end

NS_ASSUME_NONNULL_END
