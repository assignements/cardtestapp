#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString* const ZETAStringValueTransformerName;
FOUNDATION_EXPORT NSString* const ZETANumberValueTransformerName;
FOUNDATION_EXPORT NSString* const ZETABooleanValueTransformerName;
FOUNDATION_EXPORT NSString* const ZETATimestampValueTransformerName;
FOUNDATION_EXPORT NSString* const ZETAReferenceDateValueTransformerName;
FOUNDATION_EXPORT NSString* const ZETAURLJSONTransformerName;
FOUNDATION_EXPORT NSString* const ZETADictionaryValueTransformerName;
FOUNDATION_EXPORT NSString* const ZETAArrayValueTransformerName;

@interface NSValueTransformer (ZETABasicTransformers)

@end
