#import <Foundation/Foundation.h>

@interface NSDictionary (ZETAAdditions_MTL)
+ (nonnull NSDictionary *)
zeta_identityPropertyMapAndPrefixPropertyMapWithModel:(nonnull Class)modelClass
withPrefix:(nonnull NSString *)prefix
prioritizePrefixProperty:(BOOL)prioritizePrefixProperty;
@end
