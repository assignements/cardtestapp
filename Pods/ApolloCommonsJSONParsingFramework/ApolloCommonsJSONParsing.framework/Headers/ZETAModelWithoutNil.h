#import <Mantle/Mantle.h>

/**
 This class takes care of removing the NSNull values while serializing to JSON.
 */

@interface ZETAModelWithoutNil : MTLModel

@end
