#import <Mantle/Mantle.h>

@protocol ZETAOriginalPayloadModelProtocol<NSObject>

@required
@property (nonatomic, copy) NSDictionary *originalPayload;

@end
