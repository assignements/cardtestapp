#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString* const ZETABase64EncodedDataValueTransformerName;
FOUNDATION_EXPORT NSString* const ZETAEscapedJSONStringTransfomerName;
FOUNDATION_EXPORT NSString* const ZETAReverseDefaultMantleObjectTransformer;

@interface NSValueTransformer (ApolloAdditions)

@end
