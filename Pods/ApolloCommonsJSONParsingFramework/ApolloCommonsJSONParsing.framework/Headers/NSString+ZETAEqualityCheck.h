#import <Foundation/Foundation.h>

@interface NSString (ZETAEqualityCheck)

- (BOOL)zeta_caseInsensitiveEqual:(NSString *)toCompare;

@end
