#import "ZETAJSONAdapter.h"

@interface ZETAJSONAdapter (ValueTransformers)

/**
 * Creates a reversible transformer to convert (escaped) JSON strings into an
 * MTLModel object, and vice-versa.
 */
+ (NSValueTransformer *)stringTransformerWithModelClass:(Class)modelClass;

+ (NSValueTransformer *)dictionaryValueTransformerWithModelClass:(Class)modelClass;

@end
