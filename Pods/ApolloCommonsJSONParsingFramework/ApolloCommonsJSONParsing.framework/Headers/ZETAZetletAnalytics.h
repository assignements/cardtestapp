#import <Mantle/Mantle.h>

@interface ZETAZetletEvent : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *eventName;
@property (nonatomic, readonly) NSDictionary *eventAttributes;
@property (nonatomic, readonly) BOOL isImmediate;

@end

@interface ZETAZetletAnalytics : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) ZETAZetletEvent *display;
@property (nonatomic, readonly) ZETAZetletEvent *dismiss;
@property (nonatomic, readonly) ZETAZetletEvent *click;

@end
