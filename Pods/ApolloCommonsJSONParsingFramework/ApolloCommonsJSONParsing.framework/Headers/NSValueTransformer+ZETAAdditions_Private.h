#import <Foundation/Foundation.h>

@interface NSValueTransformer (ZETAAdditions_Private)

+ (id)zeta_handleInvalidValue:(id)value success:(BOOL *)success error:(NSError **)error;

+ (NSError *)zeta_invalidInputErrorForValue:(id)value;

@end
