#import <Foundation/Foundation.h>

@interface NSValueTransformer (ZETAMultiplePathsTransformer)

/**
 If you want to support multiple JSON paths mapped to single property then provide array of those
 JSON paths for that property. Specify JSON path in the order of their priority. Eg: if you want to
 support both ios:marginLeft and marginLeft for marginLeft property, and ios:marginLeft has to be
 given priority if in case both path has valid values, then specify like this:

 propertyString(marginLeft): @[@"ios:marginLeft", @"marginLeft"]
 */

+ (NSValueTransformer *)zeta_multiplePathsTransformerWithTransformer:
                            (NSValueTransformer *)transformer
                                                   pathPriorityOrder:(NSArray *)pathPriorityOrder;

+ (NSValueTransformer *)zeta_multiplePathsTransformerWithTransformerName:(NSString *)transformerName
                                                       pathPriorityOrder:
                                                           (NSArray *)pathPriorityOrder;

@end
