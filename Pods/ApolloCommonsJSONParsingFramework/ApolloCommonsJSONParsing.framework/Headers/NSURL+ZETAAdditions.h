#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface NSURL (ZETAAdditions)

/**
 Returns YES if URL is a share URL.
 e.g., share://?message=share-message
 Refer http://phab.corp.zeta.in/w/inbox/ for more
 */
- (BOOL)zeta_isZetaShareURL;

/**
 Returns YES if URL is a http URL.
 e.g., http://host/image.png
 */
- (BOOL)zeta_isZetaHTTPURL;

/**
Return YES if the param url is same as self url
 */
- (BOOL)zeta_isSameURLString:(NSURL *)URL;

/**
  Returns a NSDictionary of query params by ignoring the duplicate keys
  Example:
  It will return @{@"param1":@"value1" or @"value2"} for www.google.com/?param1=value1&param1=value2
  @note It does not guarantee whether value1 or value2 will be returned.
  So please use only when you are sure that there will be only single value for a given param
 */
- (NSDictionary *)zeta_simpleQueryParams;

/*
 Return NSDictionary by calculating parameters using NSURLComponents
 */
-(NSDictionary *)zeta_complexQueryParams;

/**
Returns a new NSURL after merging the passed query params. Also takes care of hashbang URL (
http://example.com/path/#/?key=value)
 */
- (NSURL *)zeta_URLByAddingQueryParams:(NSDictionary *)queryParams;

/**
 Returns a new NSURL after merging the passed query params. If canOverrideExisting is set to true,
 merge will use the value from newParams when there is collision of keys
 */
- (NSURL *)zeta_URLByMergingQueryParams:(NSDictionary *)newParams canOverrideExisting:(BOOL) canOverrideExisting;

/**
 Returns a new NSURL by adding the passed fragment params
 */
- (NSURL *)zeta_URLByAddingFragmentParams:(NSDictionary *)fragmentParams;

/**
 Returns a new NSURL after merging the properties of the MTLModel
 */
- (NSURL *)zeta_URLByAddingQueryParamsFromModel:(MTLModel<MTLJSONSerializing> *)model;

- (NSURL *)zeta_URLBySerializingQueryParams:(NSDictionary *)params;

- (NSDictionary *)queryParamsFromSerialzedURL;

+ (instancetype)zeta_phoneURLFromNumberString:(NSString *)numberString;

+ (instancetype)zeta_fileURLFromImageName:(NSString *)imageName;

- (NSString *)zeta_shareText;

- (NSURL *)zeta_addCallbackURL:(NSURL *)callbackURL;

@end
