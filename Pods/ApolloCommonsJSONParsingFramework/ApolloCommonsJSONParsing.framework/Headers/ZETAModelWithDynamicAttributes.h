#import <Foundation/Foundation.h>

/**
 This protocol helps in adding any dynamic attributes during deserialization of any MTLModel
objects. If implemented, all the dynamicAttributes are added to the final JSON dictionary created.

 e.g., If the MTLModel subclass(complying with ZETAModelWithDynamicAttributes) has following
structure
 @property NSString *property1;
 @property NSString *property2;

 object.property1 = @"value1";
 object.property2 = @"value2";
 object.dynamicAttributes = @{@"innerkey1" : @"innerValue1"};

 then final JSON produced will be:
 {
  "property1" : "value1",
  "property2" : "value2",
  "innerKey1" : "innerValue1"
 }
 */
@protocol ZETAModelWithDynamicAttributes<NSObject>

@property (nonatomic, readonly) NSDictionary *dynamicAttributes;

@end
