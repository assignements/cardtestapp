#import <Mantle/Mantle.h>

@interface ZETADictionaryPayload : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly, copy) NSDictionary *dictionaryPayload;

- (instancetype)initWithDictionaryPayload:(NSDictionary *)dictionaryPayload;

@end
