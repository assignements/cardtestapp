#import <Mantle/Mantle.h>

FOUNDATION_EXPORT NSString *const ZETAQueryParamsTypeJSONString;

@interface ZETASerializedURLQueryParams : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *type;
@property (nonatomic, readonly) NSDictionary *value;

- (instancetype)initWithType:(NSString *)type value:(NSDictionary *)value;

@end
