#import <Mantle/Mantle.h>
#import "ZETAJSONAdapter.h"

extern NSString * _Nullable MTLModelLibCategory;

@interface MTLModel (ZETAAdditions)

/**
 Try and convert the receiver into a JSON dictionary.
 
 ZETAJSONAdapter is used for the serialization.
 
 @note The receiver must conform to the @p MTLJSONSerializing protocol
 @return If the receiver can be successfully serialized into JSON, then
 return the corresponding JSON dictionary. Otherwise return @p nil.
 */
- (nullable NSDictionary *)zeta_JSONDictionary;

/**
 Convenience initializer for @p zeta_modelFromJSONDictionary:assertOnDebug: with @p assertOnDebug as
 @p YES
 */
+ (nullable instancetype)zeta_modelFromJSONDictionary:(nonnull NSDictionary *)JSONDictionary;

/**
 Try and create an instance of the receiver from @p JSONDictionary.
 
 ZETAJSONAdapter is used for the deserialization.
 
 @param assertOnDebug It asserts on DEBUG builds if the deserialization fails.
 @note The receiver must conform to the @p MTLJSONSerializing protocol
 
 @return If @p JSONDictionary can be successfully deserialized into
 an instance of the receiver class, then return the instance.
 Otherwise log and error and return @p nil. Additionally, on debug
 builds, the program execution is stopped by failing an assert.
 */
+ (nullable instancetype)zeta_modelFromJSONDictionary:(nonnull NSDictionary *)JSONDictionary
                                        assertOnDebug:(BOOL)assertOnDebug;

/**
 Try and create an instance of the receiver from @p JSONString.
 
 It uses @p zeta_modelFromJSONDictionary internally.
 */
+ (nullable instancetype)zeta_modelFromJSONString:(nonnull NSString *)JSONString;

- (nullable NSString *)base64Representation;

@end

