#import <Mantle/Mantle.h>

/**
 Custom JSON adapter for serializing and deserializing JSON requests.
 It performs two functions:


 1. Discards NSNulls (only applicable during serialization - Mantle automatically
 does that during deserialization).

 This subclass takes care of removing the property keys which arent present
 in `dictionaryValue` of model object.

 e.g., It can be used to get rid of NSNull values. Use `ZETAModelWithoutNil` for that.


 2. Converts pipes to dots in the key names.

 The ZETAJSONAdapter implementation is hardcoded to convert dots
 in the keyPath to nested dictionaries:

 - (NSDictionary *)JSONDictionaryFromModel:(id<MTLJSONSerializing>)model
 error:(NSError **)error {
 ...
 void (^createComponents)(id, NSString *) = ^(id obj, NSString *keyPath) {
 NSArray *keyPathComponents = [keyPath componentsSeparatedByString:@"."];
 ...

 This behaviour is inappropriate for certain APIs (e.g. analytics) that have
 JSON keys containing dots. The model objects for such APIs can instead specify
 their key paths (in @p JSONKeyPathsByPropertyKey) by using the pipe
 character instead of the dot character, and this class will convert the pipes
 back to dots before the final serialization.

 In the reverse direction, dots in the JSON key names are converted to pipes
 when trying to match object properties.
 */
@interface ZETAJSONAdapter : MTLJSONAdapter

@end
