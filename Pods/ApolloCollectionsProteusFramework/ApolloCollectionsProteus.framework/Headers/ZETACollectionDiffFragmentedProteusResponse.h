#import <Mantle/Mantle.h>
#import <ApolloCollectionsCore/ZETACollectionsMessage.h>

@interface ZETACollectionDiffFragmentedProteusResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSArray *fragments;
@property (nonatomic, readonly) ZETACollectionsCompletionMessage *response;

@end
