#import <ApolloHttpClient/ZETAHTTPSessionClient.h>
#import <ApolloCollectionsCore/ZETACollectionClientProtocol.h>

@protocol ZETAProteusCollectionsClientDelegate;

@interface ZETAProteusCollectionsClient : ZETAHTTPSessionClient <ZETACollectionClientProtocol>

@property (nonatomic, weak) id<ZETAProteusCollectionsClientDelegate> tokenProvider;

- (void)getCollectionItem:(ZETAGetCollectionItemRequestPayload *)requestPayload
       requestUniquingTag:(NSString *)requestUniquingTag
                itemClass:(Class)itemClass
                  success:(ZETAGetCollectionItemResponseBlock)success
                  failure:(ZETAFailureBlockType)failure;

- (void)triggerCollectionDiffForCollectionsRequest:(ZETAGetCollectionRequestPayload *)requestPayload
                                requestUniquingTag:(NSString *)requestUniquingTag
                                         itemClass:(Class)itemClass
                                       diffHandler:(ZETACollectionsResponseBlock)diffHandler
                                           success:(ZETACollectionsCompletionBlock)success
                                           failure:(ZETAFailureBlockType)failure;

- (void)loadCollectionDataFromFileName:(NSString *)fileName
                               success:(ZETACollectionFileLoadResponse)sucess
                               failure:(ZETAFailureBlockType)failure;

@end

@protocol ZETAProteusCollectionsClientDelegate <NSObject>

- (void)proteusCollectionsClient:(ZETAProteusCollectionsClient *)client
 didRequestTokenWithSuccessBlock:(ZETAStringBlockType)successBlock
                    failureBlock:(ZETAFailureBlockType)failureBlock;

@optional
- (NSString *)proteusCollectionsClientDidRequestAuthToken:(ZETAProteusCollectionsClient *)client;

@end
