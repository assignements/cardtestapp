#import <Foundation/Foundation.h>

@interface RIVAHostnameResolver : NSObject

- (void)resolveHostname:(NSString *)hostname
        completionBlock:(void (^)(NSError *error, NSArray *addresses))completionBlock;

@end