#import <Foundation/Foundation.h>
#import <ApolloAnalyticsCore/ApolloAnalyticsManager.h>

@protocol ZETALanguageServiceDelegate;

@interface ZETALanguageService : NSObject

@property (nonatomic, weak) id<ZETALanguageServiceDelegate> delegate;

+ (void)setupWithSupportedLocales:(NSArray *)defaultSupportedLocales
                 analyticsManager:(ApolloAnalyticsManager *)analyticsManager;

+ (ZETALanguageService *)sharedInstance;
+ (void)reset;

- (void)setupCurrentLocale;
- (void)saveSelectedLanguage:(NSString *)language;
- (void)clearLanguageStore;
- (NSString *)currentLocale;
- (NSString *)currentLanguageCodeWithoutLocale;
- (void)sendLanguageChangeRequestToInbox;
- (NSString *)localizedStringForKey:(NSString *)key
                   withDefaultValue:(NSString *)defaultValue;

@end

@protocol ZETALanguageServiceDelegate<NSObject>

-(void)languageService:(ZETALanguageService *)languageService
didChangeCurrentLocaleToLocale:(NSString *)updatedLocale;

@end
