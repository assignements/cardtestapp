@interface ApolloSdkInstallData : NSObject <NSCoding>

@property (nonatomic, readonly) NSString *sdkInstallationID;
@property (nonatomic, readonly) NSString *installationID;
@property (nonatomic, readonly) NSString *sdkName;
@property (nonatomic, readonly) NSString *platform;
@property (nonatomic, readonly) NSString *sdkVersion;
@property (nonatomic, readonly) NSDictionary *attrs;

- (instancetype)initWithSdkInstallationID:(NSString *)sdkInstallationID
                           installationID:(NSString *)installationID
                                  sdkName:(NSString *)sdkName
                                 platform:(NSString *)platform
                               sdkVersion:(NSString *)sdkVersion
                                    attrs:(NSDictionary *)attrs;

@end
