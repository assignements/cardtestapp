#import "ZETAApolloUserStore.h"

@interface ZETAApolloUserStore (KVStore)

- (id)getValueFromKVStoreForKey:(NSString *)key;
- (void)setValueToKVStore:(id)value forKey:(NSString *)key;
- (void)removeKeyFromKVStore:(NSString *)key;
- (void)removeAllKeysFromKVStore;

@end
