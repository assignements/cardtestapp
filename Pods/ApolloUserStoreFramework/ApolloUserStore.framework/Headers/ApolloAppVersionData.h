#import <Foundation/Foundation.h>

@interface ApolloAppVersionData : NSObject <NSCoding>

@property (nonatomic, readonly) NSString *installationID;
@property (nonatomic, readonly) NSString *versionID;

- (instancetype)initWithInstallationID:(NSString *)installationID
                             versionID:(NSString *)versionID;

@end
