#import <Mantle/Mantle.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>
#import "ZETAGenericMessageAttributes.h"

@interface ZETAGenericMessagePayload : ZETAModelWithoutNil<MTLJSONSerializing>

- (instancetype)initWithMessageID:(NSString *)messageID
                       attributes:(ZETAGenericMessageAttributes *)attributes
                         payloads:(NSDictionary *)payloads
                   lastUpdateTime:(NSString *)lastUpdateTime;

@property (nonatomic, readonly, copy) NSString *messageID;
@property (nonatomic, readonly, copy) NSString *lastUpdateTime;
@property (nonatomic, readonly, copy) ZETAGenericMessageAttributes *attributes;
@property (nonatomic, readonly, copy) NSDictionary *payloads;

@end
