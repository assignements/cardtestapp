#import <Mantle/Mantle.h>

@interface ApolloAppInstallData : NSObject <NSCoding>

@property (nonatomic, readonly) NSString *installationID;
@property (nonatomic, readonly) NSString *appID;
@property (nonatomic, readonly) NSString *versionID;
@property (nonatomic, readonly) NSString *deviceID;
@property (nonatomic, readonly) NSString *deviceInstallationID;
@property (nonatomic, readonly) NSDictionary *uaInfo;

- (instancetype)initWithAppID:(NSString *)appID
               installationID:(NSString *)installationID
                    versionID:(NSString *)versionID
                     deviceID:(NSString *)deviceID
         deviceInstallationID:(NSString *)deviceInstallationID
                       uaInfo:(NSDictionary *)uaInfo;

@end
