#import <Foundation/Foundation.h>

@interface ApolloUserSessionData : NSObject <NSCoding>

@property (nonatomic, readonly) NSString *apolloUserSessionID;
@property (nonatomic, readonly) NSString *installationID;
@property (nonatomic, readonly) NSString *userID;
@property (nonatomic, readonly) NSString *resourceID;
@property (nonatomic, readonly) NSString *locale;

- (instancetype)initWithApolloUserSessionID:(NSString *)userSessionID
                             installationID:(NSString *)installationID
                                     userID:(NSString *)userID
                                 resourceID:(NSString *)resourceID
                                     locale:(NSString *)locale;

@end
