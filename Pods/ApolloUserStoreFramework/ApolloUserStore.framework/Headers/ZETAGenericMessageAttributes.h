#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

/**
 This class is archived to DB using NSCoding. Please do not change class name.
 */

@interface ZETAGenericMessageAttributes : ZETAModelWithoutNil<MTLJSONSerializing,ZETAOriginalPayloadModelProtocol>

@property (nonatomic, copy) NSString *messageType;
@property (nonatomic, copy) NSString *templateID;
@property (nonatomic) BOOL isNonZetletLegacyMessage;

@end
