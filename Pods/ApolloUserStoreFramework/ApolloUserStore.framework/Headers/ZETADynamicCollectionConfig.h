#import <Mantle/Mantle.h>

@interface ZETADynamicCollectionConfig : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSArray *zetlet;

@end
