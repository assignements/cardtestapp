#import "ZETACollectionSyncInfo.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>

@interface ZETAApolloUserStore (CollectionSyncInfo)

- (void)updateLatestVersion:(NSString *)latestVersion forCollectionID:(NSString *)collectionID;
- (void)updateOldestVersion:(NSString *)oldestVersion forCollectionID:(NSString *)collectionID;
- (void)setHasSyncedOnceForCollectionID:(NSString *)collectionID;
- (void)setHasFetchedLatestItemsOnceForCollectionID:(NSString *)collectionID;
- (void)setIsSubscribedForCollectionID:(NSString *)collectionID
                             withValue:(BOOL)isSubscribed;
- (NSString *)latestVersionForCollectionID:(NSString *)collectionID;
- (NSString *)oldestVersionForCollectionID:(NSString *)collectionID;
- (BOOL)hasSyncedOnceForCollectionID:(NSString *)collectionID;
- (BOOL)hasFetchedLatestItemsOnceForCollectionID:(NSString *)collectionID;
- (BOOL)isSubscribedForCollectionID:(NSString *)collectionID;
- (void)removeCollectionSyncInfoForCollectionID:(NSString *)collectionID;
- (NSDictionary *)collectionSyncStatusInfo;

@end
