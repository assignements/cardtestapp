#import <Foundation/Foundation.h>
#import "ZETASpotlightMessage.h"
#import <ApolloCommonsCore/ApolloAuthDataProtocol.h>
#import "ApolloAppInstallData.h"
#import "ApolloUserSessionData.h"
#import "ApolloAppVersionData.h"
#import "ApolloSdkInstallData.h"

@protocol ApolloUserStoreDelegate;
@class ZETAAppConfig;

@interface ZETAApolloUserStore : NSObject <ApolloServerTimeSyncProtocol>

@property (nonatomic, weak) id<ApolloUserStoreDelegate> delegate;

@property (atomic, nullable) NSString *lastSeenAppVersion;
@property (atomic, nonnull) NSDictionary *collectionVersions;
@property (atomic, nonnull, copy) NSDictionary *collectionSyncInfoPerCollectionID;
@property (atomic, nonnull, copy) NSDictionary *isCollectionLoadedFromFilePerCollectionID;
@property (atomic, nullable) ZETAAppConfig *appConfig;
@property (atomic, nonnull, copy) NSDictionary *isCollectionSubscribedPerCollectionID;
@property (atomic, nonnull) NSDictionary *gatekeepers;
@property (nonatomic, nullable) NSDictionary *persistentKVStoreDictionary;
@property (atomic, nullable) NSDictionary *templateIDsPerCollectionID;
@property (atomic, nullable) NSDictionary *templateVersionPerTemplateID;
@property (atomic, nonnull, copy) NSDictionary *inboxItemsHeight;
@property (atomic, nullable) ZETASpotlightMessage *lastReadSpotlightMessage;
@property (atomic, nullable) NSDictionary *userTriggersInjectedData;
@property (atomic, nullable) NSString *pushToken;
@property (atomic, nullable) NSDate *lastPushTokenSyncedTime;
@property (atomic) BOOL hasUserSkippedLoadingYourZetaExperienceScreen;
@property (atomic, nullable) NSDate *lastLaunch;
@property (nonatomic) BOOL isCertPinningEnabled;
@property (nonatomic, nullable) ApolloAppInstallData *apolloAppInstallData;
@property (nonatomic, nullable) ApolloSdkInstallData *apolloSdkInstallData;
@property (nonatomic, nullable) ApolloUserSessionData *apolloUserSessionData;
@property (nonatomic, nullable) ApolloAppVersionData *apolloAppVersionData;
@property (nonatomic) NSString *apolloDeviceInstallationID; /* Unique per app install. Not to be deleted on logout */

- (instancetype)initWithStoreName:(NSString *)storeName;
- (void)save;
- (void)saveCollectionVersions;
- (void)saveCollectionSyncInfoPerCollectionID;
- (void)savePersistentKVStore;
- (void)saveTemplateIDsPerCollectionIDs;
- (void)saveTemplateVersionPerTemplateID;
- (nullable instancetype)storeByRemovingAllItems;

@end

@interface ZETAApolloUserStore (Updates)

- (void)setCollectionVersion:(nonnull NSString *)collectionVersion
             forCollectionID:(nonnull NSString *)collectionID;

- (void)setIsGateOpen:(BOOL)isGateOpen forGatekeeper:(nonnull NSString *)gatekeeper;

@end

@protocol ApolloUserStoreDelegate<NSObject>

- (void)apolloUserStoreDidUpdateCollectionSyncInfo:(ZETAApolloUserStore *)userStore;

@end
