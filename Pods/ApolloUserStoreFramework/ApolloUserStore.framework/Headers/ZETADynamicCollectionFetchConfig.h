#import <Mantle/Mantle.h>

typedef NS_ENUM(NSUInteger, ZETADynamicCollectionFetchTrigger) {
  ZETADynamicCollectionFetchTriggerDoorAuth,
  ZETADynamicCollectionFetchTriggerAppConfigUpdate,
  ZETADynamicCollectionFetchTriggerDoorConnect,
  ZETADynamicCollectionFetchTriggerOnDemand
};

typedef NS_ENUM(NSUInteger, ZETADynamicCollectionSyncMode) {
  ZETADynamicCollectionSyncModeCompleteSync,
  ZETADynamicCollectionSyncModeLazySync
};

@interface ZETADynamicCollectionFetchConfig: MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *collectionID;
@property (nonatomic, readonly) BOOL subscribe;
@property (nonatomic, readonly) NSNumber *batchSize;
@property (nonatomic, readonly) ZETADynamicCollectionSyncMode syncMode;
@property (nonatomic, readonly) ZETADynamicCollectionFetchTrigger fetchTrigger;

- (instancetype)initWithCollectionID:(NSString *)collectionID
                     shouldSubscribe:(BOOL)shouldSubscribe
                           batchSize:(NSNumber *)batchSize
                            syncMode:(ZETADynamicCollectionSyncMode)syncMode
                        fetchTrigger:(ZETADynamicCollectionFetchTrigger)fetchTrigger;

@end

