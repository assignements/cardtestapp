#import "ZETAGenericMessagePayload.h"

@interface ZETASpotlightMessage : MTLModel

- (instancetype)initWithGenericMessage:(ZETAGenericMessagePayload *)genericMessage
                      collectionItemID:(NSString *)collectionItemID
                             createdAt:(NSString *)createdAt
                             updatedAt:(NSString *)updatedAt;

@property (nonatomic, readonly) ZETAGenericMessagePayload *message;
@property (nonatomic, copy, readonly) NSString *collectionItemID;
@property (nonatomic, copy, readonly) NSString *createdAt;
@property (nonatomic, copy, readonly) NSString *updatedAt;

@end
