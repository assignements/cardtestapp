#import <Mantle/Mantle.h>
#import "ZETADynamicCollectionConfig.h"

@interface ZETAAppConfig : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSNumber *supportStartTimeInHrs;
@property (nonatomic, readonly) NSNumber *supportEndTimeInHrs;
@property (nonatomic, readonly) NSString *billUploadProcessingText;
@property (nonatomic, readonly) NSDictionary *currentVersionPerCollectionIDs;
@property (nonatomic, readonly) NSNumber *A2APublicKeysLastUpdateTime;
@property (nonatomic, readonly) NSArray *screenCaptureRestrictedTemplateIDs;
@property (nonatomic, readonly) NSString *defaultCurrency;
@property (nonatomic, readonly) NSString *countryISOCode;
@property (nonatomic, readonly) NSArray *supportedLocales;
@property (nonatomic, readonly) NSString *themeID;
@property (nonatomic, readonly) NSString *countryCode;
@property (nonatomic, readonly) NSString *appID;
@property (nonatomic, readonly) NSString *deeplinkName;
@property (nonatomic, readonly) ZETADynamicCollectionConfig *dynamicCollectionsConfig;

@end
