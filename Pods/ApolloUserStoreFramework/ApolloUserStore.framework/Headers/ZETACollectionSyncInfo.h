@interface ZETACollectionSyncInfo : NSObject

@property (nonatomic, copy) NSString *latestVersion;
@property (nonatomic, copy) NSString *oldestVersion;
@property (nonatomic) BOOL hasSyncedOnce;
@property (nonatomic) BOOL hasFetchedLatestItemsOnce;
@property (nonatomic) BOOL isSubscribed;

@end
