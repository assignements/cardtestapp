#import "ZETAApolloUserStore.h"

@interface ZETAApolloUserStore (ZetletItemsHeight)

- (void)updateZetletItemsHeight:(NSDictionary *)heights;

@end
