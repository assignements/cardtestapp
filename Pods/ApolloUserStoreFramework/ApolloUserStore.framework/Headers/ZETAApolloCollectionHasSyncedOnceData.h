#import <Mantle/Mantle.h>

@interface ZETAApolloCollectionHasSyncedOnceData : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) BOOL hasSyncedOnce;

- (instancetype)initWithHasSyncedOnce:(BOOL)hasSyncedOnce;

@end
