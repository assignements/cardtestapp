#import <ApolloUserStore/ZETAApolloUserStore.h>

@interface ZETAApolloUserStore (CollectionSubscription)

- (BOOL)isCollectionSubscribedForCollectionID:(nonnull NSString *)collectionID;
- (void)markCollectionSubscribedForCollectionID:(nonnull NSString *)collectionID;

@end
