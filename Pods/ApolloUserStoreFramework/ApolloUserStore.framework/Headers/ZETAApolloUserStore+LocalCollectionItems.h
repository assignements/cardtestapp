#import <ApolloUserStore/ZETAApolloUserStore.h>

@interface ZETAApolloUserStore (LocalCollectionItems)

- (BOOL)isCollectionLoadedFromFileForCollectionID:(nonnull NSString *)collectionID;
- (void)markCollectionLoadedFromFileForCollectionID:(nonnull NSString *)collectionID;
- (void)removeCollectionLoadedFromFileInfoForCollectionID:(nonnull NSString *)collectionID;

@end
