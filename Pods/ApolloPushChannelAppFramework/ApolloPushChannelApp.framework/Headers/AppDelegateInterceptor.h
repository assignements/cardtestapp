#import <Foundation/Foundation.h>
#import <UIKit/UIApplication.h>

@protocol AppDelegateInterceptorDelegate;

@interface AppDelegateInterceptor : NSObject <UIApplicationDelegate>

@property (nonatomic, weak) id<AppDelegateInterceptorDelegate> delegate;

+ (instancetype)sharedInstance;

@end

@protocol AppDelegateInterceptorDelegate <NSObject>

- (void)appDelegateInterceptor:(AppDelegateInterceptor *)interceptor
        didGenerateDeviceToken:(NSData *)deviceToken;

- (void)appDelegateInterceptor:(AppDelegateInterceptor *)interceptor
    didReceivePushNotification:(NSDictionary *)notification;

- (void)appDelegateInterceptor:(AppDelegateInterceptor *)interceptor
  didFailToGenerateDeviceToken:(NSError *)error;

@end
