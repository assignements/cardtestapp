#import <Foundation/Foundation.h>

@interface ZETAPushTokenSyncConfig : NSObject

@property (nonatomic, readonly) NSString *appID;
@property (nonatomic, readonly) NSString *deviceID;
@property (nonatomic, readonly) NSString *sdkAuthID;
@property (nonatomic, readonly) NSDictionary *attrs;
@property (nonatomic, readonly) NSDictionary *config;
@property (nonatomic, readonly) NSDictionary *uaInfo;
@property (nonatomic, readonly) NSNumber *tenantID;
@property (nonatomic, readonly) NSNumber *mercuryTenantID;

- (instancetype)initWithAppID:(NSString *)appID
                     deviceID:(NSString *)deviceID
                     tenantID:(NSNumber *)tenantID
              mercuryTenantID:(NSNumber *)mercuryTenantID
                    sdkAuthID:(NSString *)sdkAuthID
                        attrs:(NSDictionary *)attrs
                       uaInfo:(NSDictionary *)uaInfo
                       config:(NSDictionary *)config;


@end
