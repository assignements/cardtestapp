#import <Foundation/Foundation.h>

@interface ZETAPushNotificationClientStore : NSObject

@property (nonatomic) NSDate *lastSyncTime;
@property (nonatomic) NSString *syncedPushToken;

- (instancetype)initWithAuthID:(NSString *)authID;

- (void)save;

- (void)deleteStore;

@end
