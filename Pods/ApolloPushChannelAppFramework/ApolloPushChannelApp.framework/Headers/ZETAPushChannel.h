#import <Foundation/Foundation.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

static NSString *const ZetaPushIDKey = @"pushID";
typedef BOOL (^ApolloPushMessagePacketQualifierBlockType)(NSString *messageType, NSDictionary *message);
typedef void (^ApolloPushPacketCompletionBlockType)(NSDictionary *message);
@class ZETADoorStreamManager;
@class ZETAApolloUserStore;

@interface ZETAPushChannel : NSObject

- (instancetype)initWithApolloUserStore:(ZETAApolloUserStore *)apolloUserStore
                 sharedPushDataAppGroup:(NSString *)sharedPushDataAppGroup;

- (void)handleNotificationWithInfo:(NSDictionary *)userInfo
                  actionIdentifier:(NSString *)actionIdentifier;

- (void)addMessageListenerWithListenerID:(NSString *)listenerID
                          qualifierBlock:(ApolloPushMessagePacketQualifierBlockType)qualifierBlock
                         completionBlock:(ApolloPushPacketCompletionBlockType)completionBlock;

- (void)removeMessageListenerWithID:(NSString *)listenerID;

- (void)isPushNotificationValidWithInfo:(NSDictionary *)userInfo
                                 source:(NSString *)source
                             completion:(ZETABooleanStatusBlockType)completion;

@end
