#import <Foundation/Foundation.h>

@class ZETAPushNotificationUserInfo;

@interface ZETARichPushNotificationHandler : NSObject

+ (BOOL)isRichPushMessage:(ZETAPushNotificationUserInfo *)messag source:(NSString *)source;
+ (void)handleRichPushMessage:(ZETAPushNotificationUserInfo *)message
             actionIdentifier:(NSString *)actionIdentifier
                       source:(NSString *)source
              isUserInitiated:(BOOL)isUserInitiated;

@end
