#import <Foundation/Foundation.h>
#import <ApolloCoreCrypto/ApolloCoreCryptoHeaders.h>
#import "ZETAPublicKeyCertificate.h"
#import <ApolloCommonsCore/ApolloAuthDataProtocol.h>
#import "ApolloSession.h"

@interface ZETAApolloUserCredentials : NSObject <ApolloAuthDataProtocol>

- (instancetype)initWithIdentifier:(NSString *)identifier;

@property (nonatomic) ZETAPublicKey *publicKey;
@property (nonatomic) NSString *publicKeyCertificate;
@property (nonatomic) BOOL hasCurrentSessionExpired;
@property (nonatomic) ApolloSession *sessionData;

- (void)removeSessionData;

- (void)setData:(NSData *)data key:(NSString *)key;
- (NSData *)dataForKey:(NSString *)key;

- (void)setString:(NSString *)string forKey:(NSString *)key;
- (NSString *)stringForKey:(NSString *)key;

- (void)setCodingCompliantData:(id<NSCoding>)data key:(NSString *)key;
- (id<NSCoding>)codingCompliantDataForKey:(NSString *)key;

- (void)removeDataForKey:(NSString *)key;

- (void)deleteAllItems;

@end
