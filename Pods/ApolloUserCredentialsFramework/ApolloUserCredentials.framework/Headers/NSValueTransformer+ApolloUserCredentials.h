#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString* const ZETAJIDJSONTransformer;
FOUNDATION_EXPORT NSString* const ZETAPublicKeyDataValueTransformerName;

@interface NSValueTransformer (ApolloUserCredentials)

@end
