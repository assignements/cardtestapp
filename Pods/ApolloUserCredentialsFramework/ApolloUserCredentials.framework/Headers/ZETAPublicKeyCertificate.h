#import <Mantle/Mantle.h>
#import <ApolloCoreCrypto/ApolloCoreCryptoHeaders.h>

// Certificate types
FOUNDATION_EXPORT NSString *const ZETACertificateTypePublicKey;

// Purposes
FOUNDATION_EXPORT NSString *const ZETACertificatePurposeDebit;
FOUNDATION_EXPORT NSString *const ZETACertificatePurposeTransact;

@interface ZETAPublicKeyCertificate : ZETASignablePayload

/**
 * Reference : http://phab.corp.zeta.in/w/api/userservice/#add-public-key
 */
- (instancetype)initWithCertID:(NSString *)certID
                          type:(NSString *)type
                     issuerJID:(NSString *)issuerJID
                    subjectJID:(NSString *)subjectJID
                      issuedOn:(NSDate *)issuedOn
                     validTill:(NSDate *)validTill
                     isRevoked:(BOOL)isRevoked
                     publicKey:(NSData *)publicKey
         purposeWithAttributes:(NSDictionary *)purposes
                certAttributes:(NSDictionary *)certAttributes;

- (ZETAPublicKey *)publicKeyFromBase64EncodedPublicKey;

@property (nonatomic, copy, readonly) NSString *certID;
@property (nonatomic, copy, readonly) NSString *type;
@property (nonatomic, copy, readonly) NSString *issuerJID;
@property (nonatomic, copy, readonly) NSDate *issuedOn;
@property (nonatomic, copy, readonly) NSString *subjectJID;
@property (nonatomic, copy, readonly) NSDate *validTill;
@property (nonatomic, readonly) BOOL isRevoked;
@property (nonatomic, copy, readonly) NSData *publicKey;
@property (nonatomic, copy, readonly) NSDictionary *purposeWithAttributes;
@property (nonatomic, copy, readonly) NSDictionary *certificateAttributes;

@end
