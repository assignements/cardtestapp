#import <Mantle/Mantle.h>

@interface ZETAUserSession : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *sessionJID;
@property (nonatomic, readonly) NSString *authToken;
@property (nonatomic, readonly) NSDate *validTill;

- (instancetype)initWithSessionJID:(NSString *)sessionJID
                         authToken:(NSString *)authToken
                         validTill:(NSDate *)validTill;

@end
