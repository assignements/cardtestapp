#import <Foundation/Foundation.h>

/**
 A "JID" is the identifier of an XMPP entity.
 @see http://tools.ietf.org/html/rfc6122
 
 A JID has the following components:
 
 * userID (the "localpart")
 * serviceName (the "domainpart")
 * resourceID (the "resourcepart")
 
 The combination "localpart@domainpart" is called a "bareJID".
 
 For example, the JID string "10033@zetauser.zeta.in/7" has
 the following components:
 * userID: 10033
 * serviceName: zetauser.zeta.in
 * bareJID: 1033@zetauser.zeta.in
 @ resourceID: 7
 */
@interface ZETAJID : NSObject

- (nullable instancetype)initWithString:(nullable NSString *)string;

@property (nonatomic, readonly, nonnull) NSString *string;

- (nonnull NSString *)userID;
- (nonnull NSString *)bareJID;

- (nullable NSString *)resourceID;

/**
 Create a new JID with the receiver's components, but with the
 resource component set to @p resourceID.
 */
- (nullable ZETAJID *)JIDWithResourceID:(nonnull NSString *)resourceID;

@end
