#import <Mantle/Mantle.h>
#import "ZETAUserSession.h"
#import <ApolloCoreCrypto/ApolloCoreCryptoHeaders.h>

NS_ASSUME_NONNULL_BEGIN

@class ZETAECKeyPair;

@interface ApolloSession : ZETAUserSession

@property (nonatomic, readonly, nonnull) NSString *userSessionJid;

/**
 resourceValidity: It tells for how long previously created resource is valid. If
 its still valid, then session can be created using authData. If it has expired, then
 session needs to be created using tenantAuthToken
 **/
@property (nonatomic, readonly, nonnull) NSDate *resourceValidity;
@property (nonatomic, readonly, nonnull) NSString *resourceJid;
@property (nonatomic, readonly, nonnull) ZETAPublicKey *clientPublicKey;
@property (nonatomic, readonly, nonnull) ZETAPublicKey *resourcePublicKey;
@property (nonatomic, readonly, nonnull) NSString *resourcePublicKeyCert;

- (instancetype)initWithUserSessionJid:(NSString *)userSessionJid
                             sessionId:(NSString *)sessionId
                             authToken:(NSString *)authToken
                     authTokenValidity:(NSDate *)authTokenValidity
                           resourceJid:(NSString *)resourceJid
                      resourceValidity:(NSDate *)resourceValidity
                     resourcePublicKey:(ZETAPublicKey *)resourcePublicKey
                       clientPublicKey:(ZETAPublicKey *)clientPublicKey
                 resourcePublicKeyCert:(NSString *)resourcePublicKeyCert;

@end

NS_ASSUME_NONNULL_END
