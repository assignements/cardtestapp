#import <Mantle/Mantle.h>

@interface ZETAThemeConfig: MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSDictionary *colors;
@property (nonatomic, readonly) NSDictionary *fonts;
@property (nonatomic, readonly) NSString *themeID;
@property (nonatomic, readonly) NSString *themeName;

+ (instancetype)defaultConfig;

- (UIColor *)getColor:(NSString *)colorName;

- (UIColor *)getColor:(NSString *)colorName withAlphaDenominator:(NSInteger)alphaDenominator;

- (UIFont *)getFont:(NSString *)fontName;

- (UIColor *)currentThemeColorForDefaultThemeColorHexCode:(NSString *)hexCode
                                     withAlphaDenominator:(NSInteger)alphaDenominator;

- (UIColor *)getColorFromPattern:(NSString *)colorPattern;

@end


