#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString* const ZETAThemeHexColorCodeTransformerName;
FOUNDATION_EXPORT NSString* const ZETAOldHexColorCodeTransformerName;
FOUNDATION_EXPORT NSString* const ZETANewHexColorCodeTransformerName;

@interface NSValueTransformer (ThemeAdditions)

@end

