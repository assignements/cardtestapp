#import <Foundation/Foundation.h>

// Color Constants
FOUNDATION_EXPORT NSString *const ZETAThemeColorBackgroundTop;
FOUNDATION_EXPORT NSString *const ZETAThemeColorBackgroundBottom;
FOUNDATION_EXPORT NSString *const ZETAThemeColorTabbar;
FOUNDATION_EXPORT NSString *const ZETAThemeColorNavigationBar;
FOUNDATION_EXPORT NSString *const ZETAThemeColorPrimaryTop;
FOUNDATION_EXPORT NSString *const ZETAThemeColorPrimaryBottom;
FOUNDATION_EXPORT NSString *const ZETAThemeColorAccent;
FOUNDATION_EXPORT NSString *const ZETAThemeColorSecondary;
FOUNDATION_EXPORT NSString *const ZETAThemeColorPostive;
FOUNDATION_EXPORT NSString *const ZETAThemeColorNegative;
FOUNDATION_EXPORT NSString *const ZETAThemeColorNeutral;
FOUNDATION_EXPORT NSString *const ZETAThemeColorTextDark;
FOUNDATION_EXPORT NSString *const ZETAThemeColorTextLight;
FOUNDATION_EXPORT NSString *const ZETAThemeColorLink;
FOUNDATION_EXPORT NSString *const ZETAThemeColorComplementaryOne;
FOUNDATION_EXPORT NSString *const ZETAThemeColorComplimentaryTwo;
FOUNDATION_EXPORT NSString *const ZETAThemeColorComplimentaryThree;
FOUNDATION_EXPORT NSString *const ZETAThemeColorTextOnPrimary;
FOUNDATION_EXPORT NSString *const ZETAThemeColorTextOnBackground;
FOUNDATION_EXPORT NSString *const ZETAThemeColorTextOnSecondary;
FOUNDATION_EXPORT NSString *const ZETAThemeColorTextOnToolbar;

// Font Constants
FOUNDATION_EXPORT NSString *const ZETAThemeFontNormal;
FOUNDATION_EXPORT NSString *const ZETAThemeFontSemibold;
FOUNDATION_EXPORT NSString *const ZETAThemeFontBold;
FOUNDATION_EXPORT NSInteger const ZETAThemedAlphaDenominator;

