#import "ZETAThemeConfig.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>
#import <ApolloCollectionsCore/ZETAGenericCollectionManager.h>

@protocol ZETAThemeConfigManagerDelegate;

@interface ZETAThemeConfigManager : NSObject

@property (nonatomic, readonly) ZETAThemeConfig *currentThemeConfig;

+ (ZETAThemeConfigManager *)sharedManager;

+ (void)setupWithCollectionsManager:(ZETAGenericCollectionManager *)collectionsManager
            themeConfigCollectionID:(NSString *)themeConfigCollectionID
                     defaultThemeID:(NSString *)defaultThemeID
                           delegate:(id<ZETAThemeConfigManagerDelegate>)delegate;

+ (void)resetSharedManager;

- (BOOL)switchAppThemeAndApplyUpdates;

- (ZETAThemeConfig *)getConfigForThemeID:(NSString *)themeID;

@end

@protocol ZETAThemeConfigManagerDelegate <NSObject>

- (ZETAThemeConfig *)themeConfigManagerDidRequestCurrentThemeConfig:(ZETAThemeConfigManager *)manager;

@end
