#import <UIKit/UIKit.h>

@interface UIColor (ZETAColor)

/**
 * Light Theme Color F89C4D
 */
+ (UIColor *)zeta_PT0;

/**
 * Theme Color - F78727
 */
+ (UIColor *)zeta_PT1;

/**
 * Dark Theme Color - F37A11
 */
+ (UIColor *)zeta_PT2;

/**
 * @description Hex : F9F9F9
 */
+ (UIColor *)zeta_G0;

/**
 * @description Hex : E6E6E6
 */
+ (UIColor *)zeta_G1;

/**
 * @description Hex : A5A5A5
 */
+ (UIColor *)zeta_G2;

/**
 * @description Hex : 5A5A5A
 */
+ (UIColor *)zeta_G3;

/**
 * @description Hex : DB4436
 */
+ (UIColor *)zeta_red;

/**
 * @description Hex : DB5565
 */
+ (UIColor *)zeta_pdf_red;

/**
 * @description Hex : 7ED321
 */
+ (UIColor *)zeta_green;

/**
 * @description Hex : 62B600
 */
+ (UIColor *)zeta_green2;

/*
 * @description Theme Color: positiveColor
 */
+ (UIColor *)zeta_supercardButtonGreen;

/**
 * @description Hex : FFFFFF
 */
+ (UIColor *)zeta_whiteWithOpacity:(CGFloat)opacity;

/**
 * @description Hex : 222222
 */
+ (UIColor *)zeta_G4;

/**
 * @description Hex : 4A90E2
 */
+ (UIColor *)zeta_blue;

/**
 * @description Hex : F1F1F1
 */
+ (UIColor *)zeta_G5;

/**
 * @description Hex : F0F0F0
 */
+ (UIColor *)zeta_intentGreyColor;

/**
 * @description Hex : 835C1E
 */
+ (UIColor *)zeta_labelBrown;

/**
 * @description Hex : D8D8D8
 */
+ (UIColor *)zeta_G6;

/**
 * @description Hex : 4A4A4A
 */
+ (UIColor *)zeta_G7;

/**
 * @description Hex : F4F4F4
 */
+ (UIColor *)zeta_G8;

/**
 * @description Hex : 32264C
 */
+ (UIColor *)zeta_purple1;

/**
 * @description Hex : 503B7F
 */
+ (UIColor *)zeta_purple4;

/**
 @description Hex : 3B5998
 */
+ (UIColor *)zeta_facebookBlue;

/**
 * @description Hex : 6A44AC
 */
+ (UIColor *)zeta_purple5;

/**
 * @description Hex : 484058
 */
+ (UIColor *)zeta_purple6;

/**
 * @description Hex : 2B2042
 */
+ (UIColor *)zeta_purple7;

/**
 * @description Hex : 625579
 */
+ (UIColor *)zeta_purple8;

/**
 * @description Hex : 3C334D
 */
+ (UIColor *)zeta_purple9;

/**
 * @description Hex : 895EC9
 */
+ (UIColor *)zeta_purple10;

/**
 * @description Hex : 6336A5
 */
+ (UIColor *)zeta_purple11;

/*
 * @description Theme Color: accentColor
 */
+ (UIColor *)zeta_purple12;

/**
 * @description Hex : 170d24
 */
+ (UIColor *)zeta_purple13;

/**
 * @description Hex : 141414
 */
+ (UIColor *)zeta_G9;

/**
 * @description Hex : FAFAFA
 */
+ (UIColor *)zeta_G10;

/**
 * @description Hex : ECECEC
 */
+ (UIColor *)zeta_G11;

/**
 * @description Hex : 838188
 */
+ (UIColor *)zeta_G12;

/**
 * @description Hex : E5E5E5
 */
+ (UIColor *)zeta_G13;

/**
 * @description Hex : 070707
 *              Alpha : 0.4
 */
+ (UIColor *)zeta_G14;

/**
 * @description Hex : 070707
 */
+ (UIColor *)zeta_G15;

/**
 * @description Hex : F5F5F5
 */
+ (UIColor *)zeta_G16;

/**
 * @description Hex : 979797
 */
+ (UIColor *)zeta_G17;

/**
 * @description Hex : 4F4F4F
 */
+ (UIColor *)zeta_G18;

/**
 * @description Hex : F2F2F2
 */
+ (UIColor *)zeta_G19;

/**
 * @description Hex : 000000
 */
+ (UIColor *)zeta_blackWithOpacity:(CGFloat)opacity;

/*
 * @description Theme Color: secondaryColor
 */
+ (UIColor *)zeta_yellow;

/**
 * @description Hex : e15f53
 */
+ (UIColor *)zeta_red1;

/**
 * @description Hex : f79037
 */
+ (UIColor *)zeta_warningOrange;

#pragma mark V3 Colors

/*
 * @description Theme Color: primaryTop
 */
+ (UIColor *)zeta_P1;

/*
 * @description Theme Color: primaryBottom
 */
+ (UIColor *)zeta_P2;

/**
 * @description Hex : 3e2767
 */
+ (UIColor *)zeta_P3;

/**
 * @description Hex : d4bcff
 */
+ (UIColor *)zeta_P4;

/**
 * @description Hex : 9278c0
 */
+ (UIColor *)zeta_sectionHeaderColor;

/*
 * @description Hex : f3f1f1
 */
+ (UIColor *)zeta_G20;

/*
 * @description Hex : 9b9b9b
 */
+ (UIColor *)zeta_G21;

/*
 * @description Hex : b3b3b3
 */
+ (UIColor *)zeta_tableSeperatorColor;

/*
 * @description Hex : b19ed2
 */
+ (UIColor *)zeta_purpleText;

/*
 * @description Theme Color: secondaryColor
 */
+ (UIColor *)zeta_KYCYellow;

+ (UIColor *)zeta_referralYellow;

+ (UIColor *)zeta_potOrange;

/*
 * @description Hex : fdd442
 */
+ (UIColor *)zeta_lightGold;

/*
 * @description Hex : 722c49
 */
+ (UIColor *)zeta_grape;

/*
 * @description Hex : e94d4c
 */
+ (UIColor *)zeta_pastel_red;

/*
 * @description Hex : f3f2f2
 */
+ (UIColor *)zeta_gray_bg;

/*
 * @description Hex : 5e3e96
 */
+ (UIColor *)zeta_supercardCodePurple;

/*
 * @description Hex : 603191
 */
+ (UIColor *)zeta_supercardSettingsPurple;

/*
 * @description Hex : ded7ea
 */
+ (UIColor *)zeta_paleGray;

/*
 * @description Hex : 353535
 */
+ (UIColor *)zeta_black1;

/*
 * @description Theme Color: textColorDark
 */
+ (UIColor *)zeta_textDarkWithOpacity:(CGFloat)opacity;

/*
 * @description Theme Color: textColorLight
 */
+ (UIColor *)zeta_textLightWithOpacity:(CGFloat)opacity;

/*
 * @description Theme Color: backgroundTop
 */
+ (UIColor *)zeta_backgroundTop;

/*
 * @description Theme Color: backgroundBottom
 */
+ (UIColor *)zeta_backgroundBottom;

/*
 * @description Theme Color: bottomToolbarColor
 */
+ (UIColor *)zeta_tabbar;

/*
 * @description Theme Color: toolbarColor
 */
+ (UIColor *)zeta_navigationBar;

/*
 * @description Theme Color: textColorOnPrimary
 */
+ (UIColor *)zeta_textOnPrimary;

/*
 * @description Theme Color: textColorOnSecondary
 */
+ (UIColor *)zeta_textOnSecondary;

/*
 * @description Theme Color: textColorOnToolbar
 */
+ (UIColor *)zeta_textOnToolbar;

/*
 * @description Theme Color: textColorOnBackground
 */
+ (UIColor *)zeta_textOnBackgroundWithOpacity:(CGFloat)opacity;

/*
 * @description Theme Color: Numeric Keypad bg
 */
+ (UIColor *)zeta_numPad;
@end
