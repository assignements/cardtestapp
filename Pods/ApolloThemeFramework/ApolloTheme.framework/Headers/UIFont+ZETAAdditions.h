#import <UIKit/UIKit.h>

@interface UIFont (ZETAAdditions)

+ (UIFont *)zeta_defaultFont;

+ (CGFloat)zeta_defaultFontSize;

+ (UIFont *)zeta_defaultBoldFont;

+ (UIFont *)zeta_defaultItalicFont;

+ (UIFont *)zeta_semiBoldItalicFont;

+ (UIFont *)zeta_boldItalicFont;

+ (UIFont *)zeta_fontWithDefaultSizeAndName:(NSString *)fontName;

@end

