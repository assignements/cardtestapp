#import <UIKit/UIKit.h>

#define TDTAppBackgroundColor 0xf0f0f0
#define TDTAppBackgroundAlpha 1.0

@interface UIColor (TDTAdditions)

+ (UIColor *)tdt_colorWithIntegerValue:(NSUInteger)value alpha:(CGFloat)alpha;

+ (UIImage *)tdt_imageWithColor:(UIColor *)color;

+ (NSUInteger)integerFromHexString:(NSString *)hexString;

+ (NSString *)hexStringFromColor:(UIColor *)color;

+ (UIColor *)colorFromHexString:(NSString *)hexString
           withAlphaDenominator:(NSInteger)alphaDenominator;

+ (NSString *)hexStringByRemovingAlphaComponent:(NSString *)hexString;

+ (NSNumber *)alphaComponentFromHexString:(NSString *)hexString;

+ (NSString *)stripHexStringWithHash:(NSString *)hexString;

@end
