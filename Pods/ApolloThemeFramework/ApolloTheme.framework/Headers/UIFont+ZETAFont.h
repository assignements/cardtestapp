#import <UIKit/UIKit.h>

@interface UIFont (ZETAFont)

// New fonts

/**
 * @description Size : 12
 */
+ (UIFont *)zeta_H0;

/**
 * @description Size : 12
 */
+ (UIFont *)zeta_H0Semibold;

/**
 * @description Size : 12
 */
+ (UIFont *)zeta_H0Bold;

/**
 * @description Size : 12
 */
+ (UIFont *)zeta_H0Ocra;

/**
 * @description Size : 12
 */
+ (UIFont *)zeta_H0IBMPlexSans;

/**
 * @description Size : 14
 */
+ (UIFont *)zeta_H1;

/**
 * @description Size : 14
 */
+ (UIFont *)zeta_H1Semibold;

/**
 * @description Size : 14
 */
+ (UIFont *)zeta_H1Bold;

/**
 * @description Size : 14
 */
+ (UIFont *)zeta_H1Ocra;

/**
 * @description Size : 14
 */
+ (UIFont *)zeta_H1IBMPlexSans;

/**
 * @description Size : 14
 */
+ (UIFont *)zeta_H1IBMPlexSansMedium;

/**
 * @description Size : 14
 */
+ (UIFont *)zeta_H1IBMPlexSansBold;

/**
 * @description Size : 16
 */
+ (UIFont *)zeta_H2;

/**
 * @description Size : 16
 */
+ (UIFont *)zeta_H2Semibold;

/**
 * @description Size : 16
 */
+ (UIFont *)zeta_H2Bold;

/**
 * @description Size : 16
 */
+ (UIFont *)zeta_H2Ocra;

/**
 * @description Size : 16
 */
+ (UIFont *)zeta_H2IBMPlexSans;

/**
 * @description Size : 16
 */
+ (UIFont *)zeta_H2IBMPlexSansMedium;

/**
 * @description Size : 16
 */
+ (UIFont *)zeta_H2IBMPlexSansBold;

/**
 * @description Size : 18
 */
+ (UIFont *)zeta_H2_5;

/**
 * @description Size : 18
 */
+ (UIFont *)zeta_H2_5IBMPlexSans;

/**
 * @description Size : 18
 */
+ (UIFont *)zeta_H2_5IBMPlexSansMedium;

/**
 * @description Size : 18
 */
+ (UIFont *)zeta_H2_5IBMPlexSansBold;

/**
 * @description Size : 18
 */
+ (UIFont *)zeta_H2_5SemiBold;

/**
 * @description Size : 20
 */
+ (UIFont *)zeta_H3;

/**
 * @description Size : 20
 */
+ (UIFont *)zeta_H3Semibold;

/**
 * @description Size : 20
 */
+ (UIFont *)zeta_H3Bold;

/**
 * @description Size : 20
 */
+ (UIFont *)zeta_H3Ocra;

/**
 * @description Size : 20
 */
+ (UIFont *)zeta_H3IBMPlexSansBold;

/**
 * @description Size : 24
 */
+ (UIFont *)zeta_H4;

/**
 * @description Size : 24
 */
+ (UIFont *)zeta_H4Semibold;

/**
 * @description Size : 24
 */
+ (UIFont *)zeta_H4Bold;

/**
 * @description Size : 24
 */
+ (UIFont *)zeta_H4Ocra;

/**
 * @description Size : 24
 */
+ (UIFont *)zeta_H4IBMPlexSans;

/**
 * @description Size : 24
 */
+ (UIFont *)zeta_H4BanglaBold;

/**
 * @description Size : 28
 */
+ (UIFont *)zeta_H5;

/**
 * @description Size : 28
 */
+ (UIFont *)zeta_H5Semibold;

/**
 * @description Size : 28
 */
+ (UIFont *)zeta_H5Bold;

/**
 * @description Size : 30
 */
+ (UIFont *)zeta_H6;

/**
 * @description Size : 30
 */
+ (UIFont *)zeta_H6Semibold;

/**
 * @description Size : 30
 */
+ (UIFont *)zeta_H6Bold;

/**
 * @description Size : 36
 */
+ (UIFont *)zeta_H7Semibold;

/**
 * @description Size : 36
 */
+ (UIFont *)zeta_H7Bold;

/**
 * @description Size : 40
 */
+ (UIFont *)zeta_H8;

/**
 * @description Size : 40
 */
+ (UIFont *)zeta_H8Semibold;

/**
 * @description Size : 40
 */
+ (UIFont *)zeta_H8Bold;

/**
 * @description Size : 40
 */
+ (UIFont *)zeta_H8Brandon;

/**
 * @description Size : 46
 */
+ (UIFont *)zeta_H9_5IBMPlexSansBold;

/**
 * @description Size : 48
 */
+ (UIFont *)zeta_H10;

/**
 * @description Size : 48
 */
+ (UIFont *)zeta_H10Semibold;

/**
 * @description Size : 48
 */
+ (UIFont *)zeta_H10Brandon;

/**
 * @description Size : 48
 */
+ (UIFont *)zeta_H10Bold;

/**
 * @description Size : 10
 */
// FIXME: Needs to be confirmed with vd if this is supposed to be a standard font
+ (UIFont *)zeta_badgeFont;

/**
 * @description Size : 10
 */
+ (UIFont *)zeta_badgeFontSemibold;

/**
 * @description Size : 10, Bold
 */
+ (UIFont *)zeta_PN8Bold;

/**
 * @description Size : 11, Bold
 */
+ (UIFont *)zeta_banglaSangamBoldSmall;

/**
 * @description Size : 24, Bold
 */
+ (UIFont *)zeta_banglaSangamBoldLarge;

/**
 * @description Size : 28, Bold
 */
+ (UIFont *)zeta_H5BrandonBold;

/**
 * @description Size : 50, Bold
 */
+ (UIFont *)zeta_H11BrandonBold;

/*
 * @description Size : 18 Ocra Std
 */
+ (UIFont *)zeta_H18OcraStd;

/**
 * @description Size : 40, System Std
 */
+ (UIFont *)zeta_NumericKeypadFont;
@end
