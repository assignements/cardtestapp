#import <Foundation/Foundation.h>

@protocol ZETAKVStore <NSObject>

- (instancetype _Nonnull)initWithStoreName:(NSString *_Nonnull)storeName;

- (void)setCodingCompliantData:(id<NSCoding>_Nonnull)data forKey:(NSString *_Nonnull)key;

- (void)setValue:(NSObject *_Nonnull)value forKey:(NSString *_Nonnull)key;

- (id<NSCoding>_Nullable)codingCompliantDataForKey:(NSString *_Nonnull)key;

- (id _Nonnull)getValueForKey:(NSString *_Nonnull)key;

- (void)save;

- (void)removeKey:(NSString *_Nonnull)key;

- (BOOL)deleteStore;

@end
