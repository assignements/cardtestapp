#import <Foundation/Foundation.h>
#import "ZETADefaultKVStore.h"

@interface ZETAMigrationUtility : NSObject

- (instancetype)initWithUserDefaults:(NSUserDefaults *)userDefaults
                               store:(ZETADefaultKVStore *)KVStore;

- (void)migrateKeys:(NSArray *)keys parentKey:(NSString *)parentKey;

@end
