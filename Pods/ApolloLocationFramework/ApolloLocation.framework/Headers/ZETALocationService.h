#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

FOUNDATION_EXPORT CLLocationDistance const ZETAMinimumDistanceAccuracy;

FOUNDATION_EXPORT NSString *const ZETAAppLocationPermissionStatusDidChange;
FOUNDATION_EXPORT NSString *const ZETAAppDidReceiveLocationUpdate;
FOUNDATION_EXPORT NSString *const ZETALocationValueKey;
FOUNDATION_EXPORT NSString *const ZETALocationServiceErrorDomain;

typedef NS_ENUM(NSUInteger, ZETAAppLocationPermissionStatus) {
  ZETAAppLocationPermissionAuthorized,
  ZETAAppLocationPermissionDenied,
  ZETAAppLocationPermissionNotDetermined
};

typedef NS_ENUM(NSUInteger, ZETALocationServiceErrorCode) {
  ZETALocationServiceErrorCodeUnknown,
  ZETALocationServiceErrorCodeLocationPermissionDenied,
  ZETALocationServiceErrorCodeLocationDisabled
};

@interface ZETALocationService : NSObject

+ (instancetype)sharedLocationService;
+ (ZETAAppLocationPermissionStatus)appLocationPermissionStatus;
+ (BOOL)isGlobalLocationSettingOn;
+ (BOOL)isLocationAccessible;
+ (BOOL)isLocationPermissionDenied;
+ (NSError *)locationUnavailableError;
- (void)startMonitoringLocation;
- (void)stopMonitoringLocation;
- (CLLocation *)lastKnownLocation;

@end
