#import <Foundation/Foundation.h>

@interface ZETACellDerivationUtil : NSObject

+ (double)convertLatitudeToDistance:(double)latitude;
+ (double)convertLongitudeToDistance:(double)longitude;
+ (double)convertDistanceToLatitude:(double)distanceX;
+ (double)convertDistanceToLongitude:(double)distanceY;

@end
