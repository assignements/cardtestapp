#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>
#import "ZETALocationCell.h"

/**
 * Grid contains various cells with various cell IDs and a grid is uniquely identified with a grid
 * ID.
 * originX : X of bottom left corner position of the grid
 * originY : Y of bottom left corner position of the grid
 * endX : X of Top right corner position of the grid
 * endY : Y of top right corner position of the grid
 * hexSideLength : hex length for every cell in the grid
 */

@interface ZETALocationGrid : NSObject

- (instancetype)initWithID:(int)locationGridID
                   originX:(double)originX
                   originY:(double)originY
                      endX:(double)endX
                      endY:(double)endY
             hexSideLength:(double)hexSideLength
                 stateCode:(NSString *)stateCode;

@property (nonatomic, readonly) int gridID;
@property (nonatomic, readonly) NSString *stateCode;

- (BOOL)locationLiesInsideWithLatitude:(double)latitude longitude:(double)longitude;

- (ZETALocationCell *)cellForLatitude:(double)latitude longitude:(double)longitude;

@end
