#import <Foundation/Foundation.h>

/**
 * A cell is uniquely determined by a triple of (gridId, cellX, cellY).
 * We need to create a 1:1 function to map this triple to an integer cellID that can be passed
 * around.
 *
 * The least significant 10 bits (1024) of cellID will be used to store cellY.
 * The next 10 bits will store cellX and the most significant 12 bits (4096) store the gridID.
 */

@interface ZETALocationCell : NSObject

- (instancetype)initWithCellX:(int)cellX cellY:(int)cellY gridID:(int)gridID;

@property (nonatomic, readonly) int gridID;

- (int)cellID;
- (int)homingNumber;
- (BOOL)isEqualToLocationCell:(ZETALocationCell *)locationCell;
- (NSArray *)cellHive;
- (NSArray *)cellHiveIDs;

@end
