#import <CoreLocation/CoreLocation.h>
#import "ZETALocationBundle.h"

@interface ZETALocationBundle (CLLocation)

+ (instancetype)zeta_locationBundleFromLocation:(CLLocation *)location;

@end
