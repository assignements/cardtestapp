#import <Foundation/Foundation.h>
#import "ZETALine.h"
#import "ZETALocationCell.h"

/**
 * Bounding box is a rectangle made up from 3 different cells, cutting the lower triangle of the
 * hexagon of the
 * base cell and including the top left triangle from the top left cell and the top right triangle
 * from the top right cell, creates a bounding box to simplify the calculations.
 */

@interface ZETABoundingBox : NSObject

- (instancetype)initWithBoxXNumber:(int)boxXNumber
                        boxYNumber:(int)boxYNumber
                          leftLine:(ZETALine *)leftLine
                         rightLine:(ZETALine *)rightLine
                           originX:(double)originX
                           originY:(double)originY
                            height:(double)height
                             width:(double)width
                            gridID:(int)gridID;

- (ZETALocationCell *)cellFromLatitude:(double)latitude longitude:(double)longitude;

@end
