#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

@interface ZETALocationUtils : NSObject

+ (double)secondsFromLocationInDegrees:(double)degrees;

+ (int32_t)distanceFrom:(CLLocation *)fromLocation
             toLatitude:(NSNumber *)latitude
            toLongitude:(NSNumber *)longitude;

@end
