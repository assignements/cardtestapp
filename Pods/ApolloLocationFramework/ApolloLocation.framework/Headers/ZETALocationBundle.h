#import <Mantle/Mantle.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

@interface ZETALocationBundle : ZETAModelWithoutNil<MTLJSONSerializing>

- (instancetype)initWithLatitude:(NSNumber *)latitude
                       longitude:(NSNumber *)longitude
              horizontalAccuracy:(NSNumber *)horizontalAccuracy
                verticalAccuracy:(NSNumber *)verticalAccuracy
                           floor:(NSNumber *)floor
                           speed:(NSNumber *)speed
                         bearing:(NSNumber *)bearing
                        altitude:(NSNumber *)altitude
                            time:(NSDate *)time;

@property (nonatomic, readonly) NSNumber *latitude;
@property (nonatomic, readonly) NSNumber *longitude;
@property (nonatomic, readonly) NSNumber *horizontalAccuracy;
@property (nonatomic, readonly) NSNumber *verticalAccuracy;
@property (nonatomic, readonly) NSNumber *floor;
@property (nonatomic, readonly) NSNumber *speed;
@property (nonatomic, readonly) NSNumber *bearing;
@property (nonatomic, readonly) NSNumber *altitude;
@property (nonatomic, readonly) NSDate *time;

@end
