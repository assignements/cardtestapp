#import <Foundation/Foundation.h>

@interface ZETALine : NSObject

- (instancetype)initWithSlope:(double)slope yIntercept:(double)yIntercept;
- (BOOL)pointIsAboveLineWithX:(double)x y:(double)y;

@end
