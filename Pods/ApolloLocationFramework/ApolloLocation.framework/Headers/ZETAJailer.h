#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>
#import "ZETALocationCell.h"

@interface ZETAJailer : NSObject

- (ZETALocationCell *)cellForLatitude:(double)latitude longitude:(double)longitude;
- (ZETALocationCell *)cellForLocation:(CLLocation *)location;

// TODO: Can be moved to `ZETALocationCell`
- (NSString *)cityNameForCell:(ZETALocationCell *)cell;
- (NSString *)stateCodeForCell:(ZETALocationCell *)cell;

- (instancetype)initWithDefaultGridParamsBottomLeftX:(double)bottomLeftX
                                         bottomLeftY:(double)bottomLeftY
                                           topRightX:(double)topRightX
                                           topRightY:(double)topRightY
                                       hexSideLength:(double)hexSideLength
                                  denseHexSideLength:(double)denseHexSideLength
                                         startGridID:(int)startGridID;

@end
