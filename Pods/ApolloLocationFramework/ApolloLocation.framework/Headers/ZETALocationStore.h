#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface ZETALocationStore : NSObject

@property (atomic, nullable) CLLocation *lastKnownLocation;

- (void)save;
- (instancetype) initWithUserDefaults:(nonnull NSUserDefaults *)userDefaults;

@end
