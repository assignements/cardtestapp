#import <Mantle/Mantle.h>
#import <CoreLocation/CLLocation.h>

@interface ZETABasicLocationPayload : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSNumber *latitude;
@property (nonatomic, readonly) NSNumber *longitude;
@property (nonatomic, readonly) NSString *stateCode;

- (instancetype)initWithLatitude:(NSNumber *)latitude
                       longitude:(NSNumber *)longitude
                       stateCode:(NSString *)stateCode;
- (instancetype)initWithCLLocation:(CLLocation *)location stateCode:(NSString *)stateCode;

@end
