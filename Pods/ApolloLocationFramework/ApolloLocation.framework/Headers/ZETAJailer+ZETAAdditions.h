#import "ZETAJailer.h"

@interface ZETAJailer (ZETAAdditions)

- (NSString *)cityNameFromLocation:(CLLocation *)location;
- (NSString *)stateCodeFromLocation:(CLLocation *)location;

@end
