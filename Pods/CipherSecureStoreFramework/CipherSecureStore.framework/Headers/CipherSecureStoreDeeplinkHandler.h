#import <Foundation/Foundation.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import "CipherSecureStoreConfiguration.h"

NS_ASSUME_NONNULL_BEGIN

@protocol CipherSecureStoreDeeplinkHandlerDelegate;
@interface CipherSecureStoreDeeplinkHandler : NSObject

- (instancetype)initWithConfiguration:(CipherSecureStoreConfiguration *)configuration
                             delegate:(id<CipherSecureStoreDeeplinkHandlerDelegate>)delegate;
@end

@protocol CipherSecureStoreDeeplinkHandlerDelegate <NSObject>

-(UIView *)didRequestPinMigrationViewForHandler:(CipherSecureStoreDeeplinkHandler *)deeplinkHandler;

- (void)setData:(NSData *)data
         forKey:(NSString *)key
     completion:(void (^)(NSError *error))completion;

- (void)dataForKey:(NSString *)key
        completion:(void (^)(NSError *error, NSData *data))completion;

- (void)forceAuthenticateWithClearSession:(BOOL)clearSession
                               completion:(ZETAFailureBlockType)completion;

@end


NS_ASSUME_NONNULL_END
