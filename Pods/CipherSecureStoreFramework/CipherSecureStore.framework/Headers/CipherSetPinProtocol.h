
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CipherSetPinProtocol <NSObject>
- (UIViewController *)cipherDidRequestSetPinViewController;
@end

NS_ASSUME_NONNULL_END
