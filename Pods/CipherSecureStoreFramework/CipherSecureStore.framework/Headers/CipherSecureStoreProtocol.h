#import <Foundation/Foundation.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import "CipherEnterPinProtocol.h"
#import "CipherSetPinProtocol.h"

typedef NS_ENUM(NSUInteger, CipherSecureStoreType) {
  CipherSecureStoreTypeUnknown,
  CipherSecureStoreTypePIN,
  CipherSecureStoreTypeDeviceLock,
  CipherSecureStoreTypeNone
};

static NSString * _Nonnull  const CipherSecureStoreSetupSuccessNotification = @"CIPHER_SECURE_STORE_SETUP_SUCCESS";
static NSString * _Nonnull const CipherSecureStoreSetupFailureNotification = @"CIPHER_SECURE_STORE_SETUP_FAILURE";

NS_ASSUME_NONNULL_BEGIN

@protocol CipherSecureStore<NSObject>

- (void)setUpSecuredStoreWithCompletion:(ZETAFailureBlockType) completion;

- (void)forceAuthenticateWithCompletion:(ZETAFailureBlockType)completion;

- (void)forceAuthenticateWithClearSession:(BOOL)clearSession
                               completion:(ZETAFailureBlockType)completion;

- (BOOL)isStoreSetUp;

- (BOOL)doesRequireUnlock;

- (CipherSecureStoreType)getCurrentStoreType;

- (BOOL)containsDataForKey:(NSString *)key;

- (void)dataForKey:(NSString *)key
        userPrompt:(BOOL)promptUser
        completion:(void (^)(NSError *error, NSData *data))completion;

- (void)dataForKey:(NSString *)key
        completion:(void (^)(NSError *error, NSData *data))completion;

- (void)setData:(NSData *)data forKey:(NSString *)key completion:(ZETAFailureBlockType)completion;

- (void)setUnAuthenticatedData:(NSData *)data forKey:(NSString *)key;

- (NSData *)getUnAuthenticatedDataForKey:(NSString *)key;

- (void)resetAllData;

- (void)invalidateSession;

- (void)migrateStoreFromTouchIDToPinWithCompletion:(ZETAFailureBlockType)completion;

- (void)migrateStoreFromPinToTouchIDWithCompletion:(ZETAFailureBlockType)completion;

- (void)createFallbackToPinWithCompletion:(ZETAFailureBlockType)completion;

- (void)setSetPinDelegate:(id<CipherSetPinProtocol>)setPinDelegate
      andSetEnterPinDelegate:(id<CipherEnterPinProtocol>)enterPinDelegate;

- (void)unlockSecureStoreWithPin:(NSString *)pin;

- (void)setSecureStorePin:(NSString *)pin;

- (void)forgetPin;

- (NSUInteger)maxNumberOfLoginAttempts;

- (NSUInteger)remainingNumberOfLoginAttempts;


@end

NS_ASSUME_NONNULL_END
