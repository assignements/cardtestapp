#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CipherSecureStoreConfiguration : NSObject

+(instancetype)sharedInstance;

-(void)syncConfigurationFromFile:(NSString *)fileName;
-(NSString *)themeID;
-(NSString *)themeCollectionID;
-(NSString *)scheme;
-(NSNumber *)appInForegroundAuthCacheDuration;
-(NSNumber *)appInBackgroundAuthCacheDuration;
-(NSTimeInterval)secureStoreCacheDuration;
-(NSNumber *)maxLoginAttempts;
@end

NS_ASSUME_NONNULL_END
