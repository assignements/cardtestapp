#import <Foundation/Foundation.h>
#import "ZETAEncryptionKeyManager.h"

@interface ZETAEncryptionKeyManagerHelper : NSObject <ZETAEncryptionKeyManagerProtocol>

- (instancetype)initWithApolloUserCredentials:(ZETAApolloUserCredentials *)apolloUserCredentials
                        templateCollectionIDs:(NSArray *)templateCollectionIDs;

- (void)shouldShowAlerts:(BOOL)showAlerts;

- (NSUInteger)maxNumberOfLoginAttempts:(ZETAEncryptionKeyManager *)manager;

- (NSUInteger)remainingNumberOfLoginAttempts:(ZETAEncryptionKeyManager *)manager;


@end
