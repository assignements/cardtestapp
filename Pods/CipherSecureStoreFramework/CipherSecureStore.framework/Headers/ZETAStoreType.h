#ifndef ZETAStoreType_h
#define ZETAStoreType_h

typedef NS_ENUM(NSUInteger, ZETAStoreType) {
  ZETAStoreTypeUnknown,
  ZETAStoreTypePIN,
  ZETAStoreTypeTouchID,
  ZETAStoreTypeNone
};

#endif /* ZETAStoreType_h */
