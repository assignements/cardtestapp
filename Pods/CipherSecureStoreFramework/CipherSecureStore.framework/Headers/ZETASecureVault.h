#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ZETASecureVaultError) {
  ZETASecureVaultErrorUnknown,
  ZETASecureVaultErrorUserCancel,
  ZETASecureVaultErrorItemsDeleted,
  ZETASecureVaultErrorPasscodeDisabled
};

FOUNDATION_EXPORT NSString *const ZETASecureVaultErrorDomain;

@protocol ZETASecureVaultProtocol;

@interface ZETASecureVault : NSObject

/**
 Instantiates a SecureVault with "User authentication required" access control.
 "User authentication required" requires user to authenticate using his touchID/passcode before
 the values can be read from keychain.
 @note It returns nil if `canCreateSecureEnclave` returns NO.
 */
+ (void)setupSharedSecureVaultWithIdentifier:(NSString *)identifier
                                    delegate:(id<ZETASecureVaultProtocol>)delegate;

+ (instancetype)sharedSecureVault;

/**
 Returns status on whether secure enclave store can be create on device.
 @note Returns NO if :
 - The device doesn't have secure enclave
 - User has neither enabled touchID nor set a passcode.
 - If the device iOS version doesn't support secure enclave.
 */
+ (BOOL)canCreateSecureEnclave;

- (BOOL)canAccessKeyChain;

- (NSString *)version;

- (BOOL)isValueStoredForKey:(NSString *)key;

- (void)setData:(NSData *)data key:(NSString *)key;

- (NSData *)dataForKey:(NSString *)key error:(NSError **)error;

- (void)removeDataForKey:(NSString *)key;

@end

@protocol ZETASecureVaultProtocol <NSObject>

- (BOOL)secureVaultShouldUseSimplifiedKeychainWrapper:(ZETASecureVault *)vault;

@end
