#import <Foundation/Foundation.h>

@protocol ZETAKeychainAcessor<NSObject>

- (nullable NSData *)objectForKey:(nonnull NSString *)key
                       userPrompt:(nonnull NSString *)userPrompt
                    userCancelled:(nullable inout BOOL *)userCancelled;

- (BOOL)setObject:(nonnull NSData *)data forKey:(nonnull NSString *)key;

- (BOOL)removeObjectForKey:(nonnull NSString *)key;

@end

@interface ZETAKeychainWrapper : NSObject<ZETAKeychainAcessor>

- (nullable instancetype)initWithIdentifier:(nonnull NSString *)identifier;

@end
