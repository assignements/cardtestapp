#import <Foundation/Foundation.h>
#import "CipherSecureStoreProtocol.h"

NS_ASSUME_NONNULL_BEGIN


@class ZETAApolloUserCredentials, CipherSecureStoreService;
@protocol CipherSecureStoreServiceDelegate <NSObject>

- (void)didResetDataForSecureStore:(CipherSecureStoreService *)securestore;

- (BOOL)shouldEnableDeviceLockForSecureStore:(CipherSecureStoreService *)securestore;

@optional
- (ZETAApolloUserCredentials *)didReceiveUserCredentialForSecureStore:(CipherSecureStoreService *)securestore;

- (void)secureStore:(CipherSecureStoreService *)securestore
didChangeStoreTypeFrom:(CipherSecureStoreType)oldStoreType
        toStoreType:(CipherSecureStoreType)newStoreType;

@end

@interface CipherSecureStoreService : NSObject<CipherSecureStore>

+ (id<CipherSecureStore>)createStoreWithIdentifier:(NSString *)identifier
                  prioritizedTemplateCollectionIDs:(nullable NSArray *)prioritizedTemplateCollectionIDs
                                           appName:(NSString *)appName
                                         debugMode:(BOOL)debugMode
                                    configFileName:(nullable NSString *)configFileName
                                          delegate:(nullable id<CipherSecureStoreServiceDelegate>) delegate;
@end

NS_ASSUME_NONNULL_END
