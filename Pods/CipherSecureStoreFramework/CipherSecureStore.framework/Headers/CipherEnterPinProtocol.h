
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CipherEnterPinProtocol <NSObject>
- (UIViewController *)cipherDidRequestEnterPinViewController;
- (void)pinDidMismatchWithRemainingNumberOfTries:(NSUInteger)remainingNumberOfTries;
@end

NS_ASSUME_NONNULL_END
