#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CSSMigrationPinHandlerDelegate;
@interface CSSMigrationPinHandler : NSObject

-(instancetype)initWithDelegate:(id<CSSMigrationPinHandlerDelegate>)delegate;
-(void)configureSecureStoreState:(BOOL)isDeviceLock;
-(UIView *)createAndGetMigrationPinView;
@end

@protocol CSSMigrationPinHandlerDelegate <NSObject>

-(void)migrationPinHandler:(CSSMigrationPinHandler *)pinHandler didMigrateDeviceLock:(BOOL)isDeviceLock;
-(BOOL)didRequestSecureStoreStateForMigrationPinHandler:(CSSMigrationPinHandler *)pinHandler;

@end

NS_ASSUME_NONNULL_END
