#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <ApolloDeeplinkResolver/ApolloDeeplinkResolverHeaders.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CSSMigrationPinViewDelegate;
@interface CSSMigrationPinView : ZETANativeView

-(instancetype)initWithDelegate:(id<CSSMigrationPinViewDelegate>) delegate;
-(void)configureViewWithImage:(NSString *)imageName
                        title:(NSString *)title
                     subtitle:(NSString *)subtitle
                  switchState:(BOOL)isOn;
-(void)configureSwitchState:(BOOL)isOn;
@end

@protocol CSSMigrationPinViewDelegate <NSObject>

-(void)pinView:(CSSMigrationPinView *)pinView didToggleSwitch:(BOOL)isOn;

@end


NS_ASSUME_NONNULL_END
