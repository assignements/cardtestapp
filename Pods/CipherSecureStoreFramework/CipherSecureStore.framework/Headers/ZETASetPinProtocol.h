#ifndef ZETASetPinProtocol_h
#define ZETASetPinProtocol_h

typedef void (^SetPinSuccessBlockType)(NSString *pin);
typedef void (^SetPinFailureBlockType)(NSError *error);

@protocol ZETASetPinProtocol <NSObject>

- (void)setPinWithSuccessBlock:(SetPinSuccessBlockType)success
                  failureBlock:(SetPinFailureBlockType)failure;

@end

#endif /* ZETASetPinProtocol_h */
