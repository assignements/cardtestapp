
#import "AuthenticationBots.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppInBackgroundAuthenticationBot : NSObject <AuthenticationBots>
- (instancetype)initWithDelegate:(id<AuthenticationBotDelegate>)delegate
                        duration:(NSTimeInterval)duration;
- (instancetype)initWithDelegate:(id<AuthenticationBotDelegate>)delegate
              authenticationMode:(AuthenticationModeType)mode
                        duration:(NSTimeInterval)duration;
@end

NS_ASSUME_NONNULL_END
