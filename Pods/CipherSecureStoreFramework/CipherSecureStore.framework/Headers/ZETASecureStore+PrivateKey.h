#import "ZETASecureStore.h"
#import <ApolloCoreCrypto/ApolloCoreCryptoHeaders.h>

static NSString *const EncryptedPrivateKeyKey = @"EncryptedPrivateKey";

@interface ZETASecureStore (PrivateKey) <ApolloCoreCryptoSecureStoreProtocol>

- (void)privateKeyWithIdentifier:(NSString *)keyID
                      userPrompt:(BOOL)promptUser
                      completion:(ZETAPrivateKeyGetterBlock)completion;

- (void)setPrivateKey:(ZETAErasablePrivateKey *)privateKey
       withIdentifier:(NSString *)keyID
           completion:(ZETAFailureBlockType)completion;

/** Folowing secure store methods
    - privateKeyWithUserPrompt:completion:
    - setPrivateKey:completion:
    use "EncryptedPrivateKey" key identifier to store and get private key.
 */
- (void)privateKeyWithUserPrompt:(BOOL)promptUser
                      completion:(ZETAPrivateKeyGetterBlock)completion;
- (void)setPrivateKey:(ZETAErasablePrivateKey *)privateKey
           completion:(ZETAFailureBlockType)completion;

- (void)removePrivateKeyData;

- (void)removePrivateKeyDataWithIdentifier:(NSString *)keyID;

@end
