#import "CipherEnterPinProtocol.h"
#import <Foundation/Foundation.h>
#import "ZETAEnterPinProtocol.h"
#import <ApolloAnalyticsCore/ApolloAnalyticsCore.h>
#import <CocoaNuggetsViews/Views.h>

@protocol ZETAEnterPinHandlerDelegate;

@interface ZETAEnterPinHandler : NSObject <ZETAEnterPinProtocol>

@property (nonatomic, weak) id<ZETAEnterPinHandlerDelegate> delegate;

- (instancetype)initWithAppName:(NSString *)appName
               analyticsManager:(ApolloAnalyticsManager *)analyticsManager;

- (void)setEnterPinDelegate:(id<CipherEnterPinProtocol>)enterPinDelegate;

- (void)pinEnteredByUser:(NSString *)pin;

- (void)userDidForgotPin;

@end

@protocol ZETAEnterPinHandlerDelegate <NSObject>

- (PinViewControllerConfig *)enterPinHandlerDidRequestViewConfig:(ZETAEnterPinHandler *)handler;

@end
