#import "ZETASetPinProtocol.h"
#import "CipherSetPinProtocol.h"
#import <Foundation/Foundation.h>
#import "ZETASetPinProtocol.h"
#import <ApolloAnalyticsCore/ApolloAnalyticsCore.h>
#import <CocoaNuggetsViews/Views.h>

@protocol ZETASetPinHandlerDelegate;

@interface ZETASetPinHandler : NSObject <ZETASetPinProtocol>

@property (nonatomic, weak) id<ZETASetPinHandlerDelegate> delegate;

- (instancetype)initWithAnalyticsManager:(ApolloAnalyticsManager *)analyticsManager
                   templateCollectionIDs:(NSArray *)templateCollectionIDs;

- (void)setSetPinDelegate:(id<CipherSetPinProtocol>)setPinDelegate;

- (void)pinEnteredByUser:(NSString *)pin;

@end

@protocol ZETASetPinHandlerDelegate <NSObject>

- (PinViewControllerConfig *)setPinHandlerDidRequestViewConfig:(ZETASetPinHandler *)handler;

@end
