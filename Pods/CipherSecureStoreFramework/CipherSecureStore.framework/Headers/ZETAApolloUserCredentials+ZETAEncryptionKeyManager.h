#import "ZETAStoreType.h"
#import <ApolloUserCredentials/ApolloUserCredentialsHeaders.h>

@interface ZETAApolloUserCredentials (ZETAEncryptionKeyManager)

- (void)setStoreType:(ZETAStoreType)storeType;

- (ZETAStoreType)storeType;

- (void)setSaltForPKDF:(NSData *)salt;

- (NSData *)saltForPKDF;

- (void)setEncryptedMasterKey:(NSData *)encryptionMasterKey;

- (NSData *)encryptedMasterKey;

- (void)setMasterKey:(NSData *)encryptionMasterKey;
- (NSData *)masterKey;
- (void)removeMasterKey;

- (void)setMasterKeySetupForDeviceLock;
- (BOOL)isMasterKeySetupForDeviceLock;

- (NSString *)pinHash;
- (void)setPinHash:(NSString *)pinHash;
- (void)removePinHash;

- (NSUInteger)wrongPinTries;
- (void)incrementWrongPinTriesCount;
- (void)resetWrongPinTriesCount;

- (void)removeAllEncryptionKeyData;

@end
