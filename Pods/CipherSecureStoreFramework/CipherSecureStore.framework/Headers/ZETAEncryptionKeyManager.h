#import <Foundation/Foundation.h>
#import "ZETASecureVault.h"
#import <ApolloUserCredentials/ApolloUserCredentialsHeaders.h>
#import <ApolloCommonsTDTFoundationAdditions/ApolloCommonsTDTFoundationAdditions.h>
#import <ApolloAnalyticsCore/ApolloAnalyticsCore.h>
#import "ZETASetPinProtocol.h"
#import "ZETAEnterPinProtocol.h"

FOUNDATION_EXPORT NSString *const ZETAEncryptionKeyDidUnlockNotification;
FOUNDATION_EXPORT NSString *const ZETAEncryptionKeyResetNotification;
FOUNDATION_EXPORT NSString *const ZETAUserCancelledForgetPin;

typedef void (^EncryptionKeyFailureBlock)(NSError * error);
static NSString *const EnterPinErrorDomain = @"EnterPinErrorDomain";
typedef NS_ENUM(NSUInteger, ZETAEnterPinErrorType) {
  ZETAEnterPinUnknownError,
  ZETAEnterPinErrorIncorrectPin,
  ZETAEnterPinForgotKeyError,
  ZETAEnterPinMaxWrongPinTriesError,
  ZETAEnterPinInProgress
};

@protocol ZETAEncryptionKeyManagerProtocol;

typedef void (^ZETAEncryptionKeyGetterBlock)(NSError *error, NSData *encryptionKey);

typedef NS_ENUM(NSUInteger, ZETAEncryptionKeyManagerErrorType) {
  ZETAEncryptionKeyManagerUnknownError,
  ZETAEncryptionKeyManagerKeyNotCreatedError,
  ZETAEncryptionKeyManagerDuplicateKeyCreationError,
  ZETAEncryptionKeyManagerAuthRequired
};

FOUNDATION_EXPORT NSString *const ZETAEncryptionKeyManagerErrorDomain;

@interface ZETAEncryptionKeyManager : NSObject

@property (nonatomic, weak) id<ZETAEncryptionKeyManagerProtocol> delegate;

- (instancetype)initWithApolloUserCredentials:(ZETAApolloUserCredentials *)apolloUserCredentials
                             analyticsManager:(ApolloAnalyticsManager *)analyticsManager
             prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs
                                      appName:(NSString *)appName
                                    debugMode:(BOOL)debugMode
                                setPinHandler:(id<ZETASetPinProtocol>)setPinHandler
                              enterPinHandler:(id<ZETAEnterPinProtocol>)enterPinHandler;

/**
 Resets the existing timer.
 @note Should be called by owner of the object before deallocating.
 */
- (void)invalidateTimer;

- (BOOL)isEncryptionKeyCached;

- (BOOL)isEncryptionKeyGenerated;

- (void)generateEncryptionKeyWithCompletion:(ZETAEncryptionKeyGetterBlock)completion;

- (void)getEncryptionKeyWithUserPrompt:(BOOL)promptUser
                            completion:(ZETAEncryptionKeyGetterBlock)completion;

- (void)migrateEncryptionKeyWithCompletion:(TDTSimpleBlock)completion;

- (void)migrateMasterEncryptionKeyFromDeviceLockToPinWithCompletion:(EncryptionKeyFailureBlock)completion;

- (void)migrateMasterEncryptionKeyFromPinToDeviceLockWithCompletion:(EncryptionKeyFailureBlock)completion;

- (void)createFallbackToPinWithCompletion:(EncryptionKeyFailureBlock)completion;

- (void)resetAllData;

- (void)invalidateSession;

- (NSData *)getEncryptionKey;

- (NSData *)generateAndGetEncryptionKey;

@end

@protocol ZETAEncryptionKeyManagerProtocol <NSObject>

- (void)encryptionKeyManagerDidForgetPin:(__unused ZETAEncryptionKeyManager *)manager
                         completionBlock:(ZETAFailureBlockType)completionBlock;

- (void)encryptionKeyManagerDidEnterWrongPin:(ZETAEncryptionKeyManager *)manager
                             completionBlock:(ZETAEmptyBlockType)completionBlock;

- (void)encryptionKeyManagerDidEnterCorrectPin:(ZETAEncryptionKeyManager *)manager
                               completionBlock:(ZETAEmptyBlockType)completionBlock;

@optional

- (NSUInteger)remainingNumberOfLoginAttempts:(ZETAEncryptionKeyManager *)manager;

- (BOOL)encryptionKeyManagerShallAllowAttempts:(ZETAEncryptionKeyManager *)manager;

- (void)encryptionKeyManagerDidExhaustAttempts:(ZETAEncryptionKeyManager *)manager
                               completionBlock:(ZETAEmptyBlockType)completionBlock;

@end
