#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CipherSecureStoreThemeManager : NSObject

- (instancetype)initWithThemeID:(NSString *)themeID
           andThemeCollectionID:(NSString *)themeCollectionID;
-(void)syncTheme;

@end

NS_ASSUME_NONNULL_END
