#import <Foundation/Foundation.h>
#import "ZETAEncryptionKeyManager.h"
#import <ApolloCoreCrypto/ApolloCoreCryptoHeaders.h>
#import <ApolloUserCredentials/ApolloUserCredentialsHeaders.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

typedef void (^ZETAPrivateKeyGetterBlock)(NSError *error, ZETAErasablePrivateKey *privateKey);
typedef void (^ZETATOTPSecretGetterBlock)(NSError *error, NSData *TOTPSecret);
typedef void (^ZETASecuredSupercardsGetterBlock)(NSError *error, NSArray *securedSupercards);

@protocol ZETASecureStoreProtocol;

@interface ZETASecureStore : NSObject

@property (nonatomic, weak) id<ZETASecureStoreProtocol> delegate;

- (instancetype)initWithUserCredentials:(ZETAApolloUserCredentials *)userCredentials
                   encryptionKeyManager:(ZETAEncryptionKeyManager *)encryptionManager;

@property (nonatomic, readonly) ZETAApolloUserCredentials *apolloUserCredentials;

/**
 Get the private key object.

 @param promptUser This param is used to determine whether to trigger auth for this. If NO is
 passed, then user action auth won't be triggered and value will be supplied only if the encryption
 key is cached.
 @prama completion Completion block is called once data has been decoded/<failed-to-decode>
 */

- (void)dataForKey:(NSString *)key
        userPrompt:(BOOL)promptUser
        completion:(void (^)(NSError *error, NSData *data))completion;

- (BOOL)containsDataForKey:(NSString *)key;
/**
  This method will store the passed data into secure store and call completionBlock post
  completion.
 */
- (void)setData:(NSData *)data forKey:(NSString *)key completion:(ZETAFailureBlockType)completion;

/**
  Deletes all data stored by the secure store.
 */
- (void)resetAllData;

- (void)migrateMasterEncryptionKeyIfNecessaryWithCompletion:(TDTSimpleBlock)completion;

- (void)migrateMasterEncryptionKeyFromDeviceLockToPinWithCompletion:(EncryptionKeyFailureBlock)completion;

- (void)migrateMasterEncryptionKeyFromPinToDeviceLockWithCompletion:(EncryptionKeyFailureBlock)completion;

- (void)createFallbackToPinWithCompletion:(EncryptionKeyFailureBlock)completion;

@end

@protocol ZETASecureStoreProtocol <NSObject>

- (BOOL)secureStoreShouldEnableTouchID:(ZETASecureStore *)store;

- (void)secureStoreDidRequestResetAllData:(ZETASecureStore *)store;

@end
