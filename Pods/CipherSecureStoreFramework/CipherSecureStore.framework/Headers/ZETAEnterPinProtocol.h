#ifndef ZETAEnterPinProtocol_h
#define ZETAEnterPinProtocol_h
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

typedef void (^CipherIncorrectPinEnteredBlockType)(BOOL status, NSUInteger remainingNumberOfTries, TDTSimpleBlock onDismissal);
typedef void (^EnterPinSuccessBlockType)(NSString *pin, CipherIncorrectPinEnteredBlockType completion);
typedef void (^EnterPinFailureBlockType)(NSError *error, ZETAEmptyBlockType completion);

@protocol ZETAEnterPinProtocol <NSObject>

- (void)enterPinWithSuccessBlock:(EnterPinSuccessBlockType)success
                    failureBlock:(EnterPinFailureBlockType)failure;

@end


#endif /* ZETAEnterPinProtocol_h */
