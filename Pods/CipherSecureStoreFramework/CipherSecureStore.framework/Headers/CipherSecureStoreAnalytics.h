#import <Foundation/Foundation.h>

#pragma mark - Event Names

static NSString *SecureStoreResetEventName = @"APL_Secure_Store_Reset";

#pragma mark - Event Params

static NSString *SecureStoreEventParamReason = @"reason";

#pragma mark - Event Param Values
static NSString *SecureStoreReasonValueForgotPin = @"forgotPin";
static NSString *SecureStoreReasonValueRetriesExhausted = @"retriesExhausted";
static NSString *SecureStoreReasonValueDeviceLockReset = @"deviceLockReset";
