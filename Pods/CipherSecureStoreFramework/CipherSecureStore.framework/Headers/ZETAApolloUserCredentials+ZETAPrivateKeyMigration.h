#import <ApolloUserCredentials/ApolloUserCredentialsHeaders.h>
#import <ApolloCoreCrypto/ApolloCoreCryptoHeaders.h>

@interface ZETAApolloUserCredentials (ZETAPrivateKeyMigration)

@property (nonatomic) ZETAErasablePrivateKey *privateKey;

- (void)deletePrivateKey;

- (void)deletePIN;

@end
