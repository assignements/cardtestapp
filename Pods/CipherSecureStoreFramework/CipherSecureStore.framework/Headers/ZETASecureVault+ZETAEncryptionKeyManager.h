#import "ZETASecureVault.h"

@interface ZETASecureVault (ZETAEncryptionKeyManager)

- (void)setEncryptedMasterKey:(NSData *)masterEncryptionKey;

- (NSData *)masterEncryptionKeyError:(NSError **)error;

- (BOOL)isMasterEncryptionKeyStored;

- (void)removeMasterEncryptionKey;
@end
