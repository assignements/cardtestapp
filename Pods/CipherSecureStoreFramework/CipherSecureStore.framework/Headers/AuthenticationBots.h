
// This is an Abstract Class. Don't use it without subclassing it.

#import <Foundation/Foundation.h>
#import <ApolloCommonsCore/ApolloMacros.h>

NS_ASSUME_NONNULL_BEGIN

// AuthenticationModeTypeInstant: Force authentication would be done
// AuthenticationModeTypeOnClick: Session would be invalidated so that the authentication would be asked if and when user performs a sensitive action
typedef NS_ENUM(NSUInteger, AuthenticationModeType) {
  AuthenticationModeTypeInstant,
  AuthenticationModeTypeOnClick
};

@protocol AuthenticationBotDelegate <NSObject>
- (void)forceAuthenticateWithClearSession:(BOOL)clearSession
                               completion:(ZETAFailureBlockType)completion;
- (void)invalidateSession;
@end

@protocol AuthenticationBots <NSObject>
- (void)enableAuthenticationBot;
- (void)disableAuthenticationBot;
@end

NS_ASSUME_NONNULL_END
