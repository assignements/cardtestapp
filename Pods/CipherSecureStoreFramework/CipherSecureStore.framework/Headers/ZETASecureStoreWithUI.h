#import "ZETAEncryptionKeyManager.h"
#import "ZETASecureStore.h"
#import "CipherSetPinProtocol.h"
#import "CipherEnterPinProtocol.h"
#import <Foundation/Foundation.h>
#import <CocoaNuggetsViews/Views.h>

@protocol ZETASecureStoreWithUIDataSource;
@protocol ZETASecureStoreWithUIDelegate;

@interface ZETASecureStoreWithUI : NSObject

@property (nonatomic, weak) id<ZETASecureStoreWithUIDataSource> dataSource;
@property (nonatomic, weak) id<ZETASecureStoreWithUIDelegate> delegate;
@property (nonatomic, readonly) ZETAEncryptionKeyManager *keyManager;
@property (nonatomic, readonly) ZETASecureStore *secureStore;

- (instancetype)initWithApolloUserCredentials:(ZETAApolloUserCredentials *)apolloUserCredentials
                             analyticsManager:(ApolloAnalyticsManager *)analyticsManager
             prioritizedTemplateCollectionIDs:(NSArray *)prioritizedTemplateCollectionIDs
                                      appName:(NSString *)appName
                                    debugMode:(BOOL)debugMode;

- (void)setSetPinDelegate:(id<CipherSetPinProtocol>)setPinDelegate andSetEnterPinDelegate:(id<CipherEnterPinProtocol>)enterPinDelegate;

- (void)setSecureStorePin:(nonnull NSString *)pin;

- (void)unlockSecureStoreWithPin:(nonnull NSString *)pin;

- (void)forgetPin;

- (NSUInteger)maxNumberOfLoginAttempts;

- (NSUInteger)remainingNumberOfLoginAttempts;


@end

@protocol ZETASecureStoreWithUIDataSource <NSObject>

@optional
- (PinViewControllerConfig *)secureStoreUIDidRequestSetPinViewConfig:(ZETASecureStoreWithUI *)storeWithUI;
- (PinViewControllerConfig *)secureStoreUIDidRequestEnterPinViewConfig:(ZETASecureStoreWithUI *)storeWithUI;

@end

@protocol ZETASecureStoreWithUIDelegate <NSObject>

- (void)secureStoreWithUIDidRequestResetAllData:(ZETASecureStoreWithUI *)store;
- (BOOL)secureStoreWithUIShouldEnableTouchID:(ZETASecureStoreWithUI *)store;

@end
