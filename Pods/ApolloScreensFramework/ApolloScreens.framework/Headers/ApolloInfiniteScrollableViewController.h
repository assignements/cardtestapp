#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CocoaNuggetsUIKitAdditions/ApolloViewControllerNavigationBarConfig.h>
#import "ApolloScreensProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApolloInfiniteScrollableViewController : UIViewController

- (instancetype)initWithNFRC:(NSFetchedResultsController *)nfrc
         navigationBarConfig:(ApolloViewControllerNavigationBarConfig *)config
       templateCollectionIDs:(NSArray *)templateCollectionIDs
              showFullScreen:(BOOL)showFullScreen
              errorStateView:(UIView *)errorStateView
                   sheetType:(nullable NSString *)sheetType
        emptyStateCustomView:(nullable UIView *)emptyStateCustomView
               pullToRefresh:(BOOL)pullToRefresh
              statusBarColor:(UIColor *)statusBarColor
           sheetCornerRadius:(CGFloat)sheetCornerRadius;

@property (nonatomic, weak) id <ApolloScreensProtocol> delegate;

@end

NS_ASSUME_NONNULL_END
