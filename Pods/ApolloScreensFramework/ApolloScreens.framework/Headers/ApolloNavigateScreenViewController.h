
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "ApolloScreensProtocol.h"
#import "ApolloScreenNavigateScreenParams.h"
#import <CocoaNuggetsUIKitAdditions/ApolloViewControllerNavigationBarConfig.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloNavigateScreenViewController : UIViewController

- (instancetype)initWithTopContainerNFRC:(NSFetchedResultsController *)topContainerNFRC
                     navigationBarConfig:(ApolloViewControllerNavigationBarConfig *)config
                    navigateScreenParams:(ApolloScreenNavigateScreenParams *)navigateScreenParams
                   templateCollectionIDs:(NSArray *)templateCollectionIDs
                          showFullScreen:(BOOL)showFullScreen
                          statusBarColor:(UIColor *)statusBarColor;

@property (nonatomic, weak) id <ApolloScreensProtocol> delegate;

@property (nonatomic) UIView *topContainerErrorView;

@end

NS_ASSUME_NONNULL_END
