#import <ApolloUserAuthManager/ApolloSDKConfigModel.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloScreensConfig : ApolloSDKConfigModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSURL *marioBaseUrl;
@property (nonatomic, readonly) NSArray *templatesCollectionIds;
@property (nonatomic, readonly) NSString *themeConfigCollectionId;
@property (nonatomic, readonly) NSString *scheme;
@property (nonatomic, readonly) NSArray *zetletCollectionIds;
@property (nonatomic, readonly) NSString *defaultThemeId;
@property (nonatomic, readonly) NSArray *supportedLocales;

@end

NS_ASSUME_NONNULL_END
