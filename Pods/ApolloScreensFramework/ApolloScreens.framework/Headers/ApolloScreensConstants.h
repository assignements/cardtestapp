#import <ApolloCommonsCore/ApolloCommonAdditions.h>

typedef NS_ENUM(NSUInteger, ApolloScreenRenderState) {
  ApolloScreenRenderStateUnknown,
  ApolloScreenRenderStateSuccess,
  ApolloScreenRenderStateFailed
};

#pragma mark - Screen Types

FOUNDATION_EXPORT NSString *const ScreenTypeStaticZetlet;
FOUNDATION_EXPORT NSString *const ScreenTypeInfiniteScrollable;
FOUNDATION_EXPORT NSString *const ScreenTypeNavigateScreen;

#pragma mark - Parameters

FOUNDATION_EXPORT NSString *const ScreenTypeQueryParam;
FOUNDATION_EXPORT NSString *const ParamViewGroupName;
FOUNDATION_EXPORT NSString *const ParamViewGroupItem;
FOUNDATION_EXPORT NSString *const ParamViewGroupComponent;
FOUNDATION_EXPORT NSString *const ScreenParamSdkName;
FOUNDATION_EXPORT NSString *const FullScreenParam;
FOUNDATION_EXPORT NSString *const ErrorStateViewGroupNameParam;
FOUNDATION_EXPORT NSString *const SheetTypeParam;
FOUNDATION_EXPORT NSString *const InfiniteScreenEmptyViewGroupNameParam;
FOUNDATION_EXPORT NSString *const PullToRefreshParam;
FOUNDATION_EXPORT NSString *const OTVGComponentsParam;
FOUNDATION_EXPORT NSString *const StatusBarColorParam;
FOUNDATION_EXPORT NSString *const StatusBarThemedColorParam;

#pragma mark - Other Constants

FOUNDATION_EXPORT NSString *const FallbackLanguage;
FOUNDATION_EXPORT NSString *const ScreensModuleStoreName;
FOUNDATION_EXPORT NSString *const BottomSheetType;
FOUNDATION_EXPORT CGFloat const BottomSheetBackgroundOpacity;
FOUNDATION_EXPORT NSString *const SheetCornerRadiusParam;

#pragma mark - UI Strings

#define ScreenFailureMessage ZETALocalizedString(@"Failed to refresh", nil)

#pragma mark - Notification Constants

FOUNDATION_EXPORT NSString *const ScreenRenderSuccessEventName;
FOUNDATION_EXPORT NSString *const ScreenRenderFailedEventName;
FOUNDATION_EXPORT NSString *const ScreenRetriedEventName;
FOUNDATION_EXPORT NSString *const ScreenPullToRefreshSuccessEventName;
FOUNDATION_EXPORT NSString *const ScreenPullToRefreshFailedEventName;
FOUNDATION_EXPORT NSString *const ScreenPullToNavigateSuccessEventName;
FOUNDATION_EXPORT NSString *const ViewGroupNameEventParam;
FOUNDATION_EXPORT NSString *const ScreenTypeEventParam;
FOUNDATION_EXPORT NSString *const SdkNameEventParam;
FOUNDATION_EXPORT NSString *const ViewGroupComponentEventParam;
