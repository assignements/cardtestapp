#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CocoaNuggetsUIKitAdditions/ApolloViewControllerNavigationBarConfig.h>
#import "ApolloScreensProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApolloStaticZetletScreenViewController : UIViewController

- (instancetype)initWithNFRC:(NSFetchedResultsController *)nfrc
         navigationBarConfig:(ApolloViewControllerNavigationBarConfig *)config
       templateCollectionIDs:(NSArray *)templateCollectionIDs
               viewGroupName:(NSString *)viewGroupName
           viewGroupItemName:(nullable NSString *)viewGroupItemName
              showFullScreen:(BOOL)showFullScreen
              errorStateView:(UIView *)errorStateView
                   sheetType:(nullable NSString *)sheetType
               pullToRefresh:(BOOL)pullToRefresh
              statusBarColor:(UIColor *)statusBarColor
           sheetCornerRadius:(CGFloat)sheetCornerRadius;

@property (nonatomic, weak) id <ApolloScreensProtocol> delegate;


@end

NS_ASSUME_NONNULL_END
