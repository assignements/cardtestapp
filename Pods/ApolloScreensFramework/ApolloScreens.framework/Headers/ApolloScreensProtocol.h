#import <UIKit/UIKit.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

@protocol ApolloScreensProtocol <NSObject>

- (void)screenDidRefreshScreen:(UIViewController *)viewController
                       success:(ZETAEmptyBlockType)success
                       failure:(ZETAFailureBlockType)failure;

- (void)screenViewController:(UIViewController *)viewController
                didFireEvent:(NSString *)eventName
                  withParams:(NSDictionary *)params;

@end
