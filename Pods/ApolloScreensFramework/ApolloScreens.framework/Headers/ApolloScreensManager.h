#import <Foundation/Foundation.h>
#import <ApolloViewGroups/ApolloViewGroupManager.h>
#import <ApolloDeeplinkResolver/ZETADeeplinkURLOpener.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ApolloScreensManagerProtocol;

@interface ApolloScreensManager : NSObject

/**
 Before using ApolloScreensManager, make sure that you have set up ZETALanguageService for the screens module to pick up the correct locale.
 */
- (instancetype)initWithSdkName:(NSString *)sdkName
                         authID:(NSString *)authID
               viewGroupManager:(ApolloViewGroupManager *)viewGroupManager
              deeplinkURLOpener:(ZETADeeplinkURLOpener *)deeplinkURLOpener
          templateCollectionIDs:(NSArray *)templateCollectionIDs;

/// Creates a Screen Manager instance with build mode. Screens auth context and dependencies are initialized internally.
/// @note config file screenAuthConfig.plist to be added.
- (nullable instancetype)init;

- (void)refreshScreenWithViewGroupName:(NSString *)viewGroupName
                         viewGroupItem:(NSString *)viewGroupItem;

@end

NS_ASSUME_NONNULL_END
