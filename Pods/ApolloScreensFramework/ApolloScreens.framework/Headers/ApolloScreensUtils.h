#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloScreensUtils : NSObject

+ (void)publishNotificationWithName:(NSString *)name
                             params:(NSDictionary *)params;

+ (CGFloat)detachViewAndCalculateHeightOfZetletView:(UIView *)zetletView
                                           forWidth:(CGFloat)width;

+ (UIColor *)fetchColorFromInput:(NSString *)input
                 andDefaultColor:(UIColor *)defaultColor;

+ (UIColor *)fetchColorFromInput:(NSString *)input
        andDefaultColorHexString:(NSString *)defaultColorHexString;

@end

NS_ASSUME_NONNULL_END
