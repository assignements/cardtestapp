#import <ApolloDeeplinkResolver/ZETABaseFlowManager.h>
#import <ApolloViewGroups/ApolloViewGroupManager.h>
#import <CocoaNuggetsUIKitAdditions/ApolloViewControllerNavigationBarConfig.h>
#import "ApolloScreensProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApolloBaseScreenFlowManager : ZETABaseFlowManager <ApolloScreensProtocol>

@property (nonatomic, readonly) NSDictionary *params;
@property (nonatomic, readonly) ApolloViewControllerNavigationBarConfig *navigationBarConfig;
@property (nonatomic, readonly) NSArray *templateCollectionIDs;
@property (nonatomic, readonly) ApolloViewGroupManager *viewGroupManager;
@property (nonatomic, readonly) UIViewController *errorStateViewController;
@property (nonatomic, readonly) NSString *viewGroupName;
@property (nonatomic, readonly) BOOL pullToRefresh;
@property (nonatomic, readonly) UIColor *statusBarColor;

- (instancetype)initWithParams:(NSDictionary *)params
              viewGroupManager:(ApolloViewGroupManager *)viewGroupManager
         templateCollectionIDs:(NSArray *)templateCollectionIDs
                       sdkName:(NSString *)sdkName;

- (NSDictionary *)getSortCriteria;

- (NSDictionary *)getSearchCriteria;

@end

NS_ASSUME_NONNULL_END
