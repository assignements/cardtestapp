#import <Mantle/Mantle.h>

@interface ApolloScreenNavigateScreenParams : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSURL *bottomScreenUrl;
@property (nonatomic, readonly) NSURL *navigateScreenUrl;
@property (nonatomic, readonly) NSString *topViewGroup;
@property (nonatomic, readonly) NSNumber *bottomContainerCornerRadius;
@property (nonatomic, readonly) UIColor *bottomContainerBackgroundColor;
@property (nonatomic, readonly) NSString *themedBottomContainerBackgroundColor;
@property (nonatomic, readonly) NSURL *sliderImageUrl;
@property (nonatomic, readonly) NSString *topContainerErrorViewGroupName;

@end
