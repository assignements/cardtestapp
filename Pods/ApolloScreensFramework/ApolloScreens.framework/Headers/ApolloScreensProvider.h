#import <Foundation/Foundation.h>
#import <ApolloDeeplinkResolver/ZETABaseFlowManager.h>
#import <ApolloViewGroups/ApolloViewGroupManager.h>

@interface ApolloScreensProvider : NSObject

+ (ZETABaseFlowManager *)flowManagerForUrl:(NSURL *)url
                          viewGroupManager:(ApolloViewGroupManager *)viewGroupManager
                     templateCollectionIDs:(NSArray *)templateCollectionIDs
                                   sdkName:(NSString *)sdkName
                                completion:(ZETAEmptyBlockType)completion;

@end
