#import <Foundation/Foundation.h>
#import <ApolloCoreCrypto/ApolloCoreCryptoHeaders.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloUserAuthResourceData : NSObject

@property (nonatomic, readonly) ZETAPublicKey *publicKey;
@property (nonatomic, readonly) NSString *publicKeyCertificate;
@property (nonatomic, readonly) ZETAECKeyPair *keyPair;
@property (nonatomic, readonly) NSString *userSessionJID;
@property (nonatomic, readonly) NSDate *resourceValidity;
@property (nonatomic, readonly) NSNumber *nonce;

- (instancetype)initWithPublicKey:(ZETAPublicKey *)publicKey
             publicKeyCertificate:(NSString *)publicKeyCertificate
                    clientKeyPair:(ZETAECKeyPair *)clientKeyPair
                   userSessionJID:(NSString *)userSessionJID
                         validity:(NSDate *)validity
                            nonce:(NSNumber *)nonce;

@end

NS_ASSUME_NONNULL_END
