#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ApolloUserAuthModuleEventListener;
@interface ApolloUserAuthModuleEventListenerDecorator : NSObject
@property(nonatomic, weak) id<ApolloUserAuthModuleEventListener> listener;

-(instancetype)initWithListener:(id<ApolloUserAuthModuleEventListener>)listener;
@end

NS_ASSUME_NONNULL_END
