#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloSDKConfigModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSNumber *tenantId;
@property (nonatomic, readonly) NSNumber *ifiTenantId;
@property (nonatomic, readonly) NSString *projectId;
@property (nonatomic, readonly) NSString *appId;
@property (nonatomic, readonly) NSString *appName;
@property (nonatomic, readonly) NSString *platform;
@property (nonatomic, readonly) NSString *sdkName;
@property (nonatomic, readonly) NSString *sdkVersion;
@property (nonatomic, readonly) NSString *authId;
@property (nonatomic, readonly) NSString *apiKey;
@property (nonatomic, readonly) NSURL *sdkAuthBaseUrl;
@property (nonatomic, readonly) NSURL *apolloUserBaseUrl;
@property (nonatomic, readonly) BOOL enableCompression;
@property (nonatomic, readonly) NSString *doorTlsHostName;
@property (nonatomic, readonly) NSNumber *doorTlsHostPort;
@property (nonatomic, readonly) NSString *doorHostName;
@property (nonatomic, readonly) NSNumber *doorHostPort;
@property (nonatomic, readonly) BOOL shouldUseTls;

@end

NS_ASSUME_NONNULL_END
