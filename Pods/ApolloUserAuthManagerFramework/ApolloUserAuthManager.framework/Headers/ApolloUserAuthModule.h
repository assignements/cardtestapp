#import <Foundation/Foundation.h>
#import "ApolloUserAuthResourceData.h"
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import <ApolloDoorStream/ApolloDoorStreamHeaders.h>
#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>
#import <ApolloAnalyticsCore/ApolloAnalyticsCore.h>
#import <CipherSecureStore/CipherSecureStoreProtocol.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, ApolloUserAuthModuleEvent) { ApolloUserAuthModuleEventResourceCreationSuccessful,
  ApolloUserAuthModuleEventResourceCreationFailure,
  ApolloUserAuthModuleEventSessionCreationSuccessful,
  ApolloUserAuthModuleEventSessionCreationFailure
};

typedef void (^GetPrivateKeySuccessBlock)(ZETAErasablePrivateKey *privateKey);

@protocol ApolloUserAuthModuleEventListener;

@interface ApolloUserAuthModule : NSObject <ApolloAuthProviderProtocol>

+ (instancetype)sharedInstance;

/**
 @param authID sdk auth id
 @param sdkConfig sdk config passed as dictionary
 @param clearData whether clear data is needed before attempting authentication.
 This is useful in case of re-install where it is needed to delete persisted auth info from the previous session which may not be deleted on app uninstall.
 */

- (void)setupAuthManagerWithAuthID:(NSString *)authID
                         sdkConfig:(NSDictionary *)sdkConfig
                          listener:(id<ApolloUserAuthModuleEventListener>)listener
                         clearData:(BOOL)clearData;

- (void)setupAuthManagerWithAuthID:(NSString *)authID
                         sdkConfig:(NSDictionary *)sdkConfig
                         clearData:(BOOL)clearData;

/* Adding the SDK bundle as the parameter. Current use is to pick the sdkVersion (CFBundleShortVersionString) from the SDK's info.plist rather than its config file.*/

- (void)setupAuthManagerWithAuthID:(NSString *)authID
                         sdkConfig:(NSDictionary *)sdkConfig
                            bundle:(NSBundle *)bundle
                          listener:(id<ApolloUserAuthModuleEventListener>)listener
                         clearData:(BOOL)clearData;

- (void)setupAuthManagerWithAuthID:(NSString *)authID
                         sdkConfig:(NSDictionary *)sdkConfig
                            bundle:(NSBundle *)bundle
                         clearData:(BOOL)clearData;

- (void)setAnalyticsManager:(ApolloAnalyticsManager *)analyticsManager
                  forAuthID:(NSString *)authID;

- (void)setSecureStore:(id <CipherSecureStore>) secureStore
                authID:(NSString *)authID;
/**
 To be used for authentication of already created resource.
 */
- (void)authenticateUsingResourceData:(ApolloUserAuthResourceData *)resource
                            forAuthID:(NSString *)authID
                         successBlock:(nullable GetAuthSessionSuccessBlock)success
                         failureBlock:(nullable ZETAFailureBlockType)failure;

/**
 To be used for authentication by creating resource using apollo-sdk-auth service and by creating session using door or apollo-sdk-auth based on sdk config for provided authID
 */
- (void)authenticateWithToken:(NSString *)token
                    forAuthID:(NSString *)authID
                 successBlock:(nullable GetAuthSessionSuccessBlock)success
                 failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)getAuthSessionForAuthID:(NSString *)authID
                   successBlock:(nullable GetAuthSessionSuccessBlock)success
                   failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)getUserSessionJidForAuthID:(NSString *)authID
                      successBlock:(nullable ZETAStringBlockType)success
                      failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)getAuthTokenForAuthID:(NSString *)authID
                 successBlock:(nullable ZETAStringBlockType)success
                 failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)logoutForAuthID:(NSString *)authID
           successBlock:(nullable ZETAEmptyBlockType)success
           failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)clearDataForAuthID:(NSString *)authID;

- (nullable NSString *)getInstallationIDForAuthID:(NSString *)authID;

- (nullable ZETADoorStreamManager *)getDoorStreamForAuthID:(NSString *)authID;

- (nullable NSDate *)getServerAdjustedCurrentTimeForAuthID:(NSString *)authID;

- (void)updateUserOnlineStatusForAuthID:(NSString *)authID;

- (void)updateUserOnlineStatusForAuthID:(NSString *)authID
                                  attrs:(nullable NSDictionary *)attrs
                               isOnline:(BOOL)isOnline
                                isForce:(BOOL)isForce;

- (ZETAPublicKey *)getUserAuthPublicKeyForAuthID:(NSString *)authID;

- (void)getUserAuthPrivateKeyForAuthID:(NSString *)authID
                          successBlock:(GetPrivateKeySuccessBlock)success
                          failureBlock:(ZETAFailureBlockType)failure;

@end

@protocol ApolloUserAuthModuleEventListener <NSObject>

- (void)apolloUserAuthModule:(ApolloUserAuthModule *)authModule
                      authID:(NSString *)authID
                   fireEvent:(ApolloUserAuthModuleEvent)event;

@end

NS_ASSUME_NONNULL_END
