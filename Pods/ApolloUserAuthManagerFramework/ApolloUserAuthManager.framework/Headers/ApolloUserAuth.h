#import <Foundation/Foundation.h>
#import "ApolloUserAuthResourceData.h"
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import <ApolloDoorStream/ApolloDoorStreamHeaders.h>
#import <ApolloUserAdvanced/ApolloUserAdvancedHeaders.h>
#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>
#import <ApolloAnalyticsCore/ApolloAnalyticsCore.h>
#import <CipherSecureStore/CipherSecureStoreProtocol.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ApolloUserAuthEvent) {
    ApolloUserAuthEventResourceCreationSuccessful,
    ApolloUserAuthEventResourceCreationFailure,
    ApolloUserAuthEventSessionCreationSuccessful,
    ApolloUserAuthEventSessionCreationFailure
};
typedef void (^GetPrivateKeySuccessBlock)(ZETAErasablePrivateKey *privateKey);

@protocol ApolloUserAuthManagerEventListener;
@interface ApolloUserAuthManager : NSObject

@property (nonatomic, readonly) ApolloUserHelper *apolloUserHelper;
@property (nonatomic, weak) id<ApolloUserAuthManagerEventListener> eventListener;
@property (nonatomic, readonly) ZETADoorStreamManager *doorStream;

- (instancetype)initWithAuthID:(NSString *)authID
                        config:(NSDictionary *)config
                     clearData:(BOOL)clearData;

- (instancetype)initWithAuthID:(NSString *)authID
                        config:(NSDictionary *)config
                        bundle:(nullable NSBundle *)bundle
                     clearData:(BOOL)clearData;

- (void)setAnalyticsManager:(ApolloAnalyticsManager *)analyticsManager;

- (void)setSecureStore:(id<CipherSecureStore>)secureStore;

- (void)authenticateUsingResourceData:(ApolloUserAuthResourceData *)resources
                         successBlock:(nullable GetAuthSessionSuccessBlock)success
                         failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)authenticateWithToken:(NSString *)token
                 successBlock:(nullable GetAuthSessionSuccessBlock)success
                 failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)getAuthSessionWithSuccessBlock:(nullable GetAuthSessionSuccessBlock)success
                          failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)getUserSessionJIDWithSuccessBlock:(nullable ZETAStringBlockType)success
                             failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)getAuthTokenWithSuccessBlock:(nullable ZETAStringBlockType)success
                        failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)logoutWithSuccessBlock:(nullable ZETAEmptyBlockType)success
                  failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)updateUserOnlineStatusIfNeeded;

- (void)updateUserOnlineStatusWithAttrs:(nullable NSDictionary *)attrs
                               isOnline:(BOOL)isOnline
                                isForce:(BOOL)isForce;

- (NSDate *)getServerAdjustedCurrentTime;

- (ZETAPublicKey *)getUserAuthPublicKey;

- (void)getUserAuthPrivateKeyWithSuccessBlock:(GetPrivateKeySuccessBlock)success
                                 failureBlock:(ZETAFailureBlockType)failure;

- (void)clearPersistentData;

@end

@protocol ApolloUserAuthManagerEventListener <NSObject>

-(void)apolloUserAuthManager:(ApolloUserAuthManager *)authManager
                      authID:(NSString *)authID
                   fireEvent:(ApolloUserAuthEvent)event;

@end

NS_ASSUME_NONNULL_END
