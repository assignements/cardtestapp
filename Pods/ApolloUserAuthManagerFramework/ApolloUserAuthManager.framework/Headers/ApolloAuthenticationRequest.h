#import <Foundation/Foundation.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import <iSdkAuthCore/iSdkAuthCoreHeaders.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloAuthenticationRequest : NSObject

@property (nonatomic, readonly, copy) GetAuthSessionSuccessBlock success;
@property (nonatomic, readonly, copy) ZETAFailureBlockType failure;

- (instancetype)initWithSuccessBlock:(GetAuthSessionSuccessBlock)success
                        failureBlock:(ZETAFailureBlockType)failure;

@end

NS_ASSUME_NONNULL_END
