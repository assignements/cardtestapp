#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

FOUNDATION_EXPORT NSString *const ApolloSdkAuthenticatedNotificationParamAuthID;

@interface ApolloUserAuthUtils : NSObject

+ (nonnull NSString *)domainIDFromUserJID:(nonnull NSString *)userJID;
+ (nonnull NSString *)userIDFromUserJID:(nonnull NSString *)userJID;
/// Translates userJID to apollo user ID and domainID
/// @param userJID userJID to be transalated
/// @param params block in following format (domainID,userID)
+ (void)translateUserJID:(nonnull NSString *)userJID params:(nonnull void (^)(NSString *domainID, NSString *userID))params;

@end

NS_ASSUME_NONNULL_END
