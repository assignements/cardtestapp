#import <Foundation/Foundation.h>
#import "ZETACurrencyConfig.h"

@interface ZETACurrency : NSObject<NSCoding>

#pragma mark - Init methods

- (instancetype)initWithMainCurrencyValue:(NSDecimalNumber *)mainCurrencyValue
                           currencyConfig:(ZETACurrencyConfig *)currencyConfig;

- (instancetype)initWithFractionalCurrencyValue:(NSInteger)fractionalCurrencyValue
                                 currencyConfig:(ZETACurrencyConfig *)currencyConfig;

- (instancetype)initWithFractionalCurrencyValue:(NSInteger)fractionalCurrencyValue
                                   currencyCode:(NSString *)currencyCode;

- (instancetype)initWithMainCurrencyValue:(NSDecimalNumber *)mainCurrencyValue
                             currencyCode:(NSString *)currencyCode;

- (instancetype)initWithMainCurrencyString:(NSString *)mainCurrencyString
                              currencyCode:(NSString *)currencyCode;

+ (instancetype)zeroForCurrencyConfig:(ZETACurrencyConfig *)currencyConfig;

+ (instancetype)zeroForCurrencyCode:(NSString *)currencyCode;


#pragma mark - Value getter methods

- (NSInteger)fractionalCurrencyValue;
- (NSDecimalNumber *)mainCurrencyValue;
- (ZETACurrencyConfig *)currencyConfig;


- (NSString *)getAmountWithSymbol;
- (NSString *)getAmountWithName;
- (NSString *)getMainCurrencyValueString;

#pragma mark - Operation methods

- (instancetype)subtract:(ZETACurrency *)amountToSubtract;
- (instancetype)add:(ZETACurrency *)amountToSubtract;
- (BOOL)zeta_isLessThan:(ZETACurrency *)secondCurrencyValue;
- (BOOL)zeta_isEqualTo:(ZETACurrency *)secondCurrencyValue;
- (BOOL)zeta_isGreaterThan:(ZETACurrency *)secondCurrencyValue;
- (BOOL)zeta_isLessThanOrEqualTo:(ZETACurrency *)secondCurrencyValue;
- (BOOL)zeta_isGreaterThanOrEqualTo:(ZETACurrency *)secondCurrencyValue;

@end

@interface ZETACurrency (ZetaCardCurrencyProtected)

@property (nonatomic, readonly) NSInteger fractionalCurrencyValue;

@end
