#import <Mantle/Mantle.h>

typedef NS_ENUM(NSUInteger, ZETACurrencyPosition) {
  ZETACurrencyPositionPrefix,
  ZETACurrencyPositionPostfix
};

typedef NS_ENUM(NSUInteger, ZETADigitGrouping) {
  ZETADigitGroupingLakh,
  ZETADigitGroupingMillion
};

typedef NS_ENUM(NSUInteger, ZETADigitSeperators) {
  ZETADigitSeperatorDot,
  ZETADigitSeperatorComma
};

@interface ZETACurrencyConfig : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString* currencyCode;
@property (nonatomic, readonly) NSString* displayName;
@property (nonatomic, readonly) ZETACurrencyPosition displayNamePosition;
@property (nonatomic, readonly) NSString* displaySymbol;
@property (nonatomic, readonly) ZETACurrencyPosition displaySymbolPosition;
@property (nonatomic, readonly) NSUInteger divisionsInUnit;
@property (nonatomic, readonly) ZETADigitSeperators fractionSeparationSymbol;
@property (nonatomic, readonly) ZETADigitSeperators thousandSeparationSymbol;
@property (nonatomic, readonly) ZETADigitGrouping digitGrouping;
@property (nonatomic, readonly) NSString* locale;

@end
