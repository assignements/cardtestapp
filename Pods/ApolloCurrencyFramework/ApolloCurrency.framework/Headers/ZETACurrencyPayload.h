#import "ZETACurrency.h"
#import <Mantle/Mantle.h>

@interface ZETACurrencyPayload : MTLModel <MTLJSONSerializing>

- (instancetype)initWithCurrencyType:(NSString *)currencyType
                              amount:(ZETACurrency *)amount;
- (instancetype)initWithCurrencyType:(NSString *)currencyType
                         amountValue:(NSNumber *)amountValue;

@property(nonatomic, readonly) NSString *currency;
@property(nonatomic, readonly) ZETACurrency *amount;

@end
