#import <Foundation/Foundation.h>
#import <ApolloCollectionsCore/ZETAGenericCollectionManager.h>
#import "ZETACurrencyConfig.h"

@interface ZETACurrencyConfigManager : NSObject

+ (void)setupSharedInstanceWithCollectionsManager:(ZETAGenericCollectionManager *)collectionsManager
                       currencyConfigCollectionID:(NSString *)currencyConfigCollectionID
                                  defaultCurrency:(NSString *)defaultCurrency;

+ (ZETACurrencyConfigManager *)sharedManager;

+ (void)resetSharedManager;

- (ZETACurrencyConfig *)configForCurrency:(NSString *)currency;
- (NSString *)symbolForCurrencyCode:(NSString *)currencyCode;

- (ZETACurrencyConfig *)configForDefaultCurrency;
- (ZETACurrencyConfig *)currencyConfigWithDefaultFallbackForCurrencyCode:(NSString *)currencyCode;

@end
