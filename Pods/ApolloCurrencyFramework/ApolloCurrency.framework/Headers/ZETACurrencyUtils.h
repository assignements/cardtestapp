#import <Foundation/Foundation.h>

@interface ZETACurrencyUtils : NSObject

+ (NSInteger)roundDownMainCurrencyValue:(NSDecimalNumber *)mainCurrencyValue;

@end
