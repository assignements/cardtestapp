#import <Foundation/Foundation.h>
#import "TDTNetworkStream.h"

@class TDTTCPEndpoint;
@protocol TDTTCPSocketDelegate;

/**
 TCP Socket provides a simplified networking interface tailored for use by Knock.

 It implements the Network stream interface that is internally wired to the
 actual TCP socket. On top of the stream interface, it provides methods for
 connection management and connection state querying.
 */
@interface TDTTCPSocket : NSObject<TDTNetworkStream>

/**
 Initialize a TCP socket bound to `tcpEndpoint`.

 The receiver is not multi-thread access safe. Messages must be sent to the
 receiver only from operations scheduled on `operationQueue`.
 @pre `operationQueue` must be a serial queue.

 The network connection is not created at this point of time.
 */
- (instancetype)initWithTCPEndpoint:(TDTTCPEndpoint *)tcpEndpoint
                     operationQueue:(NSOperationQueue *)operationQueue;

/**
 Determines if the receiver should enable TLS on the TCP socket.

 The default value is NO.

 Modifications to this property will take effect during the next
 `connect` message that is sent to the receiver.
 */
@property (nonatomic) BOOL shouldUseTLS;

/**
 Initiate a connection to the TCP endpoint bound to the receiver.

 - This message is ignored if the receiver is not in the disconnected state.
 - The Network stream is ready for reading and writing after this message
 has been sent.

 @note The `disconnectImmediately` message must be sent to the receiver
 before the client lets go of the last reference to it.
 */
- (void)connect;

/**
 Immediately disconnect the network connection managed by the receiver and
 transition to the disconnected state.

 - This message is ignored if the receiver is in the disconnected state.
 - Reads and writes on the Network stream will be silently ignored after this
 message has been sent.
 */
- (void)disconnectImmediately;

/**
 Reflects the connected state of the network connection managed by the receiver.

 The TCP Socket cycles between three states:
 Disconnected -> Connecting -> Connected -> ...
 This property is initially NO, and is updated on the following state transitions:
 - Connecting -> Connected
 - Connected -> Disconnected
 */
@property (nonatomic, readonly) BOOL isConnected;

@property (nonatomic, weak) id<TDTTCPSocketDelegate> socketDelegate;

- (void)resetSocketConnectTimeout;

@end

@protocol TDTTCPSocketDelegate<NSObject>

- (void)tcpSocket:(TDTTCPSocket *)socket
      didReceiveTrust:(SecTrustRef)trust
         withHostname:(NSString *)hostname
    completionHandler:(void (^)(BOOL shouldTrustPeer))completionHandler;

- (void)tcpSocket:(TDTTCPSocket *)socket
  didReceiveError:(NSError *)error;

@end
