#import <Foundation/Foundation.h>

@protocol TDTReachabilityDetectorDelegate;

/**
 This is a wrapper over Reachability pod that sends message reachabilityDetectorDidBecomeReachable:
 to the delegate whenever network becomes reachable.
 */
@interface TDTReachabilityDetector : NSObject

/**
 Messages to `delegate` are scheduled on `delegateQueue`.
 If `delegateQueue` is not set, then mainQueue is used.
 */
@property (nonatomic, weak) id<TDTReachabilityDetectorDelegate> delegate;

/**
 The Operation Queue on which messages sent to `delegate` are scheduled.
 The main queue is used if this property is not set.
 */
@property (nonatomic, weak) NSOperationQueue *delegateQueue;

/**
 This function returns `YES` if network is reachable and `NO` otherwise.
 */
- (BOOL)isReachable;

@end

@protocol TDTReachabilityDetectorDelegate <NSObject>

- (void)reachabilityDetectorDidBecomeReachable:(TDTReachabilityDetector *)reachabilityDetector;

@end
