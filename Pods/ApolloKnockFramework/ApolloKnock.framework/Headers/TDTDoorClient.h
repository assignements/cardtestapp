#import <Foundation/Foundation.h>
#import "TDTDoorStream.h"
#import "TDTDoorJSONPacketsHandler.h"

@protocol TDTDoorClientDelegate;

/**
 Whereas one can go ahead and use all the low-level classes themselves,
 TDTDoorClient is porcelain.

 It enables the currently known set of door features.
 */
@interface TDTDoorClient : NSObject

@property (nonatomic, weak) id <TDTDoorClientDelegate> delegate;

- (instancetype)initWithDoorStream:(TDTDoorStream *)doorStream
                    operationQueue:(NSOperationQueue *)operationQueue;

- (TDTDoorJSONPacketsHandler *)createJSONPacketsHandler;

- (void)deregisterJSONPacketHandler:(TDTDoorJSONPacketsHandler *)packetsHandler;

- (void)startPinging;
- (void)stopPinging;

@end

@protocol TDTDoorClientDelegate <NSObject>
@optional

- (void)doorClient:(TDTDoorClient *)client
   didReceiveError:(NSString *)error;

- (void)doorClient:(TDTDoorClient *)client
 didProcessMessage:(NSDictionary *)message;

- (void)doorClientDidEncounterServerError:(TDTDoorClient *)client;

- (void)doorClientDidEncounterReadTimeout:(TDTDoorClient *)client;

@end
