#import <Foundation/Foundation.h>
#import "TDTDoorJSONPacketsHandler.h"

@protocol TDTDoorSessionDelegate;

/**
 Represents different states of a door session.

 - `TDTDoorSessionNotAuthenticated` : Its the initial state of the session and is not Authenticated.
 You can send packets which do not require Authenticated stream.

 - `TDTDoorSessionAuthenticating` : Session is in Authenticating state (Effectively, it means
 you can send a packet which requires authenticated stream but its not sure that it will succeed)

 - `TDTDoorSessionAuthenticated` : It means session is Authenticated and anything sent on this
 socket is treated authenticated)

 - `TDTDoorSessionAuthFailed` : Authenticated with the passed credentials failed. Its a fatal state
 and this session isnt' usable anymore.
 */
typedef NS_ENUM(NSUInteger, TDTDoorSessionState) {
  TDTDoorSessionNotAuthenticated,
  TDTDoorSessionAuthenticating,
  TDTDoorSessionAuthenticated,
  TDTDoorSessionAuthFailed
};

@interface TDTDoorSession : NSObject

@property (nonatomic, weak) id<TDTDoorSessionDelegate> delegate;

@property (nonatomic, readonly) TDTDoorSessionState state;
@property (nonatomic, readonly) TDTDoorJSONPacketsHandler *JSONPacketsHandler;

- (instancetype)initWithJSONPacketsHandler:(TDTDoorJSONPacketsHandler *)JSONPacketsHandler;

- (void)startWithPayload:(NSDictionary*)payload;

/**
 Sends a request to door for authenticating the session.

 @note Session should be in `TDTDoorSessionNotAuthenticated` state for requesitng this.
 */
- (void)authenticateWithAuthData:(NSString *)authData authToken:(NSString *)authToken;

/**
 Send an OMS request.

 @param requestObject requestData which has to be sent in the request
 @param requiresAuth whether the request needs authentication or not.
 @param logRequest whether to log the packet sent
 */
- (void)sendOMSRequest:(NSDictionary *)requestObject
 requiresAuthorization:(BOOL)requiresAuth
            logRequest:(BOOL)logRequest;

/**
 Send an OMS message.

 @param messageObject message payload which has to be sent.
 @param requiresAuth whether the message needs authentication or not.
 */
- (void)sendOMSMessage:(NSDictionary *)messageObject requiresAuthorization:(BOOL)requiresAuth;

- (void)fetchServerTime;

/**
 Resets the door session to initial state.
 */
- (void)resetState;

@end

@protocol TDTDoorSessionDelegate<NSObject>

@optional

/**
 Notifies the delegate that session is authenticated with authInfo.
 */
- (void)doorSession:(TDTDoorSession *)doorSession
    didAuthenticateWithAuthInfo:(NSDictionary *)authInfo;

/**
 Notifies the delegate that session has failed to authenticated.
 */
- (void)doorSession:(TDTDoorSession *)doorSession didFailToAutheticateWithError:(NSString *)error;

/**
 Notifies the delegate reveived OMS response.
 */
- (void)doorSession:(TDTDoorSession *)doorSession didReceiveOMSResponse:(NSDictionary *)JSONObject;

/**
 Notifies the delegate reveived OMS message.
 */
- (void)doorSession:(TDTDoorSession *)doorSession didReceiveOMSMessage:(NSDictionary *)JSONObject;

/**
 Notifies the delegate of timesync response
 */
- (void)doorSession:(TDTDoorSession *)doorSession didReceiveServerTime:(NSDate *)serverTime;

/**
 Ask the delegate for the UA-Info string to be sent in the auth packet. If
 this method returns @p nil, no UA-Info string is sent.
 */
- (NSString *)UAInfoStringForDoorSession:(TDTDoorSession *)doorSession;

/**
 Ask the delegate for the location bundle string to be sent in the auth packet. If
 this method returns @p nil, no location bundle is sent.
 */
- (NSDictionary *)locationBundleForDoorSession:(TDTDoorSession *)doorSession;

@end
