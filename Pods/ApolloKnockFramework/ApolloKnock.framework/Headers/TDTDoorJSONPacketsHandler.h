#import <Foundation/Foundation.h>
#import "TDTDoorMessageManager.h"
#import "TDTDoorMessageSender.h"

@protocol TDTDoorJSONPacketsHandlerDelegate;

/**
 This class takes care of providing wrapper for different Door interaction.

 It understands different types of door packet types and expose and API for the same. It expects
 the user of its API to pass whatever requestData/authInfo/(service data) they want
 to pass as part of request.

 It listens to all the supported door packets and lets the delegate know about them.
 */

@interface TDTDoorJSONPacketsHandler : NSObject<TDTDoorMessageListener>

@property (nonatomic, weak) id<TDTDoorMessageSender> doorMessageSender;
@property (nonatomic, weak) id<TDTDoorJSONPacketsHandlerDelegate> delegate;

- (instancetype)initWithDoorMessageSender:(id<TDTDoorMessageSender>)sender;

- (void)startSessionWithStartInfo:(NSDictionary *)startInfo;

- (void)authenticateWithAuthInfo:(NSDictionary *)authInfo;

- (void)sendOMSMessageWithMessageData:(NSDictionary *)messageData;

- (void)sendOMSRequestWithRequestData:(NSDictionary *)requestData logRequest:(BOOL)logRequest;

- (void)sendTimeSyncRequest;

@end

@protocol TDTDoorJSONPacketsHandlerDelegate<NSObject>
@optional

- (void)doorPacketsHandler:(TDTDoorJSONPacketsHandler *)handler
    didReceiveAuthSuccessWithAuthInfo:(NSDictionary *)authInfo;

- (void)doorPacketsHandler:(TDTDoorJSONPacketsHandler *)handler
    didReceiveAuthFailureWithAuthInfo:(NSDictionary *)authInfo;

- (void)doorPacketsHandler:(TDTDoorJSONPacketsHandler *)handler
     didReceiveOMSResponse:(NSDictionary *)JSONObject;

- (void)doorPacketsHandler:(TDTDoorJSONPacketsHandler *)handler
      didReceiveOMSMessage:(NSDictionary *)JSONObject;

- (void)doorPacketsHandler:(TDTDoorJSONPacketsHandler *)handler
    didReceiveTimeSyncResponseWithEpoch:(NSString *)epoch;

@end
