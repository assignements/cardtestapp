#import <Foundation/Foundation.h>

@protocol TDTDoorMessageListener;

/**
 TDTDoorMessageManager is used by TDTDoorClient to route messages to every
 Door Protocol Handler object.

 Any interested Door Protocol Handler must be initialized by TDTDoorClient and
 registered with TDTDoorMessageManager.
 */
@interface TDTDoorMessageManager : NSObject

- (void)registerListener:(id <TDTDoorMessageListener>)listener;
- (void)deregisterListener:(id <TDTDoorMessageListener>)listener;

/**
 @param message The JSON of one full message from Door.
 */
- (void)processMessage:(NSDictionary *)message;

@end

/**
 Listening to door messages 101:
 - The interested object must implement this protocol.
 - Door Client owns the Message Manager, let Door Client register the object
 on the Message Manager. This ensures that Door Client always knows what to
 register and deregister etc.
 - Message Manager will first call willListenToManager:message: to know whether
 the object is interested in this message.
 - Message Manager will next call processManager:message: if the object returned
 YES earler.
 */
@protocol TDTDoorMessageListener <NSObject>

- (BOOL)willListenToManager:(TDTDoorMessageManager *)manager
                    message:(NSDictionary *)message;

- (void)processManager:(TDTDoorMessageManager *)manager
               message:(NSDictionary *)message;

@end
