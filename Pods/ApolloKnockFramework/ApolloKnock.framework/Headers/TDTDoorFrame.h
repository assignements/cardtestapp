#import <Foundation/Foundation.h>

/**
 A "Door Frame" is a chunk of bytes prefixed with the count of bytes.

 - Interaction over the Door stream happens using these frames.
 - Each frame is guranteed to contain a complete JSON text.
 - This removes the need to delimit the stream by continually scanning it.
 */
@protocol TDTDoorFrame
@end
