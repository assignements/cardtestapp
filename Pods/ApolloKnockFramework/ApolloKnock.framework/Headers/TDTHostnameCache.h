#import <Foundation/Foundation.h>

/**
 Hostname cache to get around short TTLs from DNS responses.

 The spec is defined here: https://rdev.internal.directi.com/display/PD/DNS+Caching
 */
@interface TDTHostnameCache : NSObject

/**
 Returns a cached IP address for the given @p hostname. Parallely, a DNS lookup is performed
 and the first address from the DNS response is stored in the cache. The newly cached address is
 returned the next time this method called for the same @p hostname.

 @param hostname Name of the host for which we want a cached IP address

 @returns The cached IP address string for the hostname, or @p nil if nothing is cached.
 */
- (NSString *)cachedAddressForHostname:(NSString *)hostname;

@end
