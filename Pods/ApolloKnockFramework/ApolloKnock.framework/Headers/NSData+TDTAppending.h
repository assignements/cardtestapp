#import <Foundation/Foundation.h>

@interface NSData (TDTAppending)

- (NSData *)tdt_dataByAppendingData:(NSData *)data;

@end
