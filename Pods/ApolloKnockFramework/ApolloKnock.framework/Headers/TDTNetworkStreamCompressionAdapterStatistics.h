#import <Foundation/Foundation.h>

@interface TDTNetworkStreamCompressionAdapterStatistics : NSObject

- (instancetype)initWithUncompressedBytesRead:(NSUInteger)uncompressedBytesRead
                          compressedBytesRead:(NSUInteger)compressedBytesRead
                     uncompressedBytesWritten:(NSUInteger)uncompressedBytesWritten
                       compressedBytesWritten:(NSUInteger)compressedBytesWritten;

@property (nonatomic, readonly) NSUInteger uncompressedBytesRead;
@property (nonatomic, readonly) NSUInteger compressedBytesRead;
@property (nonatomic, readonly) double compressionRatioRead;

@property (nonatomic, readonly) NSUInteger uncompressedBytesWritten;
@property (nonatomic, readonly) NSUInteger compressedBytesWritten;
@property (nonatomic, readonly) double compressionRatioWrite;

@end
