#import <Foundation/Foundation.h>
#import "TDTDoorFrame.h"

@interface NSData (TDTDoorFrameEncoding)

/**
 Encode the receiver in a "Door Frame".

 @return NSData containing the `TDTDoorFrame` that contains the receiver
 as the message component.
 */
- (NSData<TDTDoorFrame> *)tdt_encodeAsDoorFrame;

@end
