#import <Foundation/Foundation.h>

@protocol TDTDoorMessageSender <NSObject>
@required

/**
 Convenience wrapper over @p sendMessage:logRequest: with logging enabled.
 */
- (void)sendMessage:(NSDictionary *)message;

- (void)sendMessage:(NSDictionary *)message logRequest:(BOOL)logRequest;

@end
