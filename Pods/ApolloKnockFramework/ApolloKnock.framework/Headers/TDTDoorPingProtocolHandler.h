#import <Foundation/Foundation.h>

#import "TDTDoorMessageSender.h"
#import "TDTDoorMessageManager.h"

@class TDTDoorPingProtocolHandler;
@protocol TDTDoorPingProtocolHandlerDelegate;

/**
 Door listens to the following message
 { "type" : "ping" }
 And responds with the following message immediately
 { "type" : "pong" }

 This protocol is necessary to make sure ELB does not terminate the client's socket
 to Door. ELB has a default timeout of 60 seconds for inactivility.

 We send a ping after exactly 55 seconds of inactivity.
 We notify the delegate after exactly 5 seconds of sending a ping (and not getting a pong).
 */
@interface TDTDoorPingProtocolHandler : NSObject <TDTDoorMessageListener>

@property (nonatomic, weak) id <TDTDoorPingProtocolHandlerDelegate> delegate;

- (instancetype)initWithDoorMessageSender:(id<TDTDoorMessageSender>)sender
                           operationQueue:(NSOperationQueue *)operationQueue;

/**
 Cause a ping message to be sent immediately, followed by one after every
 `timeIntervalBetweenPings` milliseconds of inactivity.

 @note Calling this again will simply cause the last ping scheduled to be cancelled
 And a new one to be scheduled immediately.
 */
- (void)startPinging;

/**
 Pause the ticker immediately.

 Any scheduled ping will be cancelled as well.

 `delegate` will still be notified of the failure of the pong for last ping!
 */
- (void)stopPinging;

@end

@protocol TDTDoorPingProtocolHandlerDelegate <NSObject>
@optional

/**
 Called if there is no pong for the last ping that was sent.

 This should ideally never be called unless the network is disconnected and the OS
 is not letting us know about it!

 Note that getting this notification does not mean next ping will not be sent.
 The next ping will be sent and the delegate will keep getting notified about it.
 */
- (void)pingProtocolHandlerDidNotReceivePong:(TDTDoorPingProtocolHandler *)handler;

@end
