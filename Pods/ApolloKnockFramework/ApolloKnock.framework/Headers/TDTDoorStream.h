#import <Foundation/Foundation.h>

@protocol TDTNetworkStream;
@protocol TDTDoorStreamDelegate;

/**
 A Door stream wraps a Network stream to make it talk in terms of Door dictionaries.

 Informally, a Door dictionary is any NSDictionary object that will be
 recognized by Door when serialized into JSON.
 */
@interface TDTDoorStream : NSObject

@property (nonatomic) BOOL logsFrames;
@property (nonatomic, weak) id<TDTDoorStreamDelegate> delegate;

/**
 Initialize a Door stream that internally talks to `networkStream`.
 */
- (instancetype)initWithNetworkStream:(id<TDTNetworkStream>)networkStream;

/**
 Serialize `dictionary` and write in on to the Network Stream.
 */
- (void)writeDictionary:(NSDictionary *)dictionary
             logRequest:(BOOL)logRequest;

/**
 Observe the Network stream and notify the delegate when the next complete
 NSDictionary is read off the Network stream.

 Sending this message multiple times while a read is in progress has no
 effect - only one read is left outstanding at a time. The read is treated as
 being completed when the `doorStream:didReadDictionary:` message is sent to
 the delegate, and it is safe to call it from within the handling of that
 message in the delegate.
 */
- (void)readDictionary;

@end

@protocol TDTDoorStreamDelegate <NSObject>

/**
 This message to is sent to the delegate when an entire Door dictionary
 has been extracted from the Network stream as a result of a previous
 `readDictionary` message being sent to the Door stream.
 */
- (void)doorStream:(TDTDoorStream *)doorStream
 didReadDictionary:(NSDictionary *)dictionary;

/**
 This message is sent to the delegate when extraction of a
 Door dictionary failed.

 At this time Door stream is in an potentially invalid state.
 Subsequent reads may or may not succeed.
 */
- (void)doorStreamRemoteDidViolateProtocol:(TDTDoorStream *)doorStream;

@end
