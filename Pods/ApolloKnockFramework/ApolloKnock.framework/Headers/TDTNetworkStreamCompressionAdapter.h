#import "TDTNetworkStream.h"
#import "TDTNetworkStreamCompressionAdapterStatistics.h"

/**
 Wrap an existing network stream to compress data travelling via it.

 This class takes an existing network stream (the "adapted" stream), and
 provides another network stream interface (the "compressed" stream). Data
 written to the compressed stream is compressed before being forwarded to
 the adapted stream, and data read from the inner stream is decompressed
 before being relayed back.

 Note that the compressed stream might read more data than is necessary
 from the adapted stream.

 Zlib is used for compression, in the "deflate" format.
 */
@interface TDTNetworkStreamCompressionAdapter : NSObject<TDTNetworkStream>

- (instancetype)initWithNetworkStream:(id<TDTNetworkStream>)networkStream;

- (TDTNetworkStreamCompressionAdapterStatistics *)resetAndReturnStatistics;

/**
 Zlib keeps various LZ77 dictionaries and dynamic Hoffman trees
 derived from the blocks it has already compressed. When we create
 a new network connection, we need to inform the compression
 adapter to reset its internal state and send data on the adapted
 stream as if it were a new one.
 */
- (void)resetCompressionDictionaries;

@end
