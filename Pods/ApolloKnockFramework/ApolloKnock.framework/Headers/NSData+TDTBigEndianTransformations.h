#import <Foundation/Foundation.h>

@interface NSData (TDTBigEndianTransformations)

/**
 @return NSData containing the four byte big endian representation of `unsignedInteger`.
 @pre `UInteger` must be representable using four bytes.
 */
+ (NSData *)tdt_dataWithBigEndianEncodingOfUnsignedInteger:(NSUInteger)unsignedInteger;

/**
 @return NSUInteger that is obtained when we consider the receiver to be
 the four byte big endian representation of an unsigned number.
 @pre The receiver must be exactly four bytes long.
 */
- (NSUInteger)tdt_interpretAsBigEndianUnsignedInteger;

@end
