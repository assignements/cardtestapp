#import <Foundation/Foundation.h>

@interface TDTTCPEndpoint : NSObject

- (instancetype)initWithHost:(NSString *)host
                        port:(UInt16)port;

@property (nonatomic, readonly) NSString *host;
@property (nonatomic, readonly) UInt16 port;

@end
