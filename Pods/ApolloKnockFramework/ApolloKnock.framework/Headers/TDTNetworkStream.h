#import <Foundation/Foundation.h>

@protocol TDTNetworkStreamDelegate;

/**
 A Network "stream" provides an bidirectional stream of bytes without exposing
 the details of the connection.

 - Underlying a Network stream will be the actual network sockets, but they will
 be transparently connected (and reconnected on errors).
 - The Network stream provides no guarantees of the completion of reads/writes.
 - In case the underlying socket gets disrupted, read/write will start failing.
 But this failure will be transient - subsequent requests might start
 succeeding if and when the underlying network socket gets (re)connected.
 */
@protocol TDTNetworkStream<NSObject>

@property (nonatomic, weak) id<TDTNetworkStreamDelegate> delegate;

/**
 Write `data` on to the underlying network socket.

 This message is ignored if the receiver is in not in the connected state.
 @pre If `data` is a mutable object (i.e. an instance of `NSMutableData`), then
 the caller must not modify it after sending this message.
 */
- (void)writeData:(NSData *)data;

/**
 Begin a read of `length` from the underlying network socket.

 The delegate will sent `networkStream:didReadDataToLength:data:` message when
 the read completes.

 @pre A read must not be in progress when this message is sent.

 This message is ignored if the receiver is not in the connected state.
 */
- (void)readDataToLength:(NSUInteger)length;

/**
 Read any sized data from the underlying network socket as it becomes available.

 @see `readDataToLength:` for contract details.
 */
- (void)readData;

/**
 Cancel any read that might be in progress on the receiver.
 */
- (void)cancelRead;

@end

@protocol TDTNetworkStreamDelegate<NSObject>

/**
 The delegate is sent this message when the read started by the Network stream
 as a result of receiving the last non-ignored `readDataToLength:` message
 has completed.
 @param networkStream Network Stream
 @param data The data read-in. If this read was a result of a
 `readDataToLength:` message, then the length of `data` is guaranteed to
 be equal to `length` parameter received by the Network stream in the last
 non-ignored `readDataToLength:` message. Otherwise, if this read was due
 to a `readData` message, then the length of `data` can be an arbitrary value.
 */
- (void)networkStream:(id<TDTNetworkStream>)networkStream didReadData:(NSData *)data;

/**
 The delegate is sent this message when the read started by the Network stream
 as a result of receiving the last non-ignored `readDataToLength:` message
 has irrevocably failed.

 Subsequent reads are guranteed to start afresh and will not return data
 expected as part of a previous read.
 */
- (void)networkStreamDidFailToReadData:(id<TDTNetworkStream>)networkStream;

@end
