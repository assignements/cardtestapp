#import <Foundation/Foundation.h>

/**
 Length of the Length field in a Door Frame
 */
extern const NSUInteger TDTDoorFrameLengthLength;
