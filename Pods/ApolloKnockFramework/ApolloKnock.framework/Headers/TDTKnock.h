#import <Foundation/Foundation.h>
#import "TDTNetworkStreamCompressionAdapterStatistics.h"

/**
 Knock is the pseudonymn of the Objective C client for Door.

 It provides the client with an ability to manage door session.
 */
@protocol TDTKnockDelegate;
@protocol TDTKnockSecurityDelegate;

@interface TDTKnock : NSObject

/**
 Set this property to YES to enable logging of door frames.
 This property is NO by default.
 */
@property (nonatomic) BOOL logsDoorFrames;
@property (nonatomic, weak) id<TDTKnockDelegate> _Nullable delegate;
@property (nonatomic, weak) id<TDTKnockSecurityDelegate> _Nullable securityDelegate;

/**
 Designated initializer.

 @param shouldCompressFrames Enable compression using zlib/deflate.
 The compression is done after door framing, before sending the
 data on the TCP socket.
 */
- (instancetype _Nonnull )initWithDoorHostname:(NSString *_Nonnull)doorHostname
                                port:(UInt16)port
                        shouldUseTLS:(BOOL)shouldUseTLS
                shouldCompressFrames:(BOOL)shouldCompressFrames;

/**
 Creates TCP socket with Door. It establishes the door session and start
 sending ping-pong to Door for keeping the connection live.

 @note It only triggers creation of a unauthenticated channels and requests which dont require
 auth can be sent after `knockDidConnectTCPSocketWithDoor` call.
 */
- (void)createDoorSession;
- (void)startDoorSessionWithPayload:(NSDictionary *_Nonnull)payload;

/**
 This method tries to authenticate the door session on the exisitng TCP socket.

 @param authData authData to be used for authentication.
 @param authToken Its the authToken that was received from previous auth-success, server will
 use it if the token will be still valid.

 @note There is only one door session active at one time. In case of auth failure too,
 this method can be used to re-authenticate with the passed token.
 */
- (void)authenticateSessionWithAuthData:(NSString *_Nullable)authData authToken:(NSString *_Nullable)authToken;

/**
 Call this to destroy the current door stream. If also causes tcp socket to be disconnected.

 @note The method does not block the caller till the pseudo connection is destroyed.
 This means that it is unsafe to assume that the instance of knock is now safe
 to destroy right after calling this. Knock doesn't retry to establish socket establshment after
 this.
 */
- (void)destroyDoorSession;

/**
 This method lets you send a request to an OMS service with the passed on data as the requestData.

 @param requestPayload JSON that you want to send as part of the request as requestData.
 @param requiresAuth tells whether the authorization is required to send this particular request.

 @note There are some requests that can be made without authorization of session. e.g.,
 Anon user service profile creatiion.
 */
- (void)sendOMSRequest:(NSDictionary *_Nonnull)requestPayload
 requiresAuthorization:(BOOL)requiresAuth
            logRequest:(BOOL)logRequest;

/**
 This method lets you send a message to a OMS service with the passed on data as the message
 payload.

 @param messagePayload JSON that you want to send as part of the message.
 @param requiresAuth tells whether the authorization is required to send this particular request.
 */
- (void)sendOMSMessage:(NSDictionary *_Nonnull)messagePayload requiresAuthorization:(BOOL)requiresAuth;

/**
 Sends the `timeSync` request.

 @note Response of this request is informed to delegate in knock:didReceiveServerTime:
 */
- (void)fetchServerTime;

@end

typedef NS_ENUM(NSUInteger, TDTKnockDoorSessionError) {
  TDTKnockDoorSessionNetworkError,
  TDTKnockDoorSessionUserRequestError
};

@protocol TDTKnockDelegate<NSObject>
@optional

/**
 Notifies the delegate when the door socket is established successfully and it is now safe to
 write data to it. Primarily from now on, unAuthenticated request can be sent to door.
 Authenticated request can be made after `knock:didAuthenticateWithAuthInfo:`
 is called.
 */
- (void)knockDidConnectTCPSocketWithDoor:(TDTKnock *_Nonnull)knock;

/**
 Notifies the delegate that now the session is authenticated and all requests requiring
 authentication can be sent safely from now on.
 */
- (void)knock:(TDTKnock *_Nonnull)knock didAuthenticateWithAuthInfo:(NSDictionary *_Nonnull)authinfo;

/**
 Notifies the delegate that now the session failed to authenticate and requests requiring
 authentications can not be sent now.

 @param error Error type returned by server is sent in this.

 @note Even after this, TCP connection is still intact and unAuthorized requests still can be sent.
 For retrying to establish the authenticated session, `authenticateSessionWithAuthToken` can be sent
 with new token.
 */
- (void)knock:(TDTKnock *_Nonnull)knock didFailToAuthenticateDoorSessionWithError:(NSString *_Nonnull)error;

/**
 Notifies the delegate when it receives responses for requests sent.
 */
- (void)knock:(TDTKnock *_Nonnull)knock didReadOMSResponse:(NSDictionary *_Nonnull)responsePayload;

/**
 Notifies the delegate when it receives any OMS message.
 */
- (void)knock:(TDTKnock *_Nonnull)knock didReadOMSMessage:(NSDictionary *_Nonnull)messagePayload;

/**
 Notifies the delegate when it receives server time.
 */
- (void)knock:(TDTKnock *_Nonnull)knock didReceiveServerTime:(NSDate *_Nonnull)serverDate;

/**
 Called to inform that socket now has been destroyed because of some error and
 door session has also been reset.

 - When it is called, Socket is also in disconnected state and client shouln't send anything
 on this socket till it gets the connected/authenticated event.
 - In case of `TDTKnockDoorSessionUserRequestError` TDTKnock disconnects the socket and door
 session.
 Whereas in all other cases, TDTKnock tries to restablish the socket and authenticate if possible.
 */
- (void)knock:(TDTKnock *_Nonnull)knock didDisconnectDoorSocketOnError:(TDTKnockDoorSessionError)error;

/**
 If compression is enabled, then Knock notifies its delegate on the compression
 statistics for the data sent on the last TCP socket with door whenever such
 a socket is disconnected.
 */
- (void)knock:(TDTKnock *_Nonnull)knock
lastDoorTCPSocketCompressionStatistics:(TDTNetworkStreamCompressionAdapterStatistics *_Nonnull)statistics;

/**
 Informs the delegate about "door errors" received on current door session.

 TDTKnock will handle the following errors from door

 - "malformed-json" TDTKnock causes the current TCP to break and reconnect the current stream again.
 - There can be other errors like "unknown-type", "missing-required-attribute" which aren't fatal.
 Door doesn't take care of them and just pass them to delegate.

 @note All errors, including "malformed-json" are passed to the delegate.
 */
- (void)knock:(TDTKnock *_Nonnull)knock didReceiveErrorFromDoor:(NSString *_Nonnull)error;

/**
 Provide the UA Info string to send with the auth packet to door.

 This method, if implemented, can return @p nil. If it does, no UA-Info string is sent.
 */
- (NSString *_Nonnull)knockUAInfoString:(TDTKnock *_Nonnull)knock;

/**
 Provide the location bundle string for the door session. It is sent along with the auth request.

 This method, if implemented, can return @p nil. If it does, no location-bundle is sent.
 */
- (NSDictionary *_Nonnull)knockLocationBundle:(TDTKnock *_Nonnull)knock;

@end

@protocol TDTKnockSecurityDelegate<NSObject>

- (void)knock:(TDTKnock *_Nonnull)knock
didReceiveTrust:(SecTrustRef _Nonnull )trust
 withHostname:(NSString *_Nonnull)hostname
completionHandler:(void (^_Nonnull)(BOOL shouldTrustPeer))completionHandler;

@end
