#import <Foundation/Foundation.h>
#import "ZETAFlowManagerProtocol.h"
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

@interface ZETABaseFlowManager : NSObject<ZETAFlowManagerProtocol>

- (instancetype)initWithPresentingViewController:(UIViewController *)viewController;

@property (nonatomic) UIViewController *initialViewController;
@property (nonatomic, weak, readonly) UIViewController *presentingViewController;

- (UINavigationController *)flowNavigationController;
- (UIViewController *)firstViewControllerOfFlow;
- (void)startFlowWithViewController:(UIViewController *)viewController;
- (void)startFlowWithViewController:(UIViewController *)viewController
                          animation:(BOOL)animation;
- (void)startFlowWithExistingInitialViewController:(UIViewController *)viewController;
- (void)popToPreviousVCAndCancelFlowIfNeeded;
- (void)dismissViewControllerAndCancelFlowIfNeeded:(UIViewController *)viewController;
- (BOOL)isFlowPushedOnAnExistingNavigationController;
- (void)dismissAllViewControllersWithAnimation:(BOOL)animation
                               completionBlock:(ZETAEmptyBlockType)completion;
- (void)dismissAllViewControllersWithCompletionBlock:(ZETAEmptyBlockType)completion;
- (void)pushViewControllerInFlow:(UIViewController *)viewController;
- (void)presentViewControllerInFlow:(UIViewController *)viewController
                           animated:(BOOL)animated
                         completion:(ZETAEmptyBlockType)completion;
- (void)startFlowWithViewController:(UIViewController *)viewController
          withTransitioningDelegate:(id<UIViewControllerTransitioningDelegate>)transitioningDelegate;
- (void)startFlowWithViewController:(UIViewController *)viewController
                          animation:(BOOL)animation
          withTransitioningDelegate:(id<UIViewControllerTransitioningDelegate>)transitioningDelegate;

@end
