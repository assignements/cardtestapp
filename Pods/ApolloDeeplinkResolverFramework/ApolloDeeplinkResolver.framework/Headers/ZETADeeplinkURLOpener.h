#import <Foundation/Foundation.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import "ZETANativeView.h"

@class ZETABaseFlowManager;
@protocol ZETADeeplinkURLOpenerDelegate;

/**
 This class helps in opening in app deeplinks. e.g., zeta://home
 In case of multiple deeplink handlers trying to handle the same deeplink, the handling will be undefined.
 Please make sure the deeplinks provided by handlers avoid any conflicts. Scoping of deeplinks by modules can be helpful.
 */
@interface ZETADeeplinkURLOpener : NSObject

+ (ZETADeeplinkURLOpener *)sharedInstance;

+ (void)reset;

+ (void)setupSharedInstanceWithDeeplinkScheme:(NSString *)deeplinkScheme;

/**
 DEPRECATED: Use addDeeplinkHandler: method to register as a handler for deeplinks
 */
@property (nonatomic, weak) id<ZETADeeplinkURLOpenerDelegate> delegate;

/**
 Call this method to register an object as the handler for deeplinks
 @param deeplinkHandler - Object to handle deeplinks
 */
- (void)addDeeplinkHandler:(id<ZETADeeplinkURLOpenerDelegate>)deeplinkHandler;

/**
 Returns @YES if URL is supported by the app.
 */
- (BOOL)isDeeplinkSupportedForURL:(NSURL *)URL;

/**
 Returns @YES if flowmanager exist for URL.
 */
- (BOOL)isFlowManagerSupportedForURL:(NSURL *)URL;

/**
 Returns @YES if nativeView exist for URL.
 */
- (BOOL)isNativeViewSupportedForURL:(NSURL *)URL;

/**
 Opens the passed URL inside the app.
 
 Returns @YES if URL was handled by app. Returns @NO otherwise.
 */
- (BOOL)openDeeplinkURL:(NSURL *)URL;
- (BOOL)openDeeplinkURL:(NSURL *)URL completion:(ZETAEmptyBlockType)completion;

- (BOOL)openDeeplinkPath:(NSString *)path;
- (BOOL)openDeeplinkPath:(NSString *)path fragment:(NSString *)fragment;
- (BOOL)openDeeplinkPath:(NSString *)path completion:(ZETAEmptyBlockType)completion;

- (NSURL *)deeplinkURLForPath:(NSString *)path;

- (ZETABaseFlowManager *)flowManagerForURL:(NSURL *)URL;
- (ZETABaseFlowManager *)flowManagerForURL:(NSURL *)URL completion:(ZETAEmptyBlockType)completion;
- (ZETABaseFlowManager *)flowManagerForPath:(NSString *)path;

- (ZETANativeView *)nativeViewForURL:(NSURL *)URL;
- (ZETANativeView *)nativeViewForURL:(NSURL *)URL completion:(ZETAEmptyBlockType)completion;
- (ZETANativeView *)nativeViewForPath:(NSString *)path;

@end

@protocol ZETADeeplinkURLOpenerDelegate<NSObject>

- (NSArray *)deeplinkURLOpenerDidRequestSupportedPaths:(ZETADeeplinkURLOpener *)deeplinkURLOpener;

- (NSArray *)deeplinkURLOpenerDidRequestSupportedFlowManagerPaths:(ZETADeeplinkURLOpener *)deeplinkURLOpener;

- (void)deeplinkURLOpener:(ZETADeeplinkURLOpener *)deeplinkURLOpener
       handleActionForURL:(NSURL *)URL
            supportedPath:(NSString *)supportedPath
               completion:(ZETAEmptyBlockType)completion;

- (ZETABaseFlowManager *)deeplinkURLOpener:(ZETADeeplinkURLOpener *)deeplinkURLOpener
                         flowManagerForURL:(NSURL *)URL
                             supportedPath:(NSString *)supportedPath
                                completion:(ZETAEmptyBlockType)completion;

- (BOOL)deeplinkURLOpener:(ZETADeeplinkURLOpener *)deeplinkURLOpener
performDeviceJailbrokenCheckForURL:(NSURL *)URL
            supportedPath:(NSString *)supportedPath;
@optional

/// Optional delegate callback used to reslove flowmanager deeplink if multiple objects support same deeplink.
/// Default value is YES and first object would be considered for deeplink handling
/// @param URL URL to
/// @param supportedPath path of the deeplink
- (BOOL)canHandleFlowManagerURL:(NSURL *)URL supportedPath:(NSString *)supportedPath;

/// Optional delegate callback used to reslove action deeplink if multiple objects support same deeplink action path.
/// Default value is YES and first object would be considered for deeplink handling
/// @param URL URL to
/// @param supportedPath path of the deeplink
- (BOOL)canHandleActionURL:(NSURL *)URL supportedPath:(NSString *)supportedPath;

- (NSArray *)deeplinkURLOpenerDidRequestSupportedNativeViewPaths:(ZETADeeplinkURLOpener *)deeplinkURLOpener;

- (ZETANativeView *)deeplinkURLOpener:(ZETADeeplinkURLOpener *)deeplinkURLOpener
                     nativeViewForURL:(NSURL *)URL
                        supportedPath:(NSString *)supportedPath
                           completion:(ZETAEmptyBlockType)completion;

@end
