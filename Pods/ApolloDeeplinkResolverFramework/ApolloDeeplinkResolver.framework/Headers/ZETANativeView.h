#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ZETANativeViewDelegate;

@interface ZETANativeView : UIView

@property (nonatomic, weak) id<ZETANativeViewDelegate> nativeViewDelegate;

/**
 Call this method when you update subviews of native view and want to refresh the native view layout.
 Useful to resize the native view when used inside a zetlet list view cell.
 */
- (void)refreshView:(BOOL)animation;

- (void)performAction:(id)action;

@end

@protocol ZETANativeViewDelegate <NSObject>

- (void)zetaNativeViewDidRequestRefresh:(ZETANativeView *)view
                          withAnimation:(BOOL)animation;

- (void)zetaNativeView:(ZETANativeView *)view
      didPerformAction:(id)action;

@end

NS_ASSUME_NONNULL_END
