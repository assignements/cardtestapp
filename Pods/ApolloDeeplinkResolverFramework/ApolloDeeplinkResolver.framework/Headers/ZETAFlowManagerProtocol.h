#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ZETAFlowManagerProtocol<NSObject>

- (void)start;
- (UIViewController *)startViewController;

@optional

+ (NSString *)flowName;
- (void)cancelFlow;

@end
