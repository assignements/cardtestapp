#import <Foundation/Foundation.h>
#import <TrustKit/TrustKit.h>

@interface ZETATrustKitWrapper : NSObject

+ (void)configureWithConfig:(NSDictionary *)config
               forDebugMode:(BOOL)isDebugMode;

@end
