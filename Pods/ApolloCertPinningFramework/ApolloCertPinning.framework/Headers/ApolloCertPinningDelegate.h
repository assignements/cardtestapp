#import <Foundation/Foundation.h>

@protocol ApolloCertPinningDelegate<NSObject>

- (void)handleAuthenticationChallenge:(NSURLAuthenticationChallenge * _Nonnull)challenge
                    completionHandler:(void (^ _Nonnull)(NSURLSessionAuthChallengeDisposition disposition,
                                                         NSURLCredential * _Nullable credential))completionHandler;

@end
