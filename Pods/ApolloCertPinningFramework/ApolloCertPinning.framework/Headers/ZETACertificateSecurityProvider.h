#import "ApolloCertPinningDelegate.h"

@interface ZETACertificateSecurityProvider : NSObject<ApolloCertPinningDelegate>

+ (void)setupSharedProviderWithCertPinningEnabled:(BOOL)isCertPinningEnabled;

+ (instancetype)sharedProvider;

+ (void)resetSharedProvider;

- (void)handleAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
                    completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition,
                                                NSURLCredential * _Nullable))completionHandler;

- (void)handleTrust:(SecTrustRef)trust
       withHostname:(NSString *)hostname
  completionHandler:(void (^)(BOOL shouldTrustPeer))completionHandler;

@end
