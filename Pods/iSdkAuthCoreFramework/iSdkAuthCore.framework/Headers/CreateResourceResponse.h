#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface CreateResourceResponse: MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *resourceJid;
@property (nonatomic, readonly) NSString *dhPublicKey;
@property (nonatomic, readonly) NSDate *validTill;
@property (nonatomic, readonly) NSString *resourcePublicKeyCertEncoded;
@property (nonatomic, readonly) NSString *userSessionId;
  
@end
