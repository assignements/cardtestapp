#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface CreateSessionResponse: MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *userSessionJid;
@property (nonatomic, readonly) NSString *sessionID;
@property (nonatomic, readonly) NSString *authToken;
@property (nonatomic, readonly) NSDate *validTill;
  
  @end
