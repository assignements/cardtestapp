#import <Foundation/Foundation.h>
#import "SdkAuthErrorUserInfo.h"

@interface ErrorUtil : NSObject

+ (SdkAuthErrorUserInfo *)parsedErrorUserInfoFromError:(NSError *)error;

+ (BOOL)isInvalidResourceError:(NSError *)error;

+ (BOOL)isResourceNotFoundError:(NSError *)error;

+ (BOOL)isTenantAuthFailureError:(NSError *)error;

+ (BOOL)isResourceCreationFailureError:(NSError *)error;

+ (BOOL)isSessionCreationFailureError:(NSError *)error;

+ (SdkAuthErrorUserInfo *)errorInfoForLocallyGeneratedErrorWithMessage:(NSString *)message;

+ (SdkAuthErrorUserInfo *)errorInfoForEmptyTenantTokenError;

@end
