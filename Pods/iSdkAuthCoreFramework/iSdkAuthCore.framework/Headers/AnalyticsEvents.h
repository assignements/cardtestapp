#ifndef AnalyticsEvents
#define AnalyticsEvents

static NSString *const  EVENT_APL_Resource_Initiated = @"APL_Resource_Initiated";
static NSString *const  EVENT_APL_Resource_Created = @"APL_Resource_Created";
static NSString *const  EVENT_APL_Resource_Failed = @"APL_Resource_Failed";
static NSString *const  EVENT_APL_Session_Initiated = @"APL_Session_Initiated";
static NSString *const  EVENT_APL_Session_Created = @"APL_Session_Created";
static NSString *const  EVENT_APL_Session_Failed = @"APL_Session_Failed";

#endif /* AnalyticsEvents */
