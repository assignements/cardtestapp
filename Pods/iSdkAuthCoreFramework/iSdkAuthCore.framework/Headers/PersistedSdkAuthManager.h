#import <Foundation/Foundation.h>
#import "SdkAuthSession.h"
#import "SdkAuthErrorUserInfo.h"
#import "AuthStore.h"
#import "TimeProvider.h"
#import "GetAuthSessionBlockAlias.h"
#import "ApolloSdkAuthStore.h"
#import <ApolloAnalyticsCore/ApolloAnalyticsCore.h>
#import <CipherSecureStore/CipherSecureStoreProtocol.h>
#import <ApolloCommonsCore/ApolloMacros.h>

typedef NS_ENUM(NSUInteger, PersistedSdkAuthEvent) {
  PersistedSdkAuthEventResourceCreationSuccessful,
  PersistedSdkAuthEventResourceCreationFailure,
  PersistedSdkAuthEventSessionCreationSuccessful,
  PersistedSdkAuthEventSessionCreationFailure
};

@protocol PersistedSdkAuthManagerListener;
@protocol PersistedSdkAuthManagerDelegate;
@interface PersistedSdkAuthManager : NSObject

/**
 This returns nil in case token has expired
 Use getAuthSessionWithSuccessBlock:failureBlock: to make sure session is renewed in case of expiry
 */
@property (nonatomic, readonly) SdkAuthSession *apolloSdkAuthSession;

@property (nonatomic, readonly) ApolloSdkAuthStore *sdkAuthStore;

@property (nonatomic, weak) id<PersistedSdkAuthManagerListener> eventListener;

@property (nonatomic, weak) id<PersistedSdkAuthManagerDelegate> delegate;

@property (nonatomic) id<CipherSecureStore> secureStore;
/**
Use preferred init method
initWithBaseURL:userAgent:tenantID:appID:sdkName:projectId:platform:version:apiKey:authID:
 */
- (instancetype)initWithAuthStore:(id<AuthStore>)authStore
                     timeProvider:(id<TimeProvider>)timeProvider
                          baseURL:(NSURL *)url
                        userAgent:(NSString *)userAgent
                         tenantID:(NSNumber *)tenantID
                       storeAppID:(NSString *)storeAppID
                          sdkName:(NSString *)sdkName
                           apiKey:(NSString *)apiKey
                           authID:(NSString *)authID;
  
/**
 Use preferred init method
 initWithBaseURL:userAgent:tenantID:appID:sdkName:projectId:platform:version:apiKey:authID:
 */
- (instancetype)initWithAuthStore:(id<AuthStore>)authStore
                          baseURL:(NSURL *)url
                        userAgent:(NSString *)userAgent
                         tenantID:(NSNumber *)tenantID
                       storeAppID:(NSString *)storeAppID
                          sdkName:(NSString *)sdkName
                           apiKey:(NSString *)apiKey
                           authID:(NSString *)authID;

/**
 If any sdk has been using this method to initialize, it can continue using this method to make sure data is properly
 migrated for already authenticated users to the new auth id scoped storage.
 */
- (instancetype)initWithAuthStore:(id<AuthStore>)authStore
                          baseURL:(NSURL *)url
                        userAgent:(NSString *)userAgent
                         tenantID:(NSNumber *)tenantID
                            appID:(NSString *)storeAppID
                          sdkName:(NSString *)sdkName
                        projectId:(NSString *)projectId
                         platform:(NSString *)platform
                          version:(NSString *)version
                           apiKey:(NSString *)apiKey
                           authID:(NSString *)authID;

/**
 Preferred init method
 */
- (instancetype)initWithBaseURL:(NSURL *)baseURL
                      userAgent:(NSString *)userAgent
                       tenantID:(NSNumber *)tenantID
                          appID:(NSString *)storeAppID
                        sdkName:(NSString *)sdkName
                      projectId:(NSString *)projectId
                       platform:(NSString *)platform
                        version:(NSString *)version
                         apiKey:(NSString *)apiKey
                         authID:(NSString *)authID;

- (void) setAnalyticsDataCollector:(ApolloAnalyticsManager *) analyticsManager;

// RequestID is used to discard duplicate requests. Only the most recent request
// will be honoured.
- (void)authenticateUsingTenantToken:(NSString *)tenantToken
                 withQueuedRequestID:(NSString *)requestID
                        successBlock:(GetAuthSessionSuccessBlock)successBlock
                        failureBlock:(GetAuthSessionFailureBlock)failureBlock;
- (void)authenticateUsingTenantToken:(NSString *)tenantToken
                           agentInfo:(TDTAgentInfo *)agentInfo
                 withQueuedRequestID:(NSString *)requestID
                        successBlock:(GetAuthSessionSuccessBlock)successBlock
                        failureBlock:(GetAuthSessionFailureBlock)failureBlock;

/* This will only create a resource and not create a session.
   Fields related to auth session will be nil in the success callback argument.
 */
- (void)createResourceUsingTenantToken:(NSString *)tenantToken
                   withQueuedRequestID:(NSString *)requestID
                          successBlock:(GetAuthSessionSuccessBlock)successBlock
                          failureBlock:(GetAuthSessionFailureBlock)failureBlock;
- (void)createResourceUsingTenantToken:(NSString *)tenantToken
                             agentInfo:(TDTAgentInfo *)agentInfo
                   withQueuedRequestID:(NSString *)requestID
                          successBlock:(GetAuthSessionSuccessBlock)successBlock
                          failureBlock:(GetAuthSessionFailureBlock)failureBlock;

- (ZETAPublicKey *)getUserAuthPublicKey;

- (void)getUserAuthPrivateKeyWithSuccessBlock:(GetPrivateKeySuccessBlock)success
                                 failureBlock:(ZETAFailureBlockType)failure;

- (void)removeAllPersistedData;
  
- (void)getAuthSessionWithSuccessBlock:(GetAuthSessionSuccessBlock)success
                          failureBlock:(GetAuthSessionFailureBlock)failure;

@end

@protocol PersistedSdkAuthManagerListener <NSObject>

-(void)persistentSDKAuthManager:(PersistedSdkAuthManager *)authManager
                      fireEvent:(PersistedSdkAuthEvent)event;

@end

@protocol PersistedSdkAuthManagerDelegate <NSObject>

-(void)persistentSDKAuthManager:(PersistedSdkAuthManager *)authManager
  didRequestToRegisterPublicKey:(NSString *)base64EncodedPublicKey
                     forUserJID:(NSString *)userJID
                   successBlock:(nullable ZETAResponseBlockType)success
                   failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)persistentSDKAuthManager:(PersistedSdkAuthManager *_Nonnull)authManager
successfullyCreatedResourceWithKeyPair:(ZETAECKeyPair *_Nonnull)keyPair;


@end
