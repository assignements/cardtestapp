#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "ClientPublicKeyContainer.h"
#import "SdkAgentInfo.h"

@interface CreateResourceRequest: MTLModel<MTLJSONSerializing>
  
- (instancetype)initWithTenantId:(NSNumber *)tenantId
                      storeAppId:(NSString *)storeAppId
                         sdkName:(NSString *)sdkName
                          apiKey:(NSString *)apiKey
                     tenantToken:(NSString *)tenantToken
                       userAgent:(SdkAgentInfo *)userAgent
              publicKeyContainer:(ClientPublicKeyContainer *)publicKeyContainer;

- (instancetype)initWithTenantId:(NSNumber *)tenantId
                           appId:(NSString *)storeAppId
                         sdkName:(NSString *)sdkName
                          apiKey:(NSString *)apiKey
                     tenantToken:(NSString *)tenantToken
                        platform:(NSString *)platform
                         version:(NSString *)version
                       projectId:(NSString *)projectId
                       userAgent:(SdkAgentInfo *)userAgent
              publicKeyContainer:(ClientPublicKeyContainer *)publicKeyContainer;
  
  @end
