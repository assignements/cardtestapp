#import <Mantle/Mantle.h>
#import <ApolloCommonsAgentInfo/ApolloAgentInfo.h>

@interface SdkAuthErrorUserInfo : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *traceId;
@property (nonatomic, readonly) NSString *errorCode;
@property (nonatomic, readonly) NSString *errorType;
@property (nonatomic, readonly) NSString *errorMessage;
@property (nonatomic, readonly) NSDictionary *additionalInfo;

- (instancetype)initWithTraceId:(NSString *)traceId
                      errorCode:(NSString *)errorCode
                      errorType:(NSString *)errorType
                   errorMessage:(NSString *)errorMessage
                 additionalInfo:(NSDictionary *)additionalInfo;

@end
