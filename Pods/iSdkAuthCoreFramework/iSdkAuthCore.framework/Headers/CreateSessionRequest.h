#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "SdkAgentInfo.h"

@interface CreateSessionRequest: MTLModel<MTLJSONSerializing>
  
- (instancetype)initWithTenantId:(NSNumber *)tenantId
                      storeAppId:(NSString *)storeAppId
                         sdkName:(NSString *)sdkName
                          apiKey:(NSString *)apiKey
                        authData:(NSString *)authData
                       userAgent:(SdkAgentInfo *)userAgent;

- (instancetype)initWithTenantId:(NSNumber *)tenantId
appId:(NSString *)storeAppId
   sdkName:(NSString *)sdkName
    apiKey:(NSString *)apiKey
  authData:(NSString *)authData
platform:(NSString *)platform
  version:(NSString *)version
projectId:(NSString *)projectId
 userAgent:(SdkAgentInfo *)userAgent;
  
  @end
