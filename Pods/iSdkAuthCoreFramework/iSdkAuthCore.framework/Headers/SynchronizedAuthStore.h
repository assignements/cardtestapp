#import <Foundation/Foundation.h>
#import "AuthStore.h"

@interface SynchronizedAuthStore : NSObject

- (instancetype)initWithAuthStore:(id<AuthStore>)authStore;

- (void)setData:(NSData *)data key:(NSString *)key;

- (NSData *)dataForKey:(NSString *)key;

- (void)removeDataForKey:(NSString *)key;

- (void)removeAllData;

@end
