#import <Mantle/Mantle.h>
#import <ApolloCommonsAgentInfo/ApolloAgentInfo.h>

@interface SdkAgentInfo : MTLModel<MTLJSONSerializing>

- (instancetype)initWithAgentInfo:(TDTAgentInfo *)agentInfo;

@property (nonatomic, readonly) NSString *deviceType;
@property (nonatomic, readonly) NSString *os;
@property (nonatomic, readonly) NSString *appVersion;
@property (nonatomic, readonly) NSString *OSVersion;
@property (nonatomic, readonly) NSString *appName;

@end
