#import <Foundation/Foundation.h>
#import "AuthStore.h"
#import "TimeProvider.h"

FOUNDATION_EXPORT NSString *const TimeSyncKeyFormat;

@interface ApolloSdkAuthStore : NSObject <AuthStore, TimeProvider>

- (instancetype)initWithAuthID:(NSString *)authID;

@end
