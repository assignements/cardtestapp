#import <Foundation/Foundation.h>

@protocol AuthStore <NSObject>

- (void)setData:(NSData *)data key:(NSString *)key;
  
- (NSData *)dataForKey:(NSString *)key;
  
- (void)removeDataForKey:(NSString *)key;

- (void)removeAllData;

@end
