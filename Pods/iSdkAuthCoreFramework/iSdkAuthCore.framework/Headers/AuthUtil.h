#import <Foundation/Foundation.h>
#import <ApolloCoreCrypto/ZETAErasablePrivateKey.h>
#import <ApolloCoreCrypto/ZETAPublicKey.h>
#import "SdkAuthErrorUserInfo.h"

@interface AuthUtil : NSObject

+ (NSString *)generateAuthDataUsingUserSessionJID:(NSString *)userSessionJID
                                     sessionNonce:(NSNumber *)sessionNonce
                                   userPrivateKey:(ZETAErasablePrivateKey *)privateKey
                               andServerPublicKey:(ZETAPublicKey *)publicKey;

+ (NSString *)generateAuthDataUsingUserSessionJID:(NSString *)userSessionJID
                                     sessionNonce:(NSNumber *)sessionNonce
                                     sharedSecret:(NSData *)sharedSecret;

+ (ZETAPublicKey *)parsedBase64EncodedPublicKey:(NSString *)base64EncodedPublicKey;

+ (SdkAuthErrorUserInfo *)parsedErrorUserInfoFromError:(NSError *)error;

+ (NSData *)generateSharedSecretUsingUserPrivateKey:(ZETAErasablePrivateKey *)privateKey
                                 andServerPublicKey:(ZETAPublicKey *)publicKey;

+ (BOOL)isInvalidResourceError:(NSError *)error;

@end
