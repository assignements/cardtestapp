#import <Mantle/Mantle.h>
#import <ApolloCommonsJSONParsing/ApolloJSONParsing.h>

@interface ServerTimeResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSDate *serverEpochTimeInMs;

@end
