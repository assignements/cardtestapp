#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface ClientPublicKeyContainer: MTLModel<MTLJSONSerializing>
  
- (instancetype)initWithBase64EncodedPublicKey:(NSString *)base64EncodedPublicKey
                                  andAlgorithm:(NSString *)algorithm;

@end
