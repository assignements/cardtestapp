#import <Foundation/Foundation.h>
#import "GetAuthSessionBlockAlias.h"
#import <ApolloCommonsCore/ApolloMacros.h>

@interface GetAuthPrivateKeyRequestQueueObject : NSObject

@property (nonatomic, readonly) GetPrivateKeySuccessBlock successBlock;
@property (nonatomic, readonly) ZETAFailureBlockType failureBlock;

- (instancetype)initWithSuccessBlock:(GetPrivateKeySuccessBlock)successBlock
                        failureBlock:(ZETAFailureBlockType)failureBlock;

@end

