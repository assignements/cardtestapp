#import <Foundation/Foundation.h>
#import <ApolloCommonsAgentInfo/ApolloAgentInfo.h>
#import "SdkAuthSession.h"
#import "SdkAuthErrorUserInfo.h"
#import "GetAuthSessionBlockAlias.h"
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import <ApolloAnalyticsCore/ApolloAnalyticsCore.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SdkAuthManagerDelegate;

@interface SdkAuthManager : NSObject

@property (nonatomic, weak) id<SdkAuthManagerDelegate> delegate;
@property (nonatomic, readonly) BOOL isAuthenticationInProgress;

- (instancetype)initWithBaseURL:(NSURL *)url
                      userAgent:(NSString *)userAgent
                       tenantID:(NSNumber *)tenantID
                     storeAppID:(NSString *)storeAppID
                        sdkName:(NSString *)sdkName
                         apiKey:(NSString *)apiKey;

- (instancetype)initWithBaseURL:(NSURL *)url
                      userAgent:(NSString *)userAgent
                       tenantID:(NSNumber *)tenantID
                          appID:(NSString *)storeAppID
                        sdkName:(NSString *)sdkName
                      projectId:(NSString *)projectId
                       platform:(NSString *)platform
                        version:(NSString *)version
                         apiKey:(NSString *)apiKey;


- (void) setAnalyticsDataCollector:(ApolloAnalyticsManager *) analyticsManager;

/*
 API 'generateAuthTokenUsingTenantToken:agentInfo' generates KeyPair and
 auth data internally using sessionNonce as 1 for the first time.
 For subsequent authToken generate, generateAuthTokenUsingAuthData:agentInfo Api should be used
 with incremented (by 1 atleast) sessionNonce everytime.
 */

- (void)generateAuthTokenUsingTenantToken:(NSString *)tenantToken
                                agentInfo:(TDTAgentInfo *)agentInfo;

- (void)generateAuthTokenUsingAuthData:(NSString *)authData
                             nonceUsed:(NSNumber *)nonce
                             agentInfo:(TDTAgentInfo *)agentInfo;

- (void)createResourceUsingTenantToken:(NSString *)tenantToken
                             agentInfo:(TDTAgentInfo *)agentInfo
                          successBlock:(GetAuthSessionSuccessBlock)successBlock
                          failureBlock:(GetAuthSessionFailureBlock)failureBlock;

- (void)syncTimeWithSuccessBlock:(ZETAResponseBlockType)successBlock
                    failureBlock:(ZETAFailureBlockType)failureBlock;

@end

@protocol SdkAuthManagerDelegate <NSObject>

- (void)sdkAuthManager:(SdkAuthManager *)sdkAuthManager
    didGenerateSession:(SdkAuthSession *)authSession;

- (void)sdkAuthManager:(SdkAuthManager *)sdkAuthManager
didFailToGenerateSessionWithError:(NSError *)error
        additionalInfo:(SdkAuthErrorUserInfo *)additionalInfo;


/*
 API 'sdkAuthManager:didReceiveResourceError:additionalInfo'
 is invoked when 'invalid resource' or 'resource not found' error is received.
 This error can be triggered only when performing auth using authData.
 It usually means the resource previously created has expired or it does not exist
 and to rectify this, consumer module/app needs to reinitiate entire flow using tenantAuthToken.
 */

- (void)sdkAuthManager:(SdkAuthManager *)sdkAuthManager
didReceiveResourceError:(NSError *)error
        additionalInfo:(SdkAuthErrorUserInfo *)additionalInfo;

/*
 API 'sdkAuthManager:didReceiveTenantAuthFailure:additionalInfo' is invoked
 when sdk is unable to validate tenantAuthToken. It usually happens when tenant auth token is invalid
 or if auth config configured in apollo framework is not in sync with auth token supplied.
 */

- (void)sdkAuthManager:(SdkAuthManager *)sdkAuthManager
didReceiveTenantAuthFailure:(NSError *)error
        additionalInfo:(SdkAuthErrorUserInfo *)additionalInfo;

- (void)sdkAuthManager:(SdkAuthManager *)sdkAuthManager
successfullyCreatedResourceWithKeyPair:(ZETAECKeyPair *)keyPair;

@end


NS_ASSUME_NONNULL_END
