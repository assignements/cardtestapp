#import <Foundation/Foundation.h>

/**
 PersistedSdkAuthManager needs to validate existing auth session info against current time.
 Any consumer of this module can provide their own implementation of TimeProvider
 (preferably server time to avoid issues around incorrect device time). Any custom
 
 **/

@protocol TimeProvider <NSObject>
  
- (NSDate *)serverAdjustedCurrentDate;

@end
