#import "SdkAuthSession.h"
#import "SdkAuthErrorUserInfo.h"

typedef void (^GetAuthSessionSuccessBlock)(SdkAuthSession *session);
typedef void (^GetAuthSessionFailureBlock)(NSError *error, SdkAuthErrorUserInfo *userInfo);
typedef void (^GetPrivateKeySuccessBlock)(ZETAErasablePrivateKey *privateKey);

