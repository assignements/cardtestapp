#import <Foundation/Foundation.h>
#import "GetAuthSessionBlockAlias.h"

@interface GetAuthSessionRequestQueueObject : NSObject

@property (nonatomic, readonly) NSString *requestID;
@property (nonatomic, readonly) GetAuthSessionSuccessBlock successBlock;
@property (nonatomic, readonly) GetAuthSessionFailureBlock failureBlock;

- (instancetype)initWithRequestID:(NSString *)requestID
                     successBlock:(GetAuthSessionSuccessBlock)successBlock
                     failureBlock:(GetAuthSessionFailureBlock)failureBlock;

@end

