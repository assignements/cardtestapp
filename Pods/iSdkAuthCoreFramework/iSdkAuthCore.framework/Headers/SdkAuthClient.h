#import <Foundation/Foundation.h>
#import <ApolloHttpClient/ZETABaseHTTPSessionClient.h>
#import "SdkAgentInfo.h"
#import <ApolloAnalyticsCore/ApolloAnalyticsCore.h>

typedef NS_ENUM(NSUInteger, SdkAuthClientErrorCode) {
  SdkAuthClientErrorCodeUnknown,
  SdkAuthClientErrorCodeUnauthorized
};

@interface SdkAuthClient : ZETABaseHTTPSessionClient


- (instancetype)initWithBaseURL:(NSURL *)url
                andUserAgent:(NSString *)userAgent;
  
  - (void)createResourceForTenantID:(NSNumber *)tenantID
                         storeAppID:(NSString *)storeAppID
                            sdkName:(NSString *)sdkName
                             apiKey:(NSString *)apiKey
                    tenantAuthToken:(NSString *)tenantAuthToken
                          userAgent:(SdkAgentInfo *)userAgent
             base64EncodedPublicKey:(NSString *)base64EncodedPublicKey
                          algorithm:(NSString *)algorithm
                            success:(ZETAResponseBlockType)success
                            failure:(ZETAFailureBlockType)failure;
  
- (void)createSessionForTenantID:(NSNumber *)tenantID
                      storeAppID:(NSString *)storeAppID
                         sdkName:(NSString *)sdkName
                          apiKey:(NSString *)apiKey
                        authData:(NSString *)authData
                       userAgent:(SdkAgentInfo *)userAgent
                         success:(ZETAResponseBlockType)success
                         failure:(ZETAFailureBlockType)failure;

- (void)createResourceForTenantID:(NSNumber *)tenantID
                            appID:(NSString *)storeAppID
                          sdkName:(NSString *)sdkName
                           apiKey:(NSString *)apiKey
                  tenantAuthToken:(NSString *)tenantAuthToken
                        projectId:(NSString *)projectId
                         platform:(NSString *)platform
                          version:(NSString *)version
                        userAgent:(SdkAgentInfo *)userAgent
           base64EncodedPublicKey:(NSString *)base64EncodedPublicKey
                        algorithm:(NSString *)algorithm
                          success:(ZETAResponseBlockType)success
                          failure:(ZETAFailureBlockType)failure;

- (void)createSessionForTenantID:(NSNumber *)tenantID
                           appID:(NSString *)storeAppID
                         sdkName:(NSString *)sdkName
                          apiKey:(NSString *)apiKey
                        authData:(NSString *)authData
                       userAgent:(SdkAgentInfo *)userAgent
                       projectId:(NSString *)projectId
                        platform:(NSString *)platform
                         version:(NSString *)version
                         success:(ZETAResponseBlockType)success
                         failure:(ZETAFailureBlockType)failure;

- (void)syncTimeWithSuccessBlock:(ZETAResponseBlockType)success
                    failureBlock:(ZETAFailureBlockType)failure;

- (void)setAnalyticsManager:(ApolloAnalyticsManager *)analyticsManager;

@end
