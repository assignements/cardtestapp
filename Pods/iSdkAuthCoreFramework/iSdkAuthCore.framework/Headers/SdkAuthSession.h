#import <Foundation/Foundation.h>
#import <ApolloCoreCrypto/ZETAErasablePrivateKey.h>
#import <ApolloCoreCrypto/ZETAPublicKey.h>
#import <ApolloCoreCrypto/ZETAECKeyPair.h>
#import <Mantle/Mantle.h>

/**
 
 NOTE: Properties 'resourceValidity', 'clientKeyPair', 'serverPublicKey' will be null if
 session is being created using authData.
 These are only available when session creation is done using tenantAuthToken
 
 **/

@interface SdkAuthSession: NSObject<NSCoding>

@property (nonatomic, readonly) NSString *userSessionJid;
@property (nonatomic, readonly) NSString *sessionId;
@property (nonatomic, readonly) NSString *authToken;
@property (nonatomic, readonly) NSDate *authTokenValidity;
@property (nonatomic, readonly) NSNumber *nonce;

/**
 resourceValidity: It tells for how long previously created resource is valid. If
 its still valid, then session can be created using authData. If it has expired, then
 session needs to be created using tenantAuthToken
 **/

@property (nonatomic, readonly) NSString *apolloUserSessionId;
@property (nonatomic, readonly) NSString *resourceJid;
@property (nonatomic, readonly) NSDate *resourceValidity;
@property (nonatomic) ZETAErasablePrivateKey *clientPrivateKey;
@property (nonatomic) ZETAPublicKey *clientPublicKey;
@property (nonatomic, readonly) ZETAPublicKey *serverPublicKey;
@property (nonatomic, readonly) NSString *resourcePublicKeyCert;
// TODO: parse above resourcePublicKeyCert into proper object.

- (instancetype)initWithUserSessionJid:(NSString *)userSessionJid
                             sessionId:(NSString *)sessionId
                             authToken:(NSString *)authToken
                     authTokenValidity:(NSDate *)authTokenValidity
                                 nonce:(NSNumber *)nonce
                           resourceJid:(NSString *)resourceJid
                      resourceValidity:(NSDate *)resourceValidity
                       serverPublicKey:(ZETAPublicKey *)serverPublicKey
                 resourcePublicKeyCert:(NSString *)resourcePublicKeyCert;

- (instancetype)initWithUserSessionJid:(NSString *)userSessionJid
                             sessionId:(NSString *)sessionId
                             authToken:(NSString *)authToken
                     authTokenValidity:(NSDate *)authTokenValidity
                                 nonce:(NSNumber *)nonce
                           resourceJid:(NSString *)resourceJid
                      resourceValidity:(NSDate *)resourceValidity
                       serverPublicKey:(ZETAPublicKey *)serverPublicKey
                 resourcePublicKeyCert:(NSString *)resourcePublicKeyCert
                   apolloUserSessionId:(NSString *)apolloUserSessionId;

/**
 Use this method when only resource data is available and session is not created.
 **/
- (instancetype)initWithResourceJid:(NSString *)resourceJid
                   resourceValidity:(NSDate *)resourceValidity
                    serverPublicKey:(ZETAPublicKey *)serverPublicKey
              resourcePublicKeyCert:(NSString *)resourcePublicKeyCert;

- (instancetype)initWithResourceJid:(NSString *)resourceJid
                   resourceValidity:(NSDate *)resourceValidity
                    serverPublicKey:(ZETAPublicKey *)serverPublicKey
              resourcePublicKeyCert:(NSString *)resourcePublicKeyCert
                apolloUserSessionId:(NSString *)apolloUserSessionId;

+ (instancetype)initializeFromData:(NSData *)data;

- (NSData *)data;

- (void)updateSessionDetailsFromNewAuthSession:(SdkAuthSession *)newSession;

- (void)clearSessionDetails;

- (BOOL)isExistingAuthResourceValidForCurrentTime:(NSDate *)date;

- (BOOL)isExistingAuthSessionValidForCurrentTime:(NSDate *)date;

- (void)incrementNonce;

- (void)updateAuthToken:(NSString *)authToken;
@end
