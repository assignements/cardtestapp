#import "ZETAKVStoreHelper.h"
#import <ApolloCollectionsCore/ZETAUserProfile.h>
#import <ApolloCollectionsCore/ZETAUserProfileConfig.h>

@interface ZETAKVStoreHelper (UserProfileAdditions)

- (ZETAUserProfile *)getCurrentUserProfile;
- (ZETAUserProfileConfig *)getUserProfileConfig;
- (void)setCurrentUserProfile:(ZETAUserProfile *)userProfile;
- (void)setUserProfileConfig:(ZETAUserProfileConfig *)userProfileConfig;

@end
