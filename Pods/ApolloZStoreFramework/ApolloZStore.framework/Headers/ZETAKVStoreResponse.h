#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ZETAKVStoreErrorCode) {
  ZETAKVStoreErrorCodeUnknownAction,
  ZETAKVStoreErrorCodeInvalidArgs,
  ZETAKVStoreErrorCodeUnknownKey,
  ZETAKVStoreErrorCodeInvalidScope
};

@interface ZETAKVStoreResponse : NSObject

@property (nonatomic, readonly) id value;
@property (nonatomic, readonly) NSError *error;

- (instancetype)initWithValue:(id)value;
- (instancetype)initWithError:(NSError *)error;

@end
