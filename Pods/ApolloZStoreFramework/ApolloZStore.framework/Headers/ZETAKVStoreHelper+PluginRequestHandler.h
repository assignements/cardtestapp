#import "ZETAKVStoreHelper.h"

@interface ZETAKVStoreHelper (PluginRequestHandler)

- (id)handleAction:(NSString *)action
              args:(NSDictionary *)args
contextLevelKVStore:(NSDictionary **)contextLevelKVStore;

@end
