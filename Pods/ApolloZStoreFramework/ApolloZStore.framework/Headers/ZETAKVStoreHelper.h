#import <Foundation/Foundation.h>
#import "ZETAKVStoreResponse.h"
#import <Mantle/Mantle.h>
#import <ApolloUserStore/ZETAApolloUserStore.h>

FOUNDATION_EXPORT NSString *const KVStore;
FOUNDATION_EXPORT NSString *const KVStoreGetValueAction;
FOUNDATION_EXPORT NSString *const KVStoreSetValueAction;
FOUNDATION_EXPORT NSString *const KVStoreRemoveKeyAction;
FOUNDATION_EXPORT NSString *const KVStoreRemoveAllKeysAction;
FOUNDATION_EXPORT NSString *const KVStoreKey;
FOUNDATION_EXPORT NSString *const KVStoreValue;
FOUNDATION_EXPORT NSString *const KVStoreScope;
FOUNDATION_EXPORT NSString *const ZETAKVStoreErrorDomain;

typedef NS_ENUM(NSUInteger, ZETAKVStoreScope) {
  ZETAKVStoreUnknownScope,
  ZETAKVStoreApplicationScope,
  ZETAKVStorePersistentScope,
  ZETAKVStoreContextScope
};

#pragma mark - GetValueInterface

@interface ZETAKVStoreGetValueRequest : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly, nonnull) NSString *key;
@property (nonatomic, readonly) ZETAKVStoreScope scope;

- (instancetype)initWithKey:(NSString *)key
                      scope:(ZETAKVStoreScope)scope;

@end

#pragma mark - SetValueInterface

@interface ZETAKVStoreSetValueRequest : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly, nonnull) NSString *key;
@property (nonatomic, readonly, nonnull) id value;
@property (nonatomic, readonly) ZETAKVStoreScope scope;

- (instancetype)initWithKey:(NSString *)key
                      value:(id)value
                      scope:(ZETAKVStoreScope)scope;

@end

#pragma mark - RemoveKeyInterface

@interface ZETAKVStoreRemoveKeyRequest : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly, nonnull) NSString *key;
@property (nonatomic, readonly) ZETAKVStoreScope scope;

- (instancetype)initWithKey:(NSString *)key
                      scope:(ZETAKVStoreScope)scope;

@end

#pragma mark - RemoveAllKeysInterface

@interface ZETAKVStoreRemoveAllKeysRequest : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) ZETAKVStoreScope scope;

- (instancetype)initWithScope:(ZETAKVStoreScope)scope;

@end


@interface ZETAKVStoreHelper : NSObject

+ (ZETAKVStoreHelper *)sharedInstance;

+ (void)setupSharedInstanceWithApolloUserStore:(ZETAApolloUserStore *)apolloUserStore;

+ (void)reset;

+ (ZETAKVStoreScope)getTransformedScope:(NSString *)scope;

- (ZETAKVStoreResponse *)handleSetValueAction:(ZETAKVStoreSetValueRequest *)request
                          contextLevelKVStore:(NSDictionary **)contextLevelKVStore;

- (ZETAKVStoreResponse *)handleGetValueAction:(ZETAKVStoreGetValueRequest *)request
                          contextLevelKVStore:(NSDictionary **)contextLevelKVStore;

- (ZETAKVStoreResponse *)handleRemoveKeyAction:(ZETAKVStoreRemoveKeyRequest *)request
                           contextLevelKVStore:(NSDictionary **)contextLevelKVStore;

- (ZETAKVStoreResponse *)handleRemoveAllKeysAction:(ZETAKVStoreRemoveAllKeysRequest *)request
                               contextLevelKVStore:(NSDictionary **)contextLevelKVStore;

@end

