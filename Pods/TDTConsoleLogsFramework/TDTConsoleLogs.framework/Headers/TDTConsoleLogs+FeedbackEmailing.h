#import "TDTConsoleLogs.h"
#import <UIKit/UIKit.h>

@interface TDTConsoleLogs (FeedbackEmailing)

/**
 Present an Mail Composer view on @p viewController, with @p emailAddress
 and a subject line prefilled, and gzipped copies of the receiver's log
 files attached if @p attachLogs is true.

 The subject line is derived from @p productName and the @p CFBundleVersion.

 @note This method will silently return if
 `[MFMailComposeViewController canSendMail]` returns NO.
 */
- (void)presentMailClientOnViewController:(UIViewController *)viewController
                             emailAddress:(NSString *)emailAddress
                               attachLogs:(BOOL)attachLogs;

/**
 Present a Mail Composer view on @p viewController, with @p emailAddress, 
 @p emailSubject and @p emailBody prefilled, and gzipped copies of the receiver's
 log files attached if @p attachLogs is true.
 
 @note This method will silently return if
 `[MFMailComposeViewController canSendMail]` returns NO.
 */
- (void)presentMailClientOnViewController:(UIViewController *)viewController
                             emailAddress:(NSString *)emailAddress
                             emailSubject:(NSString *)emailSubject
                                emailBody:(NSString *)emailBody
                               attachLogs:(BOOL)attachLogs;

@end
