#import <Foundation/Foundation.h>

/**
 TDTConsoleLogs are a pair of files (last, current) obtained by
 capturing stderr on an roughly per day basis.
 */
@interface TDTConsoleLogs : NSObject

/**
 @p productName is used when naming the log files.
 */
- (instancetype)initWithProductName:(NSString *)productName;

/**
 Redirect @p stderr to the files managed by the receiver.
 */
- (void)consumeStandardError;

/**
 Provide a hint to the receiver that the date might have changed.
 */
- (void)maybeRollover;

@property (nonatomic, readonly) NSString *productName;

/// List of log paths
- (NSArray *)logPaths;

/// String contents of the path
/// @param path of the file
- (NSString *)contentsForPath:(NSString *)path;

/// file name => string contents
- (NSDictionary *)fileContents;

/// file name => gzipped contents
- (NSDictionary *)gzippedFileContents;

@end
