#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^ApolloAnalyticsSubscriberNotificationType)(__weak __nullable id weakSelf,
                                                          NSString *__nonnull eventName,
                                                          NSDictionary *__nonnull eventData);

@interface NSObject (AnalyticsObserver)

/**
 Invokes `param` block when any analytics event is fired by the ApolloAnalyticsManager.
 Invoke zeta_removeAllObservations in dealloc of the object.
 */
- (void)apollo_observeAnalyticsWithBlock:(nonnull ApolloAnalyticsSubscriberNotificationType)block;

@end

NS_ASSUME_NONNULL_END
