#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class ApolloAnalyticsManager;

@protocol ApolloAnalyticsClient<NSObject>

- (NSString *)uniqueId;
- (void)recordEvent:(NSDictionary *)dictionary;
- (void)recordEvent:(NSDictionary *)dictionary immediate:(BOOL)immediate;
- (void)flush;

@optional
- (void)attachedToManager:(ApolloAnalyticsManager *)manager withService:(NSString *)serviceName;
- (void)detachedFromManager:(ApolloAnalyticsManager *)manager withService:(NSString *)serviceName;
- (void)apolloAnalyticsManager:(ApolloAnalyticsManager *)manager
         setUserPropertyForKey:(NSString *)key
                     withValue:(NSString *)value;
- (void)logout;

@end

@interface ApolloAnalyticsManager : NSObject

@property (nonatomic, readonly) NSString *installationID;
@property (nonatomic, readonly) NSString *userJID;

- (instancetype)initWithTenantID:(NSNumber *)tenantID
                       projectID:(NSString *)projectID
                           appID:(NSString *)appID
                         sdkName:(NSString *)sdkName
                          authID:(NSString *)authID;

- (void)setInstallationID:(NSString *)installationID;
- (void)setUserJID:(NSString *)userJID;

- (void)setUserPropertyForKey:(nonnull NSString *)key
                    withValue:(nonnull NSString *)value;

- (void)logout;

- (void)logEventWithName:(nonnull NSString *)eventName;
- (void)logEventWithName:(nonnull NSString *)eventName
               immediate:(BOOL)immediate;
- (void)logEventWithName:(nonnull NSString *)eventName
                  params:(nonnull NSDictionary *)params;
- (void)logEventWithName:(nonnull NSString *)eventName
                  params:(nonnull NSDictionary *)params
               immediate:(BOOL)immediate;

- (void)addDefaultParams:(NSDictionary *)defaultParams;

- (nullable NSString *)addClient:(id<ApolloAnalyticsClient>)client;
- (void)removeClient:(id<ApolloAnalyticsClient>)client;

- (void)startSession;

- (void)stopSession;

@end

NS_ASSUME_NONNULL_END
