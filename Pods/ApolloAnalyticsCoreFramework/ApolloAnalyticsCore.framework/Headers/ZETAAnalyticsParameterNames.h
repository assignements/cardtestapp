static NSString *const ZETAAnalyticsStatusPropertyName = @"status";
static NSString *const ZETAAnalyticsErrorTypePropertyName = @"errorType";
static NSString *const ZETAAnalyticsErrorMessagePropertyName = @"errorMessage";
static NSString *const ZETAAnalyticsServiceNamePropertyName = @"serviceName";
static NSString *const ZETAAnalyticsMethodNamePropertyName = @"methodName";
static NSString *const ZETAAnalyticsURLPropertyName = @"url";
