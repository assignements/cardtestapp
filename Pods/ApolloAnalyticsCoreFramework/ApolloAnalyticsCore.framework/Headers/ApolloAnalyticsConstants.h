static NSString *const ApolloAnalyticsEventName = @"eventName";
static NSString *const ApolloAnalyticsSdkNameParam = @"sdkName";
static NSString *const ApolloAnalyticsInstallationIDParam = @"installationId";
static NSString *const ApolloAnalyticsUserJIDParam = @"userJid";
static NSString *const ApolloAnalyticsTenantIDParam = @"tenantId";
static NSString *const ApolloAnalyticsProjectIDParam = @"projectId";
static NSString *const ApolloAnalyticsAppIDParam = @"appId";
static NSString *const ApolloAnalyticsPlatformParam = @"platform";


static NSString *const ApolloAnalyticsDefaultSdkName = @"screensauth";

static NSString *const ExternalAnalyticsSubscriptionNotificationName = @"AnalyticsSubscriptionNotification";
static NSString *const ExternalAnalyticsSubscriptionNotificationParamEventName = @"AnalyticsSubscriptionEventName";
static NSString *const ExternalAnalyticsSubscriptionNotificationParamEventParams = @"AnalyticsSubscriptionEventParams";
