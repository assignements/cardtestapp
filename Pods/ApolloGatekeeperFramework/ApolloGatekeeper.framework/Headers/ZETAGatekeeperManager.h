#import <Foundation/Foundation.h>
#import <ApolloCommonsAgentInfo/ApolloAgentInfo.h>
#import <ApolloCommonsMiscellaneous/ApolloMiscellaneous.h>
#import "ZETAGatekeeperClient.h"
#import <ApolloUserStore/ZETAApolloUserStore.h>

FOUNDATION_EXPORT NSString *const ZETAGatekeeperDidUpdateNotification;
FOUNDATION_EXPORT NSString *const ZETAGatekeeperDidUpdateNotificationKeyGatekeeperName;
FOUNDATION_EXPORT NSString *const ZETAGatekeeperDidUpdateNotificationKeyNewValue;

@protocol ZETAGatekeeperManagerDelegate;

@interface ZETAGatekeeperManager : NSObject

@property (nonatomic, weak) id<ZETAGatekeeperManagerDelegate> delegate;

- (instancetype)initWithGatekeeperClient:(ZETAGatekeeperClient *)client
                                  listID:(NSString *)listID
                         apolloUserStore:(ZETAApolloUserStore *)apolloUserStore
                               agentInfo:(TDTAgentInfo *)agentInfo;

- (BOOL)isGateOpenForGatekeeper:(NSString *)gatekeeper;
- (void)updateGatekeepers;
- (void)updateGatekeepersWithSuccess:(ZETAEmptyBlockType)success
                             failure:(ZETAFailureBlockType)failure;

@end

@protocol ZETAGatekeeperManagerDelegate <NSObject>

- (BOOL)gatekeeperManager:(ZETAGatekeeperManager *)gatekeeperManager
didRequestDefaultValueForGatekeeper:(NSString *)gatekeeper;

- (void)gatekeeperManagerDidFinishLoading:(ZETAGatekeeperManager *)gatekeeperManager version:(NSInteger)version;

@end
