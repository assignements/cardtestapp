#import <ApolloCommonsCore/ApolloMacros.h>
#import <ApolloDoorStream/ZETABaseDoorServiceClient.h>
#import "ZETAGetGatekeeperListRequestPayload.h"

FOUNDATION_EXPORT NSString *const ZETAGatekeeperClientErrorDomain;

@interface ZETAGatekeeperClient : ZETABaseDoorServiceClient

- (void)getGatekeeperListForRequestPayload:(ZETAGetGatekeeperListRequestPayload *)requestPayload
                        requestUniquingTag:(NSString *)requestUniquingTag
                                   success:(ZETAResponseBlockType)success
                                   failure:(ZETAFailureBlockType)failure;

@end
