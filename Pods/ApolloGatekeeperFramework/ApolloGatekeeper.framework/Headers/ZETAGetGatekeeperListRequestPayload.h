#import <Mantle/Mantle.h>
#import <ApolloCommonsAgentInfo/ApolloAgentInfo.h>

@interface ZETAGatekeeperUserAgentInfo : MTLModel<MTLJSONSerializing>

- (instancetype)initWithAgentInfo:(TDTAgentInfo *)agentInfo;

@property (nonatomic, readonly) NSString *deviceType;
@property (nonatomic, readonly) NSString *OS;
@property (nonatomic, readonly) NSString *appVersion;
@property (nonatomic, readonly) NSString *OSVersion;
@property (nonatomic, readonly) NSString *buildNumber;
@property (nonatomic, readonly) NSString *deviceModel;
@property (nonatomic, readonly) NSString *carrier;
@property (nonatomic, readonly) NSString *networkType;
@property (nonatomic, readonly) NSString *locale;
@property (nonatomic, readonly) NSString *deviceManufacturer;
@property (nonatomic, readonly) NSString *appID;

@end

@interface ZETAGetGatekeeperListRequestPayload : MTLModel<MTLJSONSerializing>

- (instancetype)initWithListID:(NSString *)listID
                     agentInfo:(TDTAgentInfo *)agentInfo;

@property (nonatomic, readonly) NSString *listID;
@property (nonatomic, readonly) ZETAGatekeeperUserAgentInfo *userAgent;

@end
