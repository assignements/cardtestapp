#import <Foundation/Foundation.h>

typedef void (^ZETAGatekeeperNotificationBlockType)(__weak __nullable id weakSelf, BOOL isGateOpen);
typedef void (^ZETAMultipleGatekeeperNotificationBlockType)(__weak __nullable id weakSelf,
                                                            NSString *__nullable gatekeeperName,
                                                            BOOL isGateOpen);

@interface NSObject (MTKObserving_ZETAGatekeeperNotification)

- (void)zeta_observeGatekeeper:(nonnull NSString *)gatekeeper
         withNotificationBlock:(nonnull ZETAGatekeeperNotificationBlockType)block;

- (void)zeta_observeGatekeepers:(nonnull NSArray<NSString *> *)gatekeeper
          withNotificationBlock:(nonnull ZETAMultipleGatekeeperNotificationBlockType)block;

@end
