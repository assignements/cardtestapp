#import <Mantle/Mantle.h>

@interface ZETAGetGatekeeperListResponsePayload : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString* listID;
@property (nonatomic, readonly) NSDictionary* gatekeeperStates;

- (instancetype)initWithListID:(NSString*)listID gatekeeperStates:(NSDictionary*)gatekeeperStates;

@end
