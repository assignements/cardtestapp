
#import <Foundation/Foundation.h>
#import "ApolloSDKConfigModel.h"
#import <ApolloCollectionsCore/ZETACollectionClientProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloSDKConfigModule : NSObject

+ (instancetype)sharedInstance;

+ (void)resetSharedInstance;

- (void)setupWithSdkName:(NSString *)sdkName
        collectionClient:(id<ZETACollectionClientProtocol>)collectionClient;

- (void)setupWithSdkName:(NSString *)sdkName
                  bundle:(NSBundle *)sdkBundle
        collectionClient:(id<ZETACollectionClientProtocol>)collectionClient;

- (NSDictionary *)getSDKConfigForSDKName:(NSString *)sdkName;


@end

NS_ASSUME_NONNULL_END
