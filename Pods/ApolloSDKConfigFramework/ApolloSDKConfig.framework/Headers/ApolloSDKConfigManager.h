
#import <Foundation/Foundation.h>
#import "ApolloSDKConfigModel.h"
#import "ApolloSDKConfigStore.h"
#import <ApolloCollectionsCore/ZETACollectionClientProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloSDKConfigManager : NSObject

-(instancetype)initWithConfigName:(NSString *)configName
                          kvStore:(ApolloSDKConfigStore *)kvStore
                 collectionClient:(id<ZETACollectionClientProtocol>)collectionClient;

-(instancetype)initWithConfigName:(NSString *)configName
                           bundle:(NSBundle *)sdkBundle
                          kvStore:(ApolloSDKConfigStore *)kvStore
                 collectionClient:(id<ZETACollectionClientProtocol>)collectionClient;

@end

NS_ASSUME_NONNULL_END
