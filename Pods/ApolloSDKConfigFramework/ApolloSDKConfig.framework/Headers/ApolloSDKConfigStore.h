
#import <Foundation/Foundation.h>

@interface ApolloSDKConfigStore : NSObject

- (void)setConfigFile:(NSDictionary *)configFile
        forConfigName:(NSString *)configName;

- (NSDictionary *)getConfigFileForConfigName:(NSString *)configName;

- (void)clearData;

@end
