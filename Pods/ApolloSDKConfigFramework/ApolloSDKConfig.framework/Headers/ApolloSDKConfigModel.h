
#import <ApolloCollectionsCore/ZETABaseCollectionItem.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloSDKConfig : ZETABaseCollectionItem

@property (nonatomic, readonly) NSDictionary *data;

@end

NS_ASSUME_NONNULL_END
