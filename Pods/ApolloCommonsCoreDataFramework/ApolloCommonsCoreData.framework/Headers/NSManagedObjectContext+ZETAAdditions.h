#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (ZETAAdditions)

/**
 @see @p executeFetchRequest:error: and @p tdt_executeFetchRequest:
 
 In case of errors in executing the fetch request:
 - Asserts on DEBUG builds,
 - Prints a warning on non DEBUG builds and returns an empty array.
 */
- (nonnull NSArray *)zeta_executeFetchRequest:(nullable NSFetchRequest *)fetchRequest;

/**
 @see @p countForFetchRequest:error: and @p tdt_countForFetchRequest:
 
 In case of errors in fetching the count:
 - Asserts on DEBUG builds,
 - Prints a warning on non DEBUG builds and returns 0.
 */
- (NSUInteger)zeta_countForFetchRequest:(nullable NSFetchRequest *)fetchRequest;

- (void)zeta_save;

/**
 Turns the objects with @p objectIDs into faulted objects inside the managedObjectContext so that
 changes in them can be observed by managedObjectContext
 
 @param objectIDs List of objectIDs that are to be realized in the managedObjectContext.
 
 @note NFRC doesn't call its @p didChangeObject.... delegate method when an attribute changes on
 some object thats not faulted in the managedObjectContext. So we need to realize all objectIDs
 before merging the change in main managedObjectContext so that they turn into faulted objects in
 the managedObjectContext. And thereafter the @p didChangeObject... delegate method of NFRC is
 called normally.
 */
- (void)zeta_realizeObjectIDs:(nullable NSSet *)objectIDs;

@end
