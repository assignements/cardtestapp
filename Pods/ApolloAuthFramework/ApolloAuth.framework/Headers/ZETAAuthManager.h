#import <Foundation/Foundation.h>
#import <ApolloDoorStream/ApolloDoorStreamHeaders.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

@protocol ZETAAuthManagerDelegate;

@interface ZETAAuthManager : NSObject

- (instancetype)initWithDoorStream:(ZETADoorStreamManager *)doorStream
                   serverTimeStore:(id<ApolloServerTimeSyncProtocol>)serverTimeStore
                     authDataStore:(id<ApolloAuthDataProtocol>)authDataStore
                      JWEPublicKey:(NSString *)publicKey
                          delegate:(id<ZETAAuthManagerDelegate>)delegate
                notificationPrefix:(NSString *)notificationPrefix;

/**
 This method tries to authenticate the door session by picking the relevent info from
 userCredentials.
 */
- (void)authenticateIfPossible;

- (NSString *)authToken;

- (NSNumber *)authTokenValidity;

- (NSString *)userSessionJID;

- (NSString *)base64JWEToken;

@end

@protocol ZETAAuthManagerDelegate<NSObject>

- (TDTAgentInfo *)authManagerDidRequestAgentInfo:(ZETAAuthManager *)manager;

@optional

- (void)authManagerDidAuthenticate:(ZETAAuthManager *)authManager;

- (void)authManager:(ZETAAuthManager *)authManager didFailToAuthenticateWithError:(NSError *)error;

@end
