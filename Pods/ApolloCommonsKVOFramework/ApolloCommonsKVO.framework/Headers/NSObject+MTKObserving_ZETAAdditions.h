#import "NSObject+MTKObserving.h"
typedef void (^ZETAMTKBlockNotify)(__weak __nullable id weakSelf);
typedef void (^ZETAMTKBlockNotifyWithNotification)(__weak __nullable id weakSelf,
                                                   NSNotification *__nonnull notification);

@interface NSObject (MTKObserving_ZETAAdditions)

/**
 Block KVO manually invokes @p block with a @p nil notification
 when using the `-observeNotification:fromObject:withBlock:` method.
 This behaviour is often not desirable and requires an extra early
 return for many usage scenarios.

 This method swallows the initial call, guaranteeing that @p
 block is only invoked when the posted notification is not @p nil.
 */
- (void)zeta_observeNotification:(nonnull NSString *)name
                      fromObject:(nullable id)object
           withNotificationBlock:(nonnull ZETAMTKBlockNotifyWithNotification)block;

/**
 Convenience wrapper over
 `zeta_observeNotification:fromObject:withNotificationBlock:`
 that does not forward the notification argument to the block.

 This reduces the code volume in methods that do not care
 about the notification details, and just wish to know if a
 particular event has happened.
 */
- (void)zeta_observeNotification:(nonnull NSString *)name
                      fromObject:(nullable id)object
                       withBlock:(nonnull ZETAMTKBlockNotify)block;

/**
 Convenience wrappers over `zeta_observeNotification:fromObject:withBlock:`
 that set the @p object parameter to @p nil.
 */
- (void)zeta_observeNotification:(nonnull NSString *)name
                       withBlock:(nonnull ZETAMTKBlockNotify)block;
- (void)zeta_observeNotification:(nonnull NSString *)name
           withNotificationBlock:(nonnull ZETAMTKBlockNotifyWithNotification)block;

/**
 Convenience wrappers for observing multiple notifications.
 */
- (void)zeta_observeNotifications:(nonnull NSArray *)names
                        withBlock:(nonnull ZETAMTKBlockNotify)block;
- (void)zeta_observeNotifications:(nonnull NSArray *)names
            withNotificationBlock:(nonnull ZETAMTKBlockNotifyWithNotification)block;

/**
 Alias for @p removeAllObservations.

 Included to provide a complete zeta_ namespaced layer over
 the Block KVO API when used for notifications.
 */
- (void)zeta_removeAllObservations;

- (void)zeta_observeNotification:(nonnull NSString *)name
                       withBlock:(nonnull ZETAMTKBlockNotify)block
                           delay:(NSTimeInterval)delay
                           queue:(nonnull dispatch_queue_t)queue;

@end
