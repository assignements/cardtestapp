#import <Foundation/Foundation.h>

typedef void (^ZETANotificationWithMessageBlockType)(__weak __nullable id weakSelf,
                                                     NSString *__nonnull message);

@interface NSObject (MTKObserving_ZETAMessageNotification)

- (void)zeta_observeNotification:(nonnull NSString *)name
    withMessageNotificationBlock:(nonnull ZETANotificationWithMessageBlockType)block;

- (void)zeta_postNotificationName:(nonnull NSString *)name message:(nonnull NSString *)message;

@end
