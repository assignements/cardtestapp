#import <Foundation/Foundation.h>

@interface NSPredicate (ZETAAdditions)

+ (NSPredicate *)predicateWithORConditionForItemsInArray:(NSArray *)array
                                                  format:(NSString *)format
                                appendedWithANDCondition:(NSString *)andConditionFomat
                                           argumentArray:(NSArray *)arguments;

@end
