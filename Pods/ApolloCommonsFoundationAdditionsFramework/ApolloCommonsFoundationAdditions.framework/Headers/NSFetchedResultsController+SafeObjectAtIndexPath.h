#import <CoreData/CoreData.h>

@interface NSFetchedResultsController (SafeObjectAtIndexPath)

- (id<NSFetchRequestResult>)safeObjectAtIndexPath:(NSIndexPath *)indexPath;

@end
