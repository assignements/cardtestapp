#import <Foundation/Foundation.h>

@interface NSRegularExpression (ZETAAdditions)

+ (instancetype)zeta_regexWithPattern:(NSString *)pattern;
+ (BOOL)zeta_doesRegexPattern:(NSString *)pattern matchWithString:(NSString *)str;
- (BOOL)zeta_doesRegexPatternMatchWithString:(NSString *)str;

@end
