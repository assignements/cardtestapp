#import <Foundation/Foundation.h>

@interface NSObject (ZETAAdditions)

/**
 @note This method checks for the class of the object to be __NSCFBoolean for the object to be
 called boolean. On 32bit devices, it can lead to wrong results in some cases. Refer:
 http://stackoverflow.com/questions/35759758/bool-property-kvc-is-this-behavior-a-bug/35761538#35761538
 */
- (BOOL)zeta_isBOOL;
- (NSString *)zeta_className;

@end

