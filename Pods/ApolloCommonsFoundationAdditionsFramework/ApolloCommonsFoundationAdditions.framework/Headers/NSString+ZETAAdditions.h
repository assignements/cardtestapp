#import <Foundation/Foundation.h>

typedef NSString * (^ZETACharacterMapBlock)(NSString *substring, NSRange range);

@interface NSString (ZETAAdditions)

- (NSArray *)zeta_trimmedComponents;

- (NSString *)zeta_trimmedString;

- (NSData *)zeta_dataFromBase64String;

- (NSData *)zeta_SHA1;

- (NSString *)zeta_base64EncodedSHA1;

- (NSString *)zeta_base64EncodedSHA256;

- (NSData *)zeta_SHA256;

- (NSData *)zeta_HMACUsingSHA256WithSecret:(NSData *)secret;

- (NSString *)zeta_base64EncodedHMACUsingSHA256WithSecret:(NSData *)secret;

- (NSString *)zeta_base64EncodedHMACUsingSHA256WithUTF8Secret:(NSString *)secret;

- (BOOL)zeta_isNumeric;

- (BOOL)zeta_isAlphaNumeric;

- (NSString *)zeta_normalizedSearchString;

/**
 Returns a new string by adding the separator between every "componentSize" characters in the
 original string.
 */
- (NSString *)zeta_stringByAddingSeparator:(NSString *)separator
                         withComponentSize:(NSUInteger)componentSize;

- (NSString *)zeta_stringByAddingSpaceWithComponentSize:(NSUInteger)componentSize;

/**
 Returns a new string after removing all non digit characters.
 */
- (NSString *)zeta_unformattedNumberString;
/**
 Returns the string which can be used as the last string for the given string.
 
 Use case: In place of making BEGINSWITH query, a BETWEEN query can be made between given string
 and this string can act as the ending marker for results.
 */
- (NSString *)zeta_endingStringForInclusiveSearch;

- (BOOL)tdt_caseInsensitiveEqual:(NSString *)toCompare;

/**
 *This will return nil if its not parsed properly
 */
- (NSDictionary *)zeta_JSONDictionary;

- (NSArray *)zeta_JSONArray;

- (NSString *)zeta_uppercaseStringInUserLocale;

- (NSString *)zeta_stringByMappingCharactersWithBlock:(ZETACharacterMapBlock)block;

- (BOOL)zeta_isValidPhoneNumber;

- (void)zeta_copyToClibboard;

- (NSString *)tdt_stringByEncodingQueryParameter;

+ (instancetype)zeta_base64URLEncodedStringFromBase64String:(NSString *)base64String;

- (instancetype)zeta_stringByEncodingURLFormat;

- (instancetype)zeta_escapeString;

- (instancetype)zeta_limitLength:(NSUInteger)maxLength;

+ (instancetype)zeta_HTTPEndPointFromService:(NSString *)service
                                     version:(NSString *)version
                                      method:(NSString *)method;

- (instancetype)zeta_extractDoorServiceNameFromServiceEndpoint;

/**
 *This will return nil if its not parsed properly
 
 Example:
 Sample String    : #65{{primary top}}
 Objective        : To extract ONLY and ONLY "primary top" from above string
 Regex to be used : #[0-9]*\\{\\{(.*)\\}\\}
 */

- (NSString *)zeta_firstSubstringCapturedByRegexPattern:(NSString *)regexPattern;

@end
