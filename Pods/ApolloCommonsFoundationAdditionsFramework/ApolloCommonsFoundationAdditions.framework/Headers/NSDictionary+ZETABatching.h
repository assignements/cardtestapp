#import <Foundation/Foundation.h>

typedef void (^ZETADictionaryApplyBatchBlock)(NSDictionary* batch, BOOL* stop);

@interface NSDictionary (ZETABatching)

- (void)zeta_enumerateObjectsWithBatchSize:(NSUInteger)batchSize
                                usingBlock:(ZETADictionaryApplyBatchBlock)block;

@end
