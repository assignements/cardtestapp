#import <Foundation/Foundation.h>

@interface NSBundle (ZETALanguageAddition)

+ (void)setBundleForAppLanguageWithLocale:(NSString *)locale;

@end

