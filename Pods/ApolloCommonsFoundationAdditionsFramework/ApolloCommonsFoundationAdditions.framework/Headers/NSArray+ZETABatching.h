#import <Foundation/Foundation.h>

typedef void (^ZETAApplyBatchBlock)(NSArray* batch, NSUInteger startIndex, BOOL* stop);

@interface NSArray (ZETABatching)

- (void)zeta_enumerateObjectsWithBatchSize:(NSUInteger)batchSize
                                usingBlock:(ZETAApplyBatchBlock)block;

@end
