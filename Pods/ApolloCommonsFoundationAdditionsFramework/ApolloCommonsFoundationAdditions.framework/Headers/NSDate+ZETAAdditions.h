#import <Foundation/Foundation.h>

@interface NSDate (ZETAAdditions)

- (long long)zeta_timeIntervalSince1970InMs;

- (long long)zeta_timeIntervalSinceReferenceDate;

+ (NSDate *)zeta_dateAfter5YearsFromNow;

- (NSDate *)zeta_dateAfter5Years;

+ (instancetype)zeta_dateWithMillisecondsSinceEpoch:(int64_t)millisecondsSinceEpoch;

@end
