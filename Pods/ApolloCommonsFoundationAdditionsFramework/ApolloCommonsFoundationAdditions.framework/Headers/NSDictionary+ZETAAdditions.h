#import <Foundation/Foundation.h>

typedef __nonnull id (^ZETAMapBlock)(__nonnull id obj);
typedef void (^ZETAApplyBlock)(__nonnull id key, __nonnull id value);

@interface NSDictionary (ZETAAdditions)

- (nonnull NSArray *)zeta_valuesAfterSortingKeysUsingSelector:(nonnull SEL)selector;

- (nonnull NSArray *)zeta_valuesAfterSortingKeys;

/**
 Variant of @p tdt_dictionaryByMergingDictionary that is guaranteed to
 return an immutable dictionary.
 */
- (nonnull NSDictionary *)zeta_dictionaryByMergingDictionary:(nonnull NSDictionary *)dictionary;

/**
 Try and convert the receiver into a JSON string.
 
 @return If the receiver is a valid JSON object, then return
 the corresponding JSON string. Otherwise return @p nil.
 */
- (nullable NSString *)zeta_JSONString;

- (nonnull NSDictionary *)zeta_dictionaryByAddingKey:(nonnull id<NSCopying>)key
                                               value:(nonnull id)value;

- (nonnull NSDictionary *)zeta_dictionaryByMappingKeysWithBlock:(nonnull ZETAMapBlock)block;

- (BOOL)zeta_isNonEmpty;



- (void)zeta_applyBlock:(nonnull ZETAApplyBlock)block;

/**
 This function takes a stringified form of query params. The self dict is stringified and the input
 param is appended to it
 */
- (nonnull NSString *)zeta_stringifiedQueryParamsByAppendingQueryParams:
(nonnull NSString *)queryParams;

- (nonnull NSString *)zeta_stringFromQueryComponents;
- (nonnull NSDictionary *)zeta_dictionaryByRemovingEntriesForKeys:(nonnull NSArray *)keys;

/*
 * This function takes a @{dictionary} and merges the @{dictionary} into the self dictionary.
 If there are items with same keys, then values of @{dictionary} overwrites the values of self
 dictionary. i.e. keys which are only present in self dictionary are retained, and keys which are
 only present in @{dictionary} are removed.
 In case of nested dictionary, recursive merge is performed.
 NOTE(FIXME(AJ)): This method will not work in case an array is found within an array
 */
- (nonnull NSDictionary *)zeta_dictionaryByDeepMergingDictionary:(nonnull NSDictionary *)dictionary;

@end

