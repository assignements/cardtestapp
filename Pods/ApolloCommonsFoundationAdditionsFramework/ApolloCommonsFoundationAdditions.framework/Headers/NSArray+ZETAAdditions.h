#import <Foundation/Foundation.h>

@interface NSArray (ZETAAdditions)

- (NSAttributedString *)zeta_attributedStringComponentsJoinedByString:
    (NSAttributedString *)connectorString;

/**
 [x, y, z, ...] = {f(x): x, f(y): y, f(z): z, ...}
 */
- (NSDictionary *)zeta_dictionaryByKeyingWithBlock:(id<NSCopying> (^)(id item))keyGenerationBlock;
- (NSDictionary *)zeta_dictionaryByKeyingWithSelector:(SEL)keySelector;

- (NSArray *)zeta_arrayByAddingObjectsFromArray:(NSArray *)array;
- (NSArray *)zeta_arrayByRemovingObjectsFromArray:(NSArray *)array;
- (NSArray *)zeta_arrayByAddingObject:(id)object;
- (NSArray *)zeta_arrayByRemovingObject:(id)object;
- (NSArray *)zeta_arrayByRemovingObjectAtIndex:(NSUInteger)index;
- (NSArray *)zeta_arrayByUpdatingObject:(id)object atIndex:(NSUInteger)index;

- (id)zeta_objectAfterObject:(id)object;

- (id)zeta_objectBeforeObject:(id)object;

/**
 * [@"1", "2", "3"] -> @"1,2,3"
 */
- (NSString *)zeta_componentsJoinedByComma;
/**
 * [@"1", "2", "3"] -> @"1, 2 and 3"
 */
- (NSString *)zeta_componentsJoinedByCommaAndAnd;

- (NSString *)zeta_JSONString;

- (NSArray *)zeta_copy;

@end
