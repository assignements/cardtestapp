#import <Foundation/Foundation.h>

@interface NSMutableDictionary (SafeSetObjectForKey)

- (void)safeSetObject:(id)anObject forKey:(id<NSCopying>)aKey;

@end
