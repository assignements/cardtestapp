#import <CommonCrypto/CommonCrypto.h>
#import <Foundation/Foundation.h>

@interface NSData (ZETAAdditions)

- (NSString *)zeta_base64EncodedString;

- (NSString *)zeta_base64URLEncodedString;

- (NSData *)zeta_SHA1;

- (NSData *)zeta_SHA256;

+ (NSData *)zeta_dataWithBigEndianEncodingOfUint32:(uint32_t)unsignedInteger;

+ (NSData *)zeta_dataWithBigEndianEncodingOfUint16:(uint16_t)unsignedInteger;

+ (NSData *)zeta_dataWithBigEndianEncodingOfUint64:(uint64_t)unsignedInteger;

+ (NSData *)zeta_dataWithUint8:(uint8_t)unsignedInteger;

- (NSData *)zeta_HMACUsingSHA256WithSecret:(NSData *)secret;

- (NSData *)zeta_HMACWithSecret:(NSData *)secret algorithm:(CCHmacAlgorithm)algorithm;

+ (NSUInteger)zeta_HMACLengthForAlgorithm:(CCHmacAlgorithm)algo;

@end
