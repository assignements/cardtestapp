#import <Mantle/Mantle.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import "ZETADoorStreamConstants.h"

typedef void (^ZETAOMSResponseFragmentBlock)(id responsePayload);
typedef void (^ZETAOMSRequestSuccessBlock)(ZETAOMSErrorResponseData *error, id responsePayload);

FOUNDATION_EXPORT NSString *const ZETAOMSRequestDataErrorDomain;

@interface ZETAOMSRequestData : MTLModel<MTLJSONSerializing>

- (instancetype)initWithRequestID:(NSString *)requestID
                requestUniquingID:(NSString *)requestUniquingID
                              QOS:(ZETAPacketDeliveryQOS)QOS
                     requiresAuth:(BOOL)requiresAuth
                           nodeID:(NSString *)nodeID
                      serviceName:(NSString *)serviceName
                          version:(NSString *)version
                           method:(NSString *)method
                          payload:(MTLModel<MTLJSONSerializing> *)payload
                    fragmentClass:(Class)fragmentClass
                    responseClass:(Class)responseClass
                    fragmentBlock:(ZETAOMSResponseFragmentBlock)fragmentBlock
                      logsRequest:(BOOL)logsRequest
                     successBlock:(ZETAOMSRequestSuccessBlock)success
                     failureBlock:(ZETAFailureBlockType)failure;

@property (nonatomic, copy) NSString *requestID;
@property (nonatomic, copy) NSString *requestUniquingID;
@property (nonatomic, readonly) ZETAPacketDeliveryQOS QOS;
@property (nonatomic, readonly) BOOL requiresAuth;

@property (nonatomic, copy, readonly) NSString *nodeID;
@property (nonatomic, copy, readonly) NSString *serviceName;
@property (nonatomic, copy, readonly) NSString *version;
@property (nonatomic, copy, readonly) NSString *method;
@property (nonatomic, readonly) BOOL logsRequest;
@property (nonatomic, copy, readonly) MTLModel<MTLJSONSerializing> *payload;

@property (nonatomic, readonly) Class fragmentClass;
@property (nonatomic, readonly) Class responseClass;
@property (nonatomic, copy, readonly) ZETAOMSResponseFragmentBlock fragmentBlock;
@property (nonatomic, copy, readonly) ZETAOMSRequestSuccessBlock successBlock;
@property (nonatomic, copy, readonly) ZETAFailureBlockType failureBlock;

/**
 If this flag returns true, then request is only failed after the specified timeout time.
 If returned false, we will fail the request on door disconnection too, if it happens before
 timeout.
 It can be useful for user facing request where we do want to wait for some given time before
 telling the request that it failed.
 */
- (BOOL)failRequestOnlyAfterTimeout;

@end
