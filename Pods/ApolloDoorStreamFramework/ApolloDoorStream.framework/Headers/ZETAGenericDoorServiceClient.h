#import "ZETABaseDoorServiceClient.h"
#import <Bolts/BFTask.h>

@interface ZETAGenericDoorServiceClient : ZETABaseDoorServiceClient

- (instancetype)initWithDoorStream:(ZETADoorStreamManager *)doorStream
                           service:(NSString *)service
                           version:(NSString *)version
                  analyticsManager:(ApolloAnalyticsManager *)analyticsManager;

- (BFTask *)dispatchOMSRequestTaskForRequestUniquingID:(NSString *)requestUniquingID
                                                   QOS:(ZETAPacketDeliveryQOS)QOS
                                                nodeID:(NSString *)nodeID
                                                method:(NSString *)method
                                               payload:(MTLModel<MTLJSONSerializing> *)payload
                                          requiresAuth:(BOOL)requiresAuth;

@end
