#import <ApolloKnock/TDTKnock.h>
#import "ZETADoorMockResponseProvider.h"

@interface ZETAKnockWithMockSupport : TDTKnock

- (instancetype)initWithDoorHostname:(NSString *)doorHostname
                                port:(UInt16)port
                        shouldUseTLS:(BOOL)shouldUseTLS
                shouldCompressFrames:(BOOL)shouldCompressFrames
                mockResponseProvider:(ZETADoorMockResponseProvider *)mockResponseProvider;

@end
