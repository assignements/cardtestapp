#import <Foundation/Foundation.h>

typedef BOOL (^ZETADoorMockResponseRequestVerificationBlock)(NSDictionary *request);

/**
  This class is meant for supplying mock responses for OMS requests.

  How it works:
  This class is injected into TDTKnock when running in a "DEBUG" configuration,
  and is consulted before every OMS Request is sent out.

  If the `mockedResponseForRequest` method returns a non-nil object, then that
  value is returned as the response to the API call.
*/
@interface ZETADoorMockResponseProvider : NSObject

/**
 @param request The top level request dictionary sent by the user.

 @return Mocked response for the given request, if any.
 Otherwise (when this method returns @p nil) the request is passed to real knock.
 */
- (NSDictionary *)mockedResponseForRequest:(NSDictionary *)request;

/**
 @param requestVerificationBlock This block is passed a copy of the request
 payload, and the mock response is returned only if it returns @p YES.
 This parameter can be @p nil, in which case all requests with the given
 @p serviceName and @p methodName are considered as matching.

 @param responsePayload The payload portion of the response.
 */
- (void)addMockResponseForService:(NSString *)serviceName
                       methodName:(NSString *)methodName
         requestVerificationBlock:
             (ZETADoorMockResponseRequestVerificationBlock)requestVerificationBlock
                  responsePayload:(NSDictionary *)responsePayload;

/**
 Variant of @p addMockResponseForService that can be used to mock errors.
 @param errorPayload of the form:
   "error": { "type": "SomeException",  "message": "some message" }
 */
- (void)addMockErrorForService:(NSString *)serviceName
                    methodName:(NSString *)methodName
      requestVerificationBlock:
          (ZETADoorMockResponseRequestVerificationBlock)requestVerificationBlock
                  errorPayload:(NSDictionary *)errorPayload;

@end
