#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <ApolloCommonsAgentInfo/ApolloAgentInfo.h>
#import <ApolloKnock/TDTKnock.h>
#import <ApolloLocation/ZETALocationBundle.h>
#import "ZETAOMSRequestData.h"
#import "ZETAOMSResponseData.h"
#import "ZETADoorStreamConstants.h"
#import <ApolloAnalyticsCore/ApolloAnalyticsCore.h>

@protocol ZETADoorPushMessageDelegate;

@interface ZETADoorStreamManager : NSObject

@property (nonatomic) BOOL isConnected;
@property (nonatomic) BOOL isAuthenticated;

- (instancetype)initWithKnock:(TDTKnock *)knock
                    agentInfo:(TDTAgentInfo *)agentInfo
             analyticsManager:(ApolloAnalyticsManager *)analyticsManager
           notificationPrefix:(NSString *)notificationPrefix;

@property (nonatomic, weak) id<ZETADoorPushMessageDelegate> pushMessageDelegate;

- (void)connectStream;

- (NSString *)notificationNameForType:(ZETADoorStreamNotificationType)type;

- (void)authenticateStreamWithAuthData:(NSString *)authData
                             authToken:(NSString *)authToken
                                UAInfo:(TDTAgentInfo *)UAInfo
                        locationBundle:(ZETALocationBundle *)locationBundle;

- (void)fetchServerTime;

/**
 Convenience initializer for `dispatchOMSRequestWithRequestUniquingID` with requestUniquingID as nil
 */
- (void)dispatchOMSRequestQOS:(ZETAPacketDeliveryQOS)QOS
                       nodeID:(NSString *)nodeID
                  serviceName:(NSString *)serviceName
                      version:(NSString *)version
                       method:(NSString *)method
                      payload:(MTLModel<MTLJSONSerializing> *)payload
                 requiresAuth:(BOOL)requiresAuth
                fragmentClass:(Class)fragmentClass
                responseClass:(Class)responseClass
              fragmentHandler:(ZETAOMSResponseFragmentBlock)fragmentHandler
                  logsRequest:(BOOL)logsRequest
                      success:(ZETAOMSRequestSuccessBlock)success
                      failure:(ZETAFailureBlockType)failure;

/**
 @param requestUniquingID This ID is used as a unique ID for the request. If another request is
 received with same ID till the first one is succeded/failed, then the second request will be
 qualified as duplicate and will be dropped.
 @note Uniquing wont be done if the `requestUniquingID` is sent as nil
 */
- (void)dispatchOMSRequestWithRequestUniquingID:(NSString *)requestUniquingID
                                            QOS:(ZETAPacketDeliveryQOS)QOS
                                         nodeID:(NSString *)nodeID
                                    serviceName:(NSString *)serviceName
                                        version:(NSString *)version
                                         method:(NSString *)method
                                        payload:(MTLModel<MTLJSONSerializing> *)payload
                                   requiresAuth:(BOOL)requiresAuth
                                  fragmentClass:(Class)fragmentClass
                                  responseClass:(Class)responseClass
                                fragmentHandler:(ZETAOMSResponseFragmentBlock)fragmentHandler
                                    logsRequest:(BOOL)logsRequest
                                        success:(ZETAOMSRequestSuccessBlock)success
                                        failure:(ZETAFailureBlockType)failure;

- (RACSignal *)doorStreamAuthenticatedSignal;

- (void)stop;

@end

@protocol ZETADoorPushMessageDelegate

- (void)doorStreamManager:(ZETADoorStreamManager *)doorStreamManager
       didReadPushMessage:(NSDictionary *)message;

@end
