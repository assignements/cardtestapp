#import <Foundation/Foundation.h>

#import "ZETADoorStreamManager.h"
#import "ZETADoorStreamConstants.h"

@class ZETAAnalyticsManager;

FOUNDATION_EXPORT NSString *const ZETABaseDoorServiceErrorDomain;

typedef NS_ENUM(NSUInteger, ZETADoorServiceErrorCode) {
  ZETADoorServiceErrorCodeUnknown = 100,
  ZETADoorServiceErrorCodeInvalidSignature
};

/**
 It is an abstract class.

 Subclass it to create any door service client.
 */
@interface ZETABaseDoorServiceClient : NSObject

- (instancetype)initWithDoorStream:(ZETADoorStreamManager *)doorStream
                  analyticsManager:(ApolloAnalyticsManager *)analyticsManager;

@property (nonatomic, readonly) ZETADoorStreamManager *doorStream;

/**
 @param requestUniquingTag This tag is used as a uniquing tag for a given `method`
 If a request has been sent out for a tag for a `method`, then till the point that request is
 resolved/errored, any further requests with same tag and method will be dropped.
 @note `requestUniquingTag` can be sent out as nil for not participating in uniquing and make sure
 that request is always sent.
 */
- (void)dispatchSimpleOMSRequestWithQOS:(ZETAPacketDeliveryQOS)QOS
                     requestUniquingTag:(NSString *)requestUniquingTag
                                 nodeID:(NSString *)nodeID
                                 method:(NSString *)method
                           requiresAuth:(BOOL)requiresAuth
                          responseClass:(Class)responseClass
                                payload:(MTLModel<MTLJSONSerializing> *)payload
                                success:(void (^)(id responsePayload))success
                                failure:(ZETAFailureBlockType)failure;

- (void)dispatchSimpleOMSRequestWithQOS:(ZETAPacketDeliveryQOS)QOS
                                 nodeID:(NSString *)nodeID
                                 method:(NSString *)method
                           requiresAuth:(BOOL)requiresAuth
                          responseClass:(Class)responseClass
                                payload:(MTLModel<MTLJSONSerializing> *)payload
                                success:(void (^)(id responsePayload))success
                                failure:(ZETAFailureBlockType)failure;

/**
 @param requestUniquingTag This tag is used as a uniquing tag for a given `method`
 If a request has been sent out for a tag for a `method`, then till the point that request is
 resolved/errored, any further requests with same tag and method will be dropped.
 @note `requestUniquingTag` can be sent out as nil for not participating in uniquing and make sure
 that request is always sent.
 */
- (void)dispatchSimpleOMSRequestWithQOS:(ZETAPacketDeliveryQOS)QOS
                     requestUniquingTag:(NSString *)requestUniquingTag
                                 nodeID:(NSString *)nodeID
                                 method:(NSString *)method
                           requiresAuth:(BOOL)requiresAuth
                          fragmentClass:(Class)fragmentClass
                          responseClass:(Class)responseClass
                                payload:(MTLModel<MTLJSONSerializing> *)payload
                               fragment:(ZETAOMSResponseFragmentBlock)fragment
                                success:(void (^)(id responsePayload))success
                                failure:(ZETAFailureBlockType)failure;

- (void)dispatchSimpleOMSRequestWithQOS:(ZETAPacketDeliveryQOS)QOS
                                 nodeID:(NSString *)nodeID
                                 method:(NSString *)method
                           requiresAuth:(BOOL)requiresAuth
                          fragmentClass:(Class)fragmentClass
                          responseClass:(Class)responseClass
                                payload:(MTLModel<MTLJSONSerializing> *)payload
                               fragment:(ZETAOMSResponseFragmentBlock)fragment
                                success:(void (^)(id responsePayload))success
                                failure:(ZETAFailureBlockType)failure;

/**
 Convenience wrapper over the non-fragmented @p dispatchSimpleOMSRequestWithQOS
 method with:
 - @p ZETAPacketDeliveryQOSBackgroundAction
 - NodeID @p nil
 - Authentication required
 - Optional ZETAEmptyBlockType success block
 - Optional ZETAFailureBlockType failure block

 @see dispatchSimpleOMSRequestWithQOS for an explanation of the parameters.
 */
- (void)dispatchServiceOMSRequestWithMethod:(NSString *)method
                         requestUniquingTag:(NSString *)requestUniquingTag
                               requiresAuth:(BOOL)requiresAuth
                                    payload:(MTLModel<MTLJSONSerializing> *)payload
                                    success:(ZETAEmptyBlockType)success
                                    failure:(ZETAFailureBlockType)failure;

/**
 Convenience variant of @p dispatchServiceOMSRequestWithMethod that does
 not expect a request uniquing tag.
 */
- (void)dispatchServiceOMSRequestWithMethod:(NSString *)method
                               requiresAuth:(BOOL)requiresAuth
                                    payload:(MTLModel<MTLJSONSerializing> *)payload
                                    success:(ZETAEmptyBlockType)success
                                    failure:(ZETAFailureBlockType)failure;

/**
 serviceName Name of service.
 Must be implemented by subclass.
 */
- (NSString *)serviceName;

/**
 versionNumber Version of the service.
 Must be implemented by subclass.
 */
- (NSString *)versionNumber;

/**
 serviceErrorDomain Domain name of the service for service errors.
 Must be implemented by subclass.
 */
- (NSString *)serviceErrorDomain;

/**
 It has to be implmented by subclass if there are any service specific error.
 */
- (BOOL)isServiceSpecificError:(ZETAOMSErrorResponseData *)OMSError;

/**
 Has to be implemented by subclass to return error code for service specific errors.
 */
- (NSInteger)errorCodeForServiceOMSError:(ZETAOMSErrorResponseData *)OMSError;

- (NSArray *)methodNamesWhichShouldNotBeLogged;

@end
