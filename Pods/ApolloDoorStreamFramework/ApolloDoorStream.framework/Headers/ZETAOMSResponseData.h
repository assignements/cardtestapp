#import <Mantle/Mantle.h>

typedef NS_ENUM(NSUInteger, ZETAOMSResponseType) {
  ZETAOMSResponseTypeResult,
  ZETAOMSResponseTypeError,
  ZETAOMSResponseTypeUnknown = 100
};

@interface ZETAOMSResponseData : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *from;
@property (nonatomic, readonly) ZETAOMSResponseType type;
@property (nonatomic, copy, readonly) NSDictionary *payload;
@property (nonatomic, copy, readonly) NSDictionary *error;
@property (nonatomic, copy, readonly) NSString *requestID;
@property (nonatomic, readonly) BOOL fragment;

@end
