#import <Foundation/Foundation.h>

@interface NSError (DoorStreamAdditions)

- (NSString *)zeta_unifiedErrorType;
- (NSString *)zeta_unifiedErrorMessage;
- (BOOL)zeta_isNoInternetError;

@end
