#import <Foundation/Foundation.h>
#import "ZETAOMSErrorResponseData.h"

typedef BOOL (^ZETAMessagePacketQualifierBlockType)(NSString *messageType, NSDictionary *message);
typedef void (^ZETAMessagePacketCompletionBlockType)(NSDictionary *message);

// TODO: Add more of them with automatic configured retrials.
/**
 Order of these enums decide the priority of the packets.

 ZETAPacketDeliveryQOSBackgroundAction = 0 & ZETAPacketDeliveryQOSUserInitiatedAction = 1
 So, ZETAPacketDeliveryQOSUserInitiatedAction will be prioritized over
 ZETAPacketDeliveryQOSBackgroundAction
 */
typedef NS_ENUM(NSUInteger, ZETAPacketDeliveryQOS) {
  ZETAPacketDeliveryQOSBatchAction,
  ZETAPacketDeliveryQOSBackgroundAction,
  ZETAPacketDeliveryQOSCriticalBackgroundAction,
  ZETAPacketDeliveryQOSUserInitiatedAction,
  ZETAPacketDeliveryQOSPaymentAction
};

typedef NS_ENUM(NSUInteger, ZETAStreamManagerError) {
  ZETAStreamManagerNotConnectedError,
  ZETAStreamManagerNotAuthenticatedError,
  ZETAStreamManagerTimeoutError,
  ZETAStreamManagerAuthFailWithInvalidUserException,
  ZETAStreamManagerAuthFailWithInvalidAuthTokenException,
  ZETAStreamManagerUnknownError,
  ZETAStreamManagerAuthFailWithInvalidAuthDataException,
  ZETAStreamManagerAuthFailWithInvalidNonceException,
  ZETAStreamManagerAuthFailWithSessionExpiredException,
  ZETAStreamManagerDuplicateRequestError
};

typedef NS_ENUM(NSUInteger, ZETADoorStreamNotificationType) {
  ZETADoorStreamNotificationTypeAuthenticateSuccess,
  ZETADoorStreamNotificationTypeConnect,
  ZETADoorStreamNotificationTypeAuthenticateFailure,
  ZETADoorStreamNotificationTypeServerTimeUpdate,
  ZETADoorStreamNotificationTypeDisconnect
};

/**
 
 NOTE: Notification name formats. All of them include a placeholder for prefix that needs to be
passed in init of ZETADoorStreamManager
 
 **/
FOUNDATION_EXPORT NSString *const ZETAStreamManagerDidConnectNotificationFormat;
FOUNDATION_EXPORT NSString *const ZETAStreamManagerDidDisconnectNotificationFormat;
FOUNDATION_EXPORT NSString *const ZETAStreamManagerDidAuthenticateNotificationFormat;
FOUNDATION_EXPORT NSString *const ZETAStreamManagerDidFailToAuthenticateFormat;
FOUNDATION_EXPORT NSString *const ZETAStreamManagerDidReceiveServerTimeFormat;

FOUNDATION_EXPORT NSString *const ZETAStreamManagerErrorDomain;

FOUNDATION_EXPORT NSString *const ZETAStreamManagerNotificationsUserInfoKeyServerTime;
FOUNDATION_EXPORT NSString *const ZETAStreamManagerNotificationsUserInfoKeyError;
