#import <Mantle/Mantle.h>

@interface ZETAOMSErrorResponseData : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *type;
@property (nonatomic, copy, readonly) NSString *message;
@property (nonatomic, copy, readonly) NSDictionary *attributes;

@end
