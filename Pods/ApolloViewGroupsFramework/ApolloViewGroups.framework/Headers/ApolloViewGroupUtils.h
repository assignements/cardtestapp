#import <Foundation/Foundation.h>
#import "ApolloViewGroupCoreDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApolloViewGroupUtils : NSObject

+ (NSString *)getViewGroupCollectionID:(NSNumber *)tenantID
                             projectID:(NSString *)projectID
                                userID:(NSString *)userID
                              domainID:(NSString *)domainID;

+ (NSDictionary *)getTransformedSearchFields:(NSDictionary *)searchFields
                      viewGroupManagedObject:(ApolloViewGroupCoreDataModel *)viewGroup;
+ (NSDictionary *)getTransformedSearchFields:(NSDictionary *)searchFields
                               viewGroupItem:(ApolloViewGroup *)viewGroup;

+ (NSDictionary *)getTransformedSortFields:(NSDictionary *)sortFields
                    viewGroupManagedObject:(ApolloViewGroupCoreDataModel *)viewGroup;
+ (NSDictionary *)getTransformedSortFields:(NSDictionary *)sortFields
                             viewGroupItem:(ApolloViewGroup *)viewGroup;

@end

NS_ASSUME_NONNULL_END
