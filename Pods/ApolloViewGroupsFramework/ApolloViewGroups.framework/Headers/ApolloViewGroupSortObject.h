#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ApolloViewGroupSortOrder) {
  SortOrderUndefined,
  SortOrderAscending,
  SortOrderDescending
};

@interface ApolloViewGroupSortObject : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) ApolloViewGroupSortOrder sortStringField1;
@property (nonatomic, readonly) ApolloViewGroupSortOrder sortStringField2;
@property (nonatomic, readonly) ApolloViewGroupSortOrder sortStringField3;

@property (nonatomic, readonly) ApolloViewGroupSortOrder sortNumberField1;
@property (nonatomic, readonly) ApolloViewGroupSortOrder sortNumberField2;
@property (nonatomic, readonly) ApolloViewGroupSortOrder sortNumberField3;

@end

NS_ASSUME_NONNULL_END
