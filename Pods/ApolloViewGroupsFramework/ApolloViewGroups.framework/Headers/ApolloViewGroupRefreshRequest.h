#import <Foundation/Foundation.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

@interface ApolloViewGroupRefreshRequest : NSObject

@property (nonatomic, readonly) NSString *viewGroupName;
@property (nonatomic, readonly) NSString *viewGroupItemID;
@property (nonatomic, readonly) NSString *viewGroupComponent;
@property (nonatomic, readonly, copy) ZETAFailureBlockType completion;
@property (nonatomic) NSMutableArray *viewGroupComponentItems;

- (instancetype)initWithViewGroupName:(NSString *)viewGroupName
                      viewGroupItemID:(NSString *)viewGroupItemID
                   viewGroupComponent:(NSString *)viewGroupComponent
                           completion:(ZETAFailureBlockType)completion;

@end
