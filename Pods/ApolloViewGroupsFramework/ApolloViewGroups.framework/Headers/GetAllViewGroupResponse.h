#import <Mantle/Mantle.h>

@interface GetAllViewGroupResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSArray *viewGroupList;

@end
