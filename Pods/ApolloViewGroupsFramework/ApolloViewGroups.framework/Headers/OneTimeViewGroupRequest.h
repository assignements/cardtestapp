#import <Mantle/Mantle.h>
#import "OneTimeViewGroupRequestData.h"

NS_ASSUME_NONNULL_BEGIN

@interface OneTimeViewGroupRequest : MTLModel <MTLJSONSerializing>

-(instancetype)initWithRefreshViewGroupDataRequest:(OneTimeViewGroupRequestData *)data;

@end

NS_ASSUME_NONNULL_END
