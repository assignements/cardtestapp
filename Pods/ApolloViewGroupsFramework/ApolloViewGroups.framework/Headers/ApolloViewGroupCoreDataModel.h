#import <CoreData/CoreData.h>
#import <ApolloCollectionsCore/ZETACollectionModelClass.h>
#import "ApolloViewGroup.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ApolloViewGroupMessageErrorCode) {
  ApolloViewGroupErrorCodeInsufficientInfo
};

@interface ApolloViewGroupCoreDataModel : NSManagedObject<ZETACollectionModelClassProtocol>

@property (nonatomic, retain) NSString *viewGroupCollectionId;
@property (nonatomic, retain) NSString *tenantId;
@property (nonatomic, retain) NSString *projectId;
@property (nonatomic, retain) NSString *userId;
@property (nonatomic, retain) NSString *domainId;
@property (nonatomic, retain) NSString *tenantUserId;
@property (nonatomic, retain) NSString *tenantUserProfileId;
@property (nonatomic, retain) NSString *viewGroupId;
@property (nonatomic, retain) NSString *viewGroupDefinition;
@property (nonatomic, retain) NSString *viewGroupName;
@property (nonatomic, retain) NSArray *collectionIds;
@property (nonatomic, retain) NSDictionary *attrs;
@property (nonatomic, retain) NSDictionary *localeCollectionIdMap;
@property (nonatomic, retain) NSString *createdAt;
@property (nonatomic, retain) NSString *updatedAt;

- (nullable NSDictionary *)searchCriteria;
- (nullable NSDictionary *)sortingCriteria;
- (void)updateWithAuthId:(NSString *)authId;

+ (NSArray *)viewGroupWithName:(NSString *)name
                           MOC:(NSManagedObjectContext *)MOC;

+ (NSArray *)allViewGroupItemsInMOC:(NSManagedObjectContext *)MOC;

+ (NSArray *)itemsWithAuthID:(NSString *)authID
                         MOC:(NSManagedObjectContext *)MOC;

+ (void)deleteItemsWithAuthID:(NSString *)authID
                          MOC:(NSManagedObjectContext *)MOC;

+ (instancetype)itemWithCollectionID:(NSString *)collectionID
                         viewGroupID:(NSString *)viewGroupID
                                 MOC:(NSManagedObjectContext *)MOC;

+ (instancetype)createOrUpdateWithCollectionID:(NSString *)collectionID
                                     viewGroup:(ApolloViewGroup *)viewGroup
                                           MOC:(NSManagedObjectContext *)MOC;

@end

NS_ASSUME_NONNULL_END
