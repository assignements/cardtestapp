#import <Foundation/Foundation.h>
#import <ApolloCollectionsCore/ZETAGenericCollectionManager.h>
#import <ApolloUserStore/ZETAApolloUserStore.h>
#import <CoreData/CoreData.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import "ApolloViewGroupQueryObject.h"
#import "ApolloViewGroupCoreDataModel.h"
#import "ApolloViewGroupConstants.h"

NS_ASSUME_NONNULL_BEGIN

FOUNDATION_EXPORT NSString *const ApolloViewGroupErrorDomain;

typedef NS_ENUM(NSUInteger, ApolloViewGroupErrorCode) {
  ApolloViewGroupErrorCodeInsufficientDetails,
  ApolloViewGroupErrorCodeViewGroupCollectionSyncFailed,
  ApolloViewGroupErrorCodeViewsCollectionSyncFailed,
  ApolloViewGroupErrorCodeDuplicateRequest
};

typedef void (^ViewGroupSubscriptionBlockType)(NSFetchedResultsController *nfrc);

@protocol ApolloViewGroupManagerDelegate;

@interface ApolloViewGroupManager : NSObject

@property (nonatomic, readonly) NSString *authID;
@property (nonatomic, readonly) NSString *sdkName;
@property (nonatomic, readonly) NSNumber *tenantID;
@property (nonatomic, readonly) NSString *projectID;
@property (nonatomic, weak) id<ApolloViewGroupManagerDelegate> delegate;

- (instancetype)initWithCollectionManager:(ZETAGenericCollectionManager *)collectionManager
                             marioBaseUrl:(NSURL *)marioBaseUrl
                                   authID:(NSString *)authID
                                  sdkName:(NSString *)sdkName
                                 tenantID:(NSNumber *)tenantID
                                projectID:(NSString *)projectID;

- (void)fetchViewGroupsFromMarioApi;

- (void)fetchViewGroupsForTenantID:(NSNumber *)tenantID
                         projectID:(NSString *)projectID
                          domainID:(NSString *)domainID
                      apolloUserID:(NSString *)apolloUserID
                      successBlock:(nullable ZETAEmptyBlockType)success
                      failureBlock:(nullable ZETAFailureBlockType)failure;

- (void)getViewGroupItemsForUserProfileID:(nullable NSString *)userProfileID
                            viewGroupName:(NSString *)viewGroupName
                                   locale:(NSString *)locale
                          completionBlock:(nullable ZETAArrayBlockType)completionBlock;

- (void)getViewGroupItemsForUserProfileID:(nullable NSString *)userProfileID
                            viewGroupName:(NSString *)viewGroupName
                                   locale:(NSString *)locale
                           searchCriteria:(nullable NSDictionary *)queryObject
                             sortCriteria:(nullable NSDictionary *)sortCriteria
                          completionBlock:(nullable ZETAArrayBlockType)completionBlock;

- (void)refreshViewGroupForViewGroupName:(NSString *)viewGroupName
                         viewGroupItemID:(NSString *)viewGroupItemID
                             callbackURL:(nullable NSURL *)callbackURL;

- (void)refreshViewGroupForViewGroupName:(NSString *)viewGroupName
                         viewGroupItemID:(NSString *)viewGroupItemID
                              completion:(void (^) (NSError *error))completion;

- (void)refreshViewGroupForViewGroupName:(NSString *)viewGroupName
                         viewGroupItemID:(NSString *)viewGroupItemID
                                   attrs:(nullable NSDictionary *)attrs
                             callbackURL:(nullable NSURL *)callbackURL;

- (void)refreshViewGroupForViewGroupName:(NSString *)viewGroupName
                         viewGroupItemID:(NSString *)viewGroupItemID
                                   attrs:(nullable NSDictionary *)attrs
                              completion:(void (^) (NSError *error))completion;

- (void)refreshViewGroupForViewGroupName:(NSString *)viewGroupName
                      viewGroupComponent:(NSString *)viewGroupComponent
                                   attrs:(nullable NSDictionary *)attrs
                             callbackURL:(nullable NSURL *)callbackURL;

- (void)refreshViewGroupForViewGroupName:(NSString *)viewGroupName
                      viewGroupComponent:(NSString *)viewGroupComponent
                                   attrs:(nullable NSDictionary *)attrs
                              completion:(void (^) (NSError *error))completion;

- (nullable NSFetchedResultsController *)subscribeForUserProfileID:(nullable NSString *)userProfileID
                                                     viewGroupName:(NSString *)viewGroupName
                                                            locale:(NSString *)locale;

- (nullable NSFetchedResultsController *)subscribeForUserProfileID:(nullable NSString *)userProfileID
                                                     viewGroupName:(NSString *)viewGroupName
                                                            locale:(NSString *)locale
                                                    searchCriteria:(nullable NSDictionary *)queryObject
                                                      sortCriteria:(nullable NSDictionary *)sortCriteria;

- (void)clearPersistentData;

@end

@protocol ApolloViewGroupManagerDelegate <NSObject>

- (void)apolloViewGroupManager:(ApolloViewGroupManager *)manager
               didRequestToken:(ZETAStringBlockType)successBlock
                  failureBlock:(ZETAFailureBlockType)failureBlock;

@end

NS_ASSUME_NONNULL_END
