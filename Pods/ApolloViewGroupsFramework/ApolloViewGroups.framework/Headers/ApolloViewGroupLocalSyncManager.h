#import <Foundation/Foundation.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloViewGroupLocalSyncManager : NSObject

- (void)loadViewGroupDataFromFileName:(NSString *)fileName
                              success:(ZETAResponseBlockType)sucess
                              failure:(ZETAFailureBlockType)failure;

@end

NS_ASSUME_NONNULL_END
