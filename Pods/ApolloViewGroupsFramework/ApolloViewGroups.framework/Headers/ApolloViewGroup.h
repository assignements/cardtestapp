#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@class ApolloViewGroupCoreDataModel;

@interface ApolloViewGroup : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *tenantId;
@property (nonatomic, readonly) NSString *projectId;
@property (nonatomic, readonly) NSString *domainId;
@property (nonatomic, readonly) NSString *userId;
@property (nonatomic, readonly) NSString *tenantUserId;
@property (nonatomic, readonly) NSString *tenantUserProfileId;
@property (nonatomic, readonly) NSString *viewGroupId;
@property (nonatomic, readonly) NSString *viewGroupDefinition;
@property (nonatomic, readonly) NSString *viewGroupName;
@property (nonatomic, readonly) NSArray *collectionIds;
@property (nonatomic, readonly) NSDictionary *attrs;
@property (nonatomic, readonly) NSDictionary *localeCollectionIdMap;
@property (nonatomic, readonly) NSString *createdAt;
@property (nonatomic, readonly) NSString *updatedAt;

- (nullable NSDictionary *)searchCriteria;
- (nullable NSDictionary *)sortingCriteria;

- (instancetype)initWithCoreDataModel:(ApolloViewGroupCoreDataModel *)coreDataModel;

@end

NS_ASSUME_NONNULL_END
