#import <ApolloHttpClient/ZETAHTTPSessionClient.h>
#import <ApolloCommonsCore/ApolloCommonAdditions.h>
#import "OneTimeViewGroupRequest.h"

@protocol ApolloMarioClientDelegate;

@interface ApolloMarioClient : ZETAHTTPSessionClient

@property (nonatomic, weak) id<ApolloMarioClientDelegate> tokenProvider;

- (void)getViewGroupsForTenantID:(NSNumber *)tenantID
                       projectID:(NSString *)projectID
                        domainID:(NSString *)domainID
                    apolloUserID:(NSString *)apolloUserID
                    successBlock:(ZETAResponseBlockType)successBlock
                    failureBlock:(ZETAFailureBlockType)failureBlock;

- (void)refreshViewGroupForRequestPayload:(OneTimeViewGroupRequest *)request
                                 tenantID:(NSString *)tenantID
                                projectID:(NSString *)projectID
                      viewGroupDefinition:(NSString *)definition
                       viewGroupComponent:(NSString *)component
                             successBlock:(ZETAResponseBlockType)successBlock
                             failureBlock:(ZETAFailureBlockType)failureBlock;

@end

@protocol ApolloMarioClientDelegate <NSObject>

- (void)apolloMarioClient:(ApolloMarioClient *)client
          didRequestToken:(ZETAStringBlockType)successBlock
             failureBlock:(ZETAFailureBlockType)failureBlock;

@end
