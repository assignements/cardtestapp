#import <ApolloCollectionsCore/ZETACollectionsMessage.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloViewGroupRefreshItem : ZETAStringCollectionItem

@property (nonatomic, readonly) NSString *collectionID;

@end

NS_ASSUME_NONNULL_END
