
#import <Mantle/Mantle.h>
#import "ApolloViewGroup.h"

NS_ASSUME_NONNULL_BEGIN

@interface OneTimeViewGroupRequestData : MTLModel <MTLJSONSerializing>

@property (nonatomic,readonly) ApolloViewGroup *viewGroupObject;
@property (nonatomic,readonly) NSDictionary *viewGroupItemObject;
@property (nonatomic,readonly) NSDictionary *attrs;

- (instancetype)initWithViewGroupObject:(ApolloViewGroup *)viewGroupObject
                    viewGroupItemObject:(nullable NSDictionary *)viewGroupItemObject
                                  attrs:(nullable NSDictionary *)attrs;

@end

NS_ASSUME_NONNULL_END
