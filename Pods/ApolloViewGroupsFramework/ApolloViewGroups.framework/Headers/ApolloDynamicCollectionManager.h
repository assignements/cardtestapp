#import <ApolloCollectionsCore/ZETAGenericCollectionManager.h>
#import "ApolloViewGroup.h"
#import "ApolloViewGroupDataStore.h"
#import "ViewGroupsCollectionData.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ApolloDynamicCollectionManagerDelegate;

@interface ApolloDynamicCollectionManager : NSObject

@property (nonatomic, weak) id<ApolloDynamicCollectionManagerDelegate> delegate;

- (instancetype)initWithCollectionManager:(ZETAGenericCollectionManager *)collectionManager
                          apolloUserStore:(ZETAApolloUserStore *)apolloUserStore
                            backgroundMOC:(NSManagedObjectContext *)backgroundMOC
                                   authID:(NSString *)authID;

- (void)locallySyncViewGroupCollections:(NSArray *)localViewGroups;
- (void)updateCollectionConfigs:(NSArray *)collectionConfigs;

- (void)handleViewGroupCollectionItemUpdate:(ZETAStringCollectionItem *)collectionItem
                               collectionID:(NSString *)collectionID
                                  viewGroup:(ApolloViewGroup *)viewGroup
                                itemPayload:(id)itemPayload
                               sourceMethod:(NSString *)sourceMethod
                             isInitialFetch:(BOOL)isInitialFetch
                         isViewGroupRefresh:(BOOL)isViewGroupRefresh;

@end

@protocol ApolloDynamicCollectionManagerDelegate <NSObject>

- (void)dynamicCollectionManager:(ApolloDynamicCollectionManager *)manager
    didFinishSyncForCollectionID:(NSString *)collectionID
                   viewGroupData:(ApolloViewGroup *)viewGroupData
                      withStatus:(BOOL)status;

- (void)dynamicCollectionManager:(ApolloDynamicCollectionManager *)manager
didUpdateViewGroupCollectionData:(ViewGroupsCollectionData *)viewGroupCollectionData
                   viewGroupData:(ApolloViewGroup *)viewGroupData;

- (void)dynamicCollectionManager:(ApolloDynamicCollectionManager *)manager
didFinishLocalSyncForCollectionID:(NSString *)collectionID
                   viewGroupData:(ApolloViewGroup *)viewGroupData
                      withStatus:(BOOL)status;

- (void)dynamicCollectionManager:(ApolloDynamicCollectionManager *)manager
             didPublishEventName:(NSString *)eventName
                   viewGroupName:(NSString *)viewGroupName
                    collectionID:(NSString *)collectionID;

@end

NS_ASSUME_NONNULL_END
