#import <Mantle/Mantle.h>
#import "ApolloViewGroupRefreshItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface RefreshViewGroupResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSArray<ApolloViewGroupRefreshItem *> *items;

@end

NS_ASSUME_NONNULL_END
