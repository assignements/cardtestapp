#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloMOCMergeManager : NSObject

- (instancetype)initWithMOCToMergeInto:(NSManagedObjectContext *)destinationMOC
                        MOCToMergeFrom:(NSManagedObjectContext *)sourceMOC
             managedObjectTypesToMerge:(NSArray *)managedObjectTypesToMerge;

@end

NS_ASSUME_NONNULL_END

