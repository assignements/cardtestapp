#import <Foundation/Foundation.h>
#import "ApolloViewGroupCollectionFetchConfig.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApolloViewGroupDataStore : NSObject

+ (instancetype)sharedInstance;

- (NSDictionary *)getFetchConfigsForDynamicCollectionsForIdentifier:(NSString *)identifier;
- (void)addOrUpdateFetchConfig:(ApolloViewGroupCollectionFetchConfig *)fetchConfig
                    identifier:(NSString *)identifier;
- (void)removeFetchConfig:(ApolloViewGroupCollectionFetchConfig *)fetchConfig
               identifier:(NSString *)identifier;
- (nullable NSString *)lastSeenAppVersionForIdentifier:(NSString *)identifier;
- (void)setLastSeenAppVersion:(NSString *)appVersion
                   identifier:(NSString *)identifier;
- (BOOL)hasSyncedLocalViewGroupsForIdentifier:(NSString *)identifier;
- (void)setHasSyncedLocalViewGroups:(BOOL)hasSynced
                         identifier:(NSString *)identifier;
- (BOOL)hasSyncedViewGroupsOnceForIdentifier:(NSString *)identifier;
- (void)setHasSyncedViewGroupsOnce:(BOOL)hasSynced
                        identifier:(NSString *)identifier;
- (BOOL)hasSyncedViewsCollectionsOnceForIdentifier:(NSString *)identifier;
- (void)setHasSyncedViewsCollectionsOnce:(BOOL)hasSynced
                              identifier:(NSString *)identifier;
- (void)clearDataForIdentifier:(NSString *)identifier;

@end

NS_ASSUME_NONNULL_END
