#import <CoreData/CoreData.h>
#import <ApolloCollectionsCore/ZETACollectionModelClass.h>
#import <ApolloUserStore/ZETAGenericMessageAttributes.h>
#import <ApolloZetletsCore/ZETAZetletMessage.h>
#import "ApolloViewGroup.h"
#import "ApolloViewGroupQueryObject.h"
#import "ApolloViewGroupSortObject.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ZETADynamicCollectionMessageErrorCode) {
  ZETADynamicCollectionMessageErrorCodeInsufficientInfo
};

@interface ViewGroupsCollectionData : ZETAZetletMessage<ZETACollectionModelClassProtocol>

@property (nonatomic, retain) NSNumber *sortOrder;
@property (nonatomic, retain) NSString *visibility;

+ (NSString *)entityName;

- (void)updateWithCollectionID:(NSString *)collectionID
                     viewGroup:(ApolloViewGroup *)viewGroup
                        authID:(NSString *)authID
                           MOC:(NSManagedObjectContext *)MOC;

+ (NSArray*)itemsFilteredUsingPredicate:(NSPredicate*)predicate MOC:(NSManagedObjectContext*)MOC;

+ (instancetype)itemWithCollectionID:(NSString*)collectionID
                              itemID:(NSString*)itemID
                                 MOC:(NSManagedObjectContext*)MOC;

+ (NSArray *)itemsWithCollectionID:(NSString *)collectionID
                       queryObject:(nullable ApolloViewGroupQueryObject *)queryObject
                               MOC:(NSManagedObjectContext *)MOC;

+ (NSPredicate *)updatePredicate:(NSPredicate *)predicate
                 withQueryObject:(ApolloViewGroupQueryObject *)queryObject;

+ (NSArray <NSSortDescriptor *> *)sortDescriptorsForSortObject:(ApolloViewGroupSortObject *)sortObject;

+ (void)deleteItemsWithCollectionID:(NSString *)collectionID
                                MOC:(NSManagedObjectContext *)MOC;

@end

NS_ASSUME_NONNULL_END
