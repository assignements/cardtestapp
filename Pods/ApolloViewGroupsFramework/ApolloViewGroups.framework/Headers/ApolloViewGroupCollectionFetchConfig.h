#import <ApolloUserStore/ZETADynamicCollectionFetchConfig.h>
#import "ApolloViewGroup.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApolloViewGroupCollectionFetchConfig : ZETADynamicCollectionFetchConfig

@property (nonatomic, readonly) ApolloViewGroup *viewGroup;

- (instancetype)initWithCollectionID:(NSString *)collectionID
                           viewGroup:(ApolloViewGroup *)viewGroup
                     shouldSubscribe:(BOOL)shouldSubscribe
                           batchSize:(nullable NSNumber *)batchSize
                            syncMode:(ZETADynamicCollectionSyncMode)syncMode
                        fetchTrigger:(ZETADynamicCollectionFetchTrigger)fetchTrigger;

@end

NS_ASSUME_NONNULL_END
