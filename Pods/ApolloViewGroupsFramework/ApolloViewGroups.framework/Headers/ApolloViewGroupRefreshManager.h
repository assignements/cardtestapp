#import <Foundation/Foundation.h>
#import <ApolloCommonsCore/ApolloMacros.h>
#import "ApolloMarioClient.h"
#import <ApolloCollectionsCore/ZETACollectionsMessage.h>
#import "ApolloViewGroupCoreDataModel.h"
#import "ViewGroupsCollectionData.h"

NS_ASSUME_NONNULL_BEGIN

static NSString *const ApolloViewGroupRefreshErrorDomain = @"ApolloViewGroupRefreshErrorDomain";

typedef NS_ENUM(NSUInteger, ApolloViewGroupRefreshErrorCode) {
  ApolloViewGroupRefreshDuplicateRequestError
};

@protocol ApolloViewGroupRefreshalProtocol;

@interface ApolloViewGroupRefreshManager : NSObject

- (instancetype)initWithMarioClient:(ApolloMarioClient *)marioClient
                            sdkName:(NSString *)sdkName;

- (void)initiateRefreshViewGroupForViewGroupName:(NSString *)viewGroupName
                                 viewGroupItemID:(NSString *)viewGroupItemId
                                           attrs:(nullable NSDictionary *)attrs
                                     callbackURL:(nullable NSURL *)callbackURL;

- (void)initiateRefreshViewGroupForViewGroupName:(NSString *)viewGroupName
                                 viewGroupItemID:(NSString *)viewGroupItemID
                                           attrs:(nullable NSDictionary *)attrs
                                      completion:(ZETAFailureBlockType)completion;

- (void)initiateRefreshViewGroupForViewGroupName:(NSString *)viewGroupName
                              viewGroupComponent:(NSString *)viewGroupComponent
                                           attrs:(nullable NSDictionary *)attrs
                                     callbackURL:(nullable NSURL *)callbackURL;

- (void)initiateRefreshViewGroupForViewGroupName:(NSString *)viewGroupName
                              viewGroupComponent:(NSString *)viewGroupComponent
                                           attrs:(nullable NSDictionary *)attrs
                                      completion:(ZETAFailureBlockType)completion;

- (void)handleUpdateOnViewGroupCollectionData:(ViewGroupsCollectionData *)viewGroupCollectionData
                                viewGroupData:(ApolloViewGroup *)viewGroupData;

@property (weak, nonatomic) id<ApolloViewGroupRefreshalProtocol> delegate;

@end

@protocol ApolloViewGroupRefreshalProtocol <NSObject>

- (ApolloViewGroupCoreDataModel *)viewGroupRefreshManager:(ApolloViewGroupRefreshManager *)viewGroupRefreshManager
                      didRequestViewGroupForUserProfileID:(nullable NSString *)userProfileID
                                            viewGroupName:(NSString *)viewGroupName
                                                   locale:(NSString *)locale;

- (void)viewGroupRefreshManager:(ApolloViewGroupRefreshManager *)viewGroupRefreshManager
didRequestViewGroupItemsForUserProfileID:(nullable NSString *)userProfileID
                  viewGroupName:(NSString *)viewGroupName
                         locale:(NSString *)locale
                completionBlock:(ZETAArrayBlockType)completionBlock;

- (void)viewGroupRefreshManager:(ApolloViewGroupRefreshManager *)viewGroupRefreshManager
didRequestUpdateCollectionItemInDB:(ZETAStringCollectionItem *)collectionItem
                   collectionId:(NSString *)collectionId
                      viewGroup:(ApolloViewGroup *)viewGroup;

- (void)viewGroupRefreshManager:(ApolloViewGroupRefreshManager *)viewGroupRefreshManager
                didPublishEvent:(NSString *)eventName
                  viewGroupName:(NSString *)viewGroupName
                         itemID:(NSString *)itemID
                    requestTime:(nullable NSNumber *)requestTime;

@end

NS_ASSUME_NONNULL_END
