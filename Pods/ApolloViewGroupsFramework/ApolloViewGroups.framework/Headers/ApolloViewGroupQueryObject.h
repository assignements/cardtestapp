#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApolloViewGroupQueryObject : MTLModel <MTLJSONSerializing>

@property (nonatomic) NSString *searchField1;
@property (nonatomic) NSString *searchField2;
@property (nonatomic) NSString *searchField3;
@property (nonatomic) NSString *searchField4;
@property (nonatomic) NSString *searchField5;

@property (nonatomic) NSString *sortStringField1;
@property (nonatomic) NSString *sortStringField2;
@property (nonatomic) NSString *sortStringField3;

@property (nonatomic) NSNumber *sortNumberField1;
@property (nonatomic) NSNumber *sortNumberField2;
@property (nonatomic) NSNumber *sortNumberField3;

@end

NS_ASSUME_NONNULL_END
