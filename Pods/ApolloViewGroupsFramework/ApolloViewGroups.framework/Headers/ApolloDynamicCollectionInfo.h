#import <ApolloCollectionsCore/ZETAGenericCollectionInfo.h>
#import <ApolloUserStore/ZETADynamicCollectionFetchConfig.h>
#import "ApolloViewGroup.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApolloDynamicCollectionInfo : ZETAGenericCollectionInfo

@property (nonatomic, readonly) ZETADynamicCollectionSyncMode syncMode;

- (instancetype)initWithCollectionID:(NSString *)collectionID
                     shouldSubscribe:(BOOL)shouldSubscribe
                          modelClass:(Class)modelClass
                           itemClass:(Class)itemClass
                            syncMode:(ZETADynamicCollectionSyncMode)syncMode
                       viewGroupData:(ApolloViewGroup *)viewGroupData
                    diffHandlerBlock:(ZETACollectionDiffHandlerBlock)diffHandlerBlock;

@end

NS_ASSUME_NONNULL_END
